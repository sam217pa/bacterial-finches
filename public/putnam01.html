<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="pandoc">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

        <meta name="author" content="Hilary Putnam">
    
        <meta name="dcterms.date" content="2019-02-15">
    
    
    <title>Preface</title>
    <style type="text/css">code{white-space: pre;}</style>

    
    
        <link rel="stylesheet" href="css/tufte-extra.css">
        <link rel="stylesheet" href="css/pandoc.css">
        <link rel="stylesheet" href="css/pandoc-solarized.css">
        <link rel="stylesheet" href="css/tufte.css">
        <link rel="stylesheet" href="css/latex.css">
    
    
    
  </head>

  <body>

    
    <p>
      <a rel="home"
         href="https://sam217pa.gitlab.io/bacterial-finches/">Bacterial Finches</a>
    </p>

    <article>

            <header>
        <h1 class="title">Preface</h1>
                        <p class="byline">2019-02-15 &ndash; Hilary Putnam</p>
              </header>
      
      
      <p>Le présent livre est tiré de mes Leçons à Gifford, que j’ai données à l’Université de St Andrews à l’automne 1990, et à une exception près, les chapitres sont très proches des leçons telles que je les ai délivrées. (Le chapitre 5 a été substantiellement réécrit. De plus, le cours inaugural dans lequel, peut-être un peu perversement, j’avais choisi de traiter de l’état présent de la mécanique quantique et de sa portée philosophique; j’ai depuis décidé qu’il n’avait pas vraiment sa place ici.)</p>
      <p>Au premier abord, les sujets qu’abordaient les cours pourraient sembler n’avoir aucun lien les uns envers les autres: j’ai parlé de référence et de réalisme et de religion et même de fondement de la politique démocratique. Pourtant le choix de ces sujets n’était pas arbitraire. J’étais guidé, bien sûr, par mes propres recherches antérieures, puisqu’il eut été stupide d’enseigner des sujets sur lesquels je n’avais jusqu’alors rien écrit ou pensé de sérieux; mais au-delà de ça, j’étais motivé par la conviction que l’état présent de la philosophie appelle à une revitalisation, à un renouveau du sujet. D’où ce le fait que ce livre, en sus de traiter différents sujets individuellement, offre un diagnostic de la situation présente de la philosophie dans son ensemble et suggère les directions où nous pourrions chercher un tel renouveau. Cette suggestion ne prend toutefois pas la forme d’un manifeste, mais plutôt d’une série de réflexions sur diverses idées philosophiques.</p>
      <p>L’idée que la science, et seulement la science, décrit le monde tel qu’il est en lui-même, indépendant de toute perspective en est venue à dominer de plus en plus la philosophie analytique. Bien sûr, certaines figures importantes de la philosophie analytique combattent ce scientisme: qu’il suffise de mentionner Peter Strawson, ou Saul Kripke, ou John McDowell, ou Michael Dummett. Néanmoins, l’idée que la science ne laisse pas de place à une entreprise philosophique indépendante en est au point que certains partisans suggèrent parfois que tout ce qui reste à la philosophie est de tenter d’anticiper ce que les présumées solutions scientifiques à tous nos problèmes métaphysiques ressembleront au bout du compte. (Cela va de pair avec l’étrange croyance que l’on <em>peut</em> l’anticiper d’après la science <em>actuelle</em>!) Les trois premiers chapitres de ce volume s’inquiète de montrer que cette idée tient à bien peu. Je commence par un tour d’horizon des façons dont certains philosophes ont suggéré que la science moderne explique le lien entre le langage et le monde. Le premier chapitre discute de l’enthousiasme décidément prématuré que certains ont montré pour « l’intelligence artificielle ». Le deuxième chapitre traite de l’idée que la théorie de l’évolution est la clé du phénomène de représentation, et le troisième chapitre scrute de très près la prétention d’un philosophe contemporain que l’on peut définir la référence en terme de causalité. J’essaie de montrer que ces idées manquent de substance scientifique et philosophique, si prestigieuses qu’elles soient rendues par le climat philosophique ambiant de déférence à l’endroit de la supposée portée métaphysique de la science.</p>
      <p>Peut-être le cas le plus évident de cette idée que l’on <em>devrait</em> s’intéresser à l’état <em>présent</em> de la science, en particulier de la physique, pour y trouver à tout le moins l’esquisse d’une métaphysique adéquate est celui du philosophe britannique Bernard Williams. Après un chapitre traitant des problèmes rencontrés à la fois par les métaphysiciens relativistes et matérialistes, je dévoue un chapitre à l’examen de ses conceptions.</p>
      <p>Non que tous les philosophes actuels soient étourdis par la science; certains de ceux qui ne le sont pas — des philosophes comme Derrida, ou, dans le monde anglo-saxon, Nelson Goodman ou Richard Rorty — ont réagit à la difficulté de faire sens de notre relation cognitive au monde en niant que nous ayons une relation cognitive à une réalité extra-linguistique. Au sixième chapitre, j’accuse ces penseurs d’avoir jeté le bébé avec l’eau du bain. Au septième et huitième chapitres, j’examine les « Leçons sur la Croyance Religieuse » de Wittgenstein, arguant que ces leçons démontrent de quelle façon un philosophe peut nous conduire à appréhender différemment d’autres formes de vie sans tomber dans le scientisme ou l’irresponsabilité métaphysique; au dernier chapitre je tente de montrer que la philosophie politique de John Dewey présente, mais de façon différente, les mêmes vertus.</p>
      <p>Les deux mois que j’ai passé à St Andrews à dispenser ces cours furent un réel plaisir, et j’y ai grandement profité de la compagnie et de la conversation philosophique du groupe remarquable de dévoués et brillants philosophes, en particulier Peter Clark, Bob Hale, John Haldane, Stephen Read, Leslie Stevenson, John Skorupski, et Crispin Wright. Comme toujours depuis quelques années, nombre des idées de ces chapitres ont d’abord été testées en conversations avec Jim Conant, et le chapitre cinq, en particulier, leur doit beaucoup. Le chapitre neuf a d’abord paru, dans une forme légèrement différente dans le <em>Southern California Law Review</em> 63 (1990): 1671-97, et est réédité ici avec l’accord du journal. Je suis aussi redevable à Bengt Molander de l’Université d’Uppsala et de Ben-Ami Sharfstein de l’Université de Tel Aviv, qui ont tout deux relu des versions antérieures et apporté de précieuses remarques. À un stade plus tardif, d’excellentes suggestions ont aussi été faites par le comité des Presses Universitaires d’Harvard, dont je n’ai pu tenir compte de toutes sans changer le caractère de ce travail, dont certaines auxquelles j’ai répondu, et dont certaines montreront leurs effets dans mes écrits futurs. Les remarques les plus précieuses de toutes furent le fait de Ruth Anna Putnam, qui m’a fournit non seulement l’affection et le soutien qui veulent tant dire, mais aussi dont la lecture et la fine critique ont fait de ce livre un bien meilleur livre.</p>

    </article>

    
    <footer class="footer">
      <p class="footer-copyright">
        <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
    </footer>
  </body>
</html>

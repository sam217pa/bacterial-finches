<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="pandoc">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

        <meta name="author" content="Erwin Chargaff">
    
        <meta name="dcterms.date" content="2019-07-02">
    
    
    <title>La route vers la Double-Hélice</title>
    <style type="text/css">code{white-space: pre;}</style>

    
    
        <link rel="stylesheet" href="css/tufte-extra.css">
        <link rel="stylesheet" href="css/pandoc.css">
        <link rel="stylesheet" href="css/pandoc-solarized.css">
        <link rel="stylesheet" href="css/tufte.css">
        <link rel="stylesheet" href="css/latex.css">
    
    
    
  </head>

  <body>

    
    <p>
      <a rel="home"
         href="https://sam217pa.gitlab.io/bacterial-finches/">Bacterial Finches</a>
    </p>

    <article>

            <header>
        <h1 class="title">La route vers la Double-Hélice</h1>
                        <p class="byline">2019-07-02 &ndash; Erwin Chargaff</p>
              </header>
      
      
      <blockquote>
      <p><em>The Path to the Double Helix</em>. Par Robert Olby. Seattle: Presses de l’Université de Washington, 1974. 534 pages. $23,50.</p>
      </blockquote>
      <p>« La Saga de la Rétrospection » (<em>The Hindsight Saga</em>) — l’excellent nom qu’aurait d’après certains choisi S.J Perelman pour son autobiographie — pourrait tout aussi bien servir de titre à ce livre; car on y trouve un Rhadamanthe chagrin convoquant à lui vivants et morts, l’un après l’autre, devant son trône éthéré pour les y juger sévèrement, bien plutôt pour ce qu’ils n’ont pas fait que pour ce qu’ils ont fait. Tout cela depuis le joyeux point de vue d’une gloire finale pleine et entière, payée et livrée. À la lueur triomphale du Saint Graal, semble-t-il, les Chevaliers de la Table Ronde infortunés sont voués aux gémonies de n’avoir point trouvé la noble coupe. Il se trouve que ce Saint Graal particulier fut découvert par deux chevaliers sur une même monture; mais des deux Perceval un seul semble avoir autorisé ou béni par ailleurs l’ouvrage: la préface est signée par Francis Crick.</p>
      <p>En tout temps l’historien s’est trouvé en pareille fâcheuse posture: il sait par quoi tout s’est conclut. Le sait-il vraiment? En tentant de décrire un océan distant, n’est-il pas lui-même emporté par une gigantesque déferlante? S’il est notoirement difficile à l’historien de rendre compte de façon plausible des motifs des actions de ses sujets, il est totalement impossible d’expliquer ce qu’il ne s’est pas produit. La psychohistoire des échecs n’est écrite que lorsqu’elle paie le traitement — et point, alors, pour être publiée.</p>
      <p>L’histoire des dénouements heureux est sans cesse sujette à devenir un conte de fée; et c’est d’autant plus vrai de l’une de ses sous-disciplines des plus précaires, l’histoire des sciences, qui jamais n’a de fin. De même que tous les grands artistes sont les meilleurs — seul le marchand d’art, ou son alter ego, l’historien de l’art, le niera —, tous les bons scientifiques sont les meilleurs, ventes aux enchères ou prix Nobel nonobstant. Les autres ne comptent simplement pas. Les classifications par rang, les distinctions entre majeur et mineur, m’ont toujours parues tout à fait oiseuses, particulièrement en science où le but final nous échappe invariablement, presque par définition.</p>
      <p>Là n’est pas la seule raison pour laquelle il est si délicat d’écrire l’histoire des sciences. Tout se tient cohésif dans ce vaste cosmos construit par tant d’esprits; or l’écrivain isolé, totalement démuni des moyens de distinguer entre la vérité, la plausibilité, et l’insipide, doit tenir beaucoup trop pour acquis. Nul tamisage de la trace écrite ne peut ressusciter l’atmosphère scientifique d’une période donnée qui, en outre, peut ne pas représenter ce qui s’est produit dans les esprits individuels; nul sélection d’énoncés à l’emporte-pièce, si plaisant soit leur agencement, n’est réellement indicatif de ce qui était su, et par qui, à un moment donné. Plutôt que de suivre, laborieusement, les développements d’un concept ou d’une construction — une tâche difficile et ingrate —, l’écrivain est tenté d’épicer son récit d’autant d’anecdotes qu’il puisse en trouver. Comme la plupart des scientifiques sont des gens immémorables, les anecdotes à leur sujet sont également immémorables; ce qui fait que l’essentiel de l’histoire d’événements scientifiques récents se lit comme l’almanach du lycée.</p>
      <p>Ayant dit cela, il me faut maintenant procéder à quelques louanges de l’ouvrage d’Olby. Il n’a pas ménagé sa peine, a lu d’innombrables papiers souvent tout à fait ennuyeux, a voyagé en des lieux guère gratifiants, se frayant une voie et l’empruntant (<em>burrowing and borrowing</em>) à travers des archives de correspondances privées, interrogeant les reliques fières ou navrées d’une <em>soi-disante</em> époque historique (moi y compris, quoiqu’il ait omis mon nom de la liste en page x). En d’autres termes, il a tenté de produire un aussi bon livre que possible. Qu’il reste que ça n’est pas un très bon livre est vraisemblablement moins de son fait que de celui de la décision d’écrire un si gros livre sous la seule perspective restreinte indiquée par son titre.</p>
      <p>Ça n’est pas l’histoire des sources et développement de nos conceptions de la chimie et de la physique de l’hérédité. Si c’était le cas, nous n’aurions pas 12 pages sur Avery et plus de 150 sur Crick et Watson. Nous en aurions vu plus sur des laboratoires aussi importants que ceux de Todd et ses collaborateurs à Cambridge; et le nom de Waldo E. Cohn n’en sera pas totalement absent. Ça n’est rien de plus que ce que le titre promet: une description méticuleuse, exhaustive et épuisante (<em>exhaustive, and exhausting</em>) d’un épisode de l’histoire de la biologie. D’aucuns pourraient se demander si pareille volumineuse tentative d’hagiographie scientifique était même nécessaire. Mais pourquoi pas? Tout comme la <em>Legenda Aurea</em> contient la charmante légende de l’<em>Inventio sanctea crucis</em>, nous avons là le récit de l’invention de la double hélice. Voilà qui n’est pas, pour parler mythopoétiquement, totalement inadéquat, car la double hélice, bien au delà de ses nombreux mérites scientifiques indéniables, est devenu un symbole tout-puissant: elle a remplacé la croix comme signature de l’alphabet biologique.</p>
      <p>Ce livre, bien qu’il ne soit pas, à certains égards, mal produit, est perclus de bien trop de fautes d’impressions.</p>

    </article>

    
    <footer class="footer">
      <p class="footer-copyright">
        <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
    </footer>
  </body>
</html>

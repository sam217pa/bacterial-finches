local vars = {
   ["url"]="https://sam217pa.gitlab.io/bacterial-finches/",
   ["pdfurl"]="pdfs/",
}

-- Compatibility: Lua-5.1
function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function html_modifier (text, re)
   local t = string.match(text, re)
   local t = split(t:gsub("}}", ""), ":")[2]
   local t = t:gsub("md", "html")
   local t = vars["url"] .. t
   return pandoc.Link("HTML", t)
end

function pdf_modifier (text, re)
   local t = string.match(text, re)
   local t = split(t:gsub("}}", ""), ":")[2]
   local t = t:gsub("md", "pdf")
   local t = vars["url"] .. vars["pdfurl"] .. t
   return pandoc.Link("PDF", t)
end

function linker (elem)
   local t = elem.text
   local html_re =  "{{" .. "l" .. ":.*}}"
   local pdf_re  =  "{{" .. "p" .. ":.*}}"
   if string.find(t, html_re) then
      return html_modifier(t, html_re)
   elseif string.find(t, pdf_re) then
      return pdf_modifier(t, pdf_re)
   else
      return elem
   end
end

return {
  {
     Str = linker,
  }
}

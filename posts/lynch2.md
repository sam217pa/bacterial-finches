---
title: Arrogance, Vérité et Discours Public
author: Michael P. Lynch
translator: Samuel Barreto
draft: false
bottom-notes: true
---

> Une démocracie est plus qu'une forme de gouvernement; c'est d'abord
> un mode de vie associée, d'adjonction d'expériences
> communiquées. --- John Dewey^[J. Dewey, _Democracy and Education_
> (Start Publishing LLC, 2012), page 101.]

# L'espace des raisons

Les démocraties aspirent à ce que Dewey envisageait comme un espace commun --- un espace ou les désaccords peuvent être conçus sans peur de la violence ou de l'oppression. Pour parler concrètement, cet espace commun de raison est le discours public --- le discours dans l'espace public, qu'il soit en ligne ou non. Une marque distinctive de ce qui est souvent décrit comme les conceptions « délibératives » de la démocratie est que le discours public devrait être raisonnable --- c'est-à-dire qu'il devrait permettre un échange de raisons pratiques ou épistémiques qui soient reconnues comme des raisons par ceux qui souhaitent s'engager dans le débat.^[De quelles façons et pour quelles raisons exactes est naturellement sujet à débat. Mais les théoriciens délibératifs avancent généralement que le discours public raisonnable a une valeur à la fois politique et épistémique. Ainsi, il est à la fois équitable et apte à produire plus de conduites (_policy_) épistémiques rationnelles résultantes. Pour une discussion voir par exemple Elizabeth Anderson, 'The Epistemology of Democracy', Episteme, 3/1-2 (2006/006/001 2006), 8-22. J. Rawls, A Theory of Justice (Harvard University Press, 2009), J. Bohman and William Rehg, Deliberative Democracy: Essays on Reason and Politics (MIT Press, 1997), J. Medina, The Epistemology of Resistance: Gender and Racial Oppression, Epistemic Injustice, and the Social Imagination (OUP USA, 2012).] Cela requiert que les citoyens aient des convictions en lesquelles ils croient, mais aussi qu'ils soient enclins à s'écouter les uns les autres.

Ce dernier prérequis va au-delà de la seule garantie de protection légale associée à la libre réunion, le libre discours, la presse libre et les normes institutionnelles de liberté académique.^[Voir Lynch, Michael P. Lynch, 'Academic Freedom and the Politics of Truth', in J. Lackey (ed.), The Philosophy of Academic Freedom (Oxford: Oxford University Press 2018).] Ces protections sont vitales pour l'existence de la démocracie même. Mais comme les évènements récents en attestent, leur seule existence ne suffit pas à faire du discours public qu'il soit _raisonnable_. Comme le dit Dewey: « Les seules garanties légales de la liberté civile de croyance, d'expression, de réunion ne sont pas d'un grand secours si l'expérience quotidienne de la liberté de communication, l'échange d'idée, de faits, d'expériences sont étranglés par la suspicion mutuelle, l'injure, la peur et la haine. »^[John Dewey, (1981a). Creative Democracy: The Task before Us. In The Later Works of John Dewey, 1925–1953, vol. 14: 1939–1941, Essays, ed. J. A. Boydston, 224–30. Carbondale, Ill.: Southern Illinois University Press. p. 227- 228.] L'argument de Dewey tient en ceci que pour parvenir à un commun espace de raison, notre _attitude_ ordinaire les uns envers les autres compte. Elle doit conduire à la confiance mutuelle et non à l'hostilité ouverte. Mais, Dewey ajoute, elle doit aussi inclure certaines attitudes épistémiques, ou attitudes conduisant à l'enquête --- qui nous encouragent à réellement participer à l'échange de raisons dans un paysage d'opinions plurielles et conflictuelles. En bref, si nous souhaitons nous approcher de l'idéal selon lequel le discours public est un espace de raisons, nous devons trouver les moyens d'encourager les citoyens à montrer de telles attitudes liées à des conduites épistémiquements responsables --- et de décourager celles qui ne le sont pas.

Dans ce papier, je suggère qu'une telle attitude épistémique est une
réticence à apprendre des autres provenant d'une relation distordue
à la vérité --- une sorte de mauvaise foi. L'arrogant épistémique est
celui dont l'intensité passionnée surgit de la conviction qu'il a tout
compris; qu'il sait tout, que la lumière de la vérité brille en lui.
C'est une attitude qui n'est, malheureusement, pas rare du tout parmi
les humains, particulièrement lorsque sont en jeu des
convictions --- précisément les problèmes dont la politique traite. Ce
qui suit tente de comprendre en quoi son adoption --- particulièrement
par les dominants --- mine l'idéal de la démocratie comme un espace de
raisons.

# Des personnages bien connus

Tel que j'emploi le terme ici, l'arrogance épistémique est une attitude socio-psychologique complexe.^[En traitant de l'arrogance épistémique et de phénonèmes liés comme des attitudes, je suis profondément influencé par Tanesini (2016); mais aussi Darwall (2006); voir aussi Tollefsen (2017).] Comme d'autres attitudes sociales, elle peut être fonctionnellement définie par ses relations à certains comportements et certaines motivations caractéristiques. Pour appréhender sa nature --- et son apparitions répandues dans la vie ordinaire --- il peut être utile d'illustrer divers attributs de cette attitude par des exemples. Peut-être le plus évident est:

> ONCLE ODIEUX: Votre oncle prend parti sur n'importe quel sujet politique et considère sa propre opinion comme le dernier mot de n'importe quel sujet. Il refuse de reconnaître que quelqu'un d'autre puisse avoir quelque chose à apporter et écarte bruyamment les points de vue alternatifs et les contre-exemples évidents à ses prétentions.

Nous connaissons tous ce type de personne, ou l'une de ses variantes. Il est difficile de discuter avec lui, et il est préférable de l'éviter. Il est arrogant mais aussi grossier et rustre.

Pourtant on peut être épistémiquement arrogant sans être rustre.
Considérez:

> AUDITRICE DOGMATIQUE: Vous prenez souvent un café avec un collègue, et discutez parfois de politique. Lorsque vous n'êtes pas d'accord, elle ne vous interrompt pas; et répond poliment à vos arguments. Mais elle ne change jamais de point de vue ni n'admet qu'elle pourrait avoir à prendre les choses d'un point de vue différent. À la longue, vous réalisez qu'elle ne vous écoute pas activement. Elle attend seulement son tour de parole.

L'Auditrice Dogmatique est courtoise, agréable même, mais elle montre néanmoins un attribut clé de l'arrogance épistémique. Elle ne parvient pas à avoir « l'esprit ouvert » ni à « être consciente de la faillibilité de nos croyances, et à être encline à reconnaître la possibilité qu'à tout moment lorsque nous croyons quelque chose, _il est possible d'être dans l'erreur_ » (Riggs 2010:180).

Le phénomène que j'appelle arrogance épistémique, cependant, a d'autres aspects. Il va au-delà du simple échec à reconnaître sa propre faillibilité. Lorsque les gens adoptent l'attitude de l'arrogance épistémique, ils peuvent aussi échouer à attribuer crédit et faute épistémiques de façon appropriée. Une telle manière de faire est la suivante:

> PIONNIER: Vous avancez un point à une connaissance professionnelle. Il commence par rétorquer, mais finit par dire « Ah oui, maintenant je vois que vous avez raison » et procède à une reformulation de vos arguments dans ses propres termes, tout en insistant sur le fait qu'il ait découvert les « vraies » failles dans une position antérieure.

PIONNIER sait qu'il peut avoir tort, admet ses erreurs, et pourrait même admettre qu'elles sont dues à ses propres limites. Mais il est (du moins dans ce cas) enclin à croire que ses erreurs sont dues à des preuves avancées par d'autres que lui. Dit autrement, il écoute, peut-être même qu'il apprend; mais il ne se voit pas comme en train d'apprendre de _vous_. Il voit ses croyances comme étant améliorées du fait de son propre génie, et s'attribue indûment le crédit épistémique.

Si l'on peut échouer à attribuer le crédit épistémique, on peut aussi
échouer à attribuer la faute épistémique. Par exemple:

> FEU-AUX-POUDRES: Un politicien est profondément biaisé sur la question de la race, et son racisme le conduit souvent à rendre des jugements erronés et nocifs sur des gens en particuliers et sur des conduites politiques. Sur la base de ces jugements, il nie fortement être biaisé, même lorsqu'il reconnaît les erreurs qui sont clairement dues à ce biais. Il blâme plutôt les autres d'avoir « racialisé » le problème.

FEU-AUX-POUDRES pourrait bien être conscient de ses biais. Mais il attribue la responsabilité de ses erreurs (dans la mesure où il reconnaît qu'elles sont même des erreurs) à d'autres, et en attaquant ceux qui osent le critiquer, il montre une attitude défensive typique de l'arrogant (Tanesini 2016a). Il veut se protéger de la critique en critiquant les autres.

Considérés ensemble, les personnages de ces cas montrent des comportements problématiques pour un dialogue réel et pour la délibération: ils n'écoutent pas vraiment, n'admettent pas leurs propres erreurs ni ne s'inquiètent d'elles, et échouent à attribuer  crédit ou faute. Aucun doute que la plupart d'entre nous reconnaisse --- douloureusement --- que nous pouvons illustrer et illustrons certaines sinon tous ces échecs nous-mêmes. Plus important pour nos fins, il est évident que le discours politique actuel porte souvent leur marque, ainsi que celles de « la suspicion mutuelle, l'injure, la peur et la haine » de Dewey. Alors que ces comportements peuvent survenir par parties dans différentes situations, je suggère qu'elles peuvent être le produit d'une seule attitude psychologique. Cette attitude encourage le rustre « je-sais-tout » d'ONCLE ODIEUX, le dédain de l'AUDITRICE DOGMATIQUE et les erreurs d'attributions de crédit auto-illusoires par PIONNIER et FEU-AUX-POUDRES. C'est cela que j'appelle l'arrogance épistémique.

# L'arrogance épistémique

Une leçon des reflexions ci-dessus est que l'arrogant épistémique pense que les autres n'ont rien à lui apprendre. Il est engagé, pour le dire autrement, envers l'idée que certains aspects de sa vision du monde ne seraient pas épistémiquement améliorés par les preuves et les expériences des autres. Être engagé envers une idée au sens pertinent signifie prendre cette idée comme prémisse d'un raisonnement théorique ou pratique; c'est une adoption de l'idée dans une conduite ou une pratique (2012).^[Le concept d'engagement invoqué ici est similaire of son acception dans L. Jonathan Cohen, "Belief and Acceptance", _Mind_, 98/391 (1989), 367-89] Ainsi, cela relève intrinsèquement de motifs; cela implique nécessairement une réticence à modifier sa vision du monde à la lumière des preuves que d'autres mettent sur la table.

Ainsi comprise, l'arrogance épistémique est relationnelle. C'est à la fois une attitude auto-centrée et hétéro-centrée.^[Pour un concept lié sur la relationnalité de l'humilité intellectuelle, voir Maura Priest, "Intellectual Humility as an Interpersonal Virtue". À paraître, _Ergo_.] Elle est auto-centrée en tant qu'elle est une orientation envers ses propres conceptions, l'impression de tout connaître, d'avoir tout compris. Mais elle est hétéro-centrée en ce qu'elle est une attitude qui, par définition, concerne les _relations_ possibles de votre vision du monde _à d'autres personnes_ ou à d'autres sources d'informations. Les arrogants épistémiques sont arrogants envers d'autres personnes ou d'autres sources auxquelles d'autres font confiance, desquels il se sentent supérieurs.^[La personne ou les personnes en question, dans la plupart des cas, sont bien réelles; mais cette description laisse ouverte la possibilité que quelqu'un adopte cette attitude seulement contrefactuellement --- lorsqu'elles sont seules sur une île déserte par exemple.]

De façon cruciale cependant, le fait qu'une personne pense qu'elle ne peut rien apprendre des autres (ou d'autres sources d'information) ne suffit pas à faire d'elle une personne épistémiquement arrogante. Pas plus qu'elle n'est arrogante seulement parce qu'elle surestime le mérite ou la justesse épistémique de ses propres conceptions --- quoique l'arrogant les surestime presque toujours. La vraie arrogance ne repose pas sur une évaluation erronée de la justesse de ses visions, mais sur une auto-illusion de ce _pourquoi_ elles sont correctes.^[C'est un point d'abord avancé par Alessandra Tanesini, "'Calm Down, Dear': Intellectual Arrogance, Silencing and Ignorance", _Aristotelian Society Supplementary Volume_, 90/1 (2016b), 71-92. La description présente doit une dette franche aux travaux retentissants de Tanesini.] _Les arrogants sont engagés envers la supériorité de leurs visions non parce qu'elles sont le reflet du monde, mais le reflet de leur estime de soi._ Ils sont dans l'illusion, à des degrés variés, que leur _conception du monde est correcte, seulement parce que c'est la leur_. Plutôt que d'avoir confiance en lui par l'effet naturel d'un réussite réelle, l'arrogant en vient à voir la confiance en soi comme une fin en soi. Bien sûr, ce fait ne parvient que rarement, sinon jamais, à la conscience; et lorsqu'il y parvient, il est typiquement dénié.^[Voir Lynch à paraître, "Fake News and the Politics of Truth" pour en savoir plus sur la nature de la mauvaise foi épistémique] Mais on pouvait s'y attendre. L'arrogant croit rarement qu'il l'est.

Ainsi l'arrogance épistémique implique presque toujours un degré donné d'auto-illusion, un acte de mauvaise foi épistémique. L'arrogant se berne sur la base de sa confiance. La nature exacte de cette mauvaise foi, peut sans aucun doute varier, mais je suggère qu'au fond, l'arrogant épistémique est, au moins dans une certaine mesure, auto-illusionné quant à la connection entre la vérité et sa propre confiance en soi.

Par là une personne _extrêmement_ arrogante, par exemple, pourrait agir sur la base du fait que ses croyances sont _vraies_ (et donc correctes) en vertu de ce qu'elles sont les siennes.^[Un tel engagement pourrait bien sûr refléter ce qu'une personne épistémiquement arrogante croit vrai. C'est-à-dire, elle pourrait être engagée envers la non-perfectibilité de son état épistémique (ou certains de ses aspects) parce qu'elle croit vraiment qu'il est imperfectible. Mais elle pourrait bien ne pas y croire. Dans ce cas, la réticence et l'engagement qui vient avec elle pourraient être dus au fait que la personne arrogance reconnaisse --- seulement implicitement --- qu'elle est exposée à la critique. Dans un tel cas, elle ne _croit_ pas que sa vision du monde est imperfectible, mais adopte encore l'attitude du je-sais-tout par insécurité ou stratégie défensive.] Dans ce cas, tout se passe comme si elle détenait une théorie divine de la vérité, et croit, ou du moins est engagée envers l'idée qu'elle --- au moins en ce qui concerne un problème donné --- est divine. À la première personne, cela revient à l'engagement que si je crois que _p_, alors _p_. Notez que cela n'impliques pas que je ne puisse pas changer d'avis, ni que si je crois que _p_ alors je devrai toujours croire que _p_. Ce que cela implique (en supposant la négation classique) est que si _non-p_, alors je ne crois pas que _p_, et si j'en vient à croire que _non-p_, alors _non-p_. Le divin peut changer d'avis; mais lorsqu'il le fait, la réalité change avec lui.^[Ceux aux prises de cette illusion pourraient, à un extrême encore plus avancé, agir avec l'idée que si _p_, alors je crois que _p_. Si c'est le cas (toujours en supposant la négation classique), alors si _non-p_, je crois que _non-p_.]

S'engager envers une telle conception, à la manière générale de l'auto-illusion, est irrationnel, même doublement irrationnel puisque cela implique d'adopter une vision absurde de la vérité, mais de l'adopter de façon instable --- puisque les gens sont rarement stables ou uniformes dans leur arrogance. Fondamentalement, cependant, adopter une telle vision de la vérité n'est pas nécessaire pour être épistémiquement arrogant. Il est probable qu'en de nombreux cas, la distorsion quant à la valeur de leurs conceptions --- sa justesse --- provient non d'une vision erronée de la vérité, mais du fait que les arrogants se moquent de la vérité au premier chef. Pour eux, la justesse de leurs conceptions --- leur mérite épistémique propre --- ne repose pas sur la vérité de leurs positions mais d'autres attributs dont ils pensent qu'eux-mêmes --- et par extension, ces positions --- les possèdent. Ils se pensent corrects, justifiés, certains même de leurs conceptions parce qu'ils sont au pouvoir, brillants, ou de la bonne classe, du bon genre, race, religion ou parti politique. Une telle personne pourrait bien supposer que ses conceptions sont vraies, bien sûr, mais qu'elles le soient n'est pas ce qui les rend correctes. La vérité n'a pas d'importance pour eux, au moins quant à certains aspects de leur vision du monde. Ce qui compte est ailleurs --- lié à leur confiance en eux.

Sous toutes ses formes, l'arrogance épistémique peut varier par degré --- vous pouvez être plus moins réticent à apprendre des autres à cause de cette sorte de mauvaise foi. La force de vos réticences sera reflétée par la force modale de l'engagement qui l'accompagne. En ce qui concerne les formes extrêmes d'arrogance dont nous venons de parler, l'arrogant épistémique s'engage envers l'idée que certains aspects de leur vision du monde est imperfectible au sens fort. Le contenu de leur engagement est qu'il n'y a pas de monde possible proche où ils auraient à apprendre quoi que ce soit de pertinent quant à leur vision du monde.

L'arrogance épistémique peut varier dans sa portée. Vous pouvez être arrogant sur certains aspects de votre vision du monde et non d'autres. Les aspects sont des domaines grossiers ou des pluralités de convictions, croyances, sentiments et les concepts et les capacités qui viennent avec, telles celles qui concernent la morale, la politique, la religion et peut-être la philosophie. Ce ne sont généralement pas des croyances isolées. Ça n'est pas un aspect de votre vision propre du monde que vous croyiez que deux et deux font quatre, pas plus que ça n'est une chose envers laquelle on peut avoir une attitude ou une inclination. Ça arrive, mais bien plus souvent, l'arrogance a une portée plus large.

Vous pouvez aussi être arrogant envers plus ou moins de personnes ou de sources d'information. Certains sont arrogants seulement envers des individus particuliers, et humbles avec d'autres par exemple. De la même façon, quelqu'un qui agirait sur la base de la mauvaise foi sous-jacente à l'arrogance épistémique pourrait refuser d'amender sa vision du monde en réponse à des preuves avancées par des sources d'information que vous considérez comme intrinsèquement biaisées, indignes de confiance voire tout simplement « mensongères »^[_« fake news »_] mais rester ouvert à d'autres sources.

Fondamentalement, vous pouvez aussi être arrogants envers des groupes (ou des individus en tant qu'ils font partie de ce groupe). Ainsi certains peuvent être arrogants envers les Républicains ou les Démocrates, envers les Afro-Américains ou les immigrés, envers les athées ou les croyants. Plus vous considérez de d'individus ou de groupes comme sans importance pour votre vision du monde, plus grande est la portée de votre arrogance. Et tout comme une personne peut être arrogante _envers_ un groupe, elle peut l'être aussi _à cause_ du groupe. Par là, j'entends que les gens sont souvent arrogants quant à leur vision du monde _non_ uniquement parce qu'elle est leur, mais parce qu'elle est leur _qua_ membre d'un groupe --- où l'appartenance au groupe est au fondement de la confiance en soi. Appelons cela l'_arrogance épistémique tribale_. Quelqu'un peut être arrogant en groupe --- engagé envers le mérite épistémique de la perspective du groupe --- parce qu'il se conçoit comme partie intégrante de l'« imaginaire social » d'un groupe en particulier avec lequel il s'identifie. En lien avec notre remarque ci-dessus, leur arrogance proviendrait de l'illusion que si un groupe donné ou une communauté est convaincue d'une chose, alors elle est vraie. Ou il se pourrait que la vérité de la question soit simplement sans importance ou ignorée. Ce qui compte est la loyauté au groupe. Ils sont arrogants, on pourrait remarquer, au nom de cette loyauté. Ainsi le simple fait que le groupe soutienne une certaine conviction suffit à la rendre supérieure aux autres ou à l'abri d'amendements.

Les points ci-dessus soulignent le fait que l'arrogance épistémique n'est ici pas comprise comme un trait, quoique rien n'empêche qu'il y ait des traits qui causent l'arrogance épistémique. Les traits sont des qualités dispositionnelles stables d'une personne --- dispositionnelles en ce sens qu'elle peut avoir le trait même si elle ne le montre pas ouvertement, et stable en ce qu'elle n'arrête pas de l'avoir le mardi quand elle l'avait le lundi. Un trait de personnalité fait partie de l'architecture psychologique d'une personne, pour ainsi dire. Les attitudes peuvent aussi être dispositionnelles. On peut avoir une attitude de mépris envers une chose sans qu'elle ne survienne à l'attention consciente. Elle peut être implicite. Mais contrairement aux traits, les attitudes n'ont pas besoin d'être stables dans le temps.

Il peut être utile de comparer l'arrogance, en tant qu'attitude, au mépris. Mépriser la religion, par exemple, signifie plus que croire (ou être disposé à croire) qu'une croyance religieuse donnée est fausse. Être méprisant dans ce sens, c'est s'engager envers l'idée que les visions religieuses sont inférieures, ou indignes sous certains aspects, c'est voir ceux qui ont ces idées comme faibles d'esprits, ou illusionnés, voire les deux. Avoir du mépris pour la religion (ou une idéologie politique, comme le Marxisme) c'est avoir une sorte distincte d'inclination négative par rapport aux formes de vie qui l'adoptent. Comme l'arrogance épistémique, le mépris est une attitude psychologique, non un trait, bien qu'un trait puisse être à son fondement. Et elle est relationnelle. On a du mépris pour quelqu'un ou quelque chose. De plus, comme nous le verrons dans la prochaine partie, le mépris est là encore comme l'arrogance épistémique en ce que mépriser les autres, c'est échouer à les respecter de la façon la plus élémentaire.

L'arrogance épistémique peut être vue, selon certaines définitions, comme une sorte particulière de fermeture d'esprit dogmatique. Après Battaly, nous pouvons définir la fermeture d'esprit _en général_ comme ou bien une réticence ou bien une incapacité à sérieusement dialoguer avec les alternatives pertinentes à sa propre conception (Battaly, 2018b, 2018a). Ainsi, Battaly note, certains peuvent être fermés d'esprit au sens général même lorsqu'ils sont enclins au dialogue avec d'autres sur un problème donné mais n'ont pas la capacité de le faire. On peut ne pas avoir cette capacité pour diverses raisons, lorsqu'on vit dans un environnement épistémiquement pollué où la pensée critique n'est ni encouragée ni enseignée; lorsqu'il nous manque certains concepts, ou que le gouvernement nous empêche, peut-être par la censure, de nous engager dans le débat. Une telle personne pourrait compter comme fermée d'esprit quant à un problème donné mais non épistémiquement arrogante dans le sens indiqué plus haut.

De plus, une personne peut être décidément fermée d'esprit sans être épistémiquement arrogante, puisqu'elle peut être réticente à dialoguer avec des alternatives pertinentes par respect de la vérité plutôt que par la sensation de sa propre supériorité et par respect pour son estime de soi. Considérez, par exemple, les scientifiques qui refusent de passer du temps à réfuter les positions de ceux qui défendent que la Terre est plate^[_The flat-earthers_], en dépit d'une alarmante croissance de leur nombre. Une telle personne pourrait penser que dialoguer avec cette vision ne contribue qu'à l'encourager et à distraire de la science réelle. L'arrogance, de l'autre côté, est cette sorte de fermeture d'esprit dogmatique qui provient d'un degré de mauvaise foi de la part de l'arrogant. Il est réticent à apprendre des autres parce qu'il s'est, au moins dans une certaine mesure, illusionné à croire que sa vision est supérieure parce qu'elle est sienne.

Par conséquent, si n'importe qui peut se trouver fermé d'esprit, l'arrogance est souvent, mais pas seulement, une affection des puissants, des privilégiés, ou de ceux qui se perçoivent comme tels (Medina, 2012; Tanesini, 2016b). Car l'une des façons dont le pouvoir nous corrompt est lorsqu'il nous convainc qu'en un sens nous sommes à l'abri de l'erreur, que tout la faute est ailleurs, et que la base de notre confiance est réelle et non basée sur la mauvaise foi.

Alors que nous nous focalisons ici sur l'arrogance épistémique, il est
bon de noter que l'attitude opposée ou contraire de l'arrogance
épistémique est l'humilité épistémique ou intellectuelle, que nous
pouvons définir comme l'engagement et l'inclination à apprendre des
autres et de nouvelles sources d'information; d'amender nos croyances
grâce aux preuves témoignées par d'autres.^["L'humilité
intellectuelle", est un terme technique dont le sens et la référence
est encore en cours de négotiation au sein de la philosophie et de la
psychologie. Comme d'autres termes techniques, il a été introduit dans
l'espoir de désigner plus précisément des phénomènes assortis
imprécisément capturés par le langage ordinaire. Ainsi par exemple, il
est communément pensé que l'« humilité intellectuelle » n'est pas
l'humilité au sens propre, mais ou bien une _sorte_ d'humilité
(l'humilité envers l'état intellectuel, cognitif ou épistémique d'un
autre par exemple) ou bien une extension du concept d'humilité. En
d'autres mots, il est admit que le concept d'humilité intellectuelle
réfère à des phénomènes parfois inclus dans l'extension de concepts
plus souvent utilisés dans des discussions non-techniques, mais il
y a un désaccord sur la façon dont on doit le mieux expliquer la
relation entre l'humilité intellectuelle et des concepts plus communs
comme l'ouverture d'esprit ou l'humilité. Plutôt que de tenter de
statuer sur ces questions, j'ai tenter d'indiquer où l'extension des
termes, tels que je les emploi, empiètent les unes sur les autres.
Voir Markus Christen, Mark Alfano, et Brian Robinson, 'The Semantic
Neighborhood of Intellectual Humility', in Andreas Herzig et Emiliano
Lorini (eds.), Proceedings of the European Conference on Social
Intelligence (2014), 40-49, Ian M. Church, 'The Doxastic Account of
Intellectual Humility', Logos and Episteme, 7/4 (2016), 413-33, Allan
Hazlett, 'Higher-Order Epistemic Attitudes and Intellectual Humility',
Episteme, 9/3 (2012), 205-23, Ian James Kidd, 'Intellectual Humility,
Confidence, and Argumentation', Topoi, 35/2 (2016), 395-402, James S.
Spiegel, 'Open- Mindedness and Intellectual Humility', School Field,
10/1 (2012), 27-38, Alessandra Tanesini, 'Intellectual Humility as
Attitude', Philosophy and Phenomenological Research, 94/3 (2016a),
Dennis Whitcomb et al., 'Intellectual Humility: Owning Our
Limitations', ibid.91/1 (2015).] De ce point de vue, l'humilité
épistémique, comme l'arrogance épistémique, est à la fois auto-centrée
et hétéro-centrée. Elle est auto-centrée en ce qu'elle est une
inclination envers le témoignage d'autres; mais elle est aussi une
inclination envers sa propre vision du monde.

La volonté d'amélioration implique que l'on _puisse_ s'améliorer. Partant, elle implique de reconnaître ses propres limitations intellectuelles et ses propres biais (Whitcomb et al, 2015) et de « s'approprier » ces biais en les tenant comme pertinents pour le raisonnement théorique et pratique. Bien sûr, une volonté de s'améliorer ou une réticence à s'améliorer n'est pas la même chose qu'une volonté de reconnaître ou une réticence à reconnaître simplement ses erreurs. Elle l'inclut (puisque là encore, vous ne pouvez vous améliorer si vous pensez qu'il n'y a rien à améliorer) mais va aussi au delà. Pour le voir, considérez un autre exemple:

> SANS-INTÉRÊT: Votre amie admet souvent lorsqu'elle a tort sur un sujet donné. Mais elle ne montre aucune inclination à améliorer son système de croyance ou de connaissance sur ce sujet lorsque d'autres soulignent ses torts ou lui fournissent de nouvelles preuves. Elle note simplement ses fautes, baisse la tête et reprend sa vie comme avant, et par conséquent, répète souvent les mêmes erreurs.

SANS-INTÉRÊT est comme cette personne mauvaise à un jeu et qui le
reconnaît, mais se moque même de tenter de s'améliorer. Dans ce cas,
il lui manque la motivation pour améliorer son état épistémique.
Qu'elle cependant compte sous ce _seul_ aspect comme épistémiquement
arrogante dépend d'autres facteurs --- incluant que son manque
d'intérêt pour l'amélioration soit dû à une réticence manifeste ou une
faible volonté ou une incapacité matérielle. Si son comportement était
dû à cette dernière, elle pourrait compter comme fermée d'esprit mais
ni arrogante ni dogmatique.^[Pour une description qui recoupe la
présente, voir la contribution de Heather Battaly à ce volume.] Dans
tous les cas, SANS-INTÉRÊT n'est pas épistémiquement humble
puisqu'elle n'est pas motivée à s'améliorer épistémiquement.

Enfin, alors que l'humilité et l'arrogance épistémique sont des attitudes contrastées et opposées, on peut ne pas être épistémiquement humble sans être épistémiquement arrogant. Une façon dont cela peut se produire vient d'être notée: on peut être fermé d'esprit, donc non épistémiquement humble, sans être épistémiquement arrogant. Mais cela peut aussi arriver pour des raisons plus banales. En effet, je suspecte la plupart d'entre nous de n'être ni humble et ouvert d'esprit ni arrogant et fermé d'esprit la plupart du temps. Nous formons nos croyances bon gré mal gré et sans engagement aucun, positif ou négatif, envers la vérité ou notre estime de soi, flottant comme des bouchons de lièges dans une vaste mer d'informations.

# Les maux de l'arrogance

Il est probable qu'être épistémiquement arrogant peut avoir de néfastes effets épistémiques sur l'arrogant. Être fermé d'esprit en général semble conduire à l'ignorance bien plus qu'à la connaissance. Cela empêche l'acquisition de nouvelles connaissances et institue les effets néfastes de croyances déjà fausses (voir Battaly, 2018b). Et il semble que cela pourrait avoir de néfastes effets moraux également, puisque l'arrogance semble liée à la hauteur, à l'égoïsme, et à un dédain général pour autrui.

Mais en sus des effets néfastes qu'elle peut avoir sur un individu, il y a au moins quatre façons dont l'arrogance épistémique peut pernicieusement affecter la raisonnabilité du discours public. Comme nous le verrons, ces maux sont dans nombre de cas contingents de la façon dont l'arrogance est répandue dans le public ou les publics en question; mais ils dépendent aussi de _qui_ est arrogant. Je suggère en particulier, que l'arrogance des priviligiés peut accroître les conséquences néfastes du discours.

Avant de lister ces maux, cependant, permettez-moi d'exposer deux remarques préliminaires. Premièrement, je laisserai en plan la question de ce que, même si l'arrogance constitue toujours un mal _pro tanto_ quant au discours public, l'arrogance constitue toujours un tout considéré comme néfaste. Je suis enclin à penser qu'elle l'est (et que tout avis divergent repose en partie sur une confusion entre la simple fermeture d'esprit et la véritable arrogance). Mais je remet cette question à un autre épisode, ainsi que la question plus délicate (et plus distincte) de ce pour quoi il serait parfois préférable de ne pas être épistémiquement humble.

Ma deuxième remarque concerne la sorte de maux aux discours public dont nous discuterons. Les quatre façons dont l'arrogance épistémique endommage ou mine la capacité publique de s'engager dans un discours politique raisonnable sont principalement de nature _épistémiques_. Pourtant en tant que c'est un idéal démocratique, tout ce qui mine la capacité de quelqu'un à s'engager dans un discours public raisonnable constitue aussi un mal politique. Les maux épistémiques, en quelque sorte, peuvent s'ajouter aux maux politiques lorsque les premiers minent ou empêchent par ailleurs la réalisation d'un idéal politique.

## Elle diminue la participation

Peut-être le mal le plus évident à l'idéal d'un discours public raisonnable est que l'arrogance peut nous encourager à ne pas nous engager du tout dans le discours --- qu'il soit raisonnable ou non.

Cela peut advenir de deux façons globales. D'abord, toutes choses égales par ailleurs, le citoyen arrogant n'est pas du tout enclin à s'engager dans un discours public raisonnable. En effet, il est prompt à voir la perspective d'un échange de raisons avec des visions opposées comme suspicieuse, à la fois parce que s'il pense que ses points de vue sont imperfectibles, alors parler avec des détracteurs peut être une perte de temps, et parce que cela pourrait encourager les moins convaincus à renoncer à son point de vue. Pour le dogmatique, que le discours public soit un espace de raison est à la fois un problème et une source de mauvaise influence.^[Ça n'est pas là dénier qu'une personne épistémiquement arrogante ne puisse trouver de valeur pratique à s'engager dans un discours public raisonnable. Il pourrait, par exemple, trouver utile de dialoguer avec les objections à ses conceptions afin de persuader les indécis, ou le trouver nécessaire pour éviter la violence. Mon point est qu'en faisant abstraction de ces considérations pratiques, il n'y a pas de raisons épistémiques urgentes à ce qu'il s'engage dans le dialogue, et qu'il pourrait y avoir des raisons morales ou politiques pour qu'il ne s'y engage pas.] Par conséquent, plus l'arrogance épistémique est répandue, moins les citoyens arrogants seront motivés à s'engager dans un discours avec ceux envers lesquels ils sont arrogants.

L'arrogance non seulement encourage l'arrogant à éviter le discours public, mais elle décourage ceux envers lesquels elle l'est de s'y engager. L'arrogance épistémique de groupe, particulièrement celle des puissants envers les marginalisés peut décourager ceux-là de participer au discours. Les marginalisés peuvent ressentir que leurs préoccupations ne sont pas entendues par les arrogants. Et c'est bien sûr correct: l'arrogant n'écoute pas. Il résulte de cela qu'il semble vain de s'engager dans un discours raisonné avec eux, et il peut en fait être parfaitement justifié sur cette base là de ne pas le faire.

De plus, l'expérience d'un dédain systématique de son point de vue
propre de la part de la majorité ou des privilégiés est à la racine du
doute, conduisant à remettre en question la véracité de ses
expériences et la précision de ses jugements personnels, et à s'en
remettre à la place à la représentation de cette expérience par la
majorité. En effet, c'est précisément le résultat que FEU-AUX-POUDRES
cherche à obtenir. Encouragé par la pression normative appliquée par
la majorité ou la communauté privilégiée, les moins puissants peuvent
en venir à croire de façon injustifiée qu'ils ne sont pas harcelés,
discriminés ou dévalués.^[Pour une discussion de ce genre de
phénomènes, voir Kristie Dotson, 'Tracking Epistemic Violence,
Tracking Practices of Silencing', Hypatia, 26/2 (2011), 236-57, ibid.,
Miranda Fricker, Epistemic Injustice: Power and the Ethics of Knowing
(Oxford University Press, 2007), Jennifer Saul, 'Implicit Bias,
Stereotype Threat, and Women in Philosophy', Women in philosophy: What
needs to change, (2013), 39-60.] Il en résulte qu'ils peuvent être
moins enclin à participer au discours public, non parce qu'ils
réalisent (et c'est justifié) qu'ils ne sont pas écoutés, mais parce
qu'ils en sont venus (et ça n'est pas justifié) à croire qu'ils n'ont
rien à dire.

## Elle mine la capacité mutuelle à rendre des comptes

L'idée que la démocracie doive être un espace de raison est fondée en partie sur l'idée que les gens d'une société égalitaire sont responsables et sont en mesure de rendre des comptes les uns les autres. C'est pourquoi les prétentions à une ressource publique, les propositions politiques et les exercices du pouvoir ne doivent dans une démocracie être justifiés non par la force mais par le recours à des raisons reconnaissables. En échangeant nos raisons, nous reconnaissons que nous nous devons à chacun le respect --- incluant, possiblement, une sorte de respect épistémique élémentaire. J'avance qu'être épistémiquement arrogant envers les autres est incompatible avec la possibilité de les traiter avec ce respect là; ainsi, l'arrogance épistémique est incompatible avec l'exigence de responsabilité et de capacité à rendre des comptes, et par conséquent, mine l'idéal d'un discours public raisonnable.

La connection entre l'arrogance et le manque de respect a été développé par Robin Dillon concernant l'arrogance en général, qui voit ses origines chez Kant (Dillon, 2003, 2015). Et alors que ce qui est vrai de l'arrogance n'est pas nécessairement vrai de l'arrogance épistémique, je pense qu'il est clair dans ce cas qu'il y a un lien. L'arrogant épistémique ne respecte pas les autres comme des agents épistémiques.

Comme Stephen Darwall et Dillon l'ont tout deux défendu, il y a au moins deux sortes de respect, et ces deux sortes sont pertinentes ici (S.L Darwall, 2006). Ce que Darwall appelle le « respect de reconnaissance » diffère du « respect d'appréciation ». Le respect d'appréciation est une forme d'estime --- comme lorsque nous respectons les performances d'une personne en tant que scientifique, athlète ou cheffe. Le respect de reconnaissance, de l'autre côté, n'est pas de l'estime, mais une reconnaissance de dignité; « elle se préoccupe non de la façon dont une chose doit être évaluée ou estimée mais de la façon dont nos relations à elle doivent être régulées ou gouvernées » (S. Darwall, 2013:19).

Dans le domaine épistémique, le respect d'appréciation est essentiellement un respect des vertus épistémiques d'autrui, de ses capacités et de son socle de connaissances acquises. Vous respectez quelqu'un de cette façon lorsque vous le voyez comme expert d'un sujet donné par exemple, ou comme ouvert d'esprit ou perspicace ou attentif. Ce que j'appellerai le respect de reconnaissance épistémique de base, d'un autre côté, « a lieu dans notre conduite épistémique en relation avec un autre » (2013:20). C'est-à-dire, en accordant à ses visions un rôle au moins potentiel dans la décision de ce que nous croyons nous-mêmes.

Il est bon de souligner que l'autorité ou la dignité épistémique que nous accordons à l'autre lorsque nous lui adressons la forme la plus élémentaire de respect de reconnaissance épistémique n'est pas due au fait que nous le prenions pour un expert. La reconnaissance de l'expertise est une forme de respect d'appréciation; en traitant quelqu'un comme un expert, j'asserte qu'il a les compétences et les connaissances qui garantissent ce respect. Ce que nous reconnaissons à l'autre lorsque nous lui adressons un respect de reconnaissance épistémique de base, d'un autre côté, est une sorte d'autorité plus fondamentale provenant de nos reconnaissance de ce qu'il y ait en lui un sachant ou un averti potentiel, et partant, nous sommes tenus _prima facie_ de lui rendre des comptes en retour. Lorsque nous respectons quelqu'un de cette façon, nous le voyons comme une source _possible_ de raison et de connaissance. En tant que tel, nous nous sentons dans le devoir de lui rendre des comptes, toutes choses égales par ailleurs, de répondre à ses objections s'il en soulevait. Deux personnes qui se respectent de cette façon ne sont pas nécessairement d'accord, mais elles se tiennent, dans cette mesure précise, mutuellement responsables. Elles se voient comme deux joueuses égales au jeu de l'échange de raisons.

Bien sûr, lorsque vous éliminez subséquemment les conceptions d'un autre parce que vous les voyez de façon justifiée comme n'ayant que peu ou pas d'expertise sur une question donnée, vous n'êtes ni fermé d'esprit ni arrogant. Vous montrez un manque justifié de respect d'estime envers cette personne et ses visions du problème. Mais ça n'est pas ce qu'il se passe avec des personnes du type de l'AUDITRICE DOGMATIQUE ou de PIONNIER. Ces personnages n'éliminent pas leurs interlocuteurs parce qu'ils pensent d'eux qu'ils sont moins informés, mais parce qu'ils voient leurs contributions comme n'ayant fondamentalement aucune influence épistémique positive sur les leurs. L'épistémiquement arrogant est réticent à voir les autres comme ayant des choses pertinentes à dire. Pour le véritable je-sais-tout arrogant, les autres sont comme de petits enfants; ils ne se voient pas comme des joueurs égaux dans le même jeu.

L'arrogant, en d'autres termes, ne pense pas qu'il doive répondre de quoi que ce soit à ceux envers qui il est arrogant. C'est précisément là que nous voyons en quoi une telle arrogance, lorsqu'elle se répand, peut profondément miner le discours public raisonnable. Comme je l'ai noté, l'idéal que la démocratie soit un espace de raisons est fondé en partie sur l'idée que les gens soient en mesure de se rendre des comptes les uns les autres. Voir chacun comme responsable de cette façon requiert --- au minimum --- de traiter chacun avec un respect de reconnaissance élémentaire. Et cela inclut un respect épistémique de reconnaissance. Pourtant, comme nous venons de le voir, l'arrogant croit qu'il ne répond qu'à lui-même. C'est là une attitude profondément anti-démocratique. C'est violer le respect de reconnaissance élémentaire et par là miner la norme de responsabilité mutuelle au centre de la démocratie libérale.

Il est important que, lorsqu'il ressort de questions politiques, l'arrogance épistémique interpersonnelle est ce que j'ai appelé plus tôt l'_arrogance tribale_. C'est-à-dire qu'elle provient à la fois de l'identification à un groupe et de ce qu'elle est dirigée vers un groupe. Ainsi, certaines personnes blanches par exemple, adoptent une attitude d'arrogance épistémique envers les minorités eut égard à certains aspects de leur vision du monde à cause de leurs conceptions de la supériorité de la « culture blanche ». Ainsi certains blancs peuvent ne pas se sentir en position de devoir rendre des comptes à des membres d'autres groupes. Pour de tels blancs, les expériences d'injustice que subissent les minorités et les preuves de leur existence, ne sont pas dignes de considérations ou de réfutation. Elles sont balayées, et méprisées.

Les maux causés à l'idéal d'un discours public raisonnable par ce genre de phénomènes sont profonds parce qu'ils minent la base même --- respect égal --- de la démocratie égalitaire. C'est, je le suggère, un mal intrinsèque. C'est un mal intrinsèque que les conséquences particulières d'un acte particulier d'arrogance de groupe soient particulièrement répandues et néfastes ou non.

En fait, cependant, les conséquences en _sont_ répandues et néfastes. Comme nous l'avons déjà noté, l'arrogance n'encourage pas seulement l'arrogant à renoncer au discours public, elle encourage ceux qui la perçoivent à tourner le dos au discours public. De plus, et de façon plus pernicieuse, les sujets d'une systématique arrogance de groupe peuvent peuvent également perdre en estime de soi. Fondamentalement, comme Dillon le notait, c'est parce que, il en va ici comme avec l'arrogance en général, « c'est une violation du respect d'autrui parce qu'à la fois elle nie la dignité intrinsèque d'autrui et en entame le respect de soi » (195). Ne pas reconnaître qu'un autre puisse vous enseigner quelque chose --- même si les leçons que vous en tirez ne sont pas les mêmes que celles que vous aviez en tête --- implique que non seulement vous ne reconnaissez pas leur dignité d'agent épistémique, mais vous pouvez les conduire à remettre en question leur propre estime d'eux-mêmes en tant qu'agents rationnels et sujets de connaissances.^[De cette manière, l'arrogance épistémique peut contribuer à l'injustice épistémique (Fricker, _Epistemic Injustice: Power and the Ethics of Knowing._). Si je ne tiens pas compte du témoignage averti provenant d'un membre d'un groupe marginalisé simplement à cause de leur statut, j'agis d'une façon épistémiquement injuste.]

## Elle mine la confiance épistémique

Le troisième préjudice que l'arrogance épistémique peut causer au discours public raisonnable est un préjudice de confiance. Dans la mesure où les citoyens sont épistémiquement arrogants les uns envers les autres, cela mine la confiance épistémique dont le discours public a besoin pour s'épanouir.

Nous formons nos croyances dans une communauté sociale d'avertis, et il fait donc partie de ce que c'est d'être un agent épistémique responsable d'être un membre responsable de cette communauté, une communauté qui est cohésive en raison de la fabrique commune d'une confiance épistémique. Dans ce cas, l'arrogant épistémique --- tel que les idéal-types dont nous avons discuté plus haut --- n'est pas un agent épistémique responsable non seulement parce qu'il lui manque la structure correcte de motivation, mais parce que son attitude et les comportements résultants minent les conditions nécessaires à la confiance épistémique --- c'est-à-dire, les conditions qui nous permettent de se reposer sur la connaissance des autres.

Certaines de ces conditions sont de donner et recevoir le crédit épistémique lorsqu'il est dû; et de faire confiance au fait que ceux de la communauté d'enquête sont, en fait, engagés dans l'enquête et non une autre fin, tel qu'un gain de pouvoir ou un avancement de carrière.^[Ça n'est pas nier, bien sûr, que nos caractères n'entament pas davantage la confiance épistémique par ailleurs. PIONNIER entame la confiance épistémique en commettant une forme d'injustice épistémique --- manquer de traiter autrui comme un agent épistémique à part entière. TOURISTE POLITIQUE, d'un autre côté, l'entame en dévaluant la vérité de la même façon que la prévalence de baratineurs l'entame, comme le pense Frankfurt. Comme les récents évènements en politique américaine le suggèrent, plus nous sommes tolérant envers une attitude cavalière quant à la vérité dans le discours public, moins les gens sont enclins à accorder leur confiance aux médias et aux institutions scientifiques qui sont censés, en partie, disséminer l'information.] Ainsi, par exemple, PIONNIER et FEU-AUX-POUDRES entament la confiance épistémique en commettant une forme d'injustice épistémique --- ils ne traitent pas autrui comme un agent épistémique à part entière. Et cette entame a lieu dans les deux directions, pour ainsi dire. L'arrogant ne fait pas confiance à ceux envers lesquels il est arrogant pour accroître ses connaissances, et ceux qui perçoivent autrui comme arrogant sont moins enclins à faire de même. Car percevoir quelqu'un comme arrogant (par opposition à le percevoir comme digne de confiance et averti) c'est le croire intéressé plus à l'estime de soi qu'à la vérité objective.

C'est ici que nous voyons la sorte de dommage que l'entame de la confiance peut produire sur le discours public. Nous comptons les uns sur les autres pour connaître, à condition d'un arrière-plan de distribution du travail épistémique. Même dans des conditions idéales, aucun de nous ne peut savoir tout ce qu'il y a à savoir; nous comptons sur les docteurs, les plombiers, les avocats, les comptables, les professeurs, et bien sûr, Google, pour nous fournir les information dont nous avons besoin. Ce fait est, ou devrait l'être, reflété dans le débat politique public, où nous fournirions idéalement des raisons pour nos conceptions en nous appuyant sur les conclusions d'experts. Dans la mesure où nous sommes épistémiquement arrogants, nous n'éprouverons nul besoin de recourir ni de s'en remettre à de tels experts. Et dans la mesure où lesdits experts sont eux-mêmes arrogants ou perçus comme tels, dans cette même mesure nous nous sentirions moins tentés de leur faire confiance.

L'influence réelle de ce point concerne les institutions, pas seulement les individus. Comme les évènements récents en politique américaine le suggèrent, plus nous sommes tolérants vis-à-vis d'attitudes cavalières quant à la vérité, plus nous sommes enclins à célébrer l'estime de soi et l'arrogance des puissants dans notre discours public, moins les gens le sont à accorder leur confiance aux médias et aux institutions scientifiques dont la fonction est de disséminer des informations fiables. Et moins les citoyens sont enclins à faire confiance à de telles institutions, moins le discours public est apte à être raisonnable --- ne serait-ce que parce que ce manque de confiance dévalue la monnaie commune des raisons avec laquelle le discours public est censé commercer.

## Elle mine la valeur de la vérité

Cela nous amène au dernier effet, le plus pernicieux de l'arrogance épistémique sur la vie politique. Comme nous l'avons noté plus haut, l'arrogant peut avoir une relation distordue avec la vérité et la preuve. Il est prédiposé, à l'extrême, à voir ses conceptions ou celles de son groupe comme vraies simplement parce qu'elles sont siennes, ou même à n'accorder aucune importance à la vérité. Ainsi, l'arrogant épistémique, qu'il le sache ou non, montre un manque d'intérêt manifeste pour la vérité objective. Si, comme je le crois, la vérité objective est elle-même une valeur démocratique centrale, une préoccupation importante pour un discours publique raisonnable, ce dédain pour la vérité constitue sans doute un quatrième préjudice au discours public.

J'ai discuté de la façon dont la vérité compte pour la démocratie plus en détail ailleurs (M. Lynch 2004; Michael P. Lynch 2012); je terminerai donc par un point qui semble particulièrement pertinent à la fois pour le discours public et pour ce moment politique. Il est toujours bon de rappeler qu'une démocratie égalitaire saine accorde de la valeur à l'enquête publique sur la vérité. Cela implique d'accorder de la valeur aux institutions et aux pratiques sociales transparentes, qui encouragent la dissémination publique de l'information, qui fonctionnent par des règles dont l'application peut être publiquement assertée par des moyens légaux et journalistiques. Les démocraties accordent de la valeur à l'enquête publique parce qu'elle reconnaît implicitement un respect égal des personnes --- et la responsabilité mutuelle qui va avec --- fondamental pour cette conception de la politique (Darwall, 2012, 21). Par conséquent, les démocraties saines encouragent une presse libre, une liberté de recherche académique, et un système judiciaire et légal fort, capable d'enquêter et d'examiner d'autres branches du gouvernement --- incluant tout particulièrement la branche exécutive. Les démocraties promeuvent de telles enquêtes et les institutions qui les conduisent précisément parce qu'en les promouvant, elles réalisent la responsabilité mutuelle, et l'idéal de la démocratie comme espace de raisons.

J'ai noté plus tôt que les maux de l'arrogance épistémique sont particulièrement inquiétants lorsque ce sont les puissants qui sont arrogants. C'est là encore vérifié. Lorsque les puissants deviennent, comme ils y sont prédisposés, épistémiquement arrogants, ils seront enclins à traiter l'enquête publique sur les faits comme une perte de temps. Après tout, ils se sentiront ne devoixr répondre qu'aux faits tels qu'ils les voient. Pour eux, la critique survenant de telles enquêtes ne peut qu'être « mensonges ».

De cette façon, l'arrogance épistémique encourage le despotisme, et la tyrannie.^[Merci à Heather Battaly, Alessandra Tanesini, Jason Kawall, Teresa Allen, Hanna Gunn, Dana Miranda, Toby Napoletano, Sandy Goldberg, Terry Berthelot et Paul Bloomfield, et au public de la 2017 Episteme Conference, Free University of Amsterdam, University of Pennsylvania, University of Copenhagen, and the University of Connecticut.]

# Références

Anderson, Elizabeth (2006), 'The Epistemology of Democracy', Episteme, 3 (1-2), 8-22.

Battaly, Heather (2018a), 'Closemindedness and Dogmatism', Episteme, forthcoming.

--- --- --- (2018b), 'Can Close-mindedness be an Intellectual Vice?', Philosophy forthcoming

Bohman, J. and Rehg, William (1997), Deliberative Democracy: Essays on Reason and Politics (MIT Press).

Christen, Markus, Alfano, Mark, and Robinson, Brian (2014), 'The Semantic Neighborhood of Intellectual Humility', in Andreas Herzig and Emiliano Lorini (eds.), Proceedings of the European Conference on Social Intelligence, 40-49.

Church, Ian M. (2016), 'The Doxastic Account of Intellectual Humility', Logos and Episteme, 7 (4), 413-33.

Cohen, L. Jonathan (1989), 'Belief and acceptance', Mind, 98 (391), 367-89. Darwall, S. (2013), Honor, History, and Relationship: Essays in Second-Personal Ethics II (OUP Oxford).

Darwall, Stephen L (2006), The second-person standpoint: Morality, respect, and accountability (Harvard University Press).

Dewey, J. (2012), Democracy and Education (Start Publishing LLC)

M. P. Lynch: Arrogance, Truth and Public Discourse

Dillon, Robin S (2003), 'Kant on Arrogance and Self-Respect', in Chesire Calhoun (ed.), Setting the Moral Compass: Essays by Women Philosophers (Oxford: Oxford University Press), 191-216.

--- --- --- (2015), 'Self-Respect in Kant and Hill', Reason, Value, and Respect: Kantian Themes from the Philosophy of Thomas E. Hill, Jr., 42.

Dotson, Kristie (2011), 'Tracking epistemic violence, tracking practices of silencing', Hypatia, 26 (2), 236-57.

Fricker, Miranda (2007), Epistemic injustice: Power and the ethics of knowing (Oxford University Press).

Hazlett, Allan (2012), 'Higher-Order Epistemic Attitudes and Intellectual Humility', Episteme, 9 (3), 205-23.

Kidd, Ian James (2016), 'Intellectual Humility, Confidence, and Argumentation', Topoi, 35 (2), 395-402.

Lynch, Michael (2004), True to Life: Why Truth Matters (Cambridge, Mass.: Mit Press).

Lynch, Michael P. (2012), In Praise of Reason (MIT Press).

Lynch, Michael P. (2018), 'Academic Freedom and the Politics of Truth', in J. Lackey (ed.), The Philosophy of Academic Freedom (Oxford: Oxford University Press ).

Medina, J. (2012), The Epistemology of Resistance: Gender and Racial Oppression, Epistemic Injustice, and the Social Imagination (OUP USA).

Rawls, J. (2009), A Theory of Justice (Harvard University Press).

Riggs, Wayne (2010), 'Open-mindedness', Metaphilosophy, 41 (1), 172-88.

Saul, Jennifer (2013), 'Implicit bias, stereotype threat, and women in philosophy', Women in philosophy: What needs to change, 39-60.

Spiegel, James S. (2012), 'Open-mindedness and intellectual humility', School Field, 10 (1), 27-38.

Tanesini, Alessandra (2016a), 'Intellectual Humility as Attitude', Philosophy and Phenomenological Research, 94 (3).

--- --- --- (2016b), '‘Calm Down, Dear’: Intellectual Arrogance, Silencing and Ignorance', Aristotelian Society Supplementary Volume, 90 (1), 71-92.

Whitcomb, Dennis, et al. (2015), 'Intellectual Humility: Owning Our Limitations', Philosophy and Phenomenological Research, 91 (1).

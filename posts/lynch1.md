---
title: De la Vérité en Éthique
subtitle: International Encyclopedia of Ethics
author: Michael P. Lynch
date: 2012
translator: Samuel Barreto
draft: false
---


Le sujet de la vérité en éthique peut être divisé en trois questions
principales: Les jugements moraux sont-ils capables de vérité?
Y a-t-il un jugement moral vrai? Et si certains le sont, en vertu de
quoi sont-ils vrais?

Un jugement est « capable de vérité »^[_truth apt_] lorsqu'il est
à même d'être vrai ou faux. Il y a au moins trois raisons largement
reconnues de croire que les jugements moraux sont capables de vérités.
Aucune d'entre elles n'est définitive. Mais elles constituent un cas
_prima facie_ --- comme l'indique le fait que ceux qui nient que les
jugements moraux sont capables de vérité déploient typiquement des
efforts considérables pour tenter d'écarter ces raisons.

La première raison pour que les jugements moraux soient capables de
vérité est qu'ils ont les attributs logiques de jugements capables de
vérité. Un jugement a de tels attributs lorsqu'il peut être sensément
dénié, lorsqu'il peut apparaître dans des conditionnels amovibles, et
peut être généralement compris, aux fins de la logique, comme
fonction de vérité. Les jugements moraux ont tous ces attributs.

Une deuxième raison pour que les jugements moraux soient capables de
vérité est qu'ils sont soumis à des normes d'évaluation épistémique.
Les jugements moraux ont lieu dans ce que Sellars appelle l'« espace
des raisons ». Si je rend un jugement moral sur, par exemple,
l'injustice de la peine de mort, et que vous vous opposez à ce
jugement, alors en temps normal je me doit de vous fournir une raison
ou toute autre preuve en faveur de mon jugement. Si je ne peux fournir
aucune raison ni aucune preuve --- même une preuve qui peut elle-même
être mise au défi --- je devrais retirer mon jugement, ou du moins ma
confiance en lui. C'est ce qui rend les jugements moraux différents de
la plupart des jugements de goûts. Je ne dois pas retirer mon jugement
selon lequel les Fettucine Alfredo sont délicieuses si vous le niez.
Pas plus que je ne me doive de vous fournir une raison à mon jugement.
Mais si je déclare qu'il est parfois bon de torturer des prisonniers,
je me dois de vous en fournir une raison si vous vous opposez à mon
jugement. Si je ne peux en fournir une, il semble que j'encoure une
obligation, à tout le moins, à diminuer ma confiance en cette opinion.

La troisième raison pour que les jugements moraux soient capables de
vérité est qu'ils ont des prétentions objectives. C'est-à-dire que
nous nous pensons normalement capables de faire des erreurs morales ou
d'ignorance morale. En effet, nous pensons typiquement la croissance
morale et la maturité comme un processus impliquant d'identifier et de
corriger les erreurs morales passées et d'en venir à apprécier les
facteurs moralement pertinents dont nous étions inconscients
auparavant. Ainsi, quelqu'un élevé dans un environnement raciste peut
plus tard en venir à voir ses perspectives antérieures comme
moralement erronées. De ce fait, il semble s'obliger à penser que ses
visions antérieures n'étaient pas seulement fausses, mais que leur
valeur de vérité ne dépendaient pas de ce qu'il les ait jugées comme
ayant une certaine valeur de vérité.

Chacune de ces trois raisons peut être contestée; mais toutes
ensembles représentent un cas _prima facie_ pour prendre les jugements
moraux comme capables de vérité. La question est alors seulement de
quelle sorte de vérité sont ils capables?

La tradition philosophique tient généralement pour acquis que si les
jugements moraux sont vrais, alors ils le sont de la même manière que
n'importe quelle sorte de jugement vrai. La théorie historiquement
dominante de la vérité est la théorie de la correspondance. Selon
elle, les jugements sont vrais seulement lorsqu'ils correspondent aux
faits. Toute vision de cette sorte, pour ne pas être rien de plus
qu'une platitude, doit dire quelque chose de la nature des faits, et
de la nature de la relation de correspondance. Il s'est avéré
difficile de répondre à ces questions d'une manière qui rende
plausible le fait que les jugemets moraux correspondent aux faits.

De nombreux théoriciens contemporains de la correspondance, par
exemple, arguent que pour qu'un jugement corresponde aux faits, il
faut que les concepts qui composent le jugement représentent
précisément les objets et les propriétés du monde. Leur métaphore
maîtresse est celle de la carte: les jugements vrais sont comme des
cartes précises. Ainsi,

> Représentation: Le jugement que x est F est vrai si et seulement
> l'objet représenté par \<x\> a les propriétés représentées par
> \<F\>.

Appelons une théorie de cette forme une théorie représentationnelle de
la vérité. Typiquement, les philosophes qui adoptent cette approche
générale adoptent également le réalisme moral. Ils considèrent ainsi
que les jugements pertinents représentent des objets et des propriétés
qui existent indépendamment des systèmes qui les représentent (voir
à RÉALISME MORAL).

Les théories représentationnelles de la vérité morale ont des
avantages évidents. Ils expliquent les prétentions objectives des
jugements moraux. Ce que nous pouvons représenter, nous pouvons aussi
le déformer. Dans ce sens, les théories représentationnelles de la
vérité permettent la possibilité de l'erreur morale. Juger qu'une
chose est moralement bonne ne la rend pas telle. Dans le même sens,
elles permettent aussi une notion claire de l'amélioration morale.
Plus nous représentons les propriétés morales telles qu'elles sont, et
non, par exemple, telles que nous voudrions qu'elles soient, plus nos
opinions morales s'améliorent.

Mais toute théorie représentationnelle de la vérité fait face à un
sérieux obstacle. Les théories représentationnelles sont prisonnières
d'une théorie de la représentation. La plupart des théories
contemporaines de la représentation sont naturalistes. C'est-à-dire
que

> Naturel: Les relations représentationnelles surviennent de relations
> naturelles.

Si c'est le cas, les théories représentationnelles de la vérité ne
sont plausibles que lorsque le contenu de nos jugements peut être
compris comme répondant causalement à un environnement externe qui
contribue à ce contenu. C'est-à-dire qu'on ne peut cartographier ce
qui n'est pas et qu'on ne peut cartographier correctement ce avec quoi
on n'a pas de contact causal, même indirect. Par conséquent, les
théories représentationnelles sont souvent vues comme difficiles
à appliquer aux jugements --- comme les jugements moraux ou les
jugements mathématiques --- qui semblent porter sur des objets et des
propriétés avec lesquels nous n'avons pas de relations causales
évidentes.

En particulier, il y a des raisons intuitives et célèbres de croire
que les propriétés morales, si elles existaient, ne seraient pas la
sorte de propriétés avec lesquelles nous entrons en contact causal
(Harman, 1977).^[Harman, Gilbert. 1977. The Nature of Morality.
Oxford: Oxford University Press.] Le problème, comme Crispin Wright
l'a souligné (1992),^[Wright, Crispin. 1992. Truth and Objectivity.
Cambridge, MA: Harvard University Press.] n'est pas seulement que l'on
peut se demander en quoi les propriétés morales peuvent être vues
comme expliquant causalement nos jugements moraux et nos croyances. Le
problème est qu'il est difficile de voir en quoi les propriétés
morales peuvent être vues comme faisant partie de l'explication
causale de quoi que ce soit _d'autre_ que des jugements moraux.
Partout où nous sommes certains de réagir cognitivement à des objets
physiques, nous pensons typiquement que les objets auxquels nous
réagissons causent plus que nos seules réponses. Nous réagissons à la
présences de chats sur le tapis en formant une croyance à propos des
chats, mais que les chats soient sur le tapis causent plus que ma
simple croyance --- cela a pour causes que la souris reste dans son
trou, qu'il y ait une puce sur le tapis, etc… Ce _nexus_ de
connections causales fait partie de ce qui nous convainc qu'il y a une
chose à laquelle nous réagissons lorsque nous formons nos croyances
sur les chats, ou que nous faisons des jugements sur eux. Mais avec
Wright et Harman, on peut se demander si ce large spectre de
connections causales existe dans le cas de la morale.

Ces considérations illustrent pourquoi la question de la vérité peut
sembler particulièrement troublante dans le cas de la moralité. Nous
avons des raisons de croire que les jugements moraux sont vrais. Mais
il est difficile d'expliquer en quoi il peut en être ainsi. Les
tentatives pour préciser la nature de la correspondance ou bien
semblent rendre difficile aux jugements moraux de correspondre au sens
défini, ou bien finissent par sonner dangereusement creux (comme: les
jugements moraux sont vrais lorsqu'ils correspondent à des réalités
morales).

Résultant de considérations de cette sorte, de nombreux philosophes
arguent que, en dépit de nos raisons initiales, nous devons abandonner
l'idée qu'un jugement moral peut être vrai. On peut prendre cette
pensée de deux façons. La première est le chemin de l'expressiviste
traditionnel (voir EXPRESSIVISM, EMOTIVISM). Selon cette vision, les
jugements moraux expriment des états non-représentationnels de
l'esprit. Donc si la vérité est un problème de précision de la
représentation, les jugements moraux ne sont pas même capables de
vérité, partant aucun jugement moral n'est vrai (ou faux). Un autre
alternative est d'accepter que les jugements moraux _visent_ bien
à représenter des propriétés du monde, mais seulement qu'ils manquent
toujours leur cible. Le monde, pour ainsi dire, ne coopère pas: aucune
des propriétés ou des faits que nos jugements tentent de représenter
n'existe réellement. Donc si la vérité est un problème de
représentation, alors tout jugement moral positif est faux. C'est le
chemin du théoricien de l'erreur traditionnel (voir ERROR THEORY).

Quelles alternatives restent-ils à ceux qui souhaitent s'accrocher au
fait que certains jugements moraux sont vrais? Une voie, choisie par
nombre de philosophes contemporains, consiste à nier un présupposé
jusque-là central de notre discussion: que les jugements --- moraux ou
autres --- sont vrais en vertu du fait qu'ils ont une propriété
substantive unique telle que la correspondance.

La façon la plus évidente de dénier ce présupposé est de défendre
qu'il n'y a pas de propriétés substantives des jugements en vertu
desquels un jugement --- tout jugement --- est vrai. C'est la voie
choisie par ceux qui défendent le minimalisme de la vérité (Horwich,
1998; Field, 2001; voir MINIMALISM about TRUTH, ETHICS and).^[Horwich,
Paul. 1998. Truth, 2nd ed. Oxford: Oxford University Press; Field,
Hartry. 2001. Truth and the Absence of Fact. Oxford: Oxford University
Press.] Selon le minimalisme, le concept de vérité n'est qu'un
appareil logique pour la généralisation, et ne sert pas à dénoter une
propriété théoriquement intéressante ou explicative. Notre
appréhension du concept de vérité est épuisé par notre appréhension
d'instances de la sorte de

> TS: \<p\> est vrai si et seulement si p.

Ici, cela est compris comme impliquant que, par exemple les
propositions que les roses sont rouges est équivalente, dans des
contextes non-opaques, à la proposition que les roses sont rouges. Si
c'est le cas, ainsi que les minimalistes le prétendent, alors il n'y
a rien de plus à dire de ce qui rend les jugements --- moraux ou
non --- vrais. En particulier, il n'y a rien besoin de dire sur ce en
quoi les jugements représentent des objets et des propriétés, et donc
le fait que les jugements moraux ne semblent pas être
représentationnels n'est pas un obstacle à leur véracité.

Le minimalisme est strictement cohérent avec le réalisme sur la
moralité ou la vision qu'il existe des propriétés morales
indépendantes de l'esprit (voir MORAL REALISM). Mais cela ressemble
à un étrange appariemment de point de vue des motivations théoriques.
Car s'il y a des propriétés là-dehors dans le monde, il serait en
effet curieux qu'elles n'aient rien à voir avec les raisons pour
lesquelles les jugements que nous portons sur elles sont vrais ou
faux. Peut-être en conséquence, il est plus fréquent de voir le
minimalisme adopté par les philosophes qui s'opposent au réalisme
moral.

Les expressivistes, en particulier, ont trouvé le minimalisme de la
vérité agréable. Comme indiqué, certains expressivistes classiques
soutiennent que les jugements moraux sont capables de vérité. Une
objection familière à l'expressivisme souligne que, comme nous l'avons
montré plus haut, cela est démenti par les attributs logiques de tels
jugements --- ils semblent être capables d'être significativement
niés, utilisé comme antécédents de conditionnels amovibles, et
apparaissent en prémisses ou conclusions d'arguments valides (voir
FREGE-GEACH OBJECTION). Il semble que les jugements moraux n'auraient
pas ces attributs s'ils n'étaient pas capables de vérité. Une façon de
réagir à de tels problèmes, défendue par Simon Blackburn
(1998)^[Blackburn, Simon. 1998. Ruling Passions. Oxford: Oxford
University Press.] et d'autres quasi-réalistes (voir QUASI-REALISM)
est d'adopter un minimalisme quant à la vérité. Selon le minimalisme,
être vrai n'est pas un grand succès métaphysique. Donc si rendre le
jugement moral que le meurtre est mauvais c'est exprimer son sentiment
propre, alors juger qu'il est vrai que le meurtre est mauvais c'est
simplement exprimer les même sentiments par d'autres moyens.

Certains philosophes suspectent le quasi-réaliste d'être dans une
position précaire. Le quasi-réaliste doit distinguer entre les
jugements expressifs d'un contenu et ceux qui ne le sont pas. Ce ne
peut plus être fait en termes de vérité, si le minimalisme est
correct. En particulier, nous ne pouvons dire que des jugements
non-moraux sont vrais en vertu de leur représentation de la réalité
alors que les jugements moraux ne peuvent l'être par là même. Car il
n'y a pas de propriété des jugements qui les rendent vrais si le
minimalisme est correct.

Une possibilité est que le quasi-réaliste peut soutenir qu'alors que
les jugements moraux et non-moraux peuvent être tous deux vrais, seuls
les jugements non-moraux expriment des croyances. Mais il
y a également des raisons de douter du succès de cette suggestion.
Supposons que les propositions morales soient vraies, je peux les
juger, mais aucun jugement n'exprime de croyances. Ainsi je n'exprime
pas une croyance quand je juge que:

> M: Le meutre est mauvais.

Pourtant selon le minimalisme M est équivalent à

> MT: \<Le meutre est mauvais\> est vrai.

Donc, si en jugeant M je n'exprime pas une croyance, je n'exprime pas
non plus une croyance en jugeant MT. Donc pour la question en cours,
je peux juger qu'il est vrai que le meurtre est mauvais, mais je n'y
croirai pas. Car si je croyais vraiment MT, je croirais M. Pourtant
selon le point de vue dont nous sommes en train de discuter, les
jugements moraux n'expriment pas de croyances. C'est-à-dire;

> Où P est un jugement moral, si je juge P alors il n'est pas le cas
> que je croie P.

Donc d'après le point de vue que l'on considère, il y a des vérités
morales, mais je ne peux croire aucune d'elles; pas plus que je ne
peux croire qu'elles sont vraies. Cela ne semble guère satisfaisant.
Ça n'est pas satisfaisant déjà pour la simple raison suivante.
Probablement, en établissant ses visions, la quasi-réaliste croit
qu'il y a des vérités morales, ou à tout le moins, qu'il peut y en
avoir (après tout, son minimalisme assure cette possibilité). Mais il
est très difficile de comprendre pourquoi, ou même comment, vous
pourriez croire qu'il y a des vérités morales si, pour quelle que soit
une vérité morale donnée, vous ne pouvez croire qu'elle est vraie.

Sans surprise, les expressivistes minimalistes quant à la vérité
propositionnelle sont par conséquents minimalistes quant à la vérité
des croyances. De plus, ils soutiennent que le concept de croyance est
lui-même compris minimalement comme dans: croire que p est simplement
être disposé à juger que p est vrai (Blackburn, 1998; Timmons,
1998).^[Blackburn, Simon. 1998. Ruling Passions. Oxford: Oxford
University Press; Timmons, Mark. 1998. Morality without Foundations.
Oxford: Oxford University Press.] Mais ce « minimalisme rampant »
(Dreier, 2004)^[Dreier, James. 2004. “Meta-Ethics and the Problem of
Creeping Minimalism”. Philosophical Perspectives 18 (1):23–44.] expose
seulement une autre inquiétude --- c'est-à-dire qu'une fois que le
minimalisme est accepté, les jugements expressifs s'avèrent avoir
beaucoup des attributs logiques et sémantiques de jugements
non-expressifs. Si c'est le cas, alors nous perdons la capacité
à distinguer entre les jugements expressifs et non-expressifs.

Plus haut nous avons noté que si l'on espère retenir l'idée qu'au
moins certains jugements moraux sont vrais, alors il semble que l'on
doive nier que tous les jugements sont vrais parce qu'ils représentent
la réalité. Une possibilité est d'adopter le minimalisme: soutenir
qu'il n'y a rien en vertu de quoi tous les jugements moraux vrais sont
vrais. Une autre voie est de soutenir que les jugements moraux sont
vrais de façons différentes de jugements non-moraux.

Quelles autres propriétés pourraient avoir les jugements moraux en
vertu desquels ils seraient vrais? Une suggestion, défendue par les
constructivistes (voir CONSTRUCTIVISM), est que les jugements moraux
sont vrais lorsqu'ils sont cohérents d'une certaine façon.

Les théories de la cohérence de la vérité morale commencent
généralement par l'idée qu'une théorie de la vérité morale peut être
construite à partir d'une théorie de ce qu'est une garantie ou une
justification pour un jugement moral (cf Dorsey, 2006).^[Dorsey,
Dale. 2006. “A Coherence Theory of Truth in Ethics”. Philosophical
Studies 127 (3). 493-523.] Selon une théorie dominante en
épistémologie morale, un jugement moral est garanti dans la mesure où
le cadre moral auquel il appartient est dans un état d'équilibre
réflexif large (voir REFLECTIVE EQUILIBRIUM). On peut le décrire de la
façon suivante: le jugement de S que p est garanti dans la mesure où
il est cohérent avec les autres jugements moraux et non-moraux de S.

La notion de cohérence désigne une famille de desiderata épistémiques.
On pense généralement qu'un cadre est cohérent dès lors que, et dans
la mesure où, ses membres montrent des relations de support explicatif
mutuel, il est complet, et il est consistant. Appelons cela des
_attributs de cohérence_.^[_coherence making features_] De tels
attributs eux-mêmes surviennent par degrés: les membres d'un cadre
peuvent être plus ou moins consistant, plus ou moins mutuellement
explicatifs, etc… Un cadre de jugements augmente en cohérence dans la
mesure où il illustre ces attributs, en somme, à un degré supérieur.
« En somme » parce que ces attributs ne sont pas eux-mêmes isolés dans
leur force d'augmentation de cohérence. Un cadre ne serait pas plus
cohérent en somme, par exemple, simplement en augmentant sa taille (sa
complétude) en incluant des jugements consistants mais déconnectés
explicativement. Intuitivement, en augmentant le nombre de ses
jugements explicatifs isolés, la cohérence d'un cadre resterait en
somme statique ou bien diminuerait.

Ces notions en main, nous pouvons faire sens de ce que serait pour un
cadre moral d'augmenter en cohérence. Le cadre C est plus cohérent
à t2 qu'à t1, lorsqu'en somme, il a à t2 soit plus d'attributs de
cohérence, soit certains de ces attributs à un degré supérieur. Donc
si la complétude, la consistance et la connectance^[J'emprunte ici le
terme de _connectance_ à l'écologie des réseaux, qui désigne le niveau
d'interactions réalisées entre deux partenaires écologiques (NdT). Il
paraît approprié dans la mesure où c'est à une véritable écologie des
jugements qu'appelle cette notion de cohérence d'un cadre moral.]
explicative sont des attributs de cohérence, ajouter un jugement
consistant ou connecté explicativement au système augmente la
cohérence de ce système dans ces dimensions. Par conséquent, on peut
dire que P est cohérent avec le cadre moral C si, et seulement si, le
résultat de l'inclusion de P dans C, rendrait en somme C plus
cohérent.

L'idée derrière une théorie de la cohérence de la vérité morale est
celle-ci. Si les jugements moraux garantis sont seuls cohérents, alors
les jugements moraux vrais peuvent être appelés supercohérents. On
peut définir la supercohérence comme:

> SUP: Le jugement moral P est supercohérent avec C si et seulement si
> P est cohérent avec C à un stade donné de l'enquête et continue de
> l'être à toutes les étapes successives et à toutes les améliorations
> additionnelles d'informations morales et non-morales à C.

La suggestion constructiviste en vient ensuite à: Un jugement moral
P est vrai en vertu de ce qu'il est supercohérent avec C. Ainsi,
là où le théoricien de la correspondance admet que les jugements
moraux sont vrais en vertu de leur relation aux faits (ou aux objets
et leurs propriétés) le constructiviste admet que les jugements moraux
sont vrais en vertu de leur relation à d'autres jugements.

Un avantage de la théorie de la cohérence de la vérité morale est
qu'elle semble présenter les avantages du réalisme moral et de
l'anti-réalisme moral. Par exemple, en expliquant la vérité des
jugements moraux en termes autres que ceux de la représentation, une
théorie de la cohérence semblerait s'accommoder d'au moins deux
apports clés de l'expressivisme. Premièrement, elle est cohérente avec
le fait qu'il n'y ait pas de propriétés distinctivement morales dans
le monde. Deuxièmement, elle est cohérente avec la pensée que nos
jugements de valeur ont une fonction différente de nos jugements sur
le monde naturel (Blackburn, 1984).^[Blackburn, Simon. 1984. Spreading
the Word. Oxford: Oxford University Press.] Mais contrairement au
quasi-réalisme, cette théorie autorise encore une distinction nette
entre le moral et le non-moral: la porte est laissée ouverte à la
prétention que les jugements de valeur sont vrais en vertu de leur
supercohérence et les jugements sur le monde physique sont vrais en
vertu de leur représentation du monde naturel. Du côté réaliste de
l'équation, les théories de la cohérence semblent autoriser la
possibilité de l'erreur morale. Qu'un jugement soit supercohérent
(avec un cadre donné) est un succès cognitif significatif. Nombre de
nos jugements moraux actuels peuvent n'être pas supercohérents. En
fait, il est bien possible qu'aucun d'eux ne le soit. Et comme je l'ai
déjà souligné, les théories de la cohérence permettent aussi une
notion d'amélioration morale.

Pourtant les théories de la cohérence de la vérité morale font aussi
face à des objections claires. Un problème est qu'elles semblent
autoriser des cadres également supercohérents mais moralement
incompatibles. Ce qui à son tour suggère que le cohérentiste est en
fait en train de proposer un vision relativiste de la vérité morale
(voir MORAL RELATIVISM). Ainsi, aucun jugement moral n'est absolument
vrai; tous les jugements moraux sont relativement vrais (Capps et al,
2009; Harman, 1996).^[Capps, D. Lynch, M.P., and Massey, D. 2009. “A
Coherent Moral Relativism.” Synthese 166: 413-430; Harman, G., 1996,
“Moral Relativism,” in G. Harman and J.J. Thomson (eds.) Moral
Relativism and Moral Objectivity, Cambridge MA: Blackwell Publishers.
3-64.] C'est-à-dire,

> Une vérité morale est vrai relative à C en vertu de ce qu'elle est
> supercohérente avec C.

Qu'un relativisme moral de cette sorte soit problématique n'est pas
parfaitement clair. Une raison de penser qu'il l'est, est que la
conception présente énonce qu'un jugement morale est vrai lorsqu'il
fait partie d'un système durable de jugements moraux et _non-moraux_.
Ainsi, de façon cruciale, il n'y a aucune obligation que les jugements
non-moraux soit eux-mêmes _vrais_. Et ceci pose problème. En d'autres
mots, si certaines fausses croyances non-morales tiennent bon, et que
suffisamment d'ajustements sont faits par ailleurs au système pour
compenser le fait que ces croyances fausses tiennent bon, même les
visions morales les plus folles pourraient s'avérer supercohérente, et
donc même les visions morales les plus folles pourraient être vraies.
En bref, l'objection n'est pas seulement que la théorie est contrainte
au relativisme, mais qu'elle est contrainte à une forme de relativisme
répréhensible.

La cohérentiste peut tenter de contrer l'implication que sa
perspective est condamnée à une forme répréhensible de relativisme en
lui ajoutant certaines contraintes (Lynch, 2009).^[Lynch,
Michael. 2009. Truth as One and Many. Oxford: Oxford University
Press.] Ainsi, elle pourrait arguer que la théorie correcte de la
vérité morale n'est pas SUP mais

> Concordance: Le jugement moral P est vrai si et seulement si il est
> concordant avec C, où P est concordant avec C seulement lorsque (a)
> P est supercohérent avec C; (b) P est supercohérent avec les vérités
> non-morales et (c) la proposition P _est supercohérente avec C_
> n'est pas vraie en vertu de ce qu'elle est supercohérente avec C.

L'ajout de la clause (b) signifie que, selon la théorie présente, afin
que le jugement moral P de quelqu'un soit vrai, il doit non seulement
être supercohérent avec ses jugements non-moraux, mais ces jugements
doivent être vrais --- de quelque manière qu'un jugement non-moral
soit vrai. Ainsi certains jugements moraux qui pourraient être
supercohérents avec les jugements moraux et non-moraux de quelqu'un
peuvent n'être pas concordants avec ces jugements simplement parce
qu'ils sont incohérents avec les faits empiriques (c'est-à-dire, les
jugements qui représentent précisément certaines réalités
non-morales). L'ajout de (c) signifie qu'il n'est pas suffisant pour
qu'un cadre soit supercohérent (ou qu'un jugement soit supercohérent
avec C) que ce cadre inclue le jugement qu'il est lui-même
supercohérent. Les jugements de supercohérence ne sont pas vrais en
vertu de leur supercohérence.

L'ajout de ces contraintes signifie que le fait qu'un jugement moral
soit vrai est d'autant plus un succès. Néanmoins, il semble encore au
moins logiquement qu'il y ait un jugement moral P qui soit concordant
avec un certain cadre alors que sa négation est concordante avec un
autre cadre. Si la concordance d'un cadre est suffisant pour la
vérité, il semble que cela entraîne que cette conception est encore
une forme de relativisme. Que le relativisme en question soit encore
une menace pour l'objectivité reste une question ouverte.

Les théories de la cohérence de la vérité morale admettent qu'un
jugement moral est vrai d'une manière différente d'autres jugements.
Partant, ils sont contraints à une forme de _pluralisme de la vérité_:
la conception qu'il y a plus d'une propriété des jugements en vertus
desquels ils peuvent être vrais. D'autres théories de la vérité morale
pourraient bien être contrainte à un pluralisme de la vérité. Une
théorie de la correspondance des vérités morales qui défendrait que la
façon dont les jugements moraux correspondent est distincte de la
façon dont les jugements non-moraux correspondent pourrait aussi être
contrainte au pluralisme. Il en va de même pour une théorie qui
soutiendrait que le minimalisme est vrai des jugements moraux mais que
le représentationnalisme est vrai de jugements non-moraux.

Le pluralisme de la vérité, cependant, comporte ses propres problèmes.
L'un d'eux est celui de la façon dont on rend compte de la validité
des soi-disant inférences mixtes. Considérez un débat comme: l'herbe
est bleue ou le meutre est mauvais. L'herbe n'est pas bleue, donc le
meurtre est mauvais. C'est un argument valide. Mais si les prémisses
et les conclusions sont vraies de « façons différentes » il n'est pas
clair en quoi l'argument peut être valide. Car il n'est pas clair
qu'il conserve une « façon » unique d'être vraie (Tappolet,
1997).^[Tappolet, Christine, 1997. “Mixed Inferences: A Problem for
Pluralism about Truth Predicates”. Analysis, 57: 209-11.] Certains
(Beall, 2000; Lynch, 2009) ont suggéré que ces problèmes et d'autres
peuvent être résolus en faveur du pluralisme.^[Beall, Jc. 2000. “On
Mixed Inferences and Pluralism about Truth Predicates.” Philosophical
Quarterly 50:380-82; Lynch, Michael. 2009. Truth as One and Many.
Oxford: Oxford University Press.] Qu'il soit le cas, et que les
théories de la vérité morales requièrent un présupposé de pluralisme,
restent des questions ouvertes.

# Références

- Beall, Jc. 2000. “On Mixed Inferences and Pluralism about Truth
  Predicates.” Philosophical Quarterly 50:380-82.
- Blackburn, Simon. 1984. Spreading the Word. Oxford: Oxford
  University Press.
- Blackburn, Simon. 1998. Ruling Passions. Oxford: Oxford University
  Press.
- Capps, D. Lynch, M.P., and Massey, D. 2009. “A Coherent Moral
  Relativism.” Synthese 166: 413-430.
- Dorsey, Dale. 2006. “A Coherence Theory of Truth in Ethics”.
  Philosophical Studies 127 (3). 493-523.
- Dreier, James. 2004. “Meta-Ethics and the Problem of Creeping
  Minimalism”. Philosophical Perspectives 18 (1):23–44.
- Field, Hartry. 2001. Truth and the Absence of Fact. Oxford: Oxford
  University Press.
- Harman, Gilbert. 1977. The Nature of Morality. Oxford: Oxford
  University Press.
- Harman, G., 1996, “Moral Relativism,” in G. Harman and J.J. Thomson
  (eds.) Moral Relativism and Moral Objectivity, Cambridge MA:
  Blackwell Publishers. 3-64.
- Horwich, Paul. 1998. Truth, 2nd ed. Oxford: Oxford University Press.
- Lynch, Michael. 2009. Truth as One and Many. Oxford: Oxford
  University Press.
- Tappolet, Christine, 1997. “Mixed Inferences: A Problem for
  Pluralism about Truth Predicates”. Analysis, 57: 209-11.
- Timmons, Mark. 1998. Morality without Foundations. Oxford: Oxford
  University Press.
- Wright, Crispin. 1992. Truth and Objectivity. Cambridge, MA: Harvard
  University Press.

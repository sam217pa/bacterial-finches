---
title: La démocratie deweyenne, une reconsidération
subtitle: "Renewing Philosophy, 1995"
author: Hilary Putnam
translator: Samuel Barreto
date: 2019-02-13
draft: false
---

Je ne sais pas de meilleur moyen de conclure ce travail que de
discuter d'un philosophe dont l'œuvre à son meilleur niveau illustre
si bien la manière qu'a eu le pragmatisme Américain (à son meilleur
niveau) d'éviter les illusions de la métaphysique comme celles du
scepticisme: John Dewey. Une préoccupation éclaire l'ensemble de
l'œuvre considérable de Dewey; même ce qui apparaît comme des écrits
purement épistémologiques ne peut être compris en dehors d'elle. Il
s'agit de son inquiétude quant au sens et au futur de la démocratie.
Ce sur quoi je souhaiterais attirer l'attention dans la pensée de
Dewey est une justification philosophique de la démocratie, dont je
crois qu'on peut la trouver dans ses travaux. Je l'appellerai
_justification épistémologique de la démocratie_, et, quoique
j'emploierai les miens propres, je sélectionnerai délibérément des
termes du vocabulaire philosophique propre à Dewey.

L'argument donc, est celui-ci: La Démocratie n'est pas une simple
forme de vie sociale parmi d'autres formes de vie sociales
opératoires; c'est la précondition au plein emploi de l'intelligence
pour résoudre des problèmes sociaux.

Au début de _Éthique et les Limites de la Philosophie_, Bernard
Williams opère une distinction fort utile entre deux sens dans
lesquels on peut tenter de justifier des prétentions éthiques. Le sens
le plus utopique est le suivant: on peut tenter de trouver une
justification à ses prétentions éthiques qui pourrait convaincre le
sceptique ou l'amoraliste et le persuader de changer ses façons de
faire. Williams conclut à raison que c'est un objectif peu réaliste.
« Si, poursuit-il, par contraste, la justification s'adresse à une
communauté déjà éthique, alors la politique du discours éthique,
y compris la philosophie morale, est significativement différente. Le
but n'est pas de contrôler les ennemis de la communauté ou ceux qui la
fuient, mais, en donnant des raisons à des personnes prédisposées
à les entendre, de participer à la créer une communauté maintenue
cohésive par cette même disposition. »

La conception de la philosophie morale que Williams suggère ici me
semble correspondre exactement à celle de Dewey. Pourtant dans son
livre, Williams ignore non seulement la figure historique de John
Dewey, mais aussi la possibilité même de justification que Dewey
a proposé. Quand Williams en vient à discuter des stratégies de
justification basées sur des conceptions de l'épanouissement humain,
« l'épanouissement humain » se voit attribuer un sens totalement
individualiste. Par exemple, « D'après Aristote, écrit-il, une vie
vertueuse conduirait en effet au bien être de l'homme de piètre
éducation, même lorsqu'il ne le sait pas. Le fait qu'il soit
incurable, et ne puisse pas comprendre correctement le diagnostic,
n'implique pas qu'il ne soit pas malade » (_Ethics_, p. 40). Une
justification objective, dans le seul sens possible pour Williams,
doit pouvoir être donnée à tout être humain qui n'est pas « malade ».
En bref, le seul espoir d'un fondement objectif à l'éthique que
Williams considère est ce qu'on pourrait appeler une justification
« médicale » --- une justification objective de l'éthique montrerait
que dans un sens non-ambivalent de « malade », l'homme amoral ou
immoral est malade. Le seul endroit d'où pourrait provenir une telle
justification est, selon Williams, « quelque branche de la
psychologie »; cette possibilité le laisse sceptique, quoiqu'il dise
qu'« il serait ridicule de tenter de déterminer _a priori_ et en
quelques pages s'il pourrait y avoir une telle théorie ». Le but
mentionné plus haut, de ne «  pas contrôler les ennemis de la
communauté ou ceux qui la fuient, mais, en donnant des raisons à des
personnes prédisposées à les entendre, de participer à la créer une
communauté maintenue cohésive par cette même disposition. », s'est vu
attribuer une interprétation radicalement individualiste.

Cependant, lorsque Williams explique pourquoi il est est peu probable
qu'il y ait jamais une « branche de la psychologie » qui permettrait
de fonder objectivement l'éthique, il a cette remarque très
intéressante: « Il existe [...] un caractère, quoique peut-être plus
rare que ce que Calliclès supposait, mais bien réel, qui est
suffisamment horrible, sans le moins du monde être misérable, et qui,
selon toutes normes éthologiques du regard vif et du pelage luisant,
s'épanouit dangereusement. Pour ceux qui souhaitent fonder la vie
éthique dans la santé psychologique, le fait qu'il existe de telles
personnes pose problème. » Noter la référence à « toutes normes
éthologiques du regard vif et du pelage luisant ». Selon Williams, un
standard objectif de l'épanouissement humain nous regarderait de la
même façon que si nous étions des tigres (ou peut-être des écureuils).
Bernard Williams, du moins à ce moment précis, songe à un standard de
l'épanouissement humain ignorant tout ce qu'Aristote lui-même aurait
vu comme typiquement humain. Dewey, par contre, nous voit
principalement en termes de nos capacités à initier l'action, à parler
et à expérimenter intelligemment.

Non seulement une justification sociale --- c'est-à-dire, adressée
à _nous_ plutôt qu'à chacun des « moi » ---, la justification de Dewey
est aussi, comme je l'ai dit dans l'introduction, une justification
épistémologique, et c'est aussi une possibilité que Williams ignore.
La possibilité de justification que Williams envisage est une
justification « médicale »; une preuve de ce que si vous êtes immoral
alors vous êtes malade d'une certaine façon. Si nous tentons de
reformuler la justification de Dewey en ces termes, alors nous
devrions dire d'une société non-démocratique qu'elle est d'une
certaine façon malade; mais je crois de la métaphore médicale qu'il
vaut mieux l'abandonner d'un bloc.

# Le Bon Sauvage et l'Âge d'Or

Bien que les arguments de John Dewey soient largement ignorés dans la
philosophie morale et politique contemporaine, son
entreprise --- justifier la démocratie --- perdure plus saine que
jamais. L'œuvre monumentale de John Rawls, _Une Théorie de la
Justice_, par exemple, tente de fournir à la fois une raison d'être
aux institutions démocratiques et une position d'où les échecs de ces
institutions peuvent être critiquées; cela pourrait tout aussi bien
servir de description au projet deweyien. Mais, en dehors de la
philosophie, et même dans une certaine mesure à l'intérieur d'elle, il
y en a pour qui la tentative même de justifier la démocratie fait
fausse-route. Une type d'objection provient des anthropologues ou
d'autre sciences sociales, quoiqu'elle ne leur soit pas restreinte. Le
cas que j'ai en tête est un essai de Stephen et Fredérique Marglin, un
économiste radical et une anthropologue radicale. Ces auteurs
rejettent l'idée que nous pouvons critiquer des sociétés
traditionnelles même sur des pratiques sexistes telles que l'excision.
Les Marglins défendent leur point de vue en partie en défendant le
relativisme culturel; mais leur relativisme extrême mis à part, je
crois qu'il y a autre chose en jeu --- une chose que l'on trouve dans
les écrits de nombreux sociologues moins sophistiqués que les
Marglins. Sans prendre trop de pincettes, je crois que nous avons
affaire à une résurgence du mythe du Bon Sauvage. En essence, les
sociétés traditionnelles sont vues par ces penseurs comme si
supérieures aux nôtres que nous n'avons en aucune façon le droit de
les déranger. Pour montrer ce qui ne va pas avec ce point de vue,
permettez-moi pour le moment de me concentrer sur le cas des
inégalités de sexe dans les sociétés traditionnelles.

En discutant de cela, il faut séparer deux questions: la question de
l'intervention paternaliste et la question du jugement moral, de
l'argument moral et de la persuasion. Jamais dans la vision de Dewey
n'intervient l'idée, par exemple, qu'un despote bienveillant doive
intervenir partout où il y a des problèmes sociaux et les corriger:

> L'idée d'une communauté de bien peut être clarifée en se référant
> aux tentatives de la part de ceux en position de supériorité stable
> de conférer le bien à autrui. L'Histoire montre qu'il y eut de
> bienveillants despotes qui souhaitèrent étendre le bien à d'autres.
> Ils n'ont pas réussi, sauf quand leurs actions ont pris la forme
> indirecte d'un changement des conditions dans lesquelles vivent les
> défavorisés. Le même principe est vrai des réformateurs et des
> philanthropes lorsqu'ils tentent de faire le bien à d'autres de
> façons telles qu'elles laissent passifs les bénéficiaires. Il
> y a une tragédie morale inhérente aux efforts d'accroissement du
> bien commun qui empêche leurs résultats d'être ou bons ou
> communs --- ni bons, parce qu'ils se font aux dépens de la
> croissance active de ceux qu'ils sont censés aider, ni communs,
> parce que ceux-là n'ont aucune part dans la provocation du résultat
> final. Le bien-être social ne peut être augmenté que de façons
> suscitant l'intérêt positif et l'énergie active de ceux auxquels
> elles bénéficient ou qu'elles « améliorent ». La notion
> traditionnelle du grand homme, du héros, fonctionne mal. Elle
> encourage l'idée qu'un « leader » quelconque doit montrer la voie;
> les autres doivent suivrent en l'imitant. Sortir les esprits de
> l'apathie et de la léthargie, les conduire à penser par eux-mêmes,
> à partager la conception des plans, à prendre part à leur exécution,
> tout cela prend du temps. Mais sans coopération active à la fois
> dans le choix des buts et dans leur atteinte, il n'y a pas de
> possibilité d'un bien commun.

Ceux qui s'opposent à l'_information_ des victimes d'inégalités
sexuelles --- ou de tout autres formes d'oppressions partout où elles
se trouvent --- de l'injustice de leur situation et de l'existence
d'alternatives sont les vrais paternalistes. Leur conception du bien
est essentiellement la « satisfaction » dans l'un des sens
utilitaristes classique; en faits, ils disent que la femme (ou qui que
soit l'opprimé) est satisfaite, et que c'est à « l'agitateur » qui
secoue leurs convictions que revient la faute de créer du
mécontentement.

Ce que les sociologues radicaux que j'ai mentionnés proposent en fait
est ce que Karl Popper appelait une « stratégie d'immunisation », une
stratégie à travers quoi les fondements de l'oppression d'autres
cultures peuvent être mis à l'abri de la critique. Cela repose sur
l'idée que l'aspiration à l'égalité et à la dignité sont restreintes
aux citoyens des démocraties industrielles occidentales. Les
événements de la place Tian'anmen de 1989 en sont une réfutation plus
puissante que tout mot que je pourrais écrire ici.

À l'autre extrême, du moins politique, de l'argument du Bon Sauvage
contre la tentative de justifier les institutions démocratiques est un
argument que je perçois dans les écrits récents d'Alasdair MacIntyre.
Dans ces livres, MacIntyre propose un vaste résumé philosophique de
l'histoire de la pensée occidentale, qui adopte en effet l'idée qu'un
système de croyances éthiques peut en « défaire rationnellement » un
autre; qui insiste en effet sur le fait qu'il puisse y avoir un
progrès dans le développement des conceptions du monde; mais qui est
parcouru par la suggestion que ce progrès a fondamentalement cessé
quelque part entre le douzième et le quatorzième siècle, et que nous
n'avons que régressé depuis lors.

Si la suggestion que je décris comme traversant les écrits de
MacIntyre me dérange, la suggestion que nous n'avons que regressé
depuis le Moyen Âge central (une suggestion mise en avant de façon
beaucoup plus retentissante par le best-seller d'Allan Bloom, _l'Âme
Désarmée_^[_The Closing of the American Mind_, New York: Simon &
Schuster, 1987; traduction française _l'Âme Désarmée_, Éditions
Juillard, 1987, NdT]), c'est à cause du fait que la politique que de
telles conceptions pourraient justifier ne sont rien moins
qu'effroyables.

Les défenseurs du Bon Sauvage et les défenseurs de l'Âge d'Or ont en
commun de défendre une doctrine tendant à immuniser l'oppression
institutionnalisée de la critique. Les stratégies d'immunisation sont
différentes, mais elles ont ceci en commun: elles abandonnent l'idée
qu'il pourrait être bon pour les victimes de l'oppression de connaître
des façons de vivre alternatives, des conceptions alternatives de leur
situation, et d'être libres de tester d'eux-mêmes quelle conception
est la meilleure. Et les bon-sauvagistes et les âge-d'oristes bouchent
le chemin de l'enquête.

# La Métaphysique de Dewey (ou l'absence de)

De quelles prémisses Dewey dérive la prétention que je lui ai imputée,
c'est-à-dire, la prétention que la démocratie est une précondition
au plein emploi de l'intelligence à la solution de problèmes sociaux?
Comme nous le verrons très bientôt, les « prémisses » sous-jacentes
sont des suppositions très courantes.

Dewey croit (comme nous tous, lorsque nous ne jouons pas au sceptique)
qu'il y a de meilleures et de pires résolutions aux tourments
humains --- à ce qu'il appelle des « situations problématique ». Qu'il
soit le cas n'est pas une chose dont Dewey discute sur des bases
a priori. Pas plus que ces prémisses ne découlent d'une quelconque
branche de la psychologie. Il est instructif ici de rappeler
l'argument de Peirce (comme de Dewey) pour la méthode scientifique
elle-même: dans les deux articles fameux du _Popular Science Monthly_
dans lesquels Peirce a lancé le mouvement pragmatiste, il argue que
nous avons appris de l'expérience que la méthode de l'autorité, la
méthode de la ténacité, et la méthode de Ce Qui est Agréable à la
Raison ne fonctionnent pas. Dans une veine similaire, l'œuvre de Dewey
_Logic_ conçoit la théorie de l'enquête comme produit de la sorte même
d'enquête qu'elle décrit: _l'épistémologie est hypothèse_. En bref,
Dewey croit que même si nous ne pouvons réduire la méthode
scientifique à un algorithme, _nous en avons appris sur la façon de
conduire l'enquête en général, et ce qui s'applique à l'enquête
intelligemment conduite en général s'applique à l'enquête éthique en
particulier._

Ça ne serait pas le point de vue des métaphysiciens scientistes que
j'ai critiqués. Selon eux, on ne peut supposer que des gens
intelligents soient capables de séparer les bonnes résolutions à des
situations problématiques des mauvaises (après expérimentation,
réflexion et discussion); l'on doit d'abord montrer
« ontologiquement » qu'il y a un fait (_« a fact of the matter »_)
quant aux bonnes et mauvaises résolutions aux situations
problématiques. C'est là, par exemple, ce qui inquiête Bernard
Williams; pour lui la seule façon qu'il puisse y avoir des faits quant
à quelle forme de vie sociale est bonne ou mauvaise serait que de tels
faits découlent de « quelque branche de la psychologie ». En l'absence
d'une telle branche de la psychologie (et Williams croit très peu
probable qu'il y en ait jamais) nous n'avons aucune base à croire
qu'une forme de vie sociale _puisse être_ meilleure qu'une autre
à moins que le jugement du bon ou mauvais soit admis comme n'exprimant
qu'une vérité « locale », la vérité d'un jeu de langage qui présuppose
les intérêts et les pratiques « d'un monde social ou d'un autre ».
Pour Williams, la distinction entre les faits « locaux » et
« absolus » est omni-présente; il ne peut y avoir de faits « absolus »
du genre de ce que Dewey pense que des gens intelligents soient
capables de découvrir. Dewey, tel que je le comprend, rétorquerait que
la notion même de fait « absolu » est insensée.

Cependant, c'est un fait de la philosophie analytique que, un temps
mouvement anti-métaphysique (pendant la période du positivisme
logique), il est récemment devenu l'un des mouvements les plus
pro-métaphysique de la scène philosophique mondiale. D'un point de vue
métaphysique réaliste, on ne peut jamais partir de la prémisse
épistémologique que _les gens sont capables de dire si A ou B_; on se
doit d'abord, dans la « conception absolue du monde », de montrer
qu'il y a de tels faits A et B possibles. Une description
métaphysico-réductive de ce qu'est le bien doit préceder toute
discussion de ce qui est le mieux ou le pire. Selon moi, la
contribution majeure de Dewey fut d'insister sur le fait que nous
n'avons pas ni ne requerrons de « théorie de tout », et de presser que
ce dont nous avons besoin est une connaissance de la façon dont les
être humains résolvent les situations problématiques:

> L'inquiétude primaire [de la philosophie] est de clarifier, libérer
> et étendre le bien inhérent aux fonctions naturellement générées de
> l'expérience. Il n'a pas à créer un monde de « réalité » _de novo_,
> ni à percer les secrets de l'Être cachés du sens commun et de la
> science. Elle n'a pas d'informations ou de corpus de connaissances
> particulière; si elle ne se rend pas toujours ridicule lorsqu'elle
> se pose en rivale de la science, c'est seulement parce qu'un
> philosophe donné se trouve être aussi, en tant qu'être humain, un
> homme de science prophétique. Son travail est d'accepter et
> d'utiliser à un but le meilleur des connaissances disponibles en son
> temps et lieu. Et ce but est la critique des croyances, des
> institutions, des coutumes, des politiques quant à leur influence
> sur le bien. Cela ne veut pas dire leur influence sur _le_ bien,
> comme une chose elle-même formulée et atteinte au sein de la
> philosophie. Car tout comme la philosophie n'a pas
> d'arrière-boutique privée de connaissance ou de méthode pour
> atteindre la vérité, elle n'a pas d'accès privilégié au bien. Comme
> elle accepte la connaissance des faits et des principes des
> compétents en science et en enquête, elle accepte les biens diffus
> dans l'expérience humaine. L'autorité Mosaïque ou
> Paulinienne d'une révélation ne lui est pas confiée. Mais elle
> l'autorité de l'intelligence, de la critique de ces biens communs et
> naturels.

La nécessité d'institutions démocratiques fondamentales telles que la
libérté de pensée et de parole découle, pour Dewey, des prérequis de
la procédure scientifique en général: le flux sans entraves
d'information et la liberté d'émettre et de critiquer des hypothèses.
Durkheim apporte des arguments dans une certaine mesure similaires,
mais parvint à la conclusion que les opinions politiques devraient
reposer sur l'« opinion experte », les privés d'expertises censés s'en
remettre à l'autorité des experts (et particulièrement aux
sociologues). Si Dewey a pu ne pas connaître ces essais de Durkheim,
il a pourtant considéré et rejetté ce point de vue, pour des raisons
franchement empiriques: « Une classe d'experts est immanquablement si
détachée des intérêts communs qu'elle en devient classe aux intérêts
et aux connaissances privés, ce qui, dans les questions sociales,
n'est pas connaissance du tout. » Dewey fait ici le lien avec un autre
de ses thèmes, que le privilège produit inévitablement une distorsion
cognitive: « Tous privilège réduit les perspectives de ceux qui le
possèdent, et entrave le développement de ceux qui ne le possèdent
pas. Une portion considérable de ce qui est vu comme l'égoïsme
intrinsèque de l'humanité résulte d'une distribution inéquitable du
pouvoir --- inéquitable parce qu'elle en éloigne des conditions qui
dirigent et provoquent leurs capacités, pendant qu'elle produit une
croissance unilatérale parmi les privilégiés » (Dewey et Tufts,
_Ethics_, pp 385-386). Ainsi, si une valeur aussi générale que la
valeur de la démocratie doit être défendue de la façon que Dewey
propose, les matériaux employés par la défense ne peuvent être
circonscrits à l'avance. Il n'y a pas une seule région de l'expérience
d'où proviendraient toutes les considérations pertinentes
à l'évaluation de la démocratie.

Le dilemme que recontrent les défenseurs classiques de la démocratie
tient au fait que tous présupposent que nous connaissions déjà notre
nature et nos capacités. Par contre, la vision de Dewey est que nous
ne savons pas ce que sont nos intérêts et nos besoins ni de quoi nous
sommes capables avant de nous engager réellement en politique. Un
corollaire de cette vision est qu'il ne peut y avoir de réponse finale
sur la question de comment nous devrions vivre, et donc nous devrions
toujours laisser ouverte la question à de plus amples discussions et
expérimentations. C'est précisément pourquoi nous avons besoin de la
démocratie.

En même temps, nous savons bien que certaines choses échappent à notre
nature et nos capacités. Dewey savait pertinemment qu'égalité et
liberté peuvent entrer en conflit, et qu'il n'y a pas de solution
aisée lorsqu'il y a réellement conflit; mais il penserait, je crois,
que ces conflits sont par trop soulignés dans la philosophie politique
actuelle. D'après Dewey, il ne peut y avoir de doute sur le fait que
les _inégalités_, à l'échelle de celles qui existent de nos jours,
étouffent notre nature et nos capacités, et restreignent la liberté
à une échelle massive. Si nous devons parler de « conflits entre
égalité et liberté », nous devrions aussi parler des manières dont
l'inégalité conduit à l'absence de liberté.

# Dewey et James

Si la philosophie sociale de Dewey est irrésistiblement juste, pour
autant que je sache, sa philosophie morale est moins satisfaisante
lorsque nous tentons de l'appliquer à des choix individuels
existentiels. Pour le voir, considérons le fameux exemple de choix
existentiel employé par Sartre dans _L'Existentialisme est un
Humanisme_. C'est la Seconde Guerre Mondiale, et Pierre doit faire un
choix cornélien entre rejoindre la Résistante, ce qui implique de
laisser sa mère vieillissante seule à la ferme, et rester à la ferme
et prendre soin de sa mère, mais sans aider à combattre l'ennemi.
L'une des raisons pour quoi la recommandation de Dewey d'utiliser
l'expérimentation intelligemment guidée dans la résolution de problème
éthique n'est d'aucune aide en de tels cas est le conséquentialisme
deweyien. Pierre n'est pas là pour « maximiser » le bien, quel qu'il
soit conçu, en un sens global quelconque; il est là pour faire ce qui
est _juste_. Comme toutes les visions conséquentialistes, celle de
Dewey échoppe à rendre justice des considérations du _juste_. Je ne
dis pas que la philosophie deweyienne ne s'applique jamais aux choix
existentiels individuels. Certains choix sont simplement bêtes. Mais
Pierre ne l'est pas. Les alternatives qu'il envisage ne sont stupides
en aucune façon. Pourtant il ne peux simplement jouer à pile-ou-face.

Il y a, bien sûr, des questions de choix individuel qui peuvent être
traitées de la même façon que les questions sociales. Si par exemple,
je suis indécis quant à l'école où inscrire mon enfant, je peux
décider d'expérimenter. Je peux envoyer l'enfant à l'école avec dans
l'idée que si cela ne fonctionne pas, je peux l'en enlever et
l'inscrire dans une école différente. Mais ça n'est pas le genre de
problème auquel Pierre fait face.

Ce que certains philosophes disent d'une telle situation est que
l'agent devrait chercher une conduite telle que si tout le monde, dans
une situation similaire, devait l'adopter les conséquences en seraient
pour le mieux, et ensuite adopter cette conduite. Parfois c'est
raisonnable; mais pas dans la situation de Pierre. L'une des choses en
jeu dans la situation de Pierre est la nécessité pour lui de décider
qui il _est_. Son individualité est en jeu; et l'individualité en ce
sens n'est pas seulement une « valeur bourgeoise » ou une idée des
Lumières. Dans la tradition juive sont souvent cités les écrits du
Rabin Susiah, qui dit que dans l'au-delà le Seigneur ne lui
demanderait pas « As-tu été Abraham? » ni « As-tu été Moïse? » ni
« As-tu été Hillel? » mais « As-tu été Susiah? » Pierre veut être
Pierre; ou, comme le dirait Kierkegaard, il veut devenir « celui qu'il
est déjà ». Ça n'est pas la même chose que de vouloir suivre la
« conduite optimale »; ou peut-être que ça l'est --- peut-être que
dans un tel cas, la conduite optimale est, en fait, de devenir celui
que vous êtes déjà. Mais faire ceci n'est pas une chose à quoi le
conseil d'employer la « méthode scientifique » est d'aucune aide, même
lorsque vous êtes aussi bien disposés envers la méthode scientifique
que Dewey.

Il y a plusieurs suites possibles à l'histoire de Pierre, quelle que
soit la décision qu'il prend. Des années plus tard, s'il survit,
Pierre pourrait raconter l'histoire de sa vie (de la bonne ou mauvaise
façon) décrivant sa décision (de rejoindre la Résistance ou de rester
avec sa mère) comme la décision clairement juste, sans regrets ni
doutes à leur endroit, quels que les coûts s'en soient avérés. Ou bien
il pourrait décrire sa décision comme la mauvaise, ou comme un
« dilemme moral » auquel il n'y avait pas de réponse correcte. Mais
une partie du problème que Pierre rencontre au moment où il doit
prendre la décision est qu'il ne sait pas même que ce à quoi il fait
face _est_ un « dilemme moral » en ce sens.

C'est précisément à ce genre de situation que William James pensait
lorsqu'il écrivit le fameux essai « La Volonté de Croire » (duquel il
dit plus tard qu'il aurait du s'appeler « Le _Droit_ de Croire »).
Bien que cet essai ait reçu une bonne dose de critique hostiles, je
crois que sa logique est, en fait, précise et irréprochable; mais je
ne tenterai pas de défendre cela ici. Pour James il est crucial à la
compréhension de situations comme celle de Pierre que nous
reconnaissions trois au moins de leurs caractéristiques: que le choix
de Pierre est « contraint », c'est-à-dire, que ce sont les seules
options qui s'offrent à lui de façon réaliste; que le choix est
« vital » --- il compte profondément pour lui; et qu'il n'est pas
possible pour Pierre de décider de ce qu'il doit faire sur des bases
intellectuelles. Dans une telle situation --- et _seulement_ dans une
telle situation --- James pense que Pierre a le droit de croire et
d'agir « au devant de la preuve ». La tempête de controverse autour de
« La Volonté de Croire » fut largement le fait de ce que James pensait
que la décision de croire ou non en Dieu est une décision de cette
sorte. Parce que les passions religieuses (et, même plus,
anti-religieuses) sont en jeu, la plupart des critiques ne font même
pas note de ce que l'argument de « La Volonté de Croire » est appliqué
par James et est _censé_ s'appliquer à des décisions existentielles du
type de celle de Pierre (c'est très clair d'après l'essai lui-même,
mais aussi d'après de nombreux autres essais dans lesquels James
offrent des arguments similaires). Il n'est pas non plus noté qu'il
est censé s'appliquer aux choix individuels d'une philosophie,
y compris le pragmatisme lui-même.

James croyait, tout comme Wittgenstein, que la croyance religieuse
n'est ni rationnelle ni irrationnelle mais _a-rationnelle._ Il se
peut, bien sûr, que ça ne soit pas une option de vie pour vous, soit
parce que vous êtes un fervent athéiste soit un fervent croyant. Mais
s'il s'agit d'un choix de vie pour vous, alors vous pourriez être dans
une situation complètement analogue à celle que Sartre imagine (du
moins James le croit). Le besoin de croire « au devant de la preuve »
n'est pas restreint aux décisions religieuses ou existentielles pour
James. Il joue un rôle essentiel dans la science elle-même. Quoique
cette assertion ne soit guère controversée de nos jours, ce fut, selon
le témoignage d'une personne présente, ce qui causa le plus de
controverse lorsque la leçon « La Volonté de Croire » fut repétée
devant les étudiants de l'Université de Harvard. Un très bon exemple
de la volonté de croire en science m'a été rapporté récemment par
Gerald Holton: Max Planck s'est converti très tôt à la théorie de la
relativité (spéciale) de Einstein, et a joué un rôle absolument
crucial pour attirer sur la théorie l'attention de l'élite des
physiciens. Holton m'a dit que les physiciens à Berlin recontrèrent
Planck à une occasion et l'acculèrent au pied du mur par leurs
demandes qu'il fournissent une raison expérimentale à sa préférence
pour la théorie d'Einstein contre celle de Poincaré. Et Planck ne pu
leur fournir. Il leur répondit plutôt, « Es ist mir eigentlich mehr
sympathisch » (Elle m'est simplement plus sympathique). Un autre
exemple est la propre croyance passionnelle d'Einstein à sa propre
théorie de la relativité générale. Dans une lettre, Einstein répond
à la question de ce qu'il aurait dit si l'expérience de l'éclipse
s'était avérée prendre le mauvais sens en disant, « Je me serais senti
désolé pour le Seigneur Dieu. »

L'argument de James ne concerne pas seulement l'histoire des sciences,
quoiqu'il ait tout à fait raison sur ce point. Sa prétention --- que,
paradoxalement les positivistes logiques ont contribué à intégrer à la
philosophie des sciences conventionnelle par leur distinction nette
entre le contexte de découverte et le contexte de justification ---
était que la science ne progresserait pas si nous insistions sur le
fait que la scientifique ne croient ni ne défendent de théories que
sur la base de preuves suffisantes. Quand il en ressort de la décision
institutionnelle, la décision par la science organisée académiquement
d'accepter une théorie ou non, alors il est important d'appliquer la
méthode scientifique. Dans le contexte de justification (bien que
James n'utilise pas ce jargon), James était de tout bord avec
l'attention scrupuleuse aux preuves. Mais James reconnaissait, avant
que le positivisme logique n'apparaisse, qu'il y a une autre étape de
la procédure scientifique, l'étape de découverte, et que dans ce
contexte les mêmes contraintes ne peuvent être appliquées.

La situation quant à la religion est, bien sûr, quelque peu
différente. Bien que le physicien ou le biologiste moléculaire invente
une théorie, ou que d'autres partisans qui trouvent une théorie
_sympathisch_, puissent la croire au-devant de la preuve,
l'acceptation finale par la communauté scientifique dépend de la
confirmation publique. Dans le cas de la croyance religieuse
cependant --- _pace_ Alisdair MacIntyre --- il n'y a jamais de
confirmation publique. Peut-être le seul qui puisse « vérifier » qu'il
y ait un Dieu est Dieu lui-même. Le cas de Pierre est encore un
troisième type de cas. Dans ce cas, comme je l'ai déjà remarqué,
Pierre pourrait après-coup sentir qu'il a fait le bon choix (bien
qu'il ne soit guère en mesure de le vérifier), mais il n'y a pas de
garantie que ce soit le cas. James dirait que ce que ces cas ont en
commun est qu'il est précieux, pas seulement du point de vue de
l'individu mais du public aussi, qu'il y ait des individus qui fassent
ce genre de choix.

James pensait que tout être humain a à prendre des décisions au-devant
de la preuve du genre de celle que Pierre doit prendre, même si elles
ne sont pas toutes aussi dramatiques (évidemment, c'était également le
point de Sartre). James arguait encore et encore que nos meilleures
énergies ne peuvent être libérées à moins que nous ne soyions prêts
à faire le genre d'engagement existentiel que cet exemple illustre.
Celui qui n'agit que lorsque les « retombées estimées » sont
favorables ne vit pas une vie humaine riche de sens. Même si je
choisis de faire une chose dont la valeur éthique et sociale ne fasse
aucun doute, mettons, dévouer ma vie au réconfort des mourrants, ou
à l'aide aux malades mentaux, ou au soin des malades, ou au
soulagement de la pauvreté, je dois encore décider non s'il est bon ou
non que quelqu'un fasse ces choses, mais s'il est bon que moi, Hilary
Putnam, le fasse. La réponse à cette question n'est pas un problème de
fait scientifique bien établi, quel que soit le sens même généreux de
« scientifique ».

La note existentialiste est immanquable dans la citation de Fitzjames
Stephen avec laquelle James conclut « la Volonté de Croire »:

> Quid du monde? Quid de nous-mêmes? [...] Ce sont toutes questions
> qui ne se laissent entrevoir qu'à leur fantaisie, énigmes de sphynx
> qui de manière ou d'autre se dressent sur notre chemin. Prenons-nous
> le parti de les laisser sans réponse? c'est un choix.
> Balancerons-nous? c'est encore choisir. Mais à quoi que nous nous
> arrêtions, c'est encore à notre risque et péril. Si un homme choisit
> de tourner le dos à Dieu et à une vie à venir, personne ne peut l'en
> empêcher; qui peut, sans raison de douter, démontrer qu'il est dans
> l'erreur? Si un homme pense autrement et agit en conséquence, je ne
> vois pas comment on peut trouver qu'il se trompe: à chacun d'agir
> comme il juge le mieux de le faire, et s'il a tort, tant pis pour
> lui. Nous sommes dans un passage de montagnes, au milieu du
> brouillard et des tourbillons de neige, cherchant de tous côtés des
> traces de sentiers qui peuvent n'être que des apparences.
> Cessons-nous de marcher, le froid nous saisit et nous tue; nous
> trompons-nous d esentier, nous sommes précipités et mis en pièces.
> Existe-t-il même un chemin? nous ne le savons pas. Que faire? Nous
> devons être forts et courageux, agir en vue du meilleur, espérer en
> conséquence, accepter enfin ce qui nous vient. [...] La vie est-elle
> la fin de tout? il n'y a rien à y faire. Dans le cas contraire,
> faisons notre entrée sur la nouvelle scène, quelle qu'elle puisse
> être, en honnêtes gens, sans sophismes à la bouche et sans masque au
> visage.^[James Fitzjames Stephen, _Liberté, egalité, fraternité,_
> trad. Amédée de Gréban (A. Lacroix et ce, 1876), pp 258-259.]

L'existentialisme de James est d'autant plus remarquable qu'il n'avait
lu aucun existentialiste (sauf Nietzsche, qu'il avait en pitié, et
qu'il a certainement lu sans la moindre sensibilité). Dans le même
temps, James ne manque jamais de voir qu'il incombe une _vérification_
des engagements existentiels. Mes droits à mes propres engagements
existentiels s'arrêtent, pour James, où ils empiètent sur ceux de mon
prochain. En effet, le principe de tolérance (« notre vieille doctrine
du vivre et laisser vivre ») est décrite par James comme ayant eu « un
sens bien plus profond que celui que les gens semblent lui attribuer
désormais ». Dans ses Leçons sur la Croyance Religieuse, Wittgenstein
arguait que la croyance religieuse (dans la mesure où elle ne
dégénère pas en superstition) n'est ni rationnelle ni irrationnelle,
et, en fait, les personnes religieuses que Wittgenstein connaissait
étaient plutôt gentils. Mais comme Kant et Kierkegaard nous le
rappellent, il y a certaines maladies spécifique de l'impulsion
religieuse. Kant parle du fanatisme, de l'idolâtrie, de la
sorcellerie, de la superstition, et Kierkegaard mentionne à répétition
le fanatisme et l'idolâtrie, ajoutant qu'il y a un danger constant que
la personne religieuse vénère une « idole » bien qu'il ou elle dise
toutes les bonnes paroles. Si la raison (ou « l'intelligence ») ne
peux décider de ce que doivent être mes engagements ultimes, elle peut
certainement convenir d'une longue et amère expérience que le
fanatisme est une chose terrible et destructrice. Chez James, la
compréhension emphatique du besoin d'engagement est toujours tempéré
par une saine reconnaissance des horreurs du fanatisme.

Si Dewey n'est pas si sensible aux limites de l'intelligence comme
guide de vie que James, le problème tient peut-être à sa conception
dualistique du bien humain. Pour Dewey il y a fondamentalement deux,
et seulement deux, dimensions dominantes de la vie humaine: la
dimension sociale, qui implique pour Dewey la lutte pour un monde
meilleur; et la dimension esthétique. À la critique de ce qu'il voyait
fondamentalement toute la vie comme une action sociale, Dewey
répondait qu'au contraire, en dernière analyse il voyait toute
« expérience de consommation » comme esthétique. Le problème de cette
réponse est qu'une bifurcation des biens en biens sociaux atteints par
la rationalité instrumentale et l'expérience de consommation
esthétique au final est trop proche de la division positiviste ou
empiriste de la vie en prédiction et contrôle d'expériences et
jouissance des expériences pour être adéquate. James, je crois,
succombe moins que Dewey à la tentation d'offrir une métaphysique des
valeurs.

# Conclusion

Dans ce livre j'ai tenté de décrire de quelle façon la réflexion
philosophique peut et doit aller de l'avant --- ce qu'elle peut et ne
peut pas être. J'ai argué que la décision d'une grane partie de la
philosophie analytique contemporaine de devenir une forme de
métaphysique est une erreur. En effet, la métaphysique analytique
contemporaine n'est de bien des façons qu'une parodie des grandes
métaphysiques du passé. Comme Dewey y insistait, la métaphysique
d'époques révolues avait une connexion vitale à la culture de ces
époques, et c'est pourquoi elle a pu changer les vies des hommes et
des femmes, pas toujours pour le pire. La métaphysique analytique
contemporaine n'a de connexion avec rien d'autre que les
« intuitions » d'une poignée de philosophes. Il lui manque ce que
Wittgenstein appelait du « poids ».

Dans le même temps, j'ai argué du fait que la philosophie ne doit pas
devenir un pseudo-scepticisme (ou nihilisme) qui annonce la découverte
qu'il n'y a ni monde, ni vérité, ni progrès, ni rien. Mes arguments
n'ont pas visé la technicité --- l'analyse argumentée et
rigoureuse --- ni l'engagement dans la littérature. J'accorde
entièrement aux positivistes, par exemple, d'avoir rendu un grand
service à la philosophie en montrant de quelles façons les méthodes de
de la logique mathématique moderne peuvent être utilisée pour la
l'investigation de nombreux débats philosophiques et les conduire
beaucoup plus loin qu'ils ne l'avaient été auparavant; et les
déconstructionnistes, en dépit de leurs fautes, ont attiré l'attention
sur les aspects de la littérature --- en particulier, les aspects de
la littérature philosophique --- que la tradition avait négligé. Mais
la philosophie ne peut être ni para-science ni para-politique. Si j'ai
choisi Wittgenstein comme un exemple d'évitement de ces deux
tentations, c'est à cause de son honnêteté implacable et de sa
compassion bien réelle, ses efforts constants pour comprendre
emphatiquement des formes de vie qu'il ne partageait pas lui-même. Si
j'ai choisi John Dewey comme modèle, c'est parce ses réflexions sur la
démocratie ne dégénèrent jamais en propagande pour le status quo. Il
est vrai que l'optimisme dont fait preuve Dewey quant au potentiel
humain ne s'est pas avérée juste, mais Dewey ne prétend jamais qu'il
le soit. Mais, comme Dewey y insiste emphatiquement, le pessimisme
quant au potentiel humain n'a pas non plus été prouvé juste. Au
contraire, partout où l'on a donné à des groupes auparavant opprimés
la possibilité de montrer leurs capacités, elles nous ont surpris.

Je voudrais conclure en ajoutant un peu plus à cette dimension
critique de la pensée deweyienne. Lorsqu'il parle d'employer la
méthode scientifique pour résoudre des problèmes sociaux, il n'entend
pas se reposer sur des experts. Dewey souligne que, tel qu'il en est,
les experts ne peuvent résoudre les problèmes sociaux. Ils
appartiennent à des classes privilégiées et sont sujets aux
rationalisations que Dewey décrit. Ils sont une élite, et en tant
qu'élite accoutumés à expliquer aux autres ce qu'ils doivent faire de
leurs problèmes sociaux. Mais la solution aux problèmes sociaux,
d'après Dewey, n'est pas que l'on _dise_ aux autres ce qu'ils doivent
faire, mais qu'on libère leurs énergies pour qu'ils soient capables
d'agir pour eux-mêmes. (Un exemple qui vient en tête est celui des
énergies libérées lorsque les travailleurs polonais ont formé
Solidarnosc.) La philosophie sociale de Dewey n'est pas simplement une
redite du libéralisme classique, car, comme il le dit, l'erreur au
cœur du libéralisme classique

> tient à la notion qu'il est octroyé de façon innée aux individus ont
> des droits naturels, des pouvoirs et des désirs de telle sorte qu'il
> suffit aux institutions et aux lois d'éliminer les obstacles
> qu'elles constituent à la « libre » expression des dons naturels des
> individus. L'élimination de ces obstacles a bien eu l'effet
> libérateur escompté sur ceux préalablement en possession des moyens
> intellectuel et économique de telle sorte qu'ils tirèrent avantage
> des conditions sociales changées, mais a laissé tous les autres à la
> merci des nouvelles conditions sociales résultant des pouvoirs
> librement exprimés de ceux avantageusement situés. Qu'il suffise aux
> conditions légales de s'appliquer équitablement à tous --- sans
> tenir compte des différences en éducation, des commandes du capital,
> et du contrôle de l'environnement social exercé par l'institution de
> la propriété --- pour que tout homme soit également libre d'agir est
> une pure absurdité, comme les faits l'ont démontré. Puisque les
> droits et désirs réels, effectifs, sont des produits des
> interactions et ne se trouvent pas de façon constitutive dans la
> nature humaine, qu'elle soit morale ou psychologique, la seule
> élimination des obstacles ne suffit pas. Celle-ci libère simplement
> les forces et les capacités telles qu'elles ont été distribuées par
> les accidents antérieurs de l'histoire. Cette « libre » action est
> désastreuse en ce qui concerne la multitude. La seule conclusion
> possible, intellectuellement et pratiquement, est que
> l'accomplissement de la liberté conçue comme le pouvoir d'agir en
> fonction de ses choix repose sur des changements positifs et
> constructifs du contrat social.

Nous oublions bien souvent que Dewey était un radical. Mais il était
un _démocrate_ radical, non un railleur radical de la « démocratie
bourgeoise ». Pour Dewey la démocratie que nous avons n'a pas à être
méprisée, pas plus qu'il ne faut s'en satisfaire. La démocratie que
nous avons est un emblême de ce qui pourrait être. Ce qui pourrait
être est une société qui développe les capacités de chacun des hommes
et des femmes qu'elle englobe à penser par eux-mêmes, à participer aux
desseins et aux essais des politiques sociales, et d'en juger les
résultats. D'après une telle conception, il serait fondamentalement
erroné de réduire la démocratie à la seule règle de la majorité. Une
majorité n'écoutant pas les opinions qu'elle trouve dérangeantes n'est
pas plus engagée dans la conduite intelligente de l'enquête commune
qu'une élite décidant en lieu et place d'elle; et la conduite
intelligente d'une enquête commune est tout ce qu'est la démocratie,
pour John Dewey. Dans le même sens, le libertarisme civil deweyien
n'est pas simplement accorder la priorité à une chose appelée
« liberté » sur une autre appelée « démocratie »; la liberté civile
est nécessaire _à_ la démocratie.

J'ai dit pourquoi je prenais Wittgenstein en modèle, et pourquoi je
prenais Dewey en modèle. Leurs vertus sont en un sens complémentaires,
mais je crois qu'ils ont en commun ceci qu'à leur meilleur ils
illustrent de la façon dont une réflexion philosophique complètement
honnête peut bouleverser nos préjugés, nos convictions personnelles et
nos failles sans recourir à une éclatante « déconstruction » de la
vérité et du monde lui-même. Si la morale de la déconstruction est que
_tout_ peut être « déconstruit », alors la déconstruction n'a pas de
morale. Lorsque Wittgenstein, tel que je le comprends, déconstruit de
fausses catégories philosophiques, ou quand Dewey nous met au défi de
nous demander jusqu'où nous sommes prêts à aller dans
l'accomplissement de notre foi démocratique, l'effet peut être de
changer nos vies et la façon dont nous les percevons; et ceci est le
rôle de la réflexion philosophique à son meilleur.

---
title: The Gospel of Greed
subtitle: Charles Sanders Peirce et l'évolutionnisme
author: Samuel Barreto
date: 2018-10-08
---

> Cet article correspond à ma traduction du paragraphe correspondant
> dans l'entrée
> [Peirce](https://plato.stanford.edu/entries/peirce/#anti) de la
> Stanford Encyclopedia of Philosophy.

# C.S Peirce - Anti-déterminisme, Tychisme et Évolutionisme

Contre les puissants courants du déterminisme qui dérivaient de la
philosophie des lumières du XVIIIe siècle, Peirce insistait sur le
fait qu'il n'y avait pas le moindre début de preuve scientifique pour
le déterminisme et qu'en fait, il avait de considérables évidences
contre lui. Comme toujours, par les mots de « science » et
« scientifique », Peirce comprenait leur référence aux pratiques
véritables des scientifiques en laboratoire et sur le terrain, plutôt que les chapitres de manuels de sciences.
En attaquant le déterminisme, Peirce en appelait aux évidences de phénomènes véritables de laboratoires et de terrain.
Par là, ce qui est le produit d'observations véritables (des mesures) ne s'ajuste que maladroitement à un point unique ou une fonction simple.
Si l'on prend, par exemple, un millier de mesures d'une quantité physique quelconque, même une simple mesure comme la longueur ou l'épaisseur, quelle que soit l'attention portée à la mesure, nous n'obtiendrons jamais le même résultat mille fois.
Nous obtiendrons plutôt une
distribution (souvent, mais pas toujours et encore moins par
nécessité, quelque chose proche d'une distribution gaussienne) de
centaines de résultats différents. Encore une fois, si l'on mesure la
valeur d'une certaine variable que nous supposons dépendre d'un
paramètre donné, et que nous faisons ou laissons varier le paramètre
alors qu'on prend des mesures successives, le résultat, en général,
ne sera que rarement une fonction lisse, (par exemple une ligne droite
ou une ellipse); elle sera plus souvent une forme dentelée, à
laquelle on peut au mieux ajuster une fonction de lissage en utilisant
une méthode astucieuse (par exemple, en ajustant une droite de
régression par la méthode des moindres carrés). Naïvement, on peut
imaginer que la variation et l'inexactitude relative de nos mesures
devient moins prononcée avec l'utilisation d'outils ou de procédures
plus abouties et raffinées. Mais Peirce était un scientifique en
exercice. Ce qui se passe véritablement, c'est que nos variations
deviennent relativement plus grandes avec la finesse de nos
instrumentations et des procédures délicates. (Évidemment, Peirce
n'aurait pas été le moins du monde surpris par les résultats de
mesures au niveau quantique.)

Ce que les mesures directement mesurées de la pratique scientifique
nous indiquent, c'est que, bien que l'univers montre des niveaux
variés de dispositions, d'habitudes, (c'est-à-dire d'une régularité
statistique partielle, variable et approximative), l'univers ne montre
pas de *lois* déterministiques.[^1] Directement, il ne montre rien qui
soit une régularité totale, exacte et non-statistique. De surcroît,
les dispositions que la nature montrent effectivement apparaissent
toujours avec des degrés variés de retranchement, de congestion. D'un
côté du spectre, il y a le comportement des objets physiques comme les
planètes qui suit quasiment des lois; de l'autre côté du spectre, il
y a, dans l'imagination humaine et la pensée, liberté et spontanéité
quasi-pure; dans le monde quantique du très petit, nous ne voyons les
résultats que de la chance pure.

Il résulte immédiatement des observations scientifiques par la mesure
que rien n'est exactement fixé par des lois exactes (même si tout est
contraint par un certain degré de dispositions et d'habitudes.) Dans
ses premières études de la portée de ce fait, Peirce était de l'avis
que les lois de la nature transparaissent dans le monde mais que certaines
facettes de la réalité sont juste hors de portée des lois. Plus
tard cependant, Peirce en vint à comprendre ce fait comme signifiant
que la réalité dans son entièreté était sans loi, et que la
spontanéité pure a un statut objectif dans le phaneron.[^2]

Peirce appelait cette doctrine du statut objectif de la chance dans
l'univers le « tychisme », un mot pris du Grec pour « chance » ou « ce
que les dieux en viennent à choisir ». Le tychisme est une partie
fondamentale de la doctrine du Peirce mature, et la référence au
tychisme constituait une raison supplémentaire de l'insistance de
Peirce sur l'irréductible faillibilisme de l'enquête. Car la nature
est moins un monde statique de lois inébranlables qu'un monde
dynamique de dispositions en continuelle évolution qui exhibent
directement une spontanéité considérable.
(Peirce aurait accueilli l'indétermisme quantique à bras ouverts.)

Un chemin possible le long duquel la nature évolue et acquiert ses
habitudes a été exploré par Peirce en utilisant des analyses
statistiques de situations d'essais expérimentaux dans lesquels les
probabilités d'occurrence des essais futurs dépendent du résultat
d'essais passés, des situations d'essai non-Bernoullien. Peirce a
montré que, si nous postulons d'une disposition primitive dans la
nature, d'une tendance quelque légère qu'elle soit, alors le résultat au
long cours est souvent d'un haut degré de régularité et d'exactitude
macroscopique. Pour cette raison, Peirce suggérait que la nature était
par le passé beaucoup plus spontanée qu'elle ne l'est devenue, et
qu'en général, et en tant que tout, toutes les dispositions que la
nature expose sont le produit d'une évolution.

Par cette notion évolutive de la nature et de la loi naturelle, nous
avons un nouveau support de l'insistance de Peirce sur le
faillibilisme inhérent à l'enquête scientifique. La nature pourrait
simplement changer, jusque dans ses fondamentaux les plus profonds. En
conséquence, même si les scientifiques, à un moment donné, en venaient
à une conception et des hypothèses sur la nature qui survivent à toute
tentative de les réfuter, ce fait seulement ne suffirait pas à assurer
qu'elles restent pertinentes et précises à un moment ultérieur.

Un développement particulièrement intriguant et curieux de
l'évolutionnisme de Peirce consiste en ce que la vision de Peirce de
l'évolution impliquait ce qu'il appelait l'« agapéisme.[^3] » Peirce parlait
d'amour évolutionnaire. Selon lui, le moteur le plus fondamental du
processus évolutif n'est ni la lutte, ni l'avarice, ni l'avidité ni la
compétition.
C'est bien plutôt l'amour élévateur, par lequel une entité est prête à sacrifier sa propre perfection pour le bien de son prochain.
Cette doctrine avait une portée sociale pour Peirce, qui
avait apparemment l'intention d'arguer contre le moralement répugnant
mais extrêmement populaire Darwinisme socio-économique de la fin du
XIXe siècle. Elle avait également pour Peirce une portée cosmique, que
Peirce associait à la doctrine du Gospel de John et aux idées
mystiques de Swedenborg et Henry James. Dans la quatrième partie de la
série de six papiers de Peirce dans le Popular Science Monthly
intitulées « The Doctrine of Chances », Peirce arguait même qu'être
simplement logique présuppose l'éthique du sacrifice de soi : « Celui
qui ne sacrifierait sa propre âme pour sauver le monde, est, il me
semble, illogique dans toutes ses inférences, collectivement
illogique. » Au Darwisnisme social, et à cette catégorie reliée de
pensées qui constituaient pour Herbert Spencer et d'autres une
justification supposée pour les pratiques rapaces d'un capitalisme
débridé, Peirce leur faisait référence comme au « Gospel of Greed. »

# Réferences

- Burch, Robert, "Charles Sanders Peirce", The Stanford Encyclopedia
  of Philosophy (Fall 2017 Edition), Edward N. Zalta (ed.),
  https://plato.stanford.edu/archives/fall2017/entries/peirce/#anti.

[^1]: Il est difficile ici de traduire le terme d'*habits*, et je crois que Bourdieu s'est trouvé devant la même impasse, la même incapacité du français à traduire cette notion de prédispositions et de dispositions toutes ensembles, cette sorte d'habitude pré-contrainte et non-déterministique, lorsqu'il forgea sa notion d'*habitus*.

[^2]: "Par le phaneron, je veux dire le total collectif de tout ce qui est, d'une manière ou d'une autre, dans un sens ou dans l'autre, présent au mental, quasiment sans tenir compte du fait qu'il corresponde ou non à une chose réelle. Si vous me demandez « présent quand » et « à quel mental », je réponds que je laisse ces questions sans réponses, n'ayant jamais émis de doute sur le fait que ces attributs du phaneron que j'ai trouvé dans mon esprit sont présents de tous temps et dans tous les esprits. Dans la mesure où j'ai développé cette science de la phanéroscopie, elle se préoccupe des élements formels du phaneron."

[^3]: L'Agape est un terme gréco-chrétien désignant l'amour inconditionnel, la forme la plus noble d'amour et de charité, celle de l'amour de Dieu pour l'Homme et de l'Homme pour Dieu.

---
title: Woese et Goldenfeld
subtitle: La vie est physique
author: Samuel Barreto
date: 2018-09-29
---

Ceci est une « traduction » encore une fois assez souple de l'article
de Goldenfeld et Woese paru en 2011 --- l'un des derniers articles de
Carl Woese.[^1] L'objet de l'article est de montrer que le phénomène
vital ne peut être compris que comme émergent de phénomènes complexes
et collectifs. Destiné à des physiciens --- l'article paraît dans
Annual Review of Condensed Matter Physics ---, ils arguent que, tout
comme les propriétés de la matière à l'état condensé ne sont pas
réductibles aux propriétés des atomes qui la compose, le phénomène
vital ne peut être réduit aux propriétés spécifiques de la matière sur
laquelle il opère. Celle-ci acquiert des propriétés émergentes
à l'état collectif.

(Le post ci-dessous est un mélange de résumé, de points de vue
personnels et de traduction directe de l'article.)

Ils en viennent à prendre une métaphore dans le champ de la physique
pour expliquer où en est la pensée réductionniste en biologie. Ils
expliquent qu'on pourrait croire au premier abord que toute
supraconductivité est explicable par l'activité chimique, mais
Einstein d'abord, Feynman ensuite ont montré que la supraconductivité
ne peut s'expliquer qu'en termes de théorie quantique. « On voit ici
que le réductionnisme est une étape intuitive et naturelle dans la
construction d'une théorie, et que ses modes d'échec, ses faiblesses
pointent vers les ingrédients nécessaires à leur subsumation. »

« Nous voyons un phénomène comme compris essentiellement lorsque deux
conditions sont réunies. *Primo*, les raisons pour l'existence
même d'un phénomène sont connues, usuellement à partir d'une forme de
symétrie ou de considérations topologiques. *Secundo*, nous disposons
d'une façon de déterminer dans quelles circonstances un système
particulier (atomique, moléculaire, nucléaire, etc…) représente une
instantiation, une réalisation du phénomène. »

L'idée est donc assez claire qu'il est tout aussi limitant de
réduire la vie à ses déterminants chimiques qu'il l'est de voir l'état
condensé de la matière comme ayant trait, par principes, aux atomes.

De prime abord, la génétique des populations traite de paramètres
phénoménologiques, tels que la taille efficace, la fitness, le taux de
croissance, et les essais de modélisations d'aspects génériques des
populations et de leurs gènes. Ça n'est en rien une théorie
microscopique, en ce qu'il lui manque un niveau de description
biochimique; mais c'est certainement une théorie efficace, qui
présuppose une séparation d'échelle entre la dynamique des écosystèmes
et la dynamique de la mutation génique. [&#x2026;] Nous pressentons que la
biologie évolutive peut être libérée de l'hégémonie de la génétique
des populations classique en accréditant l'idée que d'autres classes
d'universalité peuvent exister et se manifester dans les conditions
appropriées.

Cette perspective redéfinit ce que l'on attend de la compréhension
biologique de deux façons différentes. Premièrement, l'existence même
du phénomène de vie doit être compris. Deuxièmement, son instantiation
sur Terre, par exemple, doit être comprise. On peut largement dire que
la discipline biologique a négligé la première partie de la question,
et, en tentant de comprendre la seconde, a confondu la compréhension
de la réalisation avec la compréhension du phénomène. [&#x2026;] Bref, une
vision unifiée restreint l'inutile multiplication d'hypothèses ad hoc,
signe d'un manque de compréhension fondamentale (pensons aux
épicycles !).

La seconde conséquence d'un manque de compréhension fondamentale est
l'incapacité à reconnaître que la biologie est une manifestation de
l'évolution plutôt que l'inverse.

[&#x2026;] La majorité des biologistes voient probablement leur rôle
primaire comme celui de déduire la myriade de réalisations spécifiques
de la vie organique sur Terre &#x2014; l'exercice réductionniste qui est
notoirement accompli du point de vue de ses propres mesures
d'évaluation. Cependant, l'existence du phénomène vital, s'il peut
être compris en termes génériques, est sûrement un phénomène émergent,
provenant d'une façon ou d'une autre d'inévitables conséquences de
lois de physique statistique néguentropiques. Comment se fait-il que
la matière s'auto-organise en hiérarchies capables de générer des
boucles de rétro-actions connectées à de multiples niveaux
d'organisation et capable d'évolutions ?

En 1949, Max Delbruck exprimait le sentiment que la biologie pourrait
déployer des phénomènes au delà de la compréhension en termes de
mécanique quantique :

> Tout comme nous découvrons des attributs de l'atome, sa stabilité par
> exemple, qui ne sont pas réductibles à la mécanique, nous pourrions
> trouver des attributs des cellules vivantes qui ne sont pas
> réductibles à la physique atomique, mais dont l'apparance se tient
> dans une relation complémentaire avec la physique atomique.

Ils (ou plutôt il, parce que dans cette partie on se doute que c'est
plutôt Woese que Goldenfeld qui parle…) mentionnent plus bas (p. 7)
que les topoisomérases ont été d'abord proposées théoriquement, et
découvertes ensuite, un peu à la façon de Dirac et de la masse
négative.

(Je traduit ici l'ensemble du paragraphe sur la synthèse
néo-darwinienne et ses défauts. C'est l'un des paragraphes les plus
concis et les plus synthétique que j'ai pu trouver sur ce point.)

Le cadre conceptuel classique et largement accepté de la « Synthèse
Moderne » ou néo-darwinisme qui est basée sur la fusion de la
génétique avec les idées de Wallace et Darwin sur la « sélection
naturelle » (ou « survie du plus apte » d'après la terminologie
préférée par Wallace). Cette théorie et ses extensions, principalement
celles dues à Kimura, rendent compte de processus génétiques simples,
tels que la mutation ponctuelle et la recombinaison sexuelle,
conduisant à des polymorphismes simples et aléatoires. Une
caractéristique de ces théories classiques de l'évolution consiste en
ce que leur dynamique des génomes est linéaire, par essence diffusive,
que les tailles de populations des communautés sont typiquement
suffisamment larges pour que le temps de fixation soit long. La
conjonction de l'évolution et de la génétique opérée dans les années
1930-1940 présuppose que l'évolution procède par le mécanisme simple
de mutations héréditaires et de survie du plus apte. Les organismes
ont une descendance qui survie et se propage selon la qualité du
génome irrémédiablement muté dont ils héritent de leurs parents
(transfert vertical de gènes). Des traits nouveaux positifs
envahissent la population parce que les individus qui les portent sont
plus aptes à survivre et à transmettre que d'autres membres de la
population. La diversité des niches physiques possibles dans
l'environnement permettent la multiplication des espèces, qui ensuite
interagissent et construisent de nouvelles niches. Ainsi, dans cette
vision, l'évolution est essentiellement synonyme de génétique des
populations. Les gènes sont les seules supposées varibles dynamiques
qu'on peut tracer et associer à un gain de fitness qu'il est difficile
de définir ou de mesurer précisément mais qui est quantifié par un
paysage de fitness qui décrit comment la fitness de la population
dépend du génotype. Les traits sont simplement associés aux gènes, les
interactions entre gènes sont souvent ignorées, ou au mieux traitées
par l'approche du paysage de fitness.

Un présupposé tacite de cette approche en est que la temporalité de
l'évolution diffère de celle de l'écosystème. La question cruciale de
l'échelle temporelle des processus évolutifs, même en acceptant
d'emblée la perspective néo-darwinienne, reste épineuse; on peut dire
que le cadre conceptuel de cette théorie est tellement mal quantifié
qu'il est pratiquement impossible d'estimer précisément,
réalistiquement et quantitativement les échelles de temps.

Les échelles de temps évolutives et écologiques peuvent être couplées
lorsque la temporalité écologique devient très longue: Un exemple
important en est fourni par l'appareil cellulaire de traduction. Le
code génétique est [redondant.]

Le couplage entre la temporalité écologique et évolutive a également
été proposé comme conduisant à un autre attribut générique de la
biologie: la prévalence de la modularité. La modularité désigne
l'indépendance relative d'un composant biologique ou d'un réseau
&#x2014; relative en ce que les connexions intra-modules sont plus
importantes que les les connexions inter-modules. [Dans la plupart des
simulations d'évolution in-silico, les modèles présupposent qu'il
existe une forme de « sélection naturelle ».] Les réseaux évoluent par
mutations, recombinaisons et d'autres opérateurs génériques, mais
seuls ceux qui accomplissent une tâche définie sont « autorisés » à
entrer dans la nouvelle génération. [Ils citent ensuite deux études
qui ont couplé les temporalités évolutives et écologiques.] Ces
résultats, bien qu'obtenus dans un modèle bien spécifique, soulignent
néanmoins l'importance des interactions collectives et l'entre-jeu
complexe entre les fluctuations environnementales et évolutives
négligé par la Synthèse Moderne.

Un autre exemple de couplage entre l'évolution et l'écologie est
fourni par l'enquête métagénomique des environnements marins. [Cette
enquête décrit des populations des cyanobactéries *Prochlorococcus* et
*Synechococcus*.] Les phages de ces organismes contiennent également
des photosystèmes II, dont on suppose qu'ils maintiennent l'hôte dans
un état fonctionnel de phage-factory, augmentant par là la production
de phages pendant le processus lytique au cours duquel la cellule hôte
est détruite. Le groupe de Chisholm a documenté la série de
rétro-transfert des gènes du photosystème II entre les phages et leurs
hôtes. Les gènes ont évolué et la séquence brouillée, brassée
lorsqu'ils résidaient dans les phages. Ainsi, plutôt que d'accréditer
la vision traditionnelle d'une relation de prédation entre les phages
et les micro-organismes, ces découvertes suggèrent qu'il existe des
interactions collectives entre eux par échange de gènes, avec la
création d'un réservoir efficace global de diversité génétique qui
influence profondément la dynamique de ces écosystèmes marins majeurs.

[Le paragraphe suivant est un exemple d'un cycle quasi-lysogénique
d'un virus chez les eucaryotes, celui du virus de paralysie aiguë chez
l'abeille.]

L'évolution et l'écologie ne sont pas seulement couplés dans le temps
mais également dans l'espace. C'est Wallace qui le premier l'a
souligné à propos de la spéciation. [&#x2026;] Il faut souligner que le
transfert de gène horizontal subit également l'influence forte de la
structure spatiale. Par exemple, il a été établi récemment que la
fréquence de conjugaison entre bactéries dépend de la densité locale,
proche de une par génération en biofilm serrés, et un ordre de
grandeur plus petit en cultures planctoniques.[^2]

La plupart des approches de formalisation mathématique de la dynamique
évolutive partagent l'inconvénient de limiter l'évolution dans un
espace fixe. [&#x2026;] De telles approchent de l'évolution manquent ce qui
est selon nous un aspect central de l'évolution : C'est un processus
qui s'étend continuellement dans l'espace où il opère par une
dynamique essentiellement auto-référente. L'auto-référence devrait
être une part intégrante d'une compréhension de l'évolution. [Suivent
quelques phrases d'analogie avec la physique des états denses.] Les
règles qui sous-tendent l'évolution temporelle du système sont
encodées en abstractions, la plus évidente en étant le génome
lui-même. Quand le système évolue dans le temps, le génome lui-même
peut-être altéré; les règles du jeu sont elles-mêmes changées. D'un
point de vue informatique, on pourrait dire que le monde physique peut
être pensé comme modelé par deux composants distincts, le programme et
les données. Mais dans le monde biologique, le programme *est* la
donnée, et *vice-versa*. Par exemple, le génome encode l'information
qui gouverne la réponse de l'organisme à son environnement physique ou
biologique. Mais en même temps, l'environnement donne forme au génome,
par des processus de transferts de gènes ou de sélection phénotypique.
On est donc face à une situation où la dynamique doit être
auto-référente: Les règles du jeu changent au cours de l'évolution du
système, et la façon dont elles changent est fonction de l'état, donc
de l'histoire du système. [Comment est-il possible alors que les
systèmes biologiques s'affranchissent à ce point des règles ayant
cours dans les systèmes physiques ?]

La réponse simple semble être que l'auto-référence surgit de
l'émergence caractérisant les composants biologiques d'intérêt, et
nous cherchons à décrire les phénomènes biologiques uniquement par ces
mêmes composants. Finalement, si nous n'utilisions qu'un niveau de
description purement atomistique par exemple, ces attributs
auto-référentiels n'apparaîtraient pas. [Il y a là une analogie à
faire avec l'étude des états condensés de la matière.]

[Suivent des paragraphes ayant trait aux états condensés de matière et
à leurs propriétés propres, que la matière n'acquiert que dans cet
état et qu'on ne peut déduire de la seule sérialité des parties.]

Que l'évolution soit un processus qui transcende sa réalisation
signifie qu'il est capable d'agir depuis ses mécanismes propres. Cette
non-linéarité du processus évolutif est parfois appelé *évolvabilité*.
Elle a d'importantes ramifications génériques [&#x2026;], celle notamment
d'être sélectionnée préférentiellement durant une période
d'augmentation du taux de changements environnementaux.

[&#x2026;]

Bien qu'il soit délicat de définir la complexité de façons utiles et
précises, nous envisageons la complexité comme caractérisée par sa
rupture de la causalité. De façon simplifiée, les systèmes complexes
sont ceux pour lesquels les effets observés n'ont pas de causes
définissables uniquement, du fait de la nature diverse de l'espace de
phase et la multiplicité des chemins.

Les écosystèmes ne sont jamais statiques mais en changements
perpétuels, et les réponses impliquent tous les niveaux jusqu'au
génome, voir même plus bas (les virus prennent part intégrante à
l'écosystème).

En 1971, Woese spéculait sur l'émergence de l'organisation génétique.
[En bref, il avait proposé, pour expliquer la structure quaternaire
des protéines, que l'évolution fonctionne par cycles dans lesquels les
produits des gènes évolue jusqu'à un état dimérisé, suivi d'une
duplication de gène. À partir de là, les deux copies du gène codent
pour les deux sous-unités du dimère &#x2014; il appelait ça un
co-dimère &#x2014;, évoluent séparément mais de façon complémentaire, dès
lors que la fonction du co-dimère en tant qu'unité n'est pas
fondamentalement modifiée. La conséquence théorique de ce postulat
permettait d'expliquer le nombre important de protéines à l'état
dimérique ou constitué d'un nombre pair de sous-unités. Les données
accumulées depuis sont venues confirmer cette hypothèse. De plus, il
apparaît désormais que l'ontogénie des protéines résume leur phylogénie,
l'assemblage des protéines suit le développement évolutif de leur
structure en sous-unités.]

[Suit un paragraphe de proposition d'un programme de recherche
permettant d'échapper aux limitations inhérentes aux simulations
informatiques. Elles ne sont pas capables de modéliser un système
évoluant sous contraintes elles-mêmes évoluants sous d'autres
contraintes qui peuvent émerger du système qu'elles contraignent en
première instance. Il faudrait une sorte de hiérarchie à l'infinie de
contraintes. Ils proposent alors une approche théorique qui ressemble
d'assez près à celle des réseaux multi-couches utilisée désormais en
écologie.[^3]

[Plus bas, ils mentionnent les applications de la théorie des jeux à
la déterminaison d'états stables d'un système, en prenant l'exemple du
[dilemme du prisonnier](https://fr.wikipedia.org/wiki/Dilemme_du_prisonnier). L'un des avantages de ces approches est
qu'elles soulignent que les propriétés d'un individu dépendent du
système collectif dans lequel il est pris.]

L'inconvénient de ces approches est qu'elles fonctionnent également
sur un jeu de règles fixé. La matrice de coût/rétribution est donnée a
priori et n'évolue pas avec le système lui-même.

# Conclusion

Je crois qu'on a là un article des plus convaincants quant au problème
du réductionnisme en biologie. Les travaux récents et anciens de Kalin
Vetsigian, l'un des étudiants de Goldenfeld et Woese, qui s'est attelé
à la modélisation des propriétés d'écosystèmes en couplant la
dynamique évolutive et temporelle, tendent à confirmer la richesse du
programme de recherche que Woese d'abord, Goldenfeld à sa suite,
proposaient à la biologie.[^4]

[^1]: Goldenfeld N, Woese C. 2011. Life is Physics: Evolution as a Collective Phenomenon Far From Equilibrium. Annual Review of Condensed Matter Physics 2:375–399.

[^2]: (On peut le mettre en rapport avec le papier de Cowley *et al*, dans lequel ils montrent que les conditions dans lesquelles sont placés les *Streptococcus* influent sur la qualité et la quantité de transfert de gènes.) 2. Cowley LA, Petersen FC, Junges R, Jimenez MJD, Morrison DA, Hanage WP. 2018. Evolution via recombination: Cell-to-cell contact facilitates larger recombination events in Streptococcus pneumoniae. PLOS Genetics 14:e1007410.

[^3]: Pilosof S, Porter MA, Pascual M, Kéfi S. 2017. The multilayer nature of ecological networks. Nature Ecology & Evolution 1:0101.

[^4]: Kotil SE, Vetsigian K. 2018. Emergence of evolutionarily stable communities through eco-evolutionary tunnelling. Nature Ecology & Evolution 2:1644–1653.

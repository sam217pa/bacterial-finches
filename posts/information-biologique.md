---
title: Le concept d'information en biologie
author: Samuel Barreto
date: 2018-09-13
---

> Ceci est une tradition relativement libre de l'entrée [Biological
> Information](https://plato.stanford.edu/archives/sum2016/entries/information-biological/)
> de la Stanford Encyclopedia of Philosophy modifiée en 2016 par Peter
> Godfrey-Smith et Kim Sterelny.
>
> Il manque ici des paragraphes entiers; je n'y ai pris que ce qui
> m'intéressait. (Je ne suis pas certain d'avoir le droit légal de
> traduire ça ici par ailleurs.)

Le concept d'information a acquis un rôle dans de nombreux secteurs de
la biologie. Les hormones et les produits cellulaires par lesquels de
nombreux systèmes physiologiques sont régulés sont généralement
décrits comme des *signaux.* La description des gènes et des processus
métaboliques qui dépendent d'eux impliquent des termes comme
"transcription", "traduction", "édition", "expression". Le destin des
cellules d'un organisme en développement est expliqué en terme de
traitements "d'information positionnelle" envoyé par les cellules
voisines ou d'autres facteurs. John Maynard Smith suggère que
l'évolution procède par paliers, par transitions majeures d'un moyen
de transmission de l'information à un autre plus efficace.

Il est maintenant couramment admis que cette façon de parler est une
façon légitime, utile et pertinente en biologie. Dans ce sens, tout
est *source* d'information, tous les composés sont porteurs d'une
information en eux.

## Introduction

Pour beaucoup de biologistes les processus caractéristiques du vivant
les plus basiques doivent être interprétés en terme d'expression
d'information : la réponse aux signaux, l'exécution de programmes et
l'interprétation de codes. Donc bien que la biologie moderne soit un
champ ouvertement matérialiste, elle en est venue à employer des
concepts qui semblent intentionnels ou sémantiques; avec tout la
longue histoire que ces visions ont pour les fondements matérialistes
d'une science.[^1]

[^1]: D'ailleurs Dagognet et Canguilhem expliquent ça très bien dans cette vidéo de la BnF sur le vivant : https://gallica.bnf.fr/ark:/12148/bpt6k1320623h/f1


L'adoption de ces concepts sémantiques et informationnels est
particulièrement marquée en génétique et dans d'autres champs
fortement liés à la génétique — l'évolution ou la biologie du
développement. Pour la plupart des biologistes, le rôle causal des
gènes doit être compris en terme de l'*information qu'ils portent* à
propos de leurs produits; peut-être également à propos des
environnements dans lesquels ces produits maximisent la fitness.

L'un des plus important usage du langage informationnel est
relativement non controversé, parce qu'il est ancrée dans des faits
bien établis sur les rôles de l'ADN et de l'ARN dans la construction
des protéines dans les cellules, un ensemble de faits résumés par le
tableau bien connu du « code génétique », qui attribue un acide aminé à
un triplet de base d'ADN. Les applications actuelles des concepts
informationnels en biologie incluent :

-   la description de traits phénotypiques d'un organisme entier (y
    compris les traits comportementaux complexes) comme spécifiés,
    déterminés ou codés par l'information contenue dans les gènes.
-   le traitement des processus causaux dans les cellules, peut-être
    également dans la séquence du développement de l'organisme, en
    termes d'exécution d'un *programme* stocké dans les gènes.
-   le traitement de la transmission des gènes (parfois d'autres
    structures) comme un flux d'information des parents aux descendants.
-   l'idée que les gènes eux-mêmes, afin de théorisation évolutive,
    devraient être vus, en un sens, comme "faits" d'information.
    L'information devient un ingrédient fondamental dans le monde
    biologique.
-   La caractérisation, d'une manière générale, de la dynamique de
    populations idéalisées comme changeant sous l'influence de la
    sélection naturelle.




## L'information selon Shannon

(C'est le même Shannon que celui dont l'indice de diversité porte le
nom.)

On commence souvent par décrire l'analyse de la notion d'information
en biologie par la notion de conception causale ou corrélationnelle.
La fumée d'un feu peut nous servir à comprendre au loin qu'il y a un
feu quelque part; ça n'implique pas que la corrélation soit parfaite.
(Il peut y avoir de la fumée pour un grand nombre de raisons, la
principale étant souvent qu'il y a effectivement un feu.) Un signal
porte une information à propos d'une source si l'on peut prédire
l'état de la source à partir du signal. C'est cette notion qui est
théorisée par Claude Shannon. Pour lui, toute chose est *source*
d'information si elle a un nombre d'états alternatifs qui peuvent être
réalisés en une occasion particulière. Toute variable contient une
*information* à propos de cette source, *porte* une information à
propos d'elle, si son état est corrélé à celui de l'état de la source.
C'est un problème de degré; un signal contient plus d'information à
propos d'une source si son état est un meilleur prédicteur de celui de
la source.

Dans ce sens, on voit bien que la notion d'information n'est pas liée
à des contraintes matérielles, elle est purement abstraite. Un signal
peut porter de l'information à propos d'une source sans qu'il n'y ait
un système biologique pour produire ce signal, ni qu'il y ait un
usager de ce signal. (Je pense que ces notions expliquent bien les
conceptions signifiant / asignifiant d'André Pichot.) Quand un
biologiste emploie ce sens de l'information dans sa description de
l'action d'un gène ou d'un autre processus, il adopte un cadre
quantitatif pour décrire les connexions causales ou les corrélations à
propos d'un système. C'est donc une chose de porter une information à
propos d'une source; c'en est une autre d'expliquer les processus
biologiques dans le sens de signalement. L'exemple souvent utilisé
dans ce cas est celui des cernes d'un arbre. Quand il établit ses
cernes, l'arbre établit une structure porteuse d'information &#x2014; au
sens de Shannon &#x2014; sur son histoire. Mais en dépit de l'utilité de
l'information pour le dendrochronologiste, on ne peut pas expliquer
comment l'arbre fait ce qu'il fait en termes d'information. L'arbre en
lui-même n'exploite pas l'information de ses cernes pour contrôler sa
croissance ou sa floraison. De façon similaire, on peut noter que la
façon dont la distribution d'ADN différents entre et au sein de
populations biologiques "porte de l'information sur" les relations
historiques entre ces populations, et les histoires des populations
individuelles elles-mêmes. C'est l'utilisation de ces informations qui
a permis aux biologistes de décrire une histoire du vivant plus
fiable. (Ils citent Bromham 2008.) Par expemple, la plus grande
diversité de l'ADN mitochondriale dans les populations afrécaines, en
comparaison avec d'autres populations, est un indicateur d'une
dispersion relativement récente depuis les populations africaines
d'origine. En réalisant ces inférences, les concepts informationnels
peuvent être utiles. Mais ça n'est qu'une version compliquée des
cernes d'arbres ; le recours à l'information a un but *inférentiel*
qui n'est en rien *explicatif*. Une grande proportion des descriptions
informationnelles en biologie ont ce caractère.

[&#x2026;]

Par conséquent, les philosophes ont parfois élevé la discussion en
disant qu'il existe un recours à l'information en biologie, celui
décrit originellement par Shannon, qui n'est pas problématique et qui
ne requiert pas beaucoup d'attention philosophique. Ce concept est
parfois désigné sous le terme d'information "causale", qui correspond
aux travaux de Grice sur le "sens naturel". L'information dans ce sens
existe dès lors qu'il y a contingence et corrélation. Donc on peut
dire que des gènes contiennent des informations à propos des protéines
qu'ils produisent, et également que les gènes contiennent des
informations à propos du phénotype de l'organisme entier. Lorsqu'on
dit ça, on ne dit rien de plus que lorsque l'on disait qu'il existe
une connexion informationnelle entre la fumée et le feu, entre les
cernes et l'âge d'un arbre. Les questions plus contestées ont trait à
besoin de la biologie d'un *autre* concept d'information, plus riche
également. L'information dans cette acception est parfois appelée
« sémantique » ou « intentionnelle ».

Pourquoi penser que la biologie pourrait avoir besoin d'un concept
plus riche ? L'une des pensées est le fait que les gènes jouent un
rôle spécial, instructionnel dans le développement, expliquent à
l'embryon comment croître. Il est vrai que les gènes portent des
informations au sens de Shannon à propos du phénotype: le génome d'un
œuf fertilisé prédit beaucoup du phénotype résultant. Chez les
mammifères par exemple, la structure chromosomique prédit le sexe de
l'adulte animal. Mais si une relation informationnelle entre gène et
phénotype est sensée impliquer un mode de causation distinct,
« instructionnel », alors ça ne peut être une information au sens de
Shannon. Les facteurs environnementaux, pas seulement les gènes,
« portent de l'information » sur le phénotype, au sens de Shannon.
Dans son acception, il y a « parité » entre le rôle de l'environnement
et les causes génétiques. Les relations informationnelles entre
génotype et phénotype sont symétriques dans ce sens. Par exemple,
lorsqu'un lecteur sait que les deux auteurs sont des hommes, il peut
prédire que nous portons tous deux un chromosome Y. Certains discours
à propos de l'information en biologie est en accord avec cette version
de l'information de Shannon, d'autres non. En particulier, il est
souvent admis qu'au moins certaines des applications du language
informationnel aux gènes leur attribue des propriétés qui ne sont pas
attribuables aux conditions environnementales, même lorsque
l'environnement intervient dans la prédictibilité.

De plus, un message qui porte une "information sémantique" a la
capacité de représenter précisément ou de distordre ce qu'il véhicule.
Il y a possibilité d'erreur. L'information de Shannon n'a pas cet
attribut; on ne peut pas dire qu'une variable porte de fausses
informations à propos d'une source dans le sens de Shannon. Mais les
biologistes veulent de toute évidence utiliser ce language pour
décrire les gènes. Les gènes portent un message qui est *sensé* être
exprimé, qu'il le soit ou non.

Ce sont généralement les signes qu'on trouvent dans la littérature qui
montrent qu'un sens plus riche que celui de l'information de Shannon a
été introduit en biologie. Mais la difference cruciale entre les
applications plus ou moins contestées des concepts informationnels
tient à ce que, dans le cas plus riche, l'utilisation de l'information
est censée aider à expliquer comment les systèmes biologiques font ce
qu'il font, comment les cellules fonctionnent, commen un œuf devient
un adulte, comment les mécanismes de l'hérédité génétique permettent
l'évolution de phénotypes complexes.

À ce moment, plusieurs choix s'offrent à nous. L'un consiste à nier
que les gènes, les cellules et les structures biologiques traffiquent
littéralement de l'information de façons telles qu'elles permettent
d'expliquer leur comportement, mais à arguer qu'il néanmoins qu'il
s'agit d'une analogie ou d'un modèle utile. L'idée est qu'il existe
des similarités utiles entre les paradigmes des systèmes d'information
et de représentation — des agents cognitifs sophistiqués, pensants et
communicants les uns avec les autres — et les systèmes biologiques.
Les hormones, par exemple, sont souvent pensées comme des messages
puisqu'elles sont petites, stables, énergétiquement peu coûteuses et
peuvent traverser de longues distances (relativement à leur taille)
sans se dégrader, jusqu'à arriver à des localisations spécifiques dans
lesquelles elles ont des comportements prédictibles et des effets
importants. Mais s'il s'agit d'une façon de penser utile, peut-être
que l'on ne devrait pas prendre ce discours de "message" trop
sérieusement. Par exemple, nous ne devrions pas traiter d'une question
telle que celle de la prolactine en tant que *rapport* de grossesse ou
d'*instruction* des glandes mammaires comme si elle avait une réponse
correcte. Arnon Levy a développé la version a plus sophistiquée de
cette vision de l'information biologique.

Une deuxième option consiste à défendre que les gènes et les
structures biologiques portent *littéralement* des informations
sémantiques, et que leur caractère informationnel explique leur rôle
distinctif dans les processus biologiques. Si nous pensons les gènes
ou les cellules de cette façon, le problème devient : qui ou qu'est-ce
qui compte comme producteur ou récepteur de ces messages ? Les cas
paradigmatiques de structures porteuses d'informations sémantiques
— les images, les phrases, les programmes — sont construits par les
pensées et les actes d'agents intelligents. On doit alors montrer
comment les gènes et les cellules — qui ne sont ni des systèmes
intelligents en propre ni les produits d'une intelligence — peuvent
porter de l'information sémantique, et comment l'information qu'il
porte peut expliquer leur rôle biologique. Il nous faut alors une
forme d'explication réductive des information sémantique. On peut
trouver ce genre d'analyse dans la philosophie naturaliste de
l'esprit.

Une troisième option est de prétendre que les informations causales
par elles-mêmes peuvent expliquer des phénomènes biologiques, sans
qu'un concept additionnel soit nécessaire. Les systèmes biologiques,
dans ce sens, peuvent être adapté à l'émission ou la réception de
signaux qui portent de l'information causale. L'élévation de la
prolactine, par exemple, covarie bien avec la grossesse, et ça n'est
pas un hasard. La production de lait est une réponse à dessein à la
perception de ces niveaux plus élevés dans les glandes mammaires. Même
s'il est vrai que la lactation porte autant d'information causale à
propos des niveaux de prolactine que ceux-ci portent sur le flux
lactatif, il y a une asymétrie physique, donc une directionnalité,
entre la source et le récepteur. Les travaux de Brian Skyrms ont été
très importants dans le replacement des informations causales au
centre de la scène, en partie parce qu'ils montrent que les systèmes
de signalement émetteur-récepteur n'ont pas besoin d'agents
cognitivement sophistiqués. Émettre et recevoir de l'information
causale peut émerger et se stabiliser au sein de systèmes simples; de
façon certaine dans des systèmes pas plus complexes qu'une cellule.




## téléosémantique et autres concepts riches

De nombreux philosophes et biologistes proposent qu'une part
importante du discours informationnel à propos des gènes utilise un
concept plus riche que celui de Shannon, mais que ce concept peut être
analysé de façon naturaliste. Le but étaient de donner sens à l'idée
que les gènes spécifient sémantiquement leurs produits normaux, en un
sens similaire à celui d'autres cas paradigmatiques de phénomènes
symboliques.

Si les gènes sont vus comme « porteurs d'un message » en ce sens, ce
message en apparence a un contenu prescriptif ou impératif, par
opposition à un contenu descriptif ou indicatif. La « direction de
forme » de leurs effets est telle que si les gènes et leurs éventuels
produits — le phénotype — ne correspondent pas, cela correspond à des
instructions non réalisées plutôt qu'à des descriptions imprécises.
Une autre façon de voir les gènes est celle qui consiste à leur faire
« dire » au phénotype en développement l'environnement auquel il doit
s'attendre. Le pool de gènes duquel ceux-ci sont issus a été criblé
par la sélection. Dans les régions arides d'Australie, les gènes
contribuant au développement des feuilles à forme et surface adaptées
aux restrictions d'eau sont devenues communes. On peut alors voir les
gènes comme « disant » aux arbres que les conditions sont arides.

Pour donner sens à ces idées, la façon usuelle de procéder a consisté
à utiliser un concept de *fonction* biologique, selon lequel la
fonction d'une entité dérive de son histoire et de la sélection
naturelle. [&#x2026;] Quand une entité a été soumise et formée (au sens de
mise en forme, *informée*) par une histoire de sélection naturelle,
cela peut fournir la base d'une sorte de description orientée ou
normative de la capacité causale d'une entité. Pour utiliser un
exemple commun, la fonction du cœur est de pomper le sang, pas de
faire des sons retentissants, puisque c'est le premier effet qui a été
favorisé par la sélection. L'espoir est qu'une stratégie
« téléo-fonctionnelle » pourrait aider à donner sens aux propriétés
sémantiques des gènes, et peut-être à d'autres structures biologiques
aux propriétés sémantiques.

[&#x2026;]

L'une des façons de développer ces idées est de se concentrer sur les
fonctions de la machinerie génétique vue comme un *tout*. Carl
Bergstrom et Martin Rosvall utilisent cette approche, soulignant les
structures hautement adaptées des flux inter-générationnels de gènes
et la « transmission du sens de l'information. » Ils montrent que la
machinerie du transfert inter-générationnelle est structurée de façon
à permettre la transmission de séquences arbitraires (donc le message
est relativement non-contraint par le milieu); l'information est
stockée de façon stable et compacte; la « bande passante » est large
et extensible à l'infinie. Les séquences dADN sont répliquées de façon
fiable, avec une fidélité élevée; la transmission est précise,
d'autant plus que la redondance du code semble optimisée pour réduire
l'impact des erreurs qui ont pourtant lieu. En bref, il n'est pas
nécessaire de connaître ce que le signal des générations parentales
« dit » à la génération suivante pour comprendre que les
caractéristiques de la réplication de l'ADN sont expliquées par ses
capacités de porteur de l'information.

La route plus souvent empruntée, celle prise par Sterelny et Maynard
Smith, consiste à se concentrer sur la sélection naturelle d'éléments
génétiques particulaires. À cette vision se pose immédiatement un
problème qui tient en ce que les éléments génétiques spécifiques ont
une fonction évoluée n'est pas suffisante pour que les gènes portent
une information sémantique. Les jambes servent à marcher, elles ne
représentent pas la marche. Les enzymes catalysent des réactions, elle
n'instruisent pas cette activité. Les enzymes et les jambes ont des
fonctions supposées, mais ça ne les rend pas porteuses d'informations
pour autant, au sens d'information étendue. Pourquoi devrait-il en
être le cas pour les gènes ? Sterelny, Smith et Dickison (1996)
suggèrent que les différences entre les gènes and les jambes sont dues
à ce que les gènes ont été sélectionnés pour jouer un rôle causal dans
le développement. Ils ajoutent cependant que n'importe quel élément
non-génétique qui a un rôle développemental similaire et a été
sélectionné pour le jouer doit avoir également des propriétés
sémantiques. Ils veulent donc étendre les propriétés sémantiques à
d'autres entités que les gènes. Certains facteurs non-génétiques ont
alors le même statut. Mais dans ce cas, beaucoup de cas plausibles
perdent leur statut informationnel : la prolactine n'a pas un rôle
spécifique pendant le développement, donc on ne peut pas lui attribuer
un rôle de porteuse d'information. C'est l'une des raisons pour
laquelle Levy pense que ces discours sont essentiellement
métaphoriques. Maynard Smith lui argue que les gènes seuls portent de
l'information sémantique sur les phénotypes. Il suggère que les
relations entre le gène adapté et le phénotype sont arbitraires, la
relation gènes-trait est comme la relation mot-chose. Cette idée
est intriguante mais il est néanmois difficile de lui donner corps.
L'un des problèmes est que toute relation causale peut sembler
« arbitraire » si elle opère par de nombreux liens, puisqu'il existe
une grande variété d'interventions sur ces liens qui pourraient
changer le produit de la chaine causale.

Nicholas Shea distingue la fonction biologique ordinaire de la
fonction de représentation à partir de la théorie sémantique de Ruth
Millikan. Pour Millikan, tout objet aux propriétés sémantiques joue un
rôle impliquant une médiation entre deux « appareils coopérants », un
producteur et un consommateur. Dans le cas d'un signal indicatif, la
représentation est sensée affecter les activités du consommateur d'une
façon qui ne devrait qu'augmenter les performances de ses fonctions
biologiques. Dans le cas d'un signal impératif, la représentation est
sensée affecter les activités du consommateur en le conduisant à
provoquer un état de chose. Shea attribue aux messages génétiques les
deux contenus ; ils dépendent à la fois des consommateurs et des
producteurs. Lorsqu'on pense à l'hérédité et au développement, il
est moins clair qu'il y ait des mécanismes identifiables
indépendamment qui compteraient comme émetteurs et récepteurs,
producteurs et consommateurs.

L'image générale présentée par l'approche téléosémantique a
indéniablement des attributs structurants attirants. Si ce programme
réussit, nous aurions une conception indiscutable de l'information,
via Shannon, qui s'applique à toutes sortes de corrélations physiques.
Cette image peut être développée en identifiant un sous-ensemble de
cas dans lequel ces signaux ont été co-optés ou produits pour diriger
les processus biologiques. De plus, peut-être qu'on peut faire appel
aux propriétés sémantiques riches dans le cas où nous avons exactement
le type d'histoire de sélection naturelle explicant le rôle distinctif
des gènes, dans le développement. Les gènes et certains autres
facteurs non-génétiques auraient ces propriétés ; la plupart des
attributs environnementaux qui ont un rôle causal dans le
développement biologique ne les auraient pas.




## Le code génétique

Peter Godfrey-Smith et Paul Griffiths ont tous deux argué du fait
qu'il y a un cas hautement restreint d'une notion de sémantique en
génétique qui est justifiée. C'est l'idée que les gènes « codent
pour » les séquences d'acides-aminés, en vertu des attributs
particuliers de la transcription et de la traduction. Les gènes
spécifient les séquences d'acides aminés par un processus de matrice
qui implique une correspondance régulière entre deux sorte de
molécules (les acides nucléiques et aminés). Cette correspondance est
combinatoire et en apparence arbitraire. L'argument de Crick (1958)
est que les mécanismes basaux rendent l'expression de gènes en un
processus causal avec d'importantes analogies aux phénomènes
symboliques paradigmatiques. (Suit tout un discours sur la
correspondance parfois indirecte entre l'ADN, l'ARN messager,
l'épissage des introns chez les eucaryotes et la protéine subséquente.)

[&#x2026;]




## systèmes signaux

Il est très intuitif de voir les hormones comme l'insuline, la
testostérone ou l'hormone de croissance comme des sigaux, puisqu'ils
sont produits dans une partie du corps, « voyagent » vers d'autres
parties où ils interagissent avec des récepteurs de façons telles
qu'elles modifient l'activité d'un nombre variés de structures. Il est
courant de décrire les hormones comme des « messages chimiques ». Le
cadre de Skyrms est bien adapté à ces conceptions pour trois raisons.
La première, comme précédemment noté, est que ce cadre montre que le
signal ne requiert pas une intelligence ou une compréhension
intelligente de la signification du signal.

Deuxièmement, les cas les plus simples de modèles de signaux sont les
cas où il y a intérêt commun. L'émetteur et le récepteur sont
avantagés ou lésés par les mêmes effets. [&#x2026;]

Troisièmement, dans beaucoup de ces systèmes biologiques, la structure
abstraite spécifiée par les systèmes de signaux — source
environnementale, émetteur, message, récepteur, réponse —
correspondent assez naturellement à des mécanismes biologiques
concrets. Par exemple, Ron Planer voit le système d'expression de gène
comme l'opération d'un système de signal. [&#x2026;]

Cependant, il est peu clair dans quelle mesure d'autres suggestions
que ces systèmes s'accorde avec ce cadre. Dans le cas de la
transmission d'information inter-générationnelle, qui est l'émetteur,
qui est le récepteur ? Peut-être que dans le cas des organismes
multi-cellulaires, le récepteur existe indépendamment et avant le
message. Car un œuf est une système complexe et hautement structuré,
avant que l'expression de gènes ait lieu dans le noyau fertilisé, et
cette structure joue un rôle important dans l'orientation de
l'expression des gènes.

[&#x2026;]




## Programmes génétiques

Il est courant de parler de « programmes » dans le représentations de
la biologie et en biologie elle-même. Souvent, cette idée n'est qu'une
façon imagée (qui porte peut-être à confusion) d'attirer l'attention
sur le caractère ordonné, contrôlé et hautement structuré du
développement. Dans ses résultats généraux, le développement est
étonnamment stable et prévisible, en dépit de la complexité des
interactions intra- et inter-cellulaires, et en dépit du fait que le
contexte physique dans lequel il se déroule ne peut jamais vraiment
être précisemment contrôlé. Lorsque les biologistes parlent, par
exemple, de « mort cellulaire programmée », ils pourraient tout aussi
bien dire que dans une classe important de cas, la mort cellulaire est
prévisible, organisée et adaptative.

Certains ont essayé de développer des parallèles plus instructifs
entre les systèmes de calculs et le développement biologique. Roger
Sansom a travaillé sur le parallèle entre le développement et les
modèles de computations connexionistes. Cette vision a le mérite de
reconnaître qu'il n'y a pas de contrôle central du développement; les
organismes résultent d'interactions locales entre cellules et au sein
des cellules. Cependant, l'idée prometteuse à propos de ces parallèles
nous semblent être celles qui montrent une analogie en apparence forte
entre les processus au sein des cellules et les opérations bas-niveau
des ordinateurs modernes. Un type essentiel de processus au sein des
cellules est celui des cascades de régulation positives ou négatives
dans les réseaux génétiques. Le produit d'un gène s'accroche et
stimule ou réprime un autre gène, lui-même alors empêché &#x2014; dans le
cas de la répression &#x2014; de synthétiser le répresseur d'un autre gène
&#x2026; Nous avons là une cascade d'événèments qui sont souvent décrits en
termes de relations booléennes entre variables. Un événement peut
survenir uniquement sous la conjugaison ou la disjonction de deux
autres. La répression est une sorte de négation; il peut exister de
doubles ou triples négations. Les réseaux de régulations ont souvent
une structure de ce type suffisamment riche pour qu'on puisse les
envisager comme pris dans une sorte de calcul. Les portes logiques des
puces, celle des réseaux neuronaux et les puces génétiques ont des
ressemblances frappantes.

Même s'il parle de réseaux de signaux plutôt que de programmes, Brett
Calcott a montré que l'information positionnelle dans le développement
de l'embryon de la Drosophile dépend de ce type de structure
booléenne [&#x2026;]. Il montre que le fait de penser le développement en
termes de réseaux de signalisation à la structure booléenne a une
réelle valeur explicative; elle permet d'expliquer comment
l'information positionnelle, par exemple, peut être réutilisé dans
l'évolution. Les tâches des ailes peuvent évoluer très rapidement, car
le réseau qui indique aux cellules où elles se situent sur l'aile
existe déjà, donc l'évolution de la tâche demande simplement un
changement mutationnel qui lie l'information positionnelle à la
production de pigment. Ron Planer pense également que la régulation a
cette structure Booléenne, et qu'on peut en effet représenter chaque
gène comme l'instantiation d'une instruction conditionnelle. La partie
`if` de la condition spécifie les conditions moléculaires qui
« allument » le gène; la partie `then` spécifie la séquence d'acide
aminée constituée par le gène. Ces instructions conditionnelles
peuvent être et sont souvent liées ensemble pour construire un réseau
complexe de régulation. Cependant, Planer s'oppose à la vision de ces
réseaux de signalisation comme un programme d'ordinateur. Les
combinaisons d'instructions n'ont pas d'ordre intrinsèque; on peut
représenter chacun des gènes comme une instruction conditionnelle
spécifique, mais il n'y a rien dans l'ensemble d'instruction en
lui-même qui nous indique où l'instruction commence et où elle
termine.




## Information et évolution

L'information est également devenue un point de discussion générale
sur les processus évolutifs, particulièrement ceux liés aux mécanismes
d'hérédité. Tout un pan de cette discussion interprète mal
l'information et son rôle dans les processus biologiques. En
particulier, CG Williams argue que, par la réflexion sur le rôle des
gènes dans l'évolution, on peut en déduire qu'il existe un « domaine »
informationnel *le long* des domaines physiques de la matière et de
l'énergie. Richard Dawkins défend une vision similaire, arguant que le
chemin à long-terme de l'évolution est fait de changements graduels
d'information héréditaire — une rivière qui « coule dans le temps
plutôt que l'espace. » C'est l'extension de l'idée plus commune selon
laquelle il existe des choses telles que des « gènes informationnels »
qui doivent être vus comme distinct des « gènes matériels » qui sont
fait d'ADN et localisé dans le temps et l'espace. C'est une erreur de
croire qu'il existe deux choses différentes, à la fois une entité
physique &#x2014; une séquence de bases &#x2014; et une entité informationnelle,
un message. Il est vrai que d'un point de vue évolutif, les gènes sont
souvent vus depuis leur séquence nucléotidique plutôt que depuis
l'ensemble de leurs propriétés matérielles. Cette façon de penser est
en essence une abstraction. On écarte certaine propriétés de l'ADN
pour se concentrer sur d'autres. Mais c'est une erreur de traiter
cette abstraction comme un extra-entité aux relations mystérieuses
avec le domaine des choses.

D'autres façons de voir les choses paraissent plus prometteuses. Comme
JM Smith, E. Szathmáry, M. Ridley et R. Dawkins l'ont souligné de
différentes façons, les mécanismes de l'hérédité qui engendrent des
phénomènes évolutifs importants doivent satisfaire plusieurs critères
spécifiques. Maynard Smith et Szathmáry pensent par expmele que les
systèmes d'hérédité doivent être illimités, infinis dans leur capacité
à produire de nouvelles combinaisons, mais ils doivent également
maintenir une haute fidélité de transmission. Ce fait à propos des
relations entre les systèmes d'hérédité et les structures biologiques
est souvent pensé comme révélateur de l'un des problèmes les plus
urgents sur les origines de la vie. Si la reproduction dépend de la
réplication d'un ensemble d'éléments essentiels pour « propulser » la
génération suivante, ces ingrédients doivent être répliqués
fidèlement. Pourtant cette réplication fiable dépend de mécanisme
moléculaire complexes et de machineries intracellulaires elle-même
produit d'un long régime d'évolution adaptative. Comment donc la
reproduction a commencé ?

La vie elle-même dépend de l'évolution de mécanismes qui sous-tendent
la fiabilité du flux d'information qui transite d'une génération à la
suivante. Maynard Smith et Szathmáry pensent que les étapes cruciales
dans les quatre milliards d'années d'évolution — les transitions
« majeures » dans l'évolution — impliquent la création de nouvelles
façons de transmettre l'information aux générations suivantes — des
façons plus fiables, plus précises, plus puissantes de permettre la
re-création de forme à travers des événements de reproduction. La
transition d'un système d'hérédité basé sur l'ADN (probablement depuis
un système basé sur l'ARN) est un exemple central. Mais ils suggèrent
que la transition du style de vie sociale des grands singes à celle
des humains est une autre transition majeure, essentiellement parce
qu'ils voient le language humain comme une percée informationnelle
révolutionnant les possibilités de transmissions de l'apprentissage
culturel entre les générations.

Leur travail est une pierre d'angle de la pensée macro-évolutive — ils
pensent à grande échelle l'histoire de la vie. Mais la dimension
informationnelle de leurs travaux n'a pas été prise en compte en
dehors de Sterelny (2009). Selon lui, la vie multicellulaire dépend
non seulement de la transmission de davantages d'information mais
également au contrôle de cette information pendant le développement,
suggèrant que l'évolution de l'œuf — un environnement contrôlé,
structuré, riche en information — a été capitale pour l'apparition de
vivants complexes.

# References

- Godfrey-Smith, Peter and Sterelny, Kim, "Biological Information",
The Stanford Encyclopedia of Philosophy (Summer 2016 Edition), Edward
N. Zalta (ed.), URL: <https://plato.stanford.edu/archives/sum2016/entries/information-biological/>.

---
title: Vaguement voguer entre vague et détermination
subtitle: "The Essential Peirce, 2: 322-24 --- New Elements"
author: Charles Sanders Peirce
translator: Samuel Barreto
date: 2018-12-13
draft: false
abstract: >
    Cet article est une traduction d'un court extrait de New Elements
    de Charles Sanders Peirce, dans lequel il expose une forme de
    cosmologie inspirée par sa théorie séméiotique du vague et de
    l'indétermination. Il fait l'objet d'un commentaire par Nathan
    Houser, éditeur en chef du Peirce Edition Project, dans l'ouvrage
    collectif _Peirce and biosemiotics: a guess at the riddle of
    life_, Springer 2014.
---

Un symbole est une chose qui a la capacité de se reproduire elle-même, parce qu'elle n'est constituée comme symbole que par l'interprétation.
Cette interprétation implique que le symbole ait le pouvoir de causer un fait réel; et bien que je souhaiterais éviter la métaphysique, lorsqu'une métaphysique fausse envahit les terres de la logique, je suis contraint à dire qu'il n'est rien de plus futile que de tenter de former une conception de l'univers qui néglige le pouvoir qu'ont les représentations de causer des faits réels.
À quoi sert de tenter de former une conception de l'univers sinon de rendre les choses intelligibles?
Mais si cela doit se faire, nous irons à l'échec si nous persistons à tout réduire à une norme qui rende tout ce qui advient, essentiellement et _ipso facto_ inintelligible.
C'est cela, cependant, que nous ferions, si nous n'admettions pas la force des représentations à causer des faits réels.
Si nous devons expliquer l'univers, nous devons supposer qu'il y avait au commencement un état de chose dans lequel il n'y avait rien, ni réactions et ni qualités, ni matière, ni conscience, ni espace et ni temps, mais simplement rien.
Pas catégoriquement rien.
Car ce qui est catégoriquement non _A_ présuppose qu'il y ait un _A_ d'un quelconque mode.
Indétermination totale.
Or un symbole seul est indéterminé.
Partant, Rien, l'indéterminé du commencement absolu, est un symbole.
C'est là la seule façon dont le commencement des choses peut être compris.
Que s'ensuit-il logiquement?
Nous ne devons pas nous contenter de notre sens instinctif de la logique.
Est logique ce qui vient de la nature essentielle d'un symbole.
Aussi il est dans la nature essentielle d'un symbole qu'il détermine un interprètant, qui est lui-même un symbole.
Un symbole, par conséquent, produit une série infinie d'interprétants.
Y a-t-il quelqu'un qui soupçonne tout cela de n'être que pur non-sens?
_Distinguo_.
Il ne peut, il est vrai, y avoir aucune information positive sur ce qui précédait l'entier Univers des êtres; car, pour commencer, il n'y avait rien dont on puisse obtenir une information.
Mais l'univers est intelligible; et par là, il est possible d'en donner une description générale, de lui et de ses origines.
Cette description générale est un symbole; et de la nature d'un symbole, il doit commencer par l'assertion formelle qu'il y avait un rien indéterminé de la nature d'un symbole.
Ce serait faux s'il convoyait une quelconque information.
Mais c'est la manière correcte et logique de commencer une description de l'univers.
En tant que symbole il produisit une série infinie d'interprétants, qui au commencement étaient aussi absolument vagues que lui-même.
Mais les interprétants directs de tout symbole doivent à leur première étape être la _tabula rasa_ d'un interprétant.
Ainsi l'interprétant immédiat de ce Rien vague n'était pas même catégoriquement vague, mais seulement voguant vaguement entre détermination et vague; et son interprétant immédiat voguait vaguement entre voguer vaguement entre détermination et vague et vague déterminé, et détermination, et ainsi de suite, _ad infinitum_.
Mais toute série infinie doit logiquement avoir une limite.

En laissant cette ligne de pensée inachevée pour l'instant en raison du sentiment d'insécurité qu'elle provoque, permettez-moi de noter, premièrement, qu'il est dans la nature d'un symbole de créer une _tabula rasa_ et partant une série infinie de _tabulae rasae_, puisqu'une telle création n'est que représentation, les _tabulae rasae_ étant entièrement indéterminées à l'exception de ce qu'elles sont représentatives.
Ici se tient un effet réel; mais un symbole ne peut être dépourvu du pouvoir de produire un effet réel.
Le symbole se représente lui-même pour être représenté; et cette représentabilité est réelle par son vague total.
Car tout ce qui est représenté doit être rigoureusement démontré.

Car la réalité est compulsive.
Mais cette compulsivité est absolument _hic et nunc_.
Elle est, un temps, et n'est plus.
Qu'il n'y ait plus rien et il n'est absolument rien.
La réalité existe seulement en tant qu'élément de la régularité.
Et la régularité est le symbole.
La réalité, partant, ne peut être regardée que comme limite d'une série infinie de symboles.

Un symbole est essentiellement un but, c'est-à-dire, une représentation qui cherche à se rendre elle-même définie, ou cherche à produire un interprétant plus défini qu'elle-même.
Car toute sa signification consiste en ce qu'il détermine un interprétant; de telle sorte que c'est de son interprétant qu'il dérive la réalité de sa signification.

Une _tabula rasa_ ayant été déterminée comme représentative du symbole qui la détermine, cette _tabula rasa_ tend à devenir déterminée.
Le vague tend toujours à devenir déterminé, simplement parce que le vague ne le détermine pas à être vague (en tant que limite d'une série infinie).
Dans la mesure où l'interprétant est le symbole, comme il l'est à un certain degré, la détermination est en accord avec celui du symbole.
Mais dans la mesure où il ne parvient pas à être un meilleur soi, il est passible de dévier de la signification du symbole.
Son but, cependant, est de représenter le symbole dans sa représentation de son objet; et partant, la détermination est suivie d'un développement plus avant, dans lequel il est corrigé.
Il est de la nature d'un signe d'être une réplique individuelle et d'être dans cette réplique un vivant général.
En vertu de cela, l'interprétant est animé par la réplique originelle, ou le signe qu'elle contient, avec le pouvoir de représenter le vrai caractère de l'objet.
Que l'objet ait même un caractère ne peut consister qu'en la représentation de ce qu'il l'ait --- une représentation ayant le pouvoir de faire taire toute opposition.
Dans ces deux étapes, de détermination et de correction, l'interprétant vise à l'objet plus qu'à la réplique originelle et peut être plus vrai et plus complet qu'elle.
L'entéléchie même d'un être tient à ce qu'il soit représentable.
Un signe ne peut pas même être faux sans être un signe, et dans la mesure où il est signe, il doit être vrai.
Un symbole est une réalité embryonnaire doté du pouvoir de croître en la vérité même, l'entéléchie même de la réalité.
Cela ne semble mystique et mystérieux que parce que nous persistons à rester aveugles à l'évident, qu'il ne peut y avoir de réalité qui n'ait la vie d'un symbole.

Comment une idée telle que celle de _rouge_ advient?
Cela ne peut être que par détermination graduelle à partir de l'indétermination pure.
Un vague qui n'est pas déterminé à l'être, de par sa nature commence par se déterminer lui-même.
Il apparaît que nous ne pouvons nous approcher plus près de cela pour comprendre l'univers.

Il n'est rien de nécessairement logique, dans ce qui me semble aujourd'hui logique; encore moins, comme les mathématiques l'exemplifient amplement, qu'il n'est rien de logique en dehors de ce qui m'apparaît l'être.
Est logique ce qu'il est nécessaire d'admettre afin de rendre l'univers intelligible.
Et le premier de tous les principes logiques est que l'indéterminé doit se déterminer lui-même autant qu'il le peut.

Un chaos de réactions sans aucun égard aux lois n'est rien absolument; et partant le pur rien était un tel chaos.
Aussi l'indétermination pure ayant développé des possibilités déterminées, la création a consisté à médier entre les réactions sans lois et les possibilités générales par l'influx d'un symbole.
Ce symbole était le but de la création.
Son objet était l'entéléchie de l'être qui est la représentation ultime.
^[Cité dans Romanini, Vinicius, and Eliseo Fernández, eds. 2014. Peirce and Biosemiotics: A Guess at the Riddle
of Life. Biosemiotics, 1875-4651 11. Dordrecht: Springer.]

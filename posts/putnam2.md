---
title: "Matérialisme et Relativisme —"
subtitle: "Renewing Philosophy, 1995"
author: Hilary Putnam
translator: Samuel Barreto
date: "2018-10-21"
---

Bien qu'individuellement les philosophes continuent de produire et de
défendre une gamme de conceptions métaphysiques aussi étendue qu'elle
l'a toujours été, deux visions sont aujourd'hui devenues dominantes en
philosophie Française et Américaine; ce sont les visions du
matérialisme et du relativisme. Même si peu de philosophes américains
se qualifient de matérialistes, et si je ne connais pas de philosophes
français qui se disent relativistes, les termes « physicalisme » et
« naturalisme » sont maintenant synonymes de matérialisme en
philosophie analytique, pendant que l'essor des visions
déconstructionnistes est souvent clairement relativiste, quand il
n'est pas purement nihiliste. J'ai argué depuis quelques années que
ces deux styles de pensée sont trop simplistes pour être d'une aide
quelconque à la réflection philosophique.

J'ai déjà indiqué certaines des raisons de mon insatisfaction quant au
matérialisme en tant qu'idéologie. Le philosophe matérialiste pense
que les théories scientifiques actuelles contiennent déjà les grandes
lignes d'une solution aux problèmes philosophiques concernant la
nature de l'esprit et de l'intentionnalité; j'ai argué qu'il n'y a pas
de raison de croire que c'est le cas. Cependant, mon but ici n'est pas
de polémiquer contre le matérialisme et le relativisme, mais de voir
ce que nous pouvons apprendre des échecs de ces larges points de vue.
Il y a, heureusement, beaucoup de philosophes qui rejettent à la fois
le relativisme et le matérialisme sémantique, c'est-à-dire qu'ils
rejettent l'idée que le sémantique peut être réduite à la physique, ou
même aux concepts des « sciences spéciales » qui n'utilisent pas
explicitement des notions intentionnelles; mais je perçois souvent,
particulièrement parmi les étudiants, que l'abandon de ces programmes
totalitaires pourrait signifier la « fin de la philosophie ». En
effet, Richard Rorty a très fermement soutenu l'idée que la
philosophie touche, dans un certain sens, à sa fin, et que nous devons
nous préparer à une ère post-philosophique. Je ne pense pas du tout
que ça soit la juste morale à tirer de la situation présente, mais
avant que nous puissions voir ce que cette juste morale est, nous
devons étudier plus en détail en quoi le matérialisme et le
relativisme sont dans le faux.

# La peur chomskyenne de la « relativité de l'intérêt »

Je commence par contraster l'attitude de Jerry Fodor envers les
concepts qu'il voit comme primitifs --- la causalité et le
contrefactuel conditionnel --- et la mienne que j'ai esquissé dans le
précédent chapitre. À mon sens, les expressions contrefactuelles
conditionnelles et causales présupposent ce que j'appelle « le point
de vue de la raison ».

Ce que j'entends par là est que, par exemple, lorsque nous évaluons un
contrefactuel conditionnel (à moins que le contrefactuel conditionnel
soit ce que j'ai appelé un contrefactuel conditionnel « strict ») nous
ne considérons pas toutes les situations physiquement possibles dans
lesquelles l'antécédent est vrai; typiquement, nous savons qu'il y a
d'autres situations physiquement possibles dans lesquelles
l'antécédent est vrai et le conséquent faux, et pourtant bien souvent
nous acceptons le contrefactuel conditionnel comme vrai quoi qu'il en
soit. Si ma femme et moi nous attablons pour petit-déjeuner, et que
nous découvrons que j'ai oublié d'allumer le gaz, et donc qu'il n'y a
pas d'eau bouillante pour le café, ma femme pourrait dire, « si tu
n'avais pas été si distrait, et avais tourné la vanne, nous aurions
maintenant de l'eau chaude pour le café ». Je peux penser à diverses
possibilités physiques étranges telles que si elles avaient été le
cas, alors j'aurais pu tourner la vanne de gaz et l'eau n'aurait tout
de même pas chauffé. Mais mentionner l'une quelconque de ces
possibilités à ma femme, en ce moment précis, n'en sera pas une
stratégie très efficace. Pour autant que Ruth Anna sache --- et elle
utilise le contrefactuel conditionnel de façon parfaitement
correcte ---, son contrefactuel est vrai. Et ce qui la justifie de le
qualifier de vrai est sa connaissance des régularités connectant
l'allumage du gaz et l'ébullition de l'eau que l'on place dessus, et
sa connaissance que ces régularités justifient pleinement qu'elle
s'attende à ce que l'eau soit chaude, à condition que j'ai allumé le
gaz. Notez que les éventualités physiques étranges auxquelles je
pourrais penser ne falsifient pas son contrefactuel.

Ça n'est pas pour dire que le contrefactuel conditionnel de Ruth Anna
ne peut pas être annulé. S'il était advenu qu'il y avait une
interruption de la pression de gaz, inconnue de nous deux, et que la
cuisinière ne serait pas restée allumée même si j'avais tourné la
vanne, alors son contrefactuel conditionnel est défait. Quelqu'un
pourrait proposer que ce qui exclut la pertinence de ces éventualités
physiques étranges que j'ai mentionnées est qu'elles sont improbables.
Mais si j'*avais* allumé la vanne de gaz, alors la situation
résultante aurait été différente de la situation véritable
d'innombrables façons (des molécules se trouveraient dans d'autres
endroits, des chaînes entières de causalité auraient été mise en
branle et ne l'ont pas été dans le monde réel, et ainsi de suite), et
cette situation aurait certainement eu une foule d'attributs
improbables, puisque chaque situation physique réelle a toujours des
attributs hautement improbables. Ça n'est donc pas la probabilité en
tant que telle, mais la _probabilité sous des angles pertinents_ qui
compte, et cela soulève encore une fois ce que j'appelle le point de
vue de la raison.

Pour rendre ce qui est en jeu plus clair, supposons que la personne
qui émette le contrefactuel ne soit pas Ruth Anna mais quelqu'un qui
ne connaisse rien à la physique, et que moi qui l'écoute soit expert
en physique. Même dans pareil cas, je ne tient pas comme falsifiant
automatiquement l'assertion le fait que je connaisse des situations
physiques que mon interlocuteur ne peut même pas imaginer, dans
lesquelles j'aurais tourné la vanne de gaz et l'eau n'aurait pas
bouilli. Je m'engage plutôt dans ce genre de pensée : d'un côté, je me
met à la place de l'émetteur, et je m'imagine émettant le même
contrefactuel dans la même situation et les mêmes conséquences
pratiques. D'un autre côté, en évaluant le contrefactuel émis (et que
je m'imagine émettre de ma situation d'émetteur fictif), si je la
considère comme pertinente, je tient compte de mes propres
connaissances scientifiques, de même que les raisons et intentions de
l'émetteur pour émettre le contrefactuel. J'essaie de décider
--- évidemment, dans bien dans cas, la décision est quasi-automatique,
ou la question n'est pas même soulevée --- s'il y a des situations
possibles auxquelles je pense qui falsifient _réellement_ le
contrefactuel de l'émetteur. En particulier, si des situations
possibles me viennent à l'esprit dans lesquelles l'antécédent du
contrefactuel serait vrai et le conséquent faux, alors j'essaie de
décider si ces situations --- situations qui falsifient le
contrefactuel lorsqu'elles sont pertinentes --- n'ont ou pas ce genre
de pertinence. Les sémanticiens des mondes-possibles décriraient cela
en disant que j'essaye de décider si certains mondes possibles sont
plus proches du monde réel que d'autres.

S'il y a une critique à faire au langage utilisé par les sémanticiens
des mondes-possibles, celui de mondes « plus proches » ou « plus
loin » du monde réel, c'est bien que ce langage masque ce qui doit
être soulevé, que ce qui est réellement jugé n'est pas la distance
entre objets dans un hyper-espace mais la pertinence de situations
hypothétiques, et la pertinence de situations dans un jugement est
essentiellement un problème normatif. L'utilisation d'un langage de
« proximité » transforme un jugement normatif, s'il est _raisonnable_
de voir quelque chose comme pertinent, en une description d'un fait
neutre, dépourvu de valeur.[^2]

J'ai exposé mes points de vue sur les contrefactuels et la causalité
dans plusieurs publications. Pourtant, un philosophe célèbre m'a
demandé récemment si j'étais un « nihiliste causal ». Qu'est-ce qu'il
se passe ici ?

Voilà ce qu'il me semble qu'il se produit : lorsque je dis que la
vérité d'un jugement de la forme _A cause B_ dépend du contexte et des
intérêts des juges (par exemple, ce que les interlocuteurs veulent
savoir dans un contexte particulier), alors certains en concluent
directement que je dois vouloir dire que de tels jugements sont
entièrement subjectifs, ou, peut-être, qu'ils sont entièrement
arbitraires. Ça n'est pas seulement une conjecture de ma part. Noam
Chomsky a réagit de la façon suivante à mon idée de relativité
d'intérêt des expressions de la forme _A explique B_: « Par là
[Putnam] soutient comme une thèse métaphysique substantielle que la
correction en linguistique (et en psychologie) _est_ ce qui explique
au mieux les données disponibles en ce moment sur le comportement de
celui qui parle, sachant certains intérêts actuels; ce qui est vrai
aujourd'hui sera faux demain, et ce qui est correct dépend de nos
intérêts et buts actuels. » D'emblée, que ce qui est correct
aujourd'hui sera faux demain ne fait partie de rien de ce que j'ai pu
soutenir; pourtant pour Chomsky cela semble découler de visions comme
celles que j'ai défendues dans le chapitre précédent.

Si je ne m'abuse, il y a principalement deux prémisses cachés de
Chomsky: (1) nous sommes libres de choisir nos intérêts à volonté; et
(2) les intérêts ne sont pas eux-mêmes sujets à la critique normative.
Ou peut-être que la prémisse est que ce qui est normatif est en
lui-même arbitraire et subjectif? (Ça ne me semble pas être probable
dans le cas de Chomsky, bien que ce soit, je crois, l'arrière-pensée
de certains autres de mes critiques.)

Prenons l'exemple médical que j'ai utilisé plus tôt. Dans un tel
contexte je dirais que l'attaque cardiaque de John était causée par
son refus d'obéir aux ordres des docteurs; il continuait de manger de
la nourriture riche en cholestérol et rechignait à s'exercer. Pourtant
dans un contexte différent, je dirais que l'attaque cardiaque de John
a été causée par une tension artérielle élevée. En supposant qu'à la
fois la tension et le refus d'obéir aux ordres des docteurs étaient
d'importants facteurs contribuant, et que j'en étais conscient dans
les deux situations, je ne verrais certainement pas mon assertion dans
le second contexte comme contredisant celle du premier. Si quelqu'un
me disait « mais hier vous disiez que la cause était le refus d'obéir
aux ordres des docteurs », je pourrais bien répondre « oui, mais la
question d'alors était ce que John aurait pu faire pour éviter
l'attaque cardiaque, celle d'aujourd'hui est ce qui le prédisposait
physiologiquement à cette attaque ». _A a causé B_ et _A' a causé B_
peuvent sembler incompatibles, mais ne le sont pas dans un tel cas.

Je veux répéter à nouveau les deux points qui me semblent importants :
(1) Nous ne pouvons simplement choisir quels intérêts nous avons. La
langue que l'on parle reflète qui l'on est et ce qu'on est, et en
particulier les intérêts que nous avons. Puisque nous connaissons le
genre d'intérêts que les gens ont, nous sommes capables de reconnaître
ce qui ressemble à des expressions contradictoires et de les
comprendre d'une façon qui n'est pas contradictoire. En cas de
confusion, nous pouvons facilement reformuler ces expressions; par
exemple, on peut dire que « même sachant sa tension élevée, John
n'aurait pas eut cette crise cardiaque s'il avait obéit aux docteurs »
et « même avec sa mauvaise alimentation et son manque d'exercice, John
n'aurait pas eut cette crise cardiaque s'il n'avait pas de tension. »
(2) Il arrive parfois que la pertinence d'un intérêt donné soit
discutable. Si un Marxiste dit que la cause de la crise cardiaque de
John était le système capitaliste, nous risquons de nous moquer --- à
moins bien sûr que le Marxiste réussisse à nous convaincre. Qu'un
intérêt soit pertinent ou non est une chose dont on peut en soi
débattre. Dire qu'une notion est relative à des intérêts n'est pas
dire que tous les intérêts sont également raisonnables.

Mais qu'est-ce qui fait que certains intérêts soient plus raisonnables
que d'autres ? La réponse est que la raisonnabilité dépend de choses
différentes dans des contextes différents. Il n'y a pas de réponse
générale. La ligne de partage est ici entre les philosophes qui,
inconsciemment ou consciemment, supposent que les notions normatives
sont subjectives, et de là toutes choses qu'elles touchent doivent
l'être aussi et les philosophes qui ne commencent pas par cette
supposition. Posez que les notions normatives sont non-cognitives; dès
lors, bien sûr, tout ce qui rend compte de l'explication, de la
causalité, et du conditionnel contrefactuel qui impliquent des
éléments normatifs seront perçus comme faisant de toutes ces formes de
discours des formes non-cognitives.

Fodor est d'accord avec moi pour dire que ces formes de discours ont
des valeurs cognitives, mais pour de toutes autres raisons. Fodor
répond à ceux qui nous nieraient le droit d'utiliser des
contrefactuels ou des clauses _ceteris paribus_ en disant qu'après
tout, nous les utilisons en géologie (et d'autres « sciences
spéciales »). Quel est alors la signification du fait que nous les
utilisions en géologie par opposition au fait que nous les utilisions
chaque jour de notre vie, dans la cuisine pour le cas présent ? La
réponse est évidente : la géologie est une « science ». Et les
véritables sciences, comme Fodor le suppose, nous indiquent ce que
nous devons supposer comme étant là, indépendamment de l'esprit.
Pourtant les sciences comme la géologie ne prétendent pas se confiner
aux « qualités primaires » des métaphysiques réalistes. Les textes de
biologie constituent de bons exemples de façons dont nous utilisons le
langage dans certains types d'explication ; ils ne sont pas, et ne
prétendent pas l'être, un inventaire de « l'équipement de l'univers »,
et seul un cas de scientisme aiguë conduit un philosophe à les
confondre avec un tel inventaire.

# Relativisme

Richard Rorty a été ces dernières années l'un des plus importants
interprètes de la philosophie continentale pour le public américain.
Tout comme les philosophes continentaux qu'il interprète, Rorty
rejette l'étiquette « relativiste ». Mais quasiment tous ses lecteurs
l'ont classé comme une sorte de relativiste, et il n'est pas difficile
de comprendre pourquoi, particulièrement dans sa _Philosophie et le
miroir de la nature_. Bien que Rorty se soit plus tard repenti de
cette formulation, il y identifiait la vérité, du moins la vérité en
ce qu'il appelait le discours « normal », avec l'accord des pairs
culturels (« l'objectivité est accord »). Je dirai plus tard comment
Rorty échappe, ou croit échapper, à l'accusation de relativisme. Mais
il est naturel, lorsque l'on rencontre cette formulation pour la
première fois, de la prendre dans un esprit relativiste. Appréhendée
de cette façon, elle dit que la vérité dans un langage --- tout
langage --- est déterminée par ce que la majorité de ceux qui le parle
en disent.

À ce moment, il nous faut prendre en compte la distinction de Rorty
entre le discours normal et le discours herméneutique. Dans
_Philosophy an the Mirror of  Nature_, l'idée était que l'essentiel
du discours est gouverné par les standards sur lesquels ceux qui
parlent une langue sont d'accord. Dans ce livre (tout comme dans son
récent _Contingency, Irony, Solidarity_) ces standards sont comparés à
un algorithme, c'est-à-dire, à une procédure décisionnelle du genre de
ceux des ordinateurs. Ça ne sera pas là la seule fois où nous verrons
Rorty --- en dépit de sa rupture déclarée avec la philosophie
analytique --- employer des appareils très similaires à ceux employés
par les philosophes analytiques. Le recours à une notion d'algorithme
pour expliquer en quoi certaines choses sont vraies et d'autres
fausses dans la langue d'une communauté, même si elle est vue comme
une métaphore, en est un bon exemple.

L'image de Rorty est que dans des circonstances normales, les
anglophones ne sont pas en désaccord sur des questions telles que
« Y a-t-il suffisamment de chaises pour tout le monde ce soir ? ».
La proposition qu'il y a suffisamment de chaises est, si elle est
vraie, une vérité pour un « discours normal », et sa vérité est
certifiée par les procédures sur lesquelles les membres de cette
communauté sont d'accords. Si l'accord sur un point ne peut être
atteint parce que les membres de la communauté font allégeance à des
paradigmes incommensurables, le discours est « herméneutique ». Le
mieux que l'on puisse faire est d'essayer de comprendre l'autre dans
une telle dispute pour maintenir la conversation, selon Rorty. Les
propositions d'un discours herméneutique ne sont vraies qu'au sens
honorifiques; chacun des partis de la dispute qualifie ses
propositions de vraies, mais ça n'est là que rhétorique conçue pour
persuader l'autre parti de changer ses allégeances.

Bien que _Philosophy and the Mirror of Nature_ contienne de brillantes
critiques du genre de métaphysiques que Rorty rejette, ses visions
positive sont avancées de façons très elliptiques et incomplètes. En
particulier, il n'est pas clair en quoi consiste la notion d'accord
des pairs culturels, en dehors de la métaphore d'un algorithme. Si je
dis à ma femme « notre cuisine a besoin d'être repeinte », le seul
pair culturel au courant de ce que notre cuisine a besoin d'être
repeinte dans ce cas est ma femme (en supposant que je n'en discute
avec personne d'autre). Dans un sens, mon pair culturel est d'accord :
c'est-à-dire que tous mes pairs culturels sachant que j'ai émis ce
jugement sont d'accord pour dire qu'il est vrai. Est-ce que cela
signifie que ce jugement est vrai ? Prenons un cas plus extrême.
Supposons que je vive seul et que je pense que ma cuisine a besoin
d'être repeinte, et que je ne discute de la question avec personne.
Dans ce cas tous mes pairs culturels au courant de mon jugement
(c'est-à-dire moi) sont d'accords qu'il est vrai. Est-ce que cela
signifie qu'il est vrai, d'après la théorie de Rorty ?

La plupart des lecteurs de Rorty pensent de lui qu'il dit qu'un
jugement du discours normal est vrai seulement dans le cas où des
pairs culturels _seraient d'accord_ s'ils étaient présent, ou s'ils
étaient informés des circonstances pertinentes. Mais le recours aux
contrefactuels a été rejeté par Rorty lui-même, du moins dans un
papier écrit après _Philosophy and the Mirror of Nature_. Selon Rorty,
recourir à ce que d'autres diraient s'ils étaient présents, c'est
recourir à des « observateurs fantômes ». J'ignore si cela représente
un changement d'avis de la part de Rorty, ou s'il rejetait déjà
l'interprétation contrefactuelle de sa formulation lorsqu'il écrivait
_Philosophy and the Mirror of Nature_. Dans ce dernier cas, j'ignore
complètement comment l'interpréter. Il semble probable que la
séduction de la métaphore de l'algorithme ait conduit Rorty à supposer
que la vérification d'une procédure est une chose qui _conduirait_ à
un résultat si elle était appliquée, en tant que fait objectif
« computationel », indépendant de celui qui emploi l'algorithme. Dans
ce cas, il empruntait inconsciemment l'image d'une philosophie
diamétralement opposée à la sienne.

Puisque Rorty est difficile à interpréter, imaginons plus simplement
un relativiste typique qui utilise réellement des contrefactuels
inconsciemment, et qui soutient que ce qui est vrai dans une culture
est déterminé par ce que les membres de cette culture _diraient_
(s'ils tombaient sur un désaccord insolvable, alors ce relativiste
dirait, comme Rorty, que la phrase en question n'appartient pas au
« discours normal », ou bien il pourrait la voir comme n'ayant aucune
valeur de vérité, ni même ou valeur de vérité relative).
L'inconscience de soi avec laquelle un tel relativiste utilise les
contrefactuels est précisément le problème. Si la vérité ou la
fausseté de la proposition selon laquelle ma cuisine a besoin d'être
repeinte dépend de ce que mes pairs culturels en diraient, alors qui
détermine quoi ? Qu'est-ce qui détermine ce que mes pairs culturels
_diraient_ ?

Les analyses contemporaines de contrefactuels suggèrent que deux
choses le détermine : (1) quelles situations possibles (ou quels
mondes possibles) sont plus proches du monde réel (ou, comme je
préfère le dire, _pertinente pour la proposition lorsque nous
considérons la situation réelle dans laquelle elle est émise_) ; et
(2) qu'est-ce qui adviendrait dans de telles situations possibles.
Pour un physicaliste, cette dernière ne pose pas de problèmes : si les
situations possibles sont complètement décrites dans la langue de la
physique, mettons par une « fonction d'état » au sens de la mécanique
quantique, ou de n'importe quelle théorie qui succéderait à la
mécanique quantique, ce qui advient dans cette situation (ou la
probabilité qu'une chose donnée advienne dans cette situation) est
déterminée par les lois de la physique fondamentale (Fodor dirait que
ce qui advient est déterminé par les lois de la physique fondamentale
et les lois des « sciences spéciales » pertinentes). Mais cette
interprétation fait reposer la valeur de vérité d'un contrefactuel sur
cette notion d'une chose comme étant une loi de la physique (ou une
loi des sciences spéciales) --- non une loi de la physique acceptée
(ou respectivement des sciences spéciales acceptées) mais une loi de
la vraie physique (science spéciale), quoi qu'elle soit --- ce qui est
difficile à accepter pour un relativiste. Même si la notion de vérité
est elle-même interprétée relativement dans cette formulation, le
relativiste a un problème : la valeur de vérité de la proposition
selon laquelle ma cuisine a besoin d'être repeinte dépend (pour le
relativiste) de la valeur de vérité de la proposition selon laquelle
les gens (dans des situations hypothétiques variées) diraient que la
peinture de ma cuisine est décolorée et décollée, ce qui à son tour
dépend de quelles lois sont pertinentes (physiques, biologiques,
psychologiques, etc…), ce qui à son tour dépend des lois que les gens
trouveraient pertinentes.

Les relativistes pourraient évidemment dénier qu'ils requièrent une
« sémantique » pour les contrefactuels. Ils pourraient juste affirmer
que le contrefactuel est clair tel qu'il est, et que sa vérité ne
demande nulle explication. Mais ici comme ailleurs, l'innocence
métaphysique une fois perdue, est difficile à retrouver. Dès lors que
l'on a vu la difficulté d'accorder de la vérité à un contrefactuel, il
est difficile de voir pourquoi quelqu'un qui verrait la vérité d'une
proposition non-contrefactuelle comme problématique, comme une notion
à abandonner ou à modifier radicalement, verrait la vérité d'un
contrefactuel comme non-problématique.

Supposons que notre relativiste typique la voit de cette façon. À ce
stade, il ou elle fait face au paradoxe suivant. C'est un fait de
notre culture présente qu'il n'y a nulle unanimité philosophique :
nous n'acceptons pas tous la philosophie d'un seul, et nous ne sommes
assurément pas tous relativistes. De plus, il est probable que cet
état de chose persiste pour quelques temps. Rorty lui-même verrait
probablement ce manque d'unanimité philosophique comme une qualité de
notre culture, une qualité qu'il aimerait voir préservée. Mais si, au
sens d'un fait empirique, la proposition « la majorité de mes pairs
culturels ne sont pas d'accord que le relativisme est correct » est
vraie, alors, et selon les propres critères de vérité du relativiste,
le relativisme n'est pas vrai !

Cette inconsistance n'est pas une inconsistance logique, parce qu'elle
dépend d'une prémisse empirique sur notre culture, mais cette prémisse
empirique en est une que peu de personnes remettent en cause. Rorty
lui-même dirait que sa vision de la vérité dans _Philosophy and the
Mirror of Nature_ n'était pas censée s'appliquer au discours
herméneutique mais seulement au discours normal. De là les assertions
de relativisme et d'anti-relativisme ne sont elles-mêmes pas vraies ou
fausses au sens où les propositions du discours normal sont vraies ou
fausses. Si je dit d'une phrase philosophique qu'elle est vraie,
d'après Rorty, je lui « adresse seulement mes compliments ». Dit
autrement, qualifier une chose de relativisme n'est pour Rorty pas
l'annonce d'une découverte métaphysique mais un tour rhétorique : un
tour dont le but est de nous faire changer nos manières, d'arrêter de
parler de vérité ou de fausseté plutôt que de parler d'une sorte de
vérité métaphysique. Le relativisme _à la_ Rorty est rhétorique.

# Relativisme et Solipsisme

Les relativistes typiques pensent, paradoxalement, avoir fait une
sorte de découverte métaphysique. Qu'advient-il d'eux si le
relativisme implique une contradiction (ou si une contradiction peut
dériver d'un relativisme usant d'une logique qu'il ne remet pas en
cause et d'un fait empirique non-controversé) ? La « déconstruction »
passe du relativisme au nihilisme. Au lieu de nous offrir une formule
supposée nous indiquer ce qu'est la vérité, les déconstructionnistes
nous annoncent que la notion de vérité est incohérente, qu'elle fait
partie d'une « métaphysique de la présence ». Alan Montefiore m'a
rapporté qu'il a une fois entendu Derrida dire « Le concept de vérité
est inconsistant, mais absolument indispensable ! » Mais que signifie
dire d'un concept que l'on trouve indispensable dans la vie de tous
les jours qu'il est « inconsistant » ? Certains usages du mot « vrai »
peuvent être inconsistants (comme certains paradoxes sémantiques bien
connus l'illustrent), mais qu'est-ce que cela signifie de dire de
_tous_ les usages du mot « vrai » qu'ils sont inconsistants (ou que
tous ses usages « nous ramènent à » des notions suspectes telles que
« présence », _« full speech »_, etc…) ?

La seule évidence que les philosophes français proposent pour cette
étonnante prétention que certains discours de vérité sont, sinon
inconsistants, cessent du moins d'être satisfaisant métaphysiquement.
(Selon Derrida, même la notion d'un signifiant --- un mot avec un
sens --- « nous ramènent ou nous retiennent dans le cercle
logocentrique ».) La chute d'un grand nombre d'alternatives
philosophiques aux discours sur la vérité est une chose bien
différente de la chute de la notion de vérité elle-même, tout comme la
chute d'un grand nombre de discours philosophiques sur la certitude
est une chose bien différente de la chute de la notion ordinaire de
certitude, comme Wittgenstein essayait de nous l'indiquer dans ses
derniers travaux. Lorsqu'un philosophe français veut savoir si le
concept de vérité, ou le concept de signe, ou le concept de référence,
est consistant ou non, il procède en cherchant chez Aristote, Platon,
Nietzsche et Heidegger, plutôt qu'en cherchant comment les mots
« vérité », « signe » et « réfère » sont utilisés. Mais cela en dit
plus sur la philosophie française que sur la vérité, le signe ou la
référence.

Ce qui est intéressant est qu'il existe une façon de rendre le
relativisme consistant qui était autrefois plus ou moins populaire en
philosophie, du moins sous des formes déguisées, et qui ne l'est plus
du tout : le relativisme à la première personne. Si je suis un
relativiste, et si je définis la vérité comme ce avec quoi _je_ suis
d'accord, ou ce avec quoi je serais d'accord si j'enquêtais plus
longuement, alors, aussi longtemps que je continue à être en accord
avec ma propre définition de la vérité, l'argument selon lequel ma
position est face à un problème de point de vue, ou qu'elle est
auto-réfutatrice, n'est pas immédiatement soulevé. Le relativisme à la
première personne est véritablement introuvable sur la scène
philosophique contemporaine, et il peut valoir le coût de se demander
pourquoi.

L'une des raisons, superficielle mais importante, est l'attention
générale, même si elle ne va parfois pas de pair avec la
compréhension, accordée à l'argument du langage privé de Wittgenstein.
L'argument du langage privé est assurément difficile à comprendre,
mais une partie de son essor est incontestée ; ce que Wittgenstein
dirait, s'il devait s'adresser à un relativiste du type déjà décrit,
serait quelque chose du genre: « Vous parlez comme si le langage était
votre invention, soumise à vos volontés à tout moment. Les jeux de
langages que l'on joue ne sont altérable que dans une mesure très
limitée. Ce sont des formations culturelles, avec une somme d'inerties
considérable. Vérité et fausseté dans un jeu de langage lui sont
internes, ce ne sont ni des choses inventées par vous, ni des choses
qui vous font référence. » Un jeu de langage, Wittgenstein nous
indique, « consiste dans les procédures récurrentes de ce jeu au cours
du temps ». Les procédures en question existaient longtemps avant moi,
et continueront d'exister longtemps après moi.

On peut alors dire que ça n'est pas un argument. (Je ne prétend pas
ici, évidemment, avoir résumé l'argument du langage privé.) Le
relativiste à la première personne dirait que même dans un langage
naturel nous parlons parfois de choses comme si elles étaient non
relatives et que nous découvrons plus tard être relatives. Nous ne
disons ordinairement pas que deux événements sont simultanés ou non
simultanés _relativement à mon cadre _; pourtant, une fois connue la
théorie de la relativité, nous savons que la simultanéité est en fait
relative à un cadre, bien qu'il y ait de bonnes raisons de ne pas les
prendre en compte dans des contextes ordinaires. De façon similaire,
le relativiste en première personne pourrait prétendre avoir fait la
découverte philosophique que la vérité, tout comme nous en parlons
ordinairement dans le langage naturel, se réfère à une propriété
relationnelle. Il est vrai que les propriétés relationnelles ne
présupposent pas de l'existence propre du relativiste : quand vous
parlez, la vérité est une vérité relative à vous ; quand John parle,
la vérité est vérité relative à John ; quand Joan parle, la vérité est
vérité relative à Joan ; etc …

La raison pour laquelle les gens n'accordent pas de crédit à cette
vision, je crois, est qu'elle n'est pas convaincante en tant que
description de la façon dont nous utilisons le mot « vrai ». Si
j'entends quelqu'un dire que le palladium est une terre rare, je ne
pense pas que l'assertion signifie que celui qui parle croirait que le
palladium est une terre rare s'il ou elle enquêtait suffisamment (en
quoi est-ce que cela me concerne, d'ailleurs ?). Je crois que celui ou
celle qui parle fait une assertion qu'il faut vérifier --- par nous ou
un tiers --- en étudiant le palladium. Il se pourrait évidemment
qu'après avoir étudié le palladium pendant longtemps nous soyons
toujours incapables d'affirmer si l'assertion était vraie. Mais le jeu
de langage de classification des éléments de cette façon ne pourrait
pas fonctionner du tout si dans tous les cas nous n'étions pas
capables de parvenir à un accord sur ce qui rend une terre rare ou
non.

Mary Warnock a dit un jour que Sartre nous a donné non pas des
arguments ou des preuves mais « une description si claire et limpide
que lorsque je pense à sa description et l'adapte à mon cas propre, je
ne peux m'empêcher de voir son application ». Il me semble qu'il
s'agit d'une très bonne description de ce que Wittgenstein faisait,
pas seulement dans l'argument du langage privé, mais partout et dans
toute son œuvre. Supposons que nous _décrivions_ simplement la façon
dont nous utilisons le mot « vrai » attentivement et avec précaution,
supposons que nous en donnions sa « phénoménologie ». Nous verrons
alors l'idée qu'il tient lieu de la propriété d'« être ce que je
croirais si je continuais à enquêter » comme simplement fausse.

Je ne crois pas que l'argument du langage privé, aussi influent
soit il, soit la seule raison du déclin du relativisme à la première
personne. Le solipsisme n'a jamais été une position philosophique
populaire, et le relativisme à la première personne ressemble
dangereusement au solipsisme. En effet, il n'est pas clair qu'il soit
en mesure d'éviter le solipsisme.

Considérez, par exemple, une proposition à propos d'un être humain qui
n'est plus en vie, une proposition dont on n'est plus en position de
déterminer la valeur de vérité, mettons, "César fût rasé le jour où il
franchit le Rubicon". Il fait partie de ce que Cavell appelait notre
« reconnaissance »[^1] d'autres êtres humains de ce que nous pouvons
traiter des propositions qu'_ils_ sont capables de vérifier comme
ayant tout autant le droit d'être appelées signifiantes et vraies (ou
fausses) que les propositions que nous sommes nous-mêmes capables de
vérifier. Si une proposition est telle qu'_aucun_ être humain ne soit
jamais, dans quelques circonstances concevables, en mesure de la
vérifier, alors, en effet, nous commençons à nous demander --- ou
certains d'entre nous commencent à se demander --- si la proposition
_a_ même une valeur de vérité ; mais la proposition que César fut rasé
le jour où il franchit le Rubicon est une proposition qu'au moins un
être humain, c'est-à-dire Jules César lui-même, était tout à fait en
mesure de vérifier. Je ne doute donc pas plus qu'une telle proposition
ait une valeur de vérité que la proposition que je me suis rasé ce
matin en ait une. Pour un relativiste, cependant --- et maintenant
qu'il ne fait pas plus de différence que nous parlions d'un
relativiste en première personne ou d'un relativiste culturel --- il
est tout à fait probable que la proposition à propos de Jules César
n'ait aucune valeur de vérité. Il se pourrait bien, et en fait c'est
probablement le cas, que toutes traces de ces événements ou
non-événements aient été oblitérées depuis bien longtemps. Pire, la
question de savoir s'ils ont ou non une valeur de vérité est seulement
une question sur ce que le relativiste (dans le cas des relativistes à
la première personne) ou la culture (dans les cas des relativistes
culturels) en viendraient à croire, s'ils faisaient de leur mieux (par
leurs propres lumières ou celles de la culture) pour enquêter sur la
question. Jules César est une construction logique à partir des
croyances réelles ou potentielles de personnes actuelles, sur une
question de la sorte.

Pour en revenir au relativisme en première personne, ce qui est vrai
de Jules César est vrai de personnes vivantes actuellement. Si vous et
moi ne sommes pas le relativiste à la première personne en question,
alors la vérité sur vous et sur les amis et l'épouse du relativiste en
première personne est, pour lui ou elle, simplement fonction de ses
propres dispositions à croire. Voilà pourquoi le relativisme ressemble
à un solipsisme à peine masqué. Mais il est difficile de voir en quoi
le relativisme culturel lui est supérieur à cet égard. Le solipsisme
avec un « nous » est-il meilleur à un solipsisme avec un « je » ?

# Matérialisme, Relativisme, Réalisme Métaphysique

Wittgenstein introduisit l'image du langage comme un système de jeux
chevauchants. Mais il est parfois manqué que Wittgenstein souligne
quelque part que _la véracité ou la fausseté dans un jeu de langage
n'est pas toujours déterminé par des règles_. Je discuterai du passage
en question plus tard ; disons seulement pour l'instant que le passage
que nous étudierons avance également un autre point différent et moins
souligné. Wittgenstein s'intéresse aux désaccords sur ce qu'il peut
bien se passer dans l'âme d'un autre être humain (Est-il en train de
simuler une émotion ? Est-elle tombée amoureuse ?). Parfois de tels
désaccords peuvent être réglés à la satisfaction de tous. Mais
Wittgenstein décrit un cas qui ne l'est pas, et où ce qui compte est
l'« unwägbare Evidenz » (l'évidence impondérable).

Le phénomène du _controversé_, de ce qui ne peut être réglé à la
satisfaction de tous ceux qui sont « linguistiquement compétents »,
est, cependant, ubiquitaire, et sa portée s'étend bien au delà du
psychologique. Même les prétendus jugement factuels sont fréquemment
controversés, du moins dans certaines régions de la culture
--- (pensons aux débats sur l'évolution entre les scientifiques et les
fondamentalistes, ou à l'impossibilité de convaincre certains qu'il
n'y a plus de prisonniers de guerre américains au Vietnam). Il fait
partie de notre forme de vie, partie de la façon dont nous pensons et
agissons et dont nous continuerons à penser et agir, que chacun de
nous traite beaucoup de positions controversées comme ayant une valeur
de vérité. Rorty dira bien sûr que de telles phrases ne ressortent pas
du discours « normal », que de les appeler vraies n'est que leur
« donner une tape dans le dos »; mais dès lors qu'il quittera son
étude il dira des fondamentalistes opposés à l'évolution, ou de ces
énergumènes d'extrême droite qui pensent qu'il reste des prisonniers
de guerre au Vietnam, qu'ils sont idiots. Ce qu'il est juste de dire
dans un contexte donné ne peut toujours être établi à la satisfaction
de tous ; ça n'en est pas moins juste.

Pourtant la reconnaissance explicite que les jeux de langages sont des
activités humaines dans lesquelles ce qu'il est juste ou faux n'est
pas simplement conventionnel, n'est pas simplement déterminé par
consensus, mais quelque chose qui requiert d'être évalué perturbe un
grand nombre de sensibilités contemporaines. (Peut-être qu'elle était
troublante pour Wittgenstein lui-même ; peut-être que c'est pour ça
que dans les _Investigations_ il n'y a qu'une seule référence isolée à
ce fait si important. Plus tard je parlerai de quelques leçons non
publiées de Wittgenstein, que d'autres avaient prises en note et
publiées après sa mort, qui apportent un éclairage neuf sur sa
pensée.) Mais la méfiance du normatif dans la philosophie actuelle est
évidente par dessus tout dans les extrêmes jusqu'auxquels iront
certains philosophes pour éviter d'admettre que la vérité
--- c'est-à-dire la véracité de ce qui est dit --- _est_ une notion
normative.

Nous avons vu qu'et les matérialistes et les relativistes tirent parti
du contrefactuel conditionnel lorsqu'ils tentent d'expliquer ce qu'est
la vérité. À première vue, on ne s'attendrait pas à ce qu'aucune des
deux sortes de philosophes soit particulièrement heureuse de cette
façon de parler. Les questions de faits à propos de situations
non-réelles s'adaptent aussi mal à la vision du matérialisme qu'aux
préconceptions anti-métaphysiques du relativisme. Si, en dépit de ça,
l'on trouve que les deux sortes de philosophes ont recours à cette
machinerie, cela doit seulement être parce que le prix à payer pour
l'éviter semble trop élevé. Les philosophes d'un genre plus
traditionnel auraient tenté d'éviter le problème d'une façon
différente encore. Pour eux, dire qu'une phrase est vraie n'est pas
faire un jugement normatif du tout : c'est seulement dire que la
phrase « est en accord » avec quelque chose (« les faits ») ou qu'elle
« correspond » à quelque chose (« un état de chose »). Mais j'ai
montré ailleurs, en utilisant nombre de théorèmes de la théorie
contemporaine des modèles, que la notion de « correspondance » est
totalement vide dans ce contexte. Il est possible, en fait,
d'interpréter notre langage, au sens d'« interprétation » utilisé en
théorie contemporaine des modèles, de façon telle que les phrases
d'une théorie consistante « s'accordent avec la réalité » dans une
correspondance appropriée. Même si les conditions de vérité pour
toutes les phrases de notre langage sont fixées d'une manière ou d'une
autre, il est encore possible de trouver une correspondance dans
laquelle chaque phrase de notre langage retiennent ses conditions de
vérités présentes (jusqu'à l'équivalence logique), même lorsque les
références des mots individuels changent si radicalement que le mot
« cerise » finisse par se référer aux chats et que le mot « tapis »
finisse par se référer aux arbres. C'est en réponse à ce théorème que
certains philosophes physicalistes ont suggéré que ce qui rend unique
la référence d'un terme n'est pas un problème de correspondance
théorique du modèle dans l'abstrait, mais spécifiquement de
« connection causale » ou « d'attachement causal ». Dans le chapitre
précédent j'ai examiné les formes les plus récentes de ces suggestions
et vu à quel point elles se réduisent à peu de choses. Au delà de ça,
comme je l'ai également souligné, ces philosophes ignorent la
dépendance au langage de la notion ordinaire de causalité elle-même.
Dans les faits, la notion ordinaire de causalité est une notion
_cognitive_, et ces philosophes la traitent comme si elle était
purement physique.

Ce que je suggérais ici était que la philosophie du langage est dans
l'impasse parce qu'elle est déterminée à éliminer le normatif en
faveur d'autre chose, aussi problématique que cette chose puisse être.
En philosophie analytique, ce désir d'éliminer le normatif va souvent
de pair avec l'idée que la science est « sans valeurs », et que la
science et elle seule nous indique les choses « telles qu'elles sont
réellement ». Dans le prochain chapitre, j'examine cette distinction
science-éthique dans le récent et influent _Ethics and the Limits of
Philosophy_ de Bernard William.

[^1]: _Acknowlegdement_ dans le texte.

[^2]: Hilary Putnam mobilise ici son célèbre concept de _« value-neutral fact »_, dont il est difficile de trouver une traduction française adaptée.

---
title: Gaston Bachelard
subtitle: La Philosophie du Non
author: Samuel Barreto
date: 2018-09-10
---

Quelques citations prélevées dans *La Philosophie du Non* de
Bachelard.

# La philosophie du Non

_page 7_: Finalement, la philosophie de la science physique est
peut-être la seule philosophie qui s'applique en déterminant un
dépassement de ses principes. Bref, elle est la seule philosophie
ouverte. Tout autre philosophie pose ses principes comme intangibles,
ses premières vérités comme totales et achevées. Tout autre
philosophie se fait gloire de sa fermeture.


_page 8_: Pour le savant, la connaissance sort de l'ignorance comme la
lumière sort des ténèbres. Le savant ne voit pas que l'ignorance est
un tissu d'erreurs positives, tenaces, solidaires.


_page 9_: (...) Nous voulons définir la philosophie de la connaissance
scientifique comme une philosophie ouverte, comme la conscience d'un
esprit qui se fonde en travaillant sur l'inconnu, en cherchant dans le
réel ce qui contredit des connaissances antérieures. Avant tout, il
prendre conscience du fait que l'expérience naturelle dit *non*
à l'expérience ancienne, sans cela, de tout évidence, il ne s'agit pas
d'une expérience nouvelle.


_page 17_: Il nous faudra rappeler que la philosophie du non n'est pas
psychologiquement un négativisme et qu'elle ne conduit pas, en face de
la nature, à un nihilisme. Elle procède au contraire, en nous et hors
de nous, d'une activité constructive. Elle prétend que l'esprit au
travail est un facteur d'évolution. Bien penser le réel, c'est
profiter de ses ambiguïtés pour modifier et alerter la pensée.
Dialectiser la pensée, c'est augmenter la garantie de créer
scientifique des phénomènes complets, de régénérer toutes les
variables dégénérées ou étouffées que la science comme la pensée
naïve, avait négligées dans sa première étude.


_page 25_: En ce qui concerne la connaissance théorique du réel, c'est
à dire en ce qui concerne un connaissance qui dépasse la portée d'une
simple description, tout ce qui est facile à enseigner est inexact.


_page 40_: La hiérarchie des choses est plus complexe que la
hiérarchie des hommes.


_page 42_: S'il faut souvent délester le réaliste, il faut aussi
lester le rationaliste. Il faut surveiller les *a priori* du
rationaliste, leur rendre leur juste points d'*a posteriori*.


_page 53_: La métachimie doit bénéficier de la connaissance chimique
des diverses activités substantielles. Elle doit aussi bénéficier du
fait que les véritables substances chimiques sont des produits de la
technique plutôt que des corps trouvés dans la réalité.


_page 54_: Car enfin, il serait trop commode de se confier une fois de
plus à un réalisme totalitaire et unitaire et nous répondre : *tout
est réel*, l'électron, le noyau, l'atome, la molécule, la micelle, le
minéral, la planète, l'astre, la nébuleuse. À notre point de vue, tout
n'est pas réel de la même façon, la substance n'a pas, à tous les
niveaux, la même cohérence; l'existence n'est *pas une fonction
monotone*, elle ne peut pas s'affirmer partout et toujours du même
ton.


_page 74_: L'espace où l'on regarde, où l'on examine est
philosophiquement très différent de l'espace où l'on voit.


_page 78_: En suivant l'inspiration de Whitehead, on est amené
à définir une substance par la cohérence des principes rationnels qui
servent à coordonner ses caractères, plutôt que par la cohésion
interne qu'affirme le réalisme, en dépassant toujours la portée des
preuves effectives.


_page 89_: Un élément n'est donc pas un ensemble de propriétés
différentes comme le veut l'intuition substantialiste usuelle. C'est
une collection d'états possibles pour une propriété particulière. Un
élément n'est pas une hétérogénéité condensée. C'est une homogénéité
dispersée. Son caractère élémentaire est démontrée par la cohérence
rationnelle qui résulte d'une distribution régulière de ses états
possibles.


_page 90_: Il a le projet de dialectiser la catégorie d'unité, il veut
faire comprendre le « caractère relatif de la catégorie d'unité. »


_page 99_: Toute structure linéaire réelle ou réalisée renferme des
structures fines.


_page 106_: Si la dialectique qui divise les objets en classes est une
dialectique première, *fondamentale*, si elle touche les principes
assez profondément pour qu'on ne puisse espérer subsumer les objets de
deux classes dans une même classe, alors il n'y a plus de logique
transcendantale.


_page 110_: Le Plan de la représentation dûment intellectualisée est
le plan où travaille la pensée scientifique contemporaine; le monde
des phénomènes scientifiques est notre représentation
intellectualisée. (...) Le monde où l'on pense n'est pas le monde où
l'on vit.


_page 112_: La forme trop fortement substantivée produit la
contradiction. La pensée réaliste pose le sujet avant les prédicats
alors que l'expérience en microphysique part de prédicats de
prédicats, de prédicats lointains et s'efforce simplement de
coordonner les manifestations diverses d'un prédicat.


_page 127_: Il cite les travaux d'Alfred
Korzybski[@KorzybskiScienceSanityIntroduction1995] qui ont eu l'air de
lui faire beaucoup d'effet. « La pensée scientifique est le principe
qui donne le plus de continuité à une vie; elle est, entre toutes,
riche d'une puissance de cohérence temporelle (...). » Plus loin, Paul
Valéry: « On pense comme on se heurte. »


_page 128_: Il est impossible d'éduquer par référence à un passé
d'éducation. Le maître doit apprendre en enseignant, hors de son
enseignement. Fût-il très instruit, sans un *shifting character* en
exercice il ne peut donner l'expérience de l'ouverture.


_page 130_: Sans cesse, le psychisme humain, à quelque niveau que ce
soit de l'éducation, doit être rendu à sa tâche essentielle
d'invention, d'activité d'ouverture.


_page 134_: Deux hommes, s'ils veulent s'entendre vraiment, ont dû
d'abord se contredire. La vérité est fille de la discussion, non pas
fille de la sympathie.

## Références

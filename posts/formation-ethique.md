---
title: Éthique de la recherche
subtitle: Un MOOC à l'éthique de la recherche, est-ce une bonne formation à l'éthique de la recherche ?
author: Samuel Barreto
---

> §77. Et si nous développons plus avant cette comparaison, il
> apparaît alors que le degré de ressemblance qu'il *peut* y avoir
> entre l'image nette et l'image floue dépend du degré d'indistinction
> de la seconde. Car, imagine que tu aies à dessiner une image nette
> qui "corresponde" à l'image floue. Il y a, dans la seconde, un
> rectangle de couleur rouge aux contours flous ; tu le remplaces par
> un rectangle aux contours nets. Certes --- on pourrait dessiner
> plusieurs rectangles aux contours nets correspondants au rectangle
> aux contours flous. --- Mais si les couleurs de l'original se
> fondent les unes dans les autres sans qu'il y ait la moindre trace
> d'une limite, -- vouloir dessiner une image nette correspondant à
> l'image floue ne deviendra-t-il pas une entreprise sans espoir ? Ne
> faudra-t-il pas alors que tu dises : « Ici, j'aurais pu dessiner un
> cercle aussi bien qu'un rectangle ou la forme d'un cœur, étant donné
> que les couleurs se fondent vraiment toutes les unes dans les
> autres. Tout est juste -- et rien ne l'est. » --- C'est dans cette
> situation que se trouve, par exemple, celui qui en esthétique ou en
> éthique cherche des définitions qui correspondraient à nos concepts.
>
> Quand tu butes sur cette difficulté, demande-toi toujours : Comment
> avons-nous _appris_ la signification de ce mot ("bon", par
> exemple) ? Sur quel type d'exemples ? Dans quels jeux de langages ?
> (Tu verras alors plus facilement que le mot doit avoir toute une
> famille de significations.)[@WittgensteinRecherchesPhilosophiques2014, §77]

Wittgenstein commente ainsi l'effort des éthiciens pour définir à
leurs concepts des frontières délimitées, lors même qu'elles sont par
essence floues, et qu'en tracer les limites revient à leur fournir un
sens dépendant des choix opérés dans le tracé même. Ici nous buttons
sur la difficulté qui consiste à donner un sens à la notion
d'_éthique_, à la notion de _recherche_, à la notion d'_éthique de la
recherche_, enfin à la notion de _formation_. Nous cherchons à voir en
quoi une formation à distance peut être pertinente dans la
constitution d'une image de l'éthique par le (jeune) chercheur qui
soit en phase avec les enjeux actuels des sciences, la demande sociale
qu'elles se doivent --- le doivent-elles ? --- de prendre en compte ;
bref la représentation de sciences qui soient d'avantage _impliquées_
dans le monde qui est le nôtre.

Si d'emblée il paraîtrait évident qu'une telle formation soit par
nature limitée dans sa capacité à atteindre ses ambitions, il me
semble qu'elle peut --- qu'elle a pu ---, dans une certaine mesure,
poser les jalons d'une démarche qui y conduise. J'essaierai d'exposer
dans un premier temps en quoi elle contribue à définir les contours du
concept d'éthique, sur lequel on peut s'appuyer pour tenter de
dessiner ceux de la notion d'éthique de la recherche, de la notion de
"bonne" recherche --- donc de "bon" chercheur. De là je discuterai du
cadre institutionnel dans lequel elle s'effectue, des limitations
qu'il lui appose, et du message qu'elle transmet finalement, inscrite
dans ce champ, du "besoin" éthique. Enfin, je tenterai de discuter de
la nature éthique de la formation elle-même.

## Une _formation_ à l'_éthique_

L'_éthique_ fait partie de ces concepts philosophiques dont
Wittgenstein nous enjoint de préciser les applications et les
implications. C'est un mot pris dans divers jeux de langages, et dont
la signification varie selon le jeu que l'on joue; bien qu'il existe
des similitudes entre les significations des différentes propositions
dans lesquelles il est invoqué, elles n'existent pas en droit mais en
fait. Dans les _Recherches Philosophiques_, Wittgenstein montre en
quoi le sens d'un mot ne peut être compris que lorsqu'on analyse dans
quels contextes il est appris, employé, à quelles fins, etc…

On ne peut donc comprendre ce qu'_est_ l'éthique qu'en l'étudiant dans
son contexte, et c'est bien le parti pris ici par le cours. Par là, je
pense que le cours a choisi la bonne approche pour familiariser des
étudiants --- dont je fais parti --- avec un concept flou par nature:
en _montrant_ divers cas de figure, en exposant de quelles manières
l'éthique peut être employée, requise par les acteurs des conflits
éthiques, en impliquant l'étudiant dans le questionnement, le cours
réussit il me semble à montrer de la main la direction de ce qu'est
l'éthique, au degré d'exactitude nécessaire. Car il est moins question
dans ce cours de donner (de _dire_) une définition précise de ce
qu'est l'éthique, dont on a vu qu'elle est protéiforme, que de
_montrer_ si, comment, pourquoi et dans quelle mesure elle s'impose au
chercheur.

Le cours réussit ainsi sa tâche de formation à l'éthique, en tant
qu'il permet de s'en donner une image même vague, et évite l'écueil de
la normativité (de la normation ?). Bernard Williams a défendu dans
_Ethics and the Limits of Philosophy_ que:

> L'idée de base derrière la distinction entre le scientifique et
> l'éthique, énoncées en termes de convergence, est très simple. Dans
> une enquête scientifique, il devrait y avoir idéalement convergence
> vers une réponse, où la meilleure explication de la convergence
> implique l'idée que la réponse représente la façon dont les choses
> sont ; en éthique, du moins à un niveau de généralité élevé, il n'y
> a pas un tel espoir de cohérence. La distinction ne tient pas au
> fait qu'il y ait ou non convergence, et d'ailleurs ça n'est pas
> là-dessus que tourne le débat. Le point de contraste est que,
> même lorsqu'il y a convergence, il ne sera pas juste de penser
> qu'elle est advenue parce que guidée par la façon dont sont
> réellement les choses, alors que la convergence en science peut être
> expliquée de cette manière lorsqu'elle survient. (p. 136)[_Ethics and
> the Limits of Philosophy_ (Cambridge, Mass.: Harvard University
> Press, 1985), cité dans @PutnamRenewingPhilosophy1995, chapitre 5.
> La traduction est la mienne.].

Il ne peut donc y avoir une « conception éthique absolue », sur
laquelle on convergerait si l'on enquêtait suffisamment. Vouloir par
la formation fixer des contours précis aux concepts dont l'éthique
traite, ç'aurait été contribuer à imposer par l'autorité
(déformatrice) des formateurs et des formatrices des normes éthiques,
alors même que ces concepts ne doivent être fondés que sur autre chose
que la norme.

## L'éthique de la _recherche_?

En quoi alors l'éthique de la recherche est-elle requise pour un
jeune chercheur? Dans quelle mesure l'éthique de la recherche
est-elle déterminée par la nature de la recherche elle-même? Pourquoi
s'impose-t-elle maintenant plus qu'avant? S'impose-t-elle maintenant
plus qu'avant? En quoi le cours en ligne permet de commencer à
répondre à ces questions, s'il le permet?

Le statut des sciences comme entreprise de production des savoirs a
constamment changé; il s'agirait alors de les re-situer dans leurs
contextes historiques. Depuis l'idéal irénique du sociologue des
sciences Robert Merton,[@GingrasSociologieSciences] en passant par
l'application de Bourdieu de son concept de champ aux sciences,
[@BourdieuChampScientifique1976;
@BourdieuSpecificiteChampScientifique1975] voir à l'ultra-relativisme
débridé des Latour et consorts, vertement critiqué par
@GingrasAirRadicalismeQuelques1995 mais pourtant très en vogue dans le
domaine des Sciences Studies, les travaux des sociologues des sciences
ont montré la nature profondément sociale de l'entreprise
scientifique. Les historiens des sciences et les épistémologues,
notamment l'école française des Gaston
Bachelard,[@BachelardFormationEspritScientifique1938] Michel
Foucault[@FoucaultMotsChosesArcheologie1966] ou George
Canguilhem[@CanguilhemIdeologieRationaliteDans1981] ont eux montré
l'enracinement idéologique des savoirs. La grande majorité des
sciences humaines (notamment américaines) semble s'être convertie aux
excès du postmodernisme relativiste et du constructivisme social des
savoirs.[@BoghossianPeurSavoirRelativisme2009]

Ce courant a néanmoins eu le mérite de questionner le positivisme
patent qui traverse de part en part les sciences et l'enseignement des
sciences. Il est désormais presque banal de considérer après Foucault
(et @BourdieuCeQueParler1982) que savoir et pouvoir sont intimement
entrelacés, que tout discours de connaissance est aussi discours à
prétention de pouvoir. Bourdieu parmi d'autres a bien montré en quoi
la progression hiérarchique dans le champ scientifique fonctionnait
par conversion d'un capital de crédit pûrement scientifique en
autorité temporaire pûrement sociale.[@BourdieuChampScientifique1976]
Il convient dès lors pour la science de se penser comme impliquée dans
un monde social, comme se devant d'être inscrite dans un contexte de
pertinence, comme le dit Léo Coutellec:

> La science impliquée [...], c'est la science qui se pense dans un
> espace d'expression du pluralisme des sciences. La science impliquée
> n'est pas une nouvelle région des sciences, c'est le nom de la
> science lorsque celle-ci reconnaît son caractère fondamentalement
> humain. [@CoutellecScienceAuPluriel2015, p. 9]

Il me semble qu'il est donc primordial pour un chercheur, jeune de
surcroît, de se questionner sur les implications éthiques de sa
recherche, sur son devoir de pertinence, sur son inscription
contextuelle et sociale, sur ses responsabilités enfin. Ça ne sera
qu'à cette condition que la recherche pourra être dite "bonne". Est-ce
que le cours en ligne permet de répondre à ces questions? Certainement
pas, mais il a le mérite d'aider à les poser, de pointer du doigt les
failles et les nécessités qui déterminent le questionnement. Et je
crois que c'est là son enjeu principal, de placer les étudiants en
position d'enquête éthique. C'est par l'enquête et la recherche
éthique qu'elle survient.[@DeweyLogicTheoryInquiry1938;
@PutnamRenewingPhilosophy1995, chapitre 9]

## Des limites institutionnelles ?

Dans la forme du cours est dit tout le conflit éthique, le conflit de
valeur qui traverse les institutions, que ce soit l'école doctorale,
l'Université de Lyon ou le Ministère de la Recherche et de
l'Enseignement Supérieur. Elles sont doublement contraintes (_double
bind_[@EliasDynamiqueSocialeConscience2016, chapitre 6]) parce
qu'elles sont à la fois tenues de veiller à ce que « chaque doctorant
reçoive une formation à l'éthique de la recherche et à l'intégrité
scientifique »,[@Arrete25Mai] et d'assurer une production scientifique
sur la base de laquelle elles sont évaluées, financées, promues,
visibles et vues. Dit autrement, il leur faut d'une part mettre en
place les conditions dans lesquelles les étudiants en thèse produisent
une recherche éthique, et d'autre part maximiser leur production et
leur productivité scientifique dans des cadres temporels
restreints.[sur la nécessaire pluralité temporelle des sciences,
@CoutellecScienceAuPluriel2015, page 27;
@RosaAccelerationCritiqueSociale2010]

La logique du champ institutionnel s'applique ainsi à tous les
individus qui le constituent et qu'il institue. Ce sont des
« structures structurantes et
structurées ».[@BourdieuSociologieGeneraleCours2015] Tout autant que
les institutions, les individus sont traversés de part en part par les
mêmes conflits éthiques entre ce que Léo Coutellec appelle le
« productivisme scientifique », dénoncé par le mouvement Slow Science,[^1]
c'est-à-dire la maximisation de la production scientifique, bien
souvent d'un point de vue quantitatif --- avec toutes les dérives
bibliométriques que l'on
connaît[@GingrasDerivesEvaluationRecherche2014] --- et le
questionnement éthique réflexif sur sa
recherche.[@BourdieuScienceScienceReflexivite2001].

On voit donc bien que par le choix d'une formation en ligne à
l'éthique, les institutions montrent leur « bonne volonté » éthique
aux instances gouvernementales desquelles elles répondent, tout en
minimisant le coût (financier ou temporel) lié à l'investissement dans
une formation présentielle pour l'ensemble des doctorants. C'est là la
marque d'une incompréhension quant au statut de l'éthique
scientifique. Un cynique y verrait une manœuvre politique pour
s'accomoder de la réglementation, en esquivant la responsabilité
éthique. Celle-ci est vue comme quelque chose qui vient après
l'enquête, comme une formalité administrative, une sorte de crible
réglementaire qu'il s'agit de passer pour que la production
scientifique soit validée. Il me semble que la formation en ligne a
réussi à contourner cet écueil et à souligner que l'éthique devrait
compénétrer l'ensemble du processus, que ce soit à l'amont (surtout à
l'amont?) ou à l'aval de l'enquête.

## Conclusion: une formation à l'éthique est-elle éthique ?

Le conflit éthique qui point dans le cours concerne d'abord les
formateurs et formatrices, avant de concerner les étudiants. En effet,
il s'agit pour elles et eux de montrer de la main différent conflits
éthiques et de mettre l'étudiant en situation de se questionner
réflexivement sur son positionnement éthique, plutôt que de lui donner
directement des outils « clés-en-main » pour appréhender ces
situations conflictuelles. Et il ne peut s'agir que de ça: C'est par
le cheminement éthique, par la réflexion, l'enquête rationnelle, par
la mise en doute axiologique que la conviction éthique survient.

> L'enjeu d'une science plurielle est sa capacité à expliciter ce qui
> habituellement est implicite, sa capacité à ouvrir la boîte noire des
> valeurs et à mettre en partage et en discussion les intentions. Plutôt
> que de revendiquer l'autonomie, il s'agirait plutôt d'assumer ses
> options, ses points de vues en les explicitant. Ce serait donc ainsi
> une forme de contrat social, qui accompagnerait et compléterait le
> contrat méthodologique de la
> science.[@CoutellecScienceAuPluriel2015, p.36]

Je crois que le cours est ici parvenu à ne pas proposer de solutions
_ad-hoc_ toutes faites, de comportements éthiques normés. Car s'il
convient de faire advenir une recherche réflexive et éthique, cela ne
passera que par la promotion informée d'une « docte ignorance »
salutaire, par la « justification épistémologique de la
démocratie »[@PutnamRenewingPhilosophy1995, page 180] dans les
sciences. S'il faut produire de la connaissance éthique aussi bien
qu'une éthique de la connaissance, c'est parce que la science ne peut
plus se poser comme guide positiviste de ce qu'il convient à la
société de faire. Elle doit se (ré)inscrire dans le monde social,
plutôt que de se présenter comme une classe d'experts à même de lui
montrer la voie:

> A class of experts is inevitably so removed from common interests as
> to become a class with private interests and private knowledge, which
> in social matters is not knowledge at all.[cité dans
> @PutnamRenewingPhilosophy1995, pp. 188-189]

Il aurait été alors malvenu de la part des formateurs d'adopter
précisément l'attitude qu'il convient de ne pas (plus) adopter en ces
problèmes éthiques. Encore une fois, c'est en montrant du doigt le
chemin qu'il semblerait à la science qu'elle doive emprunter (donc aux
chercheurs qui la font) plutôt qu'en le traçant pour elle, que l'on
peut seulement envisager de construire une justification
épistémologique de la démocratie. C'est là il me semble l'enjeu
principal qui attend le (futur) chercheur si l'on veut pouvoir
construire un monde qui fonctionne à la raison plutôt qu'à
l'ignorance, à l'idéal des Lumières plutôt qu'à l'obscurantisme
sceptique auquel conduit le relativisme, ou à la naïve foi aveugle
dans un scientisme positiviste dont on connaît
l'issue.[@TiercelinPourquoiNousAvons2015]

> | Marcheur, ce sont tes traces
> | Le chemin, et rien d'autre ;
> | Marcheur, tu marches sans chemin,
> | Le chemin se crée en marchant.
> --- @MachadoChampsCastille1937, XXIX

## Références

[^1]: [The Slow Science Manifesto](http://slow-science.org/)

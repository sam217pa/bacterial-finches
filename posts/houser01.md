---
title: L’esprit social et la fixation de la croyance
author: Nathan Houser
translator: Samuel Barreto
time: 2019-02-20
draft: false
abstract: >
    Un article de Nathan Houser paru dans Donna E. West and Myrdene
    Anderson, Consensus on Peirce’s Concept of Habit: Before and
    Beyond Consciousness (Springer, 2016), chapitre 21, pages 379-400.
---

_Résumé_: Pour survivre et vivre dans un monde sans cesse changeant,
les humains doivent apprendre de l’expérience et en conserver une
intelligence vitale d’accès immédiat à laquelle on peut recourir pour
résoudre des situations existentielles problématiques. Peirce
décrivait ce processus comme celui de l’enquête, une démarche d’essai
et d’erreur qui résout les dilemmes (les doutes) et forge des
habitudes (les croyances) qui s’accumulent en strates d’intelligence
pratiques, des programmes pour la survie. Ces strates d’intelligence
pratique sont les systèmes embarqués de croyances constituant le cœur
de notre esprit. Mais les humains aspirent à une intelligence d’un
genre plus théorique sans importance pratique ou vitale immédiate mais
satisfaisant d’insatiables désirs intellectuels et accroissant la
connaissance en général. La quête de la connaissance au sens large,
l’enquête scientifique, n’accouche pas d’un ensemble de croyances
fixées (comme l’intelligence pratique) mais seulement d’un ensemble de
croyances provisoires, jamais vraiment définitivement acceptées. Notre
esprit comprend aussi une intelligence théorique de ce genre. Mais
l’expérience humaine individuelle ne peut jamais atteindre
à l’intelligence pratique exhaustive nécessaire à la survie de la
civilisation et le cerveau humain n’est pas apte à l’accumulation de
l’intelligence théorique nécessaire au progrès de la science. La
survie et le progrès de la civilisation dépend d’une extension de
l’esprit par delà les organismes biologiques individuels en groupes
sociaux et institutions. Ceci s’accorde avec la conception peircienne
étendue de l’esprit et avec son idée que les organismes conscients
opèrent _dans_ un esprit qui, au moins en partie, leur est extérieur.
Non seulement les institutions développent des esprits propres, mais
certaines évoluent au point d’en devenir les réservoirs cruciaux de
l’intelligence définissant les cultures et perpétuant les
civilisations. Nous pourrions spéculer de ce que la religion est
l’institution humaine incarnant l’intelligence traitant des problèmes
d’importance vitale aux civilisations, quand l’institution de la
science incarne l’intelligence théorique traitant des aspirations
humaines à découvrir ce qu’il en est (_to find things out_) et notre
seul espoir d’une marche vers la vérité.

# Le rôle de l’habitude

Les écrits de Peirce regorgent d’idées clés distinctives et de
doctrines portant sur toute sa philosophie et l’unifiant. On peut
penser à ses différentes formes de catégories, tout comme à son
idéalisme objectif ou son faillibilisme. Le pragmaticisme de Peirce
est également une marque distinctive de sa philosophie, comme l’est sa
triade métaphysique --- agapisme, tychisme et synechisme ---, le socle
de sa métaphysique évolutive. On aurait du mal à isoler une conception
primordiale parcourant la pensée systématique de Peirce et lui donnant
de la cohésion. Sa persistance à s’identifier sa vie durant à un
logicien pourrait tendre à supposer que si une conception méritait
d’être reconnue d’une importance clé, ce pourrait bien être le
_raisonnement_, ou peut-être, l’_inférence_. Il y a de nombreuses
raisons de penser d’après les écrits de Peirce que rien ne fut plus
constant chez lui que son inquiétude pour l’accroissement de la
connaissance par des processus inférentiels, et pour la façon dont les
différents types d’inférence préservent ou contribuent à l’avancement
de la vérité. Néanmoins, on pourrait tout aussi bien supposer à raison
que la sémiose est la conception la plus cruciale sachant le rôle
fondamental des signes dans la philosophie de Peirce et dans la nature
inférientielle de toute action de signe. Mais comme les contributions
à ce volume le révèlent, la conception peircienne de l’habitude, qui
fut un point d’étude continu au long de sa vie depuis ses premiers
développements du pragmatisme à ses conceptions les plus avancées de
la sémiose, doit être classée comme d’une importance très élevée. Dans
trois domaines en particulier, le rôle de l’habitude est
particulièrement pertinent: (1) la fixation de la croyance, où
l’habitude, en tant qu’elle est croyance, est la fin de l’enquête; (2)
en sémiotique, où l’habitude, en tant qu’elle est l’interprétant
final, est la fin de la sémiose; et (3) en cosmologie, où l’habitude
en tant que loi est la fin de l’évolution. C’est des ramifications des
visions de Peirce du rôle de l’habitude dans le développement mental
et la formation des croyances que traitera cet essai.

# Une théorie de l’enquête doute/croyance

La théorie peircienne de la formation de croyances, issue de sa
présentation classique du pragmatisme dans ses articles du _Popular
Science Monthly_ de 1877-1878, fournit aux considérations qui suivent
un cadre général.^[Peirce introduisit le pragmatisme dans une série de
six articles publiés en 1877 et 1878 dans le _Popular Science
Monthly_. Ces six papiers conjoints devaient faire l’objet d’un livre
appelé « Illustrations de la Logique de la Science », et ont été
ré-édités dans les éditions principales des écrits de Peirce. Les deux
premiers articles sont vus comme la présentation classique du
pragmatisme: « La fixation de la Croyance » (W3: 242-257), « Comment
rendre nos idées claires » (W3: 257-276)] Converti au paradigme
évolutif, quoiqu’il ne le restreigne pas uniquement à la théorie de
Darwin, Peirce supposait de la pensée humaine qu’elle était un
processus naturel découlant des nécessités à résoudre les difficultés
et les troubles ayant cours dans l’expérience --- Peirce appelle ces
troubles des doutes et leurs résolutions des croyances. Ce processus
de résolution de doutes et d’acquisition de croyances (des habitudes,
ou programmes stockés, pour accélérer de futures interactions
expérientielles) est en essence une question de raisonnement pratique,
et l’ensemble de croyances construit au fil du temps une réserve
d’intelligence pratique.

C’est là l’essentiel de la théorie peircienne de l’enquête par
doute-croyance. Le doute survient au cours de la vie comme un état
d’irritation ou d’anxiété dû au besoin ressenti de répondre à un objet
ou un événement qui s’inscrit mal dans le cours usuel de
l’expérience --- aucune réaction comportementale pré-faite ne
s’avance. La réponse naturelle au doute est un état d’excitation
cérébrale et une activité physique d’essai et d’erreur que Peirce
appelle l’enquête. L’enquête cesse quand l’action réussie est
entreprise et que le doute s’appaise. Le processus d’enquête conduit
à la formation d’habitudes cognitives et comportementales que Peirce
appelle des croyances. La pensée, ou l’enquête, n’est plus requise
lorsque des habitudes satisfaisantes sont formées --- quand les
croyances sont fixées. Aussi le but naturel de la pensée est de
produire des croyances: « la pensée en acte n’a d’autre motif que
d’atteindre à la pensée au repos » (issue de « Comment rendre nos
idées claires », 1878: W3: 263; voir aussi EP 1: 129 et CP 5.396).
Dans « La Fixation de la Croyance », Peirce discute de quatre méthodes
de fixation de la croyance: la ténacité, l’autorité, la méthode _a
priori_, et la méthode scientifique. Bien que les trois premières
facilitent la formation d’habitudes (de programmation comportementale)
préparant à l’expérience future, c’est seulement par la méthode de la
science que les croyances formées le sont en interaction directe avec
l’expérience, sans préjugés. Un intérêt particulier de la méthode de
la science, pour Peirce, est qu’elle peut être utilisée pour le
progrès de la connaissance et non seulement pour ajuster le
comportement à des fins pratiques.

Deux aspects de la théorie peircienne de la croyance méritent d’être
soulignés. D’abord, au lieu de voir la croyance comme une attitude
envers une proposition, une position répandue parmi les philosophes de
l’esprit, la description comportementale de Peirce se concentre sur
les dispositions plutôt que les propositions. Quine voyait en la
théorie peircienne comportementale (dispositionnelle) de la croyance
un progrès majeur envers une philosophie naturalisée (Quine 1981:
36-37). Le deuxième aspect qu’il faut souligner est l’intérêt
explicite de Peirce pour les neurosciences, telles qu’elles étaient de
son temps. Il décrivait l’acte de pensée comme une cérébration
(d’après le brouillon du premier chapitre d’un travail sur la logique
1880: W4: 45; voir aussi EP 1: 200 et CP 3:155) et la formation
d’habitudes comme d’une « propriété du système nerveux » et la base
physiologique de l’apprentissage (d’après « A Guess at the Riddle »,
1887-1888: W6, voir aussi CP 1.390). Mais Peirce n’était pas le
réductionniste scientiste que tendent à être les philosophes
naturalisés d’aujourd’hui, sous l’emprise du nominalisme. Son réalisme
lui permettait de reconnaître le pouvoir de gouvernement des croyances
(habitudes) comme _règles_ d’action et d’appréhender les croyance dans
le contexte de causation finale. Les croyances ne sont pas simplement
des états cérébraux agissant efficacement, mais des états d’esprit
agissant sémiotiquement. Aussi la description peircienne de la
croyance commence par l’interaction entre un sujet, un être sensible,
et ce qui lui est extérieur, ainsi que par la supposition que le
bien-être du sujet implique des interactions satisfaisantes avec ce
qui lui est extérieur. Au cœur de l’expérience naissent les doutes et
commence l’enquête, ou la pensée, dont la fonction est d’éliminer
l’irritation du doute et de produire de la croyance. Ainsi, d’un côté,
les conséquences de la pensées s’étendent bien au-delà de la pensée
provoquant l’action. Les croyances comme états physiologiques
accomplissent ce rôle. Pourtant, considérées plus généralement comme
des états mentaux, les croyances ne fonctionnent pas seulement comme
programmes comportementaux embarqués mais comme _règles_
d’actions --- comme propositions conditionnelles. Dans « Comment
rendre nos idées claires », Peirce insiste sur le fait que le but de
la pensée en produisant des habitudes d’actions n’est pas de conclure
en actes individuelles, mais de produire des habitudes comme _règles_
d’action. Ce qui le conduit à la découverte clé de son pragmatisme,
c’est-à-dire, que toute distinction réelle de pensée, partant, toute
distinction de sens consiste en une différence _possible_ en pratique,
où l’emphase est sur « possible ».^[« Comment rendre nos idées
claires », 1878: W3: 265; voir aussi EP 1: 131 et CP 5.400.]

# Deux types de croyance

En 1898, dans une série de leçons à Cambridge, Peirce reconsidère la
question de la croyance, essentiellement en réponse à la parution de
l’essai de William James, « La Volonté de Croire ».^[James 1896; voir
Houser 2016 pour de plus amples discussions de la réaction de Peirce
à James.] James s’inquiétait de la diffusion de l’agnosticisme parmi
ses étudiants et collègues, et souhaitait fournir un appui au choix
d’une vie religieuse. Dans son fameux essai, James offre une
« justification de la foi » et défend le « droit de croire ». James,
formé en sciences, acceptait que les croyances soient fondées sur les
preuves dès que possible, mais il défendait le droit à écouter « notre
nature passionnelle » dans les cas où l’adoption de croyance est
inévitables et où l’intellect est entravé par l’absence de preuve.
« Notre nature passionnelle, écrit-il, non seulement peut à bon droit,
mais doit, opter pour l’une de deux propositions, dès lors que c’est
véritablement une option qui par nature ne peut être décidée sur des
bases intellectuelles. »^[Une véritable option de ce type est vitale,
contrainte et capitale. Pour plus de détails voir James (1896: 11).]
Il n’est pas clair que Peirce ait été en désaccord avec James sur la
valeur de la vie religieuse, pas plus au demeurant que sur l’influence
de notre nature passionnelle sur notre développement cognitif, mais il
s’est vivement opposé à la tentative de James de justifier l’adoption
_délibérée_ de croyances sans preuves --- c’est bloquer le chemin de
l’enquête et contrarier la volonté d’apprendre. James, pensait Peirce,
a succombé à la « tendance Hellénique à confondre philosophie et
pratique », alors que, du point de vue de Peirce, si la philosophie
avait à influer sur la religion et la morale elle devrait le faire
« avec une lenteur toute séculaire et les précautions les plus
vives ».^[« Philosophy and the Conduct of Life , 1898a: EP 2: 29; voir
aussi CP 1.620»] Peirce soulignait l’importance de la distinction
entre pratique et théorie en insistant particulièrement sur la nature
et la fonction des croyances dans la vie ordinaire par opposition
à leur rôle dans des démarches théoriques.

Une idée clé que Peirce développe dans ses leçons de 1898 à Cambridge
est qu’il y a deux types de croyances: celle de laquelle nous
dépendons sur des points d’importance vitale et celle que nous
soutenons provisoirement dans nos démarches intellectuelles. Les
croyances à proprement parler sont des habitudes cérébrales, des états
d’esprit, qui se développent dans le cours naturel de la vie à la
façon qu’a la nature d’adapter les espèces à un environnement donné.
Selon Peirce, la croyance est démontrée par une inclination
à agir --- et partant, nous croyons une proposition si nous sommes
prêts à agir selon elle. « La pleine croyance, dit Peirce, est la
disposition à agir d’après une proposition au cours de crises
vitales ».^[« Philosophy and the Conduct of Life », 1898a: EP
2: 33; voir aussi CP 1.635).] Ce sont des croyances naturelles, et
elles sont d’importance pratique, ou, comme le dit Peirce, « vitale ».

Le second type de croyance, que Peirce hésitait même à qualifier de ce
nom parce qu’il implique des états mentaux plus proches de l’hypothèse
et de peu sinon d’aucune importance pratique, est la croyance
théorique servant de jalon sur la route de la connaissance et de la
vérité. Bien que l’action doive être conduite dans la poursuite d’une
science pure, et sans tenir compte du fait que ses fruits puisse être
mis à contribution de façon importante, sinon vitale, le but de la
science pure n’est pas vraiment pratique, mais seulement de découvrir
ce qu’il en est --- « d’apprendre de l’univers ce qu’il a à nous
enseigner ».^[« La Première Règle de la Logique », 1898b: EP
2: 54, voir aussi CP 5.589] Les propositions scientifiques ne sont
jamais que provisoires et tout bon scientifique « se tient prêt à les
abandonner dès lors que l’expérience les dément ».^[« Philosophie et la
Conduite de la Vie », 1898a: EP 2: 33; voir aussi CP 1.635] Aussi
Peirce maintenait-il qu’il n’y a « en science aucune proposition
répondant au nom de croyance » --- aucune « pleine croyance » sur
laquelle on peut compter sans hésitation.

Ce que Peirce appelle enquête dans « La Fixation de la Croyance »,
l’activité cérébrale jaillissant au cœur de l’expérience aboutissant
en actions qui résolvent « l’irritation du doute » et érigent les
habitudes d’actions qu’il appelait croyances à proprement parler, est,
dans les faits, le _raisonnement naturel_. Une particularité du
raisonnement naturel et des croyances qu’il génère et qui contribuent
à l’intelligence pratique est que la vérité et la bien-fondé, les
valeurs centrales du raisonnement délibéré, sont pour une grande part
sans importance. Bien que les pragmatistes aient vu juste en soutenant
qu’on peut s’attendre à ce que les croyances qui s’avèrent être
pertinentes à long terme pour les aléas de l’expérience, les croyances
qui fonctionnent, voient leur congruence avec la vérité augmenter, ces
croyances naturelles ne sont pas adoptées parce qu’elles sont vraies
mais parce qu’elles résolvent des problèmes et dissolvent des
difficultés (comme dit Peirce, elles parviennent à traiter des
questions d’importance vitale). Il n’y a pas de doute que
l’intelligence humaine va bien au-delà de l’ensemble des croyances
naturelles traitant des questions vitales et comprend aussi une bonne
part de croyances théoriques qui étend la pensée humaine aux royaumes
de la science et de l’art, où la vérité et la beauté sont
primordiales. Néanmoins, c’est le raisonnement naturel et
l’intelligence pratique qu’il encourage qui sous-tendent la poursuite
de la vie humaine et témoignent de notre parenté aux autres formes de
vie de l’univers.

Comme Peirce poursuit sa distinction entre les deux types de croyance
dans les leçons de Cambridge, il souligne les limites du raisonnement
pour la conduite de la vie. Dans une situation réellement vitale, une
action sur-le-champ est essentielle; le raisonnement n’est pas une
option. Quelque chose comme un instinct est requis, une disposition
à agir immédiatement et efficacement dans de telles circonstances.
Pareilles dispositions, qui peuvent être activées en confiance en un
clin d’œil sans aucune délibération, sont ce que Peirce conçoit comme
des croyances à proprement parler. Il n’est pas très compliqué
d’imaginer pourquoi pareilles croyances nuiraient à la science, qui
recourt au doute et à la délibération.

Cependant, l’intransigeance de Peirce sur le fait qu’aucune
proposition en science ne répond au nom de croyance a pu être une
stratégie quelque peu hyperbolique dans le but de souligner la
distinction entre la théorie et la pratique.^[Voir CP 5.538 ff, où
Peirce discute plus avant la distinction entre les croyances
pratiques et théoriques.] Néanmoins, les croyances scientifiques
semblent bien pencher du côté de la théorie et Peirce était résolu
à distinguer l’enquête théorique de l’enquête pratique. « Il est
notoirement vrai, y insistait Peirce, qu’en tout ce dans quoi vous ne
vous jetez pas à corps perdu vous n’aurez que peu de succès. Enfin,
des deux maîtres, _théorie_ et _pratique_, vous ne pouvez tout deux
les servir. L’équilibre subtil requis pour observer le système des
choses est totalement perdu si les désirs humains entrent en jeu, et
cela d’autant plus qu’ils sont plus élevés et plus sacrés. »^[De
« Philosophie et la Conduite de la Vie », 1898a: EP 2: 34; voir aussi
CP 1.642]

# Esprit externe

Admettant, avec Peirce et les pragmatistes classiques, que l’esprit
fait intégralement partie du monde naturel et qu’un aspect essentiel
de la nature est sa croissance continuelle (l’évolution), admettant
aussi que la croissance naturelle de l’esprit suit plus ou moins le
modèle doute-croyance peircien, alors la tendance naturelle du
développement mental d’organismes conscients serait à l’accumulation
d’intelligence, sous forme de croyances ou d’habitudes de pensée,
préparant à l’expérience future. L’esprit, de ce point de vue, est
essentiellement un système de croyances (d’habitudes cognitives)
élaborées au cours de l’expérience et retenues parce qu’elles
contribuèrent à résoudre des troubles et doutes antérieurs, pouvant
être mis à contribution dans le futur lorsque les conditions
environnementales ressemblent à celles qui furent résolues par les
réponses que ces croyances actionnèrent dans le passé. Les opérations
mentales en jeu dans ce processus de résolution de dilemmes de
l’expérience en les situant dans un ensemble d’expériences antérieures
et des croyances associées, sont un processus rationnel quoique pas
nécessairement délibératif mais plus ou moins automatique, incluant
même une réponse instinctive: « nos pensées logiquement contrôlées
composent une petite part de l’esprit, à peine le bourgeon d’un vaste
complexe qu’on pourrait nommer l’esprit instinctif… ».^[« Pragmatisme
et la Logique de l’Abduction », 1093a, b, c, d: EP 2: 241; voir aussi
CP 5.212] Ce raisonnement naturel est un raisonnement pratique, et il
accomplit les fonctions naturelles de l’esprit qui sont de résoudre
les troubles de l’expériences et de programmer les organismes
conscients à un passage sûr et prospère dans la vie. Assurément cela
est pour l’essentiel correct, bien que Peirce rejette la supposition
que l’esprit ne réside que dans les organismes conscients et soutienne
une conception plus générale de l’esprit qui permet de dire de façon
plus précise que les organismes conscients opèrent au sein d’un esprit
qui, du moins en partie, leur est extérieur. Et, comme ce qui suit le
développe brièvement, il pourrait aussi y avoir des institutions
conscientes qui ne soient pas des organismes à proprement parler.

C’était la thèse de Peirce que le véritable sujet de la psychologie
est l’esprit et il a maintes fois fait remarquer que l’erreur que les
psychologues commettent si souvent est de se concentrer presque
exclusivement sur la conscience. La concience, soutient Peirce,
« n’est vraiment en soi rien que sensation… guère plus qu’une
propriété du protoplasme, peut-être seulement de la matière
nerveuse ».^[Issue, comme la citation suivante, de « Minute Logic »,
1901-1902: CP 7.364.] Peirce admettait que les « organismes
biologiques », et particulièrement, les systèmes nerveux, « sont
conditionnés favorablement à l’exhibition du phénomène de l’esprit »,
aussi n’est-il pas « surprenant que l’esprit et la sensation soient
confondus ». Selon Peirce, « la philosophie moderne n’a jamais été en
mesure d’ébranler l’idée Cartésienne de l’esprit, comme une chose qui
“réside”… dans la glande pinéale. Tous en rient de nos jours, et
pourtant continuent de penser l’esprit de la même façon générale,
comme quelque chose au sein de… »^[« Les Trois Sciences Normatives »,
1903b: EP 2:199; voir aussi CP 5.128.] Mais Peirce voyait là une
conception trop étroite de l’esprit. La psychologie ne serait
« rectifiée », dit-il, que lorsqu’il sera compris que « la sensation
n’est rien d’autre que l’aspect interne des choses, quand au contraire
l’esprit est essentiellement un phénomène externe. … À mon sens il est
bien plus vrai que les pensées d’un auteur résident dans les pages
imprimées de ses livres qu’elles ne résident dans son
cerveau. »^[Issue, comme la citation suivante, de « Minute Logic »,
1901-1902: CP 7.364.]

Peirce en concluait que la conscience est une question relativement
simple, quand l’esprit, « une fois appréhendée la vérité que ça n’est
ni la conscience ni n’est proportionné d’aucune façon à la
conscience, est une chose très difficile à analyser » (CP 7.365).
Écrivant à William James en 1904 à propos de l’article de James, « La
“conscience” existe-t-elle? », Peirce souligne que l’expérience
consciente, ou ce qu’il appelle parfois le « contenu de la
conscience », se répartit en trois classes: la sensation, la conscience
bilatérale, et la pensée (CP 8.281-283).^[Peirce utilise plusieurs
noms pour ces trois forme de conscience: dans CP 7.551 il appelle la
sensation les “primisens”, la conscience de l’autre “altersens”, et
la pensée “medisens”.] Eut égard aux catégories ubiquitaires
peirciennes, ces trois classes sont la conscience de la priméité, de
la secondéité et de la tertiarité. Peirce dit à James qu’il inclut la
pensée, le monde des relations triadiques, dans la conscience
seulement parce que « nous savons qu’elle est », mais qu’elle n’est
dans la conscience en aucune façon de la même manière que la priméité
et secondéité le sont. C’est ce monde de relations triadiques, dit
Peirce à James, qui constitue le contenu principal de l’esprit. Dans
sa quatrième leçon à Harvard en 1903, Peirce dit que « La tertiarité,
tel que j’emploi ce terme, n’est qu’un synonyme de
Représentation ».^[« Les Sept Systèmes de la Métaphysique », 1903c:
EP 2: 184; voir aussi CP 5.105.] Plus tard cette année, dans le
programme de ses Leçons de Lowell, il dit qu’il utilise esprit comme
synonyme de représentation et plaisante que ses auditeurs devraient
garder à « l’esprit que cet esprit n’est pas l’esprit que les
psychologues ont à l’esprit s’il l’esprit leur est de quelque
intérêt. »^[They should « bear in mind that this mind is not the mind
that the psychologists mind if they mind any mind. », (R 478).]

Ce monde de relations triadiques, ou représentation, est l’univers peircien
des signes. L’une des découvertes les plus précoces de Peirce était
que toute pensée est signe et par « pensée » il entendait inclure tout
événement mental conscient: « dès lors que nous pensons, est présent
à la conscience quelque sensation, image, conception, ou tout autre
représentation, qui sert de signe. »^[« Some Consequences of Four
Incapacities », 1868: W2: 223; voir aussi EP 1: 38 et CP 5.283.]

Mais Peirce admettait qu’il y a également des événements mentaux qui
fonctionnent sémiotiquement, et partant doivent être vus comme de la
pensée en un sens général. Aussi pouvons-nous reformuler la théorie
doute-croyance de l’enquête peircienne en termes sémiotiques. Sans
entrer trop loin dans les détails techniques, nous savons que tout
signe sert de médiateur entre ses objets et leurs interprétants: « Un
signe est, dans quelque mode d’être que ce soit, tout ce qui sert
d’intermédiaire entre un objet et son interprétant; puisqu’il est à la
fois déterminé par l’objet _relativement à l’interprétant_, et
détermine l’interprétant _en référence à l’objet_, de telle sorte
qu’il entraîne l’interprétant à être déterminé par son objet par le
moyen de ce “signe”. »^[Issu du fameux manuscrit de Peirce, R 318,
1907: EP 2: 410; ce manuscrit, dont une portion substantielle est
publiée dans EP 2: 398-433, est la source du reste de la discussion de
la sémiotique dans ce paragraphe.] Nous savons de plus qu’il y a trois
sortes d’interprétants: émotionnels, énergétiques, et logiques.
L’interprétant logique est à prendre essentiellement au futur de
l’indicatif, ce que Peirce appelle un « would-be », et constitue
« l’appréhension intellectuelle du sens d’un signe ». Peirce soutenait
que, alors que les concepts, les propositions ou les arguments peuvent
être des interprétants logiques, seules les habitudes peuvent être des
interprétants logiques finaux. L’habitude seule, quoiqu’elle puisse
être signe de quelque autre façon, n’appelle pas à plus
d’interprétation. Elle appelle à l’action. D’après cette esquisse de
la théorie Peircienne des signes, il semblerait que la démarche
naturelle de l’enquête, initiée par l’irritation du doute, consiste en
une sémiose qui commence au niveau des sensations, génère des
interprétants émotionnels et passe rapidement au niveau actif des
interprétants énergétiques. Mais c’est seulement lorsque la sémiose
(l’enquête) s’achève en une habitude, l’interprétant final, que
l’« irritation » s’apaise et que l’enquête touche à sa fin.
L’identification peircienne de la pensée à la sémiose, et sa
reconnaissance de ce que les signes sont des transactions externes, le
conduit à ce qu’il appelle une conception logique de l’esprit, selon
laquelle l’esprit est un système de signe et la pensée l’action d’un
signe ou sémiose.

Aussi Peirce conclut-il que la pensée est intrinsèquement un processus
externe et l’esprit un réseau relationnel de signes (consistant
fondamentalement en conceptions générales entrelacées et de leurs
habitudes interprétatives associées) auquel nous contribuons et au
sein duquel nous opérons, mais qui n’est pas vraiment _nôtre_. Peirce
ne croyait pas que la pensée est nécessairement liée au cerveau, mais
qu’elle est opérante dans un processus sémiotique à l’œuvre dans les
groupes et dans le monde physique externe.^[Voir « Prolégomènes à une
Apologie du Pragmaticisme », 1906: CP 4.551; cf. n. 6 plus bas.]
Peirce étend la portée de l’esprit de telle sorte qu’elle englobe tout
signe d’action --- tout processus guidé par un but ou une causation
finale. L’esprit est au fondement de toute sémiose.

# Esprit Social

Selon la conception peircienne généralisée, l’esprit a commencé son
développement dans l’univers à l’origine de l’action de signe, ou en
d’autres termes, avec l’apparition de la formation d’habitudes^[voir
Houser 2014a: 9-32.] Çe fut le commencement de ce qui, au cours d’une
lente évolution, se développe en un réseau relationnel de signes et
d’habitudes interprétatives qui constituent les esprits à part
entière. Peirce savait que les états mentaux doivent être incarnés
d’une manière ou d’une autre, mais, comme Hilary Putnam plus d’un
siècle après, il comprenait que l’essence d’un état mental est logique
ou fonctionnel, non physique, et que l’esprit ne dépend pas de son
incarnation (_embodiment_), tout comme un programme informatique ne
dépend pas de son incarnation.^[Voir CP 7.364, de « Minute Logic »
1901-1902; Putnam introduisit son analogie informatique dans Putnam
1960.] Peirce compris aussi que le développement de l’esprit,
l’aggrégation et l’entrelacement de conceptions et d’habitudes
interprétatives constituant l’esprit proprement dit, dépend d’un
substrat conscient et axé sur un but (pour le dire vaguement) mais que
la conscience requise peut être distribuée à travers un nombre
incommensurable de sous-systèmes individuels ou même sur de longues
périodes de temps. De plus, tout comme il réalise que les pensées
d’une autrice sont littéralement plus dans ses livres que dans son
cerveau, Peirce savait qu’un esprit opératoire peut être incarné dans
les institutions, les pratiques cultures et les traditions, même dans
les artéfacts.^[Cette distinction est similaire à celle opérée par les
philosophes de l’esprits contemporains et les sciences cognitives
entre les engrammes et les exogrammes --- voir Adams et Aizawa (2010:
144).] En fait, Peirce pense que c’est cet esprit étendu qui sert de
fondement cognitif, de base, à la pensée humaine (sémiose).

Il est souvent supposé qu’à cause du fait que Peirce était un
idéaliste objectif auto-proclamé, soutenant que la « matière est un
esprit effacé, habitudes invétérées devenues lois
physiques »,^[« L’Architecture des Théories », 1890: W8: 106; voir
aussi EP 1: 293 et CP 6.25] il devait avoir supposé que l’esprit est
primordial, mais ceci est erroné. Bien que les détails de la
cosmologie de Peirce soient fluctuants, disputés même (e.g. Houser
2014a: 17-18, notes comprises), il est évident que dans le chaos
pré-historique que Peirce posait comme l’« œuf de l’univers », ni
esprit ni matière n’étaient présents. Peirce était un évolutionniste
convaincu et sa cosmologie, qu’il présentait comme une description
logique de l’origine de l’univers, dépeint le chaos primitif comme un
état d’indétermination et de possibilité duquel, simplement par
chance, quelque événement aléatoire acquit une « qualité de
persévérance balbutiante » et une tendance à acquérir des habitudes
fut initiée.^[CP 6.204; voir aussi RLT: 262.] Ceci marquait le début
de la sémiose et de l’esprit (posant ainsi le décor pour un idéalisme
objectif).

Peirce était fermement convaincu que l’esprit surgit dans le « monde
inorganique » et que la pensée (sémiose) s’y développa d’abord (CP
4.551).^[Notez que « organique » fut imprimé par erreur dans _Le
Moniste_ et reproduit dans les _Collected Papers_. Dans le manuscrit
originel de Peirce (la copie de son imprimeur), il écrivit clairement
« inorganique ».] Si un substrat conscient, ou quasi-conscient était
nécessaire pour le commencement originel primitif de la prise
d’habitude même, alors ce devait être ce dont Peirce supposait qu’elle
était une « vie d’arrière-plan psychique » inconsciente, que l’on
comprend sans doute mieux comme une force attractive qui, de quelque
manière que ce soit, imprégna les événements d’une tendance à se
repéter et se reproduire --- une volonté de survivre
naissante.^[Arthur Burks, éditeur des volumes 7 et 8 des éditions
Harvard des papiers de Peirce, rejetta le panpsychisme de Peirce mais
le croyait ingénieux pour l’époque. Voir Houser (2014a: 29).] Mais
quelle que soit la façon dont Peirce concevait cette vie psychique
d’arrière-plan, il était attentif à la distinguer de l’esprit
originel, qui émergea du chaos primitif seulement à la naissance de la
formation d’habitudes et s’épanouit en un réseau relationnel
d’habitudes, déployé et évolué en systèmes sémiotiques fonctionnels.
Lorsque l’esprit se développa dans le monde organique, il ne le fit
pas exclusivement au sein du cerveau de créatures conscientes, mais
aussi comme programmes de comportement de groupes pareils à ceux
d’essaim d’abeilles ou de colonies de fourmis (e.g. CP 4.551). Le
point central est que la conscience, ou la sensation, n’est pas un
attribut essentiel de l’esprit: « la sensation n’est rien que l’aspect
interne des choses, quand l’esprit, au contraire, est essentiellement
un phénomène externe ».^[« Minute Logic », 1901-1902: CP 7.364]
Aussi, tout comme Peirce disait que « nous sommes dans la pensée
plutôt que les pensées sont en nous »,^[« Quelques conséquences de
quatre incapacités », 1868: W2: 227, n. 4; voir aussi la première note
de bas de page de Peirce dans EP 1: 42 et CP 5.289, n. 1] il eut pu
tout aussi bien dire que nous sommes dans l’esprit plutôt que l’esprit
essentiellement en nous. Peirce y insista vigoureusement en commentant
la façon dont la connaissance est acquise: « toute connaissance nous
vient de l’observation, pour partie imposée à nous de l’extérieur par
l’esprit de la Nature et pour partie provenant de cet aspect intérieur
de l’esprit, que nous appelons égoïstement _nôtre_; bien qu’en vérité
ce soit bien plutôt nous qui flottons à sa surface et lui appartenons
qu’il ne nous appartient ».^[« Of Reasoning in General », 1895a, b: CP
7.558; voir aussi EP 2: 24.]

Pour Peirce, _l’esprit de la Nature_ fut une conception clé. Il
comprit que le système de lois naturelles ayant évolué pour diriger
l’opération physique de l’univers constituait un esprit généralisé,
récapitulé en certains de ses aspects les plus cruciaux par les
sous-esprits développés sous son influence. Alors que de simples
organismes biologiques évoluaient, la sélection naturelle dut avoir
favorisé les espèces, qui dans leur ensemble, donnaient corps à une
intelligence adaptative, ce genre d’intelligence distribuée ou en
nuées que montrent les comportements de masse d’insectes, de banc de
poissons ou de nuée d’oiseaux. La survie d’une espèce dépend de son
adéquation avec l’esprit de la nature, ce dont Peirce pensait qu’elle
est le fait d’un programme instinctif. Comme la capacité
intellectuelle d’organismes vivants augmentait, d’avantage de
programmation comportementale put être enracinée dans les esprits
individuels, mais la clé de la survie fut le profond programme
instinctif à l’échelle de l’espèce accordant l’esprit de l’organisme
conscient à celui de la nature. Même les humains, qui, pour autant que
l’on sache, ont de plus grandes capacités mentales individuelles que
tout autre organisme vivant, et partant sont capables, par le biais de
la démarche d’enquête doute-croyance, d’amasser d’importantes
habitudes comportementales des points de vue environnementaux et
culturels (des croyances naturelles), même eux restent énormément
dépendants de découvertes instinctives, d’un don naturel, conséquence
de ce que nos esprits (les esprits des membres de notre espèce) ont
évolué sous l’influence des lois de la nature.^[« How to Theorize »,
1903d: CP 5.604.] En fait, comme noté plus haut, Peirce supposait que
la portion d’un esprit individuel développée par l’enquête n’est que
le « simple bourgeon » du « vaste complexe » qui constitue l’esprit
instinctif.^[« Pragmatism as the Logic of Abduction », 1903: EP 2:
241; voir aussi CP 5.212.] Parfois Peirce faisait référence à cet
esprit instinctif comme la « lumière de la nature » ou une « lumière
interne de la raison, » qui, parce qu’elle nous accorde à la nature,
nous équipe d’une capacité adéquate à une inférence abductive
réussie --- suffisamment pour nous fournir les armes nécessaires à la
survie lorsque nous sommes confrontés à de dangereux troubles de
l’expériences.^[Voir CP 2.24 où Peirce spécule que l’idée de la
lumière de la raison, ou de très proches conceptions, peut
probablement être trouvée dans la plupart des cultures. Il mentionne,
par exemple, « le “vieux philosophe” de Chine, Lao-Tseu » et « la
vieille philosophie Babylonienne du chapitre premier de la Génèse. »]
Mais pour diriger la moindre démarche d’enquête, l’esprit instinctif
doit opérer sémiotiquement, car toute pensée est signe, et Peirce nous
dit que tout signe s’adresse à une pensée future et ainsi, en effet,
à d’autres esprits (ou des états subséquents de l’esprit signifiant).

Le dialogue est le paradigme peircien de la sémiose (voir Colapietro
1989: 22), ce qui implique que tout esprit est structuré de façon
à opérer dans un contexte social. Pour qu’un locuteur et un
interpréteur communiquent, Peirce pensait que des parties de leurs
esprits respectifs devaient fusionner en un seul, qu’il appelait les
« commens ».^[D’après des ébauches de lettres à Victoria Lady Welby,
1906: EP 2: 478; voir aussi Peirce/Welby 1977: 197.] Aussi croyait-il
que ce que nous pouvons penser comme un esprit partagé peut opérer
à travers des individus séparés, à travers même de larges processus de
groupe, mais il peut également y avoir des esprits plus autonomes
produits par l’enquête sociale (la pensée commune) et largement
distribués à travers les groupes dans les sous-esprits de leurs
membres. La pensée humaine est beaucoup plus dépendante de cet esprit
social qu’il n’est généralement admis. Peirce soutenait sans ambiguïté
que « l’homme est essentiellement un animal social » (CP 1.11),
« simple cellule de l’organisme social »^[« Philosophy and the Conduct
of Life », 1898a: EP 2: 40; voir aussi CP 1.647] et que « tout ce qui
revêt un sens important pour lui ne reçoit son interprétation que de
considérations sociales ».^[Issu d’un fragment non-identifié:
R 1573.273. Pour une discussion de cette citation, voir Houser 2014b.]

Selon cette façon de penser, nos communautés sociales et associations
culturelles peuvent être des institutions douées d’esprits, réservoirs
d’_habitudes sociales_ --- les stocks de croyances sociales de notre
culture.^[L’idée des croyances sociales a été défendue et discutée par
Émile Durkheim et Margaret Gilbert, entre autres.] L’intelligence
acquise au fil du temps par les esprits sociaux actifs, bien que
partiellement instanciée dans les systèmes nerveux distribués (les
sous-esprits) des membres individuels des institutions sociales en
question, peut être engrangée de façon plus permanente dans les
pratiques culturelles et les traditions, dans les documents écrits et
le langage lui-même, dans les œuvres d’art et les compositions
musicales, dans les artéfacts de toute sorte --- dans quelque support
que ce soit de la civilisation.^[Voir la note plus haut, quant à la
distinction entre les engrammes et les exogrammes.] Mais les esprits
sociaux ne sont pas de simples réservoirs de croyances sociales; ce
sont des programmes opérants et des réseaux relationnels pour une
sémiose distribuée en cours --- les esprits animés des groupes sociaux
dont ils régulent le comportement. Non seulement Peirce attribuait un
esprit aux institutions et groupes sociaux, il croyait qu’il
y a quelque chose comme une « conscience personnelle » dans les
groupes de personnes qui sont « en communion intime et intensément
sympathique ».^[De « Man’s Glassy Essence », 1892a, b: W8: 182; voir
aussi EP 2: 313 et CP 6.271.] Peirce appelait ce genre de groupes
sociaux de « grandes personnes » --- des communautés vibrantes d’une
personnalité collective.^[Voir W8: 183 (également EP 1: 350 et CP 6:
271), de « Man’s Glassy Essence », (1892a, b), pour les usage
peirciens de l’expressions « grandes personnes » et W8: 196
(également EP 1: 364 et CP 6.307), de « Evolutionnary Love », 1892,
pour son emploi de la « personnalité collective ».] Josiah Royce,
autrefois disciple de Peirce, exprimait bien la vision qu’il partage
avec lui:

> Une communauté n’est pas simple collection d’individu. C’est un
> genre d’unité vivante … [qui] croît et décroît, est saine ou malade,
> vieille ou jeune, tout comme chaque membre individuel de la
> communauté possède de tels caractères. … Non seulement la communauté
> vit, mais elle a un esprit propre, --- un esprit dont la psychologie
> diffère de celle d’un être humain individuel. L’esprit social montre
> ses traits psychologiques dans ses produits
> caractéristiques, --- dans le langage, les coutumes, les
> religions, --- produits qu’un esprit humain individuel, même une
> collection de tels esprits, lorsqu’elle n’est pas d’une certaine
> façon organisée en une véritable communauté, ne peut produire.
> Pourtant le langage, la coutume, la religion sont tous de ces
> produits mentaux véritables (Josiah Royce 1918: 80-81).

La recherche actuelle sur l’esprit, dans cette perspective, est
généralement conduite par la psychologie sociale (qui étudie le
comportement collectif)^[Pour un aperçu utile de l’esprit social du
point de vue de la psychologie sociale et de son histoire, voir Forgas
et al. (2001). Voir aussi Valsiner et van der Veer (2000).] ou des
sciences cognitives (qui étudient la cognition distribuée),^[Pour un
bon aperçu, voir Adams et Aizawa (2010).] et afin d’évaluer l’idée de
Peirce et lui rendre justice, de considérables efforts seraient
requis pour tenter de comparer et relier son travail avec la recherche
en cours. Pour important que soit cet effort, le but de cet essai est
beaucoup plus modeste et essentiellement conjectural, c’est-à-dire
que, si nous prenons au sérieux l’idée d’esprits sociaux, non comme de
simples stocks d’intelligence mais comme des programmes opérants et
des réseaux relationnels de sémiose distribuée en cours, alors nous
pourrions supposer que ces esprits sociaux sont, au moins
indirectement, sujets à la théorie peircienne de la formation et
fixation de croyances que nous associons d’ordinaire aux esprits
humains individuels. De plus, l’insistance controversée de Peirce sur
la différence cruciale entre les croyances naturelles relevant de
questions d’importance vitales et les croyances théoriques relevant de
la science pure pourrait être d’une pertinence instructive dans la
compréhension des institutions sociales.

Quoique l’idée d’un esprit social et de son rôle essentiel dans
l’émergence et le fonctionnement des esprits humains individuels et le
développement de conceptions de soi puisse être attribués à plusieurs
sources (voir notes plus haut), c’est généralement à George Herbert
Mead que l’on reconnaît le rôle majeur dans le développement
historique de cette approche. Travaillant au sein de la tradition
pragmatiste américaine, particulièrement sous l’influence de son
collègue, John Dewey, et de son professeur d’université, Josiah Royce,
Mead proposa une théorie générale de l’esprit, attentive à ses
fondements neurologiques dans les organismes conscients mais reposant
sur les fondations plus larges de l’esprit dans l’interaction entre
organismes --- dans leur environnement social.^[Voir en particulier
Mead 1934. Le behaviorisme social de Mead marque le début de l’école
de sociologie connue sous le nom d’interactionnisme symbolique.] Mead
soutenait que l’esprit se développe dans ces interactions sociales,
particulièrement celles impliquant le langage, et que la conscience de
soi individuelle en est un sous-produit: « Le soi est moins une
substance qu’un processus dans lequel la conversation par geste a été
internalisée sous une forme organique. Ce processus n’existe pas en
lui-même, mais est simplement une phase de toute l’organisation
sociale dont l’individu fait partie. L’organisation de l’acte social
a été importé dans l’organisme et devient son esprit » (Mead 1934:
178).

La similitude entre les conceptions de l’esprit de Peirce et Mead est
étonnante, au point que l’on se demande pourquoi l’œuvre de Peirce ne
figure pas de façon centrale dans le développement de la psychologie
sociale.^[Pour une discussion de l’influence de Peirce sur le
développement de la sociologie, voir Wiley (2006).] Il serait
particulièrement intéressant de comparer et contraster les visions de
Peirce et Mead sur la conscience de soi et ses implications pour
l’identité personnelle, mais cela est un autre sujet.^[Pour quelques
contributions à ce sujet, voir Nguyen (2011); Singer (1991); Wiley
(1994); Gelpi (2008).] La théorie de Mead de l’esprit social n’est
mentionnée ici que pour illustrée en quoi la théorie antérieure de
Peirce concorde avec un fil de pensée pragmatic courant de Royce et
Dewey à Mead et s’épanouissant ensuite dans le mouvement de la
psychologie sociale. Mais, pour les buts présents, ce qui est
important est que Peirce proposa une théorie sociale de l’esprit,
soutenant non seulement qu’il y a un esprit social externe à, et
séparé de, la mentalité humaine individuelle (qui en est néanmoins,
une conséquence), mais aussi que l’esprit social constitue son propre
réseau de croyances et peut même éventuellement illustrer un caractère
de personnalité unique.

# La Fixation de la Croyance Sociale

Dans le reste de cet essai, je vais m’étendre brièvement sur ma
conjecture que l’esprit social est sujet à la théorie percienne de la
formation et fixation de croyance, et que son insistance controversée
sur la différence cruciale entre les croyances naturelles et
théoriques est d’une pertinence instructive à la compréhension des
institutions sociales. Une étude approfondie de la théorie de Peirce
des croyances sociales impliquerait de se pencher plus avant sur les
différentes façons dont les croyances se fixent dans les esprits
sociaux. Probablement, les quatre méthodes décrites par Peirce dans
« La Fixation de la Croyance » sur la manière dont les croyances se
fixent dans les esprits humains individuels ont leurs compléments en
psychologie sociale, mais à quelques modifications près, la méthode de
l’autorité devrait être la plus pertinente: cette méthode, dit Peirce,
traite du problème de « comment fixer des croyances, non seulement
dans l’individu, mais dans la communauté ».^[De « La Fixation de la
Croyance », 1877: W3: 250; voir aussi EP 1: 177, CP 5.378 et W3: 25
pour une variante antérieure.] Et peut-être que la méthode de la
science trouve son complément dans les pratiques de recherche de la
communauté scientifique, au moins idéalement. Il serait également
nécessaire d’examiner plus attentivement les considérations plus
explicites de Peirce sur l’évolution de l’esprit, ou ce qu’il appelle
quelque part « le développement historique de la pensée
humaine ».^[« Evolutionnary Love », 1892: W8: 196; voir aussi EP 1:
363 et CP 6.307.] Guidé par ses catégories, Peirce identifie trois
modes principaux d’évolution mentale --- thycastique, anancastique et
agapastique --- et prétend que le « développement agapastique de la
pensée » dépend de la sympathie et de la continuité de l’esprit.^[Un
bon traitement de la philosophie évolutionniste de Peirce peut être
trouvé chez Hausman (1993).] Il dit que ce genre de développement
« peut affecter tout un peuple ou une communauté dans sa personnalité
collective, et être ainsi communiquée à tout individu en connection
sympathique puissante avec le peuple collectif, quoiqu’il puisse
n’être pas en mesure intellectuelle d’atteindre à l’idée de sa propre
compréhension ni même peut-être de l’appréhender consciemment ».^[De
« Evolutionnary Love », 1892: W8: 196; voir aussi EP 1: 364 et CP
6.307. Comme il est d’usage avec Peirce, il identifiait trois sortes
de développement agapastique.] Mais un examen exhaustif de la
description peircienne de l’évolution de l’esprit et de la formation
de croyances sociales va bien au-delà de la portée de cet essai. Pour
cet examen préablable, la distinction qu’opère Peirce entre les
croyances naturelles et théoriques fournit un schéma stimulant
quelques réflexions préliminaires importantes.

Comment l’esprit social évolue est la question centrale.
Vraisemblablement, dans la lutte évolutive pour la survie des espèces,
les organismes vivants (en considérant principalement les humains) ont
acquis des sympathies sociales et des sensibilités qui les ont
rassemblés en groupes accordés où, avant toute chose, ils avaient
l’avantage du nombre. D’une importance toute particulière est la
symbiose qui se développe entre les individus et les institutions
sociales. Bien que les individus dépendent en partie d’un programme
instinctif et de l’apprentissage direct de l’expérience pour les
guider dans la vie, il dépendent particulièrement de l’esprit social
des organisations et des institutions auxquelles ils appartiennent
pour les programmes comportementaux ayant trait aux pratiques sociales
et aux identités culturelles. Vraisemblablement, l’affinité naturelle
qu’ont les humains pour leur propre genre, et leur penchant à la
cohésion sociale, encouragent la formation d’institutions sociales et
de systèmes qui, d’une manière proche des instincts, fixe, ou du moins
promeut, des schèmes comportementaux (y compris des habitudes ou des
schèmes de pensée) conduisant généralement au bien-être individuel
mais augmentant aussi la viabilité à long-terme des institutions
sociales elles-mêmes. Les institutions les plus réussies, celles qui
survivent et prospèrent à travers les âges, accumulent de vastes
systèmes d’informations utiles, comme indiquées dans la section
précédente. Ces systèmes d’informations utiles, les habitudes
bénéfiques distillées d’essais et d’erreurs, et les _succès_ de
générations d’expérience humaines, constituent les esprits sociaux des
institutions définissant la culture et perpétuant les civilisations.
À travers ces esprits sociaux, les individus peuvent s’appuyer sur des
croyances conséquentes, et sur des systèmes de croyances qu’ils
n’auraient jamais pu développer (programmer) d’eux-mêmes, augmentant
d’autant leur niveau d’accès à une intelligence d’intérêt.

Dans le même temps, les institutions douées d’esprits dépendent des
individus séparés qu’elles soutiennent pour leur soutien en retour, en
les animant par le bias des sous-esprits collectifs entrelacés en un
réseau étendu de systèmes sémiotiques. Les civilisations sont pétries
d’institutions et d’organisations sociales de toute sorte aux buts
multiples --- incluant la sécurité, la communication, l’éducation,
l’esthétique, l’élévation spirituelle, le divertissement, le confort
et la camaraderie. La fonction prévalente des institutions sociales
est de programmer les comportements individuels de telle sorte qu’ils
propagent les attitudes qu’elles promeuvent. Pour accomplir cette
fonction, les institutions accumulent et fournissent l’accès à toute
sorte d’intelligence spécialisée requise. Ces réservoirs
institutionnels d’intelligence sont les esprits sociaux étendus, dont,
tout comme l’esprit de la nature, nous dépendons constamment mais
appelons égoïstement nôtres.^[Voir CP 7.558, cité plus haut dans la
section sur l’esprit social.] Ces institutions douées d’esprits, afin
d’accomplir leur rôle évolutif dans la civilisation humaine,
deviennent elles-mêmes de grandes personnes, pour employer la
terminologie de Peirce, ou de véritables communautés, pour employer
celle de Royce, aux buts évolutifs propres. C’est un point clé. Les
esprits sociaux animés des institutions dont les individus dépendent
le plus sont des systèmes sémiotiques épanouis sujets à des pressions
évolutives, ainsi qu’à tout ce qui croît et évolue en interaction avec
des forces environnementales en mesure d’éliminer et sélectionner.

Cet anthropomorphisation apparente des institutions sociales semblera
étrange à certains lecteurs. C’est une chose d’attribuer une étendue
à l’esprit, en tant que système cognitif, des humains conscients à des
systèmes inconscients ou des institutions. C’est dans la même veine
que de supposer que les livres de nos bibliothèques ou nos iPhones
font partie de notre système cognitif étendu --- ou, comme disait
Peirce de Lavoisier avec admiration, il portait « son esprit dans son
laboratoire, et a littéralement [fait] de ses alambiques et cornues
des instruments de pensée … manipulant des choses réelles au lieu de
mots et de fantaisies ».^[« La Fixation de la Croyance », 1877: W3:
243-244; voir aussi EP 1: 111 et CP 5.363.] C’est une tout autre chose
que de supposer que les institutions sociales ont un esprit opérant
propre. Sans doute que les esprits, en tant que réseaux d’information,
peuvent s’étendre à des structures inconscientes, mais assurément,
comme il est ordinairement admis, des processus de cognition réels
doivent avoir lieu dans des cerveaux vivants.^[C’est plus ou moins la
vision avancée par Adams et Awaiza (2010), qui arguent que « les
processus cognitifs [même dans des systèmes cognitifs étendus] sont
seuls trouvés dans le cerveau. » (2010: 146).] Je crois que cette
vision associe trop étroitement l’esprit à la conscience, tout comme
Peirce reprochait aux psychologue de tendre à le faire. L’unité
systématique d’un esprit dérive du genre d’expérience qui l’a
programmé, et de la façon dont le réseau d’interprétants (le contenu
de l’esprit) concorde eut égard à un but, ou peut-être à un caractère.
Quelle que soit la conscience nécessaire à une institution pour
s’engager activement dans les affaires en cours et le moment présent,
elle est fournie par la communauté d’humains qui dépendent de, et
animent, cette institution. Ce qui attribue à l’institution une
conscience distribuée, bien que, évidemment, les institutions n’aient
pas de système nerveux propres.

Je crois que les institutions, cependant, développent un esprit qui
leur est propre, qui avec le temps devient beaucoup plus indispensable
pour la préservation et le progrès de la culture, de la civilisation
elle-même, que ne le sont les esprits humains individuels. D’une
importance toute particulière est que les institutions sociales
vitales à l’épanouissement humain attirent à elles des individus qui
accomplissent leurs tâches et étendent leur influence. C’est cette
dépendence des humains individuels, et de la société en général,
à l’intelligence sociale programmée dans des esprits institutionnels
qui garantie aux organisations sociales et aux institutions un rôle
dominant dans l’ordre des choses. Je suis d’avis que l’esprit social
des institutions se développent et évoluent plus moins selon le
processus doute-croyance que décrit Peirce pour l’esprit individuel.
Vraisemblablement, l’esprit social reste au repos, organisant les
conduites selon des croyances établies (des habitudes
comportementales) pré-programmées, jusqu’à ce qu’il soit perturbé par
le doute. L’irritation du doute, pour un esprit social, survient
probablement au sein d’une communauté d’individu conscients, membres
de l’institution en question ou dépendants d’elle, probablement le
fait d’un conflit expérientiel ou d’une perturbation pour laquelle les
réponses approuvées par la communauté ou l’institution, soit ne se
manifestent pas, soit sont inadéquates. Cette irritation du doute, du
moins lorsqu’elle est assez largement partagée par la communauté,
entraînerait un analogue de l’enquête, des mesures en réaction, plus
ou moins d’essais et d’erreurs, jusqu’à ce qu’un état de choses
satisfaisant soit rétabli --- adaptant ainsi l’esprit social à une
réalité environnementale changeante. Les croyances centrales
sélectionnées et assimilées dans l’esprit social sont des habitudes
comportemantels ayant permis de résoudre des troubles existentiels qui
ont menacé la survie ou le bien-être du groupe en question ou qui ont
entravé sa marche vers quelque but commun. Si nous considérons des
groupes d’humanité étendu comme des civilisations entières, alors les
questions les plus pressantes sont probablement de résoudre les
troubles existentiels (les irritations du doute) qui menacent de
briser ou déstabiliser l’ordre social et, en les résolvant
d’encourager ou de renforcer des pratiques et des systèmes de
croyances qui ressèrent le tissu social et soutiennent les mœurs
sociales établies. Les institutions les plus fortes évoluent au cours
d’un lent développement social, satisfont vraisemblablement les
besoins les plus grands et servent les fins humaines les plus
cruciales.

Je pense qu’il serait difficile de surestimer l’importance des
institutions pour l’humanité --- on pourrait dire de la civilisation
elle-même qu’elle ne consiste qu’en un vaste entre-jeu d’institutions.
J’imagine que c’est aux sociologues et anthropologues de cartographier
le réseau complexe d’institutions structurant la société à un moment
donné, et aux psychologues sociaux d’étudier la manière dont
l’intelligence accumulée et disséminée par ces institutions
constituent l’esprit de différentes époques. Ces études seraient
requise pour appuyer toute classification autorisée des institutions
ou leur tri par importance relative. Une recherche philosophique riche
de découvertes, pour provisoires qu’elles soient, a été conduite par
Fisch, qui concluait que les institutions humaines basiques, celles
dont toutes dépendent, sont la famille et le discours; il en identifie
d’autres d’importance toute particulière pour la vie
sociale --- l’agriculture, l’industrie, le commerce (afin que « nous
puissions travailler et … nous doter du nécessaire et des commodités
de la vie »); les jeux (afin que « nous puissions jouer »); les arts
(afin que « nous puissions créer et apprécier des objets de beauté »);
la religion (afin que « nous puissions adorer et prier et rendre
solennel les grandes occasions de la vie »); les écoles, les
laboratoires, les bibliothèques, les musées, les observatoires, les
sciences (afin que « nous puissions nous engager dans la recherche »);
et le gouvernement (afin que « nous puissions doter les autres
institutions d’une forme d’harmonie ») (Fisch 1955-1956: 45).

Selon Fisch, Aristote soutenait que « les institutions en sont venues
à exister afin que les hommes vivent, et ont persévéré, comme d’autres
s’y joignaient, afin qu’ils vivent bien » (Fisch, 1955-1956: 46). Ceci
est conforme à la division fondamentale de Peirce des entreprises de
la vie entre celles qui traitent des questions d’importance vitale
(les besoins pratiques de la vie), et celles qui traitent des
aspirations humaines (en particulier, la poursuite de connaissance et
de vérité). Comme y insiste Peirce dans ses leçons de 1898
à Cambridge, ces deux grandes classes d’entreprises humaines reposent
sur différentes sortes de croyances, les croyances fermes développées
au cours de l’expérience qui programment les individus à la survie
(les croyances naturelles) et les croyances provisoires engendrées par
le raisonnement abductif dans le but de découvrir ce qu’il en est
(les croyances théoriques). Il est assurément le cas que, dans le
cours ordinaire de la vie, un corpus substantiel de croyances
pratiques se développent naturellement en tant que réservoirs
d’habitudes comportementales programmées dans le cerveau individuel,
et que, quoique de façon moins automatique, une chose similaire se
produise avec les croyances théoriques des individus dont les vies
sont dévouées à des fins intellectuelles ou scientifiques. Mais si ce
que j’ai dit du rôle de l’esprit social et des institutions durables
dans l’accumulation de connaissances spécialisées et l’accroissement
des capacités intellectuelles individuelles est vrai, alors il est
attendu que les institutions puissantes ont évolué de telle sorte
qu’elles encouragent ces deux grandes classes d’entreprises humaines.

Y a-t-il une puissante institution humaine naturelle qui contribue de
façon vitale et immédiate à la survie des sociétés et des cultures de
la façon dont les comportements instinctifs et fixés contribuent à la
survie et à la prospérité de formes de vies et d’espèces inférieures?
La fonction d’une telle institution serait d’inculquer et de soutenir
des pratiques et des valeurs qui rendent la vie meilleure à un niveau
fondamental; dans les faits, de programmer des individus à adopter des
systèmes de croyances conférant un avantage évolutif aux sociétés qui
les encouragent. Probablement beaucoup d’institutions anciennes
remplissent cette fonction dans une certaine mesure, mais je crois que
c’est, par dessus-tout, la religion qui joue ce rôle dans la vie et la
civilisation humaine. Tout comme la vérité et la validité ne sont
quasiment d’aucune importance pour le raisonnement naturel et les
croyances pratiques qui ont prouvé leur valeur dans les aléas de
l’expérience réelle, il n’est quasiment d’aucun intérêt de mettre en
question la vérité ou la valeur des systèmes de croyances que la
religion propage. La question pertinente est dans quelle mesure ces
valeurs et croyances rendent la vie meilleure à échelle humaine, et
dans quelle mesure elles sont efficaces à programmer des individus
à coopérer volontairement et de manière désintéressée pour promouvoir
leurs communautés culturelles.

Peirce semble avoir supposé que la religion jouait ce rôle dans la vie
humaine et la culture. « La _raison d’être_ d’une église, écrit-il,
est de conférer aux hommes une vie plus vaste que leurs personnalités
restreintes. »^[« Religion et Politique », c. 1895a, b: CP 6.451] « La
religion est un grand, peut-être le plus grand, facteur d’une vie
sociale qui s’étend au delà du cercle privé d’accointances
personnelles. Cette vie est toute chose pour une civilisation élevée,
charitable, et démocratique; et si l’on renonce à l’Église, de quelle
autre manière peut-on exercerc de façon satisfaisante la faculté de
fraterniser avec son prochain? »^[« Religion et Politique », c.
1895a, b: CP 6.449. Peut-être que de nos jours, dans certaines
cultures, le sport professionnel joue ce rôle aussi bien, voir mieux,
que l’institution de la religion, mais ça n’était pas le cas
à l’époque de Peirce.] Il ne pensait pas la religion, dans son
sens le plus profond, comme un ensemble de doctrines théologiques ou
de croyances mais comme une chose beaucoup plus vitale. « La Religion
est une vie, dit-il, et peut être identifiée à une croyance seulement
lorsque cette croyance est une croyance vivante --- une chose qu’il
à vivre plutôt qu’à dire ou penser. »^[« What is Christian Faith »,
1893a, b: CP 6.439.] Peirce prétendait que sa théorie des grandes
personnes (des « personnalités corporatives ») était appliquée le
mieux à la religion: « Si un tel fait est capable d’existence quelque
part, ce doit être à l’église. »^[« Man’s Glassy Essence », 1892a, b:
W8: 183; voir aussi EP 1: 350 et CP 6.271.]

Assurément la conception de la religion présentée ici se limite à ce
que je vois comme sa fonction naturelle fondamentale. La religion dans
sa globalité, comme la plupart des institutions qui constituent les
cultures humaines, est un mélange complexe d’intelligence pratique et
théorique. Mais je crois que c’est le rôle crucial que joue la
religion dans la promotion de la stabilité sociale et du bien-être
humain en programmant les individus à traiter des questions vitales en
leur inculquant des attitudes, des valeurs et des idées qui renforcent
la civilisation, qui permet d’expliquer l’ubiquité des religions dans
les cultures humaines.

Y a-t-il une autre puissante institution humaine qui promeut l’autre
grande classe d’efforts humains, une institution qui se désintéresse
des questions vitales de la vie humaine mais qui encourage les
aspirations humaines à accroître le savoir --- à découvrir ce qu’il en
est? Une telle institution serait née non pas des inquiétudes
existentielles humaines mais d’une soif intellectuelle; non d’un
besoin de croire mais d’un désir de savoir. Nombre de démarches
intellectuelles et de disciplines contribuent à cet effort, incluant
dans sa veine philosophique la théologie, mais c’est la science, telle
que la concevait Peirce, qui est la plus directement et pleinement
dévolue à ce but. En réfléchissant aux avancées scientifiques du
dix-neuvième siècle, Peirce remarquait à quel point il se sentait
privilégié d’avoir pu faire l’expérience, parfois dans sa propre
demeure, « le feu ardent de l’enthousiasme de la génération
scientifique de Darwin », et il réfléchissait au changement de sens du
mot « science ». Cela ne signifiait plus « connaissance
systématisée », comme auparavant, « ni toute chose inscrite dans un
livre », mais « science » en était venu à vouloir dire « un mode de
vie; non la connaissance, mais la quête d’une vie, la quête dévouée,
mûrement réfléchie du savoir: la dévotion à la Vérité --- non la
“dévotion à la vérité telles qu’on la voit”, car ça n’est là nulle
dévotion à la vérité, seulement au parti --- non, loin de ça, la
dévotion à cette vérité que l’homme est pour l’instant incapable de
voir, mais qu’il s’efforce d’atteindre. »^[« Les Grands Hommes du
Siècle en Science », 1901a, b: HP 1: 490-491.] La science, en ce sens,
est la quintessence de l’effort commun; elle vise à un but auquel les
individus ne peuvent qu’espérer contribuer sans jamais pouvoir
l’atteindre d’eux-mêmes: « tous les grands succès de l’esprit
dépassent les forces d’individus sans assistance. »^[« Evolutionnary
Love », 1892: W8: 203; voir aussi EP 1: 369 et CP 6.315.] Mais même si
la pratique de la science et la dévotion à la quête du savoir est la
pierre angulaire de la science, c’est l’accumulation prodigieuse de
connaissance, de méthodes, le vaste réseau d’intelligence distillée de
générations d’essai et d’erreurs, de succès et d’échecs, préservée
dans l’esprit étendu de la science en tant qu’institution, qui permet
à ce qu’un simple individu, au cerveau si petit et si éphémère, puisse
participer à un effort aussi vaste.

Contrairement à la religion, qui cultive une intelligence pratique
traitant et résolvant les questions existentielles d’importance vitale
immédiate, la science cultive une intelligence théorique, s’inquiétant
non de questions pratiques, sauf incidemment, mais de découvrir la
vérité des choses. La science n’est jamais urgente. Les croyances
pratiques que la religion inculque ont résolu les doutes, et
programmer les croyants à l’action automatique sans avoir à recourir
à la délibération. Les croyances théoriques sanctionnées par la
science ne sont jamais que des quasi-croyances, et jamais réellement
indubitables. Le faillibilisme est au cœur de l’attitude scientifique;
même en suivant les vérités scientifiques les plus fermement établies,
le véritable scientifique sera prêt à reconnaître l’erreur s’il est
confronté à une expérience qui les réfute. Comme Peirce le compris dès
sa fréquentations précoces avec les scientifiques majeurs du
dix-neuvième siècle, le progrès de la science ne peut jamais que
résulter d’un effort commun --- et nombres de déboires en pavent la
route. La contribution de tout chercheur individuel, au mieux, sera
faible.^[Pour éviter tout malentendu, il est bon de répéter que je ne
prétends pas rendre de jugements définitifs sur la nature de la
religion ou de la science. Mon but ici est de considérer en quoi la
conception peircienne de la croyance, en particulier en ce qu’elle
s’applique aux questions d’importance vitale et théorique, pourrait
aider à éclairer le développement d’esprits externes dans les
institutions et les sociétés. Les institutions de la religion et de la
science sont d’une importance clé dans cette enquête --- qui de toute
évidence est encore largement préliminaire.]

# La Place de l’Individu

En conclusion, il semble approprié de mettre en question le sens de
l’individu humain à la lueur de la théorie de Peirce de l’externalité
de l’esprit et de la dépendance critique des vies et pensées humaines
à un esprit social instinctif. Il pourrait sembler que les individus
ne soient guère que des moyens d’animation d’institutions douées
d’esprit et d’accomplissement de fins institutionnelles, que ces fins
soient de fixer des croyances qui promeuvent les comportements
bénéfiques et stabilisent les sociétés ou qu’elles soient d’adopter
des conjectures et d’expérimenter dans l’espoir de contribuer au
corpus de « vérités établies ». Nombre de remarques de Peirce vont en
ce sens. Il dit que nous ne sommes guère que des « cellules de
l’organisme social », et que nos « sentiments les plus profonds
prononcent le verdict de notre propre insignifiance »^[Issu d’une
variante de « Philosophie et la Conduite de la Vie », 1898a: CP 1.
673; voir aussi EP 2: 40 et CP 1.647.] Nos existences séparées en tant
qu’individus « ne se manifeste que dans l’ignorance et l’erreur », et,
pour autant que l’on soit une chose différente d’autrui, nous ne
sommes « qu’une négation ».^[« Some Consequences of Four
Incapacities », 1868: W2: 241-242; voir aussi EP 1:55 et CP 5.317.]
Pourtant il ne nous faut pas prendre à la légère le fait que c’est
bien aux individus vivants que revient le pouvoir de faire advenir les
choses dans le monde. Peirce critiquait Comte pour avoir fait de ses
héros des abstractions biaisées et d’avoir négligé leur « réalité
vivante et leur passion » et leurs « âmes concrètes ».^[« The Comtist
Calendar », 1892a, b: W8: 267-268.] L’importance des institutions et
des esprits sociaux pour la survie humaine et la vie civilisée est
profonde, et assurément que les succès individuels doivent toujours
être appréhendés dans le contexte d’une vie et pensée commune, c’est
pourtant l’individu, quelque rare qu’il soit, qui a le pouvoir de
sortir du courant et de confronter le neuf et l’inattendu aux vieilles
idées. C’est alors, dit Peirce, que, sur la balance des civilisations,
le futur tressaille.

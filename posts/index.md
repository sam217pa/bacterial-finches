---
title: Bacterial Finches
author: Samuel Barreto
---

Hi, I'm Samuel Barreto, a french PhD student in evolutionary biology.
This is my website, where you can find some notes I've taken when reading books, some translation I've done of chapters and articles that I liked, and some essays of mine.
I'm interested in understanding the biological world, in understanding what understanding the biological world means, and in understanding why I'm interested in understanding the biological world.

I use this website to publish ideas, mostly not mine.
They might be wrong, especially when they're mine.

(Most posts are in French.)

# Philosophy, Epistemology

Some notes, essays and translation of articles that I found cool enough to translate.

## Notes

**De la Vérité dans les Sciences** Une revue de l’essai d’Aurélien Barrau sur le concept de vérité dans les sciences. ( {{l:barrau.md}}, {{p:barrau.md}} )

**Esprit social et fixation de la croyance** Un article de Nathan Houser sur la vision peircienne de ce que l’on pourrait qualifier d’esprit social, une forme d’esprit distribué parmi les corps sociaux, découlant logiquement d’un phénomène évolutif de fixation d’habitudes, où l’habitude est à prendre au sens peircien, proche de l’habitus de Bourdieu, allant jusqu’à la sémiose inconsciente, incorporée.
( {{l:houser01.md}}, {{p:houser01.md}} )

**Vaguement voguer entre vague et détermination**
Un court extrait d'un passage de _New Elements_ de Charles Peirce, dans lequel il montre un aspect souvent critiqué de sa cosmologie basée sur la séméiotique. ( {{l:peirce1.md}}, {{p:peirce1.md}} )

**Arrogance, Vérité et Discours Public**
Un article de Michael P.  Lynch dans lequel il explique en quoi l'arrogance épistémique consiste, en quoi elle est une attitude dangereuse pour la démocratie et de quelle manière elle sape l'idéal de la démocratie comme un « espace de raison ».
( {{l:lynch2.md}}, {{p:lynch2.md}} )

**De la vérité en éthique**
Une traduction de l'article de Michael P. Lynch dans l'encyclopédie internationale d'éthique sur le sujet de la vérité en éthique.
Il y reprend les conceptions traditionnelles de ce qu'est la vérité, de quelle façon on peut y atteindre et en quoi ces conceptions s'appliquent à l'éthique et à une recherche d'objectivité morale.
( {{l:lynch1.md}}, {{p:lynch1.md}} ).

**Une formation à l'éthique est-elle éthique ?**
Le petit essai que j'ai dû réaliser en conclusion du MOOC Éthique de la Recherche suivi en tant que formation pendant ma thèse.
( {{l:formation-ethique.md}}, {{p:formation-ethique.md}} )

**Canguilhem, la Connaissance de la Vie**
Ce livre de Georges Canguilhem rassemble quelques uns de ses plus brillants travaux sur la manière dont a été appréhendé le concept de Vie.
Son travail sur la notion de milieu est selon moi l'un des plus marquants; on en retrouve l'imprégnation dans tout le travail de Bourdieu entre autres.
( {{l:canguilhem-connaissance.md}}, {{p:canguilhem-connaissance.md}} )

**Bachelard, La Philosophie du Non**
« Deux hommes, s'ils veulent s'entendre vraiment, ont dû d'abord se contredire.
La vérité est fille de la discussion, non pas fille de la sympathie. »
( {{l:bachelard-philo-du-non.md}} )

**Bachelard, Le Matérialisme Rationnel**
« La pensée scientifique repose sur un passé réformé. Elle est essentiellement en état de révolution continuée. »
( {{l:bachelard-materialisme.md}} )

**Maynard Smith, le concept d'information en biologie**
Ma traduction d'un papier de J. Maynard-Smith sur la notion d'information biologique telle qu'il l'entend, prise dans son sens symbolique.
( {{l:mindless-selection.md}} {{p:mindless-selection.md}} )

**L'information en biologie**
Ma traduction de l'article rédigé par Godfrey-Smith et Sterelny dans la Stanford Encyclopedia of Philosophy sur le concept d'information biologique.
( {{l:information-biologique.md}}, {{p:information-biologique.md}} )

**Peirce et l'antidéterminisme**
Ma traduction de la partie de l'entrée « Charles Sanders Peirce » dans la SEP dans laquelle est mentionnée la vision de l'évolution formulée par Peirce et son anti-déterminisme foncier.
( {{l:peirce-antideterminism.md}}, {{p:peirce-antideterminism.md}} )

## Hilary Putnam, *Renewing Philosophy*, 1995

J'ai entrepris de traduire quelques chapitres de ce livre de Putnam
ré-édité en 1995. Putnam y expose de quelle manière on peut échapper
au scepticisme, et tente de trouver un successeur à la dichotomie
fait/valeur dans la philosophie des pragmatistes (de Dewey et James)
et dans celle de Wittgenstein. (Les chapitres sont sous une forme de
traduction inachevée. Il pourrait bien s'y être glissées imprécisions
de traductions, erreurs de style ou de bêtes coquilles. Les traductions des passages cités sont pour l'essentiel les miennes, pour l'instant.)

**Préface** --- \[2019-02-15\] --- {{l:putnam01.md}}, {{p:putnam01.md}}

**Matérialisme et Relativisme** --- \[2018-10-21\] ---
{{l:putnam2.md}}, {{p:putnam2.md}}

**Bernard Williams et la Conception Absolue du Monde**---
\[2018-10-28\] -- {{l:putnam1.md}}, {{p:putnam1.md}}

**Irréalisme et Déconstruction** --- \[2018-11-24\] {{l:putnam3.md}}
{{p:putnam3.md}}

**Wittgenstein et la Croyance Religieuse** --- \[2018-11-25\] --- {{l:putnam4.md}}, {{p:putnam4.md}}

**Wittgenstein, la Référence et le Relativisme** --- \[2018-12-20\] --- {{l:putnam5.md}}, {{p:putnam5.md}}

**La démocratie deweyenne, une reconsidération** --- \[2019-02-13\] --- {{l:putnam6.md}}, {{p:putnam6.md}}

# Biology

**La Route vers la Double-Hélice** --- \[2019-07-03\] --- La traduction de la revue que Chargaff a faite de l'ouvrage de référence d'Olby sur la naissance de la biologie moléculaire. On l'y voit relativement amer sur la façon dont l'objet du livre s'est construit comme narrant moins l'histoire d'une découverte scientifique majeure que comme l'essor d'une communauté nouvelle, qui s'est autorisé des effets symboliques propres à une discipline neuve et prometteuse pour renverser l'ordre symbolique en place. Nul besoin d'y voir à mon sens un quelconque progrès; tout au contraire, c'est plutôt par l'oubli d'hier que l'alors s'est construit, remisant, par orgueil du présent, aux oubliettes de l'histoire toute la route parcourue. ({{l:chargaff.01.md}}, {{p:chargaff.01.md}})

**Sakoparnig et al, 2019**, (Whole genome phylogenies reflect long-tailed distributions of recombination rates in many bacterial species)
Une revue de ce preprint qui a fait relativement beaucoup de bruit, où certains voulaient y voir un encouragement à rejetter le modèle de l’arbre du vivant.
J’en expose les différentes parties à mon sens les plus intéressantes, et en quoi elles ne sont pas à la hauteur de leurs prétentions.
Dans un dernier paragraphe quelque peu spéculatif, je rapproche les objections qui ont été faites à ce genre de modèles de celles qui s’opposent aux théories de l’Acteur Réseau de Latour, Callon et consorts, dans leur incapacité à tenir compte de l’historicité d’un système et leur restriction à l’observé positivement.
({{l:sakoparnig-review.md}}, {{p:sakoparnig-review.md}})

**Dérive, une approche historique** Ma traduction de l’article d’Anya
Plutynski sur la dérive, paru en 2007 dans Biological Theory, dans
lequel elle se livre à une analyse historique du concept de dérive, et
montre quel développements conceptuels il a traversé, de sa
formulation par Fisher et Wright dans les années 1920-1930 à son usage
courant en génétique contemporaine. ({{l:plutynski01.md}},
{{p:plutynski01.md}})

**Woese & Goldenfeld**--- \[2018-09-29\]
Ma traduction (partielle) de l'un des derniers articles de Woese,
écrit avec Goldenfeld dans une revue de physique des particules, dans
lequel il(s) tentent de montrer en quoi la Vie est une propriété
émergente de la matière physique, et qu'elle ne peut être réduite à un
simple déterminisme génétique. Tout comme l'état de la matière
condensée ne peut être prédite à partir des états individuels de
chacun des éléments qui la compose, la Vie n'est pas réductible à une
somme d'individualités moléculaire. ( {{l:woese-goldenfeld-2011.md}},
{{p:woese-goldenfeld-2011.md}} )


**Biased Gene Conversion** A short description of what gene conversion is, and in what manner it can be biased.  ( {{l:biased-gene-conversion.md}} )

**Hill Robertson Interferences** A very brief description of this
complex phenomena at the interplay between two loci in linkage
desequilibrium. ( {{l:hill-robertson-effects.md}} )

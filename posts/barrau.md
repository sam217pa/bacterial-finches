---
title: 'Barrau: De la vérité dans les sciences'
author: samuel barreto
date: '2020-07-07T16:01:39+0200'
---

Premières approches avec cet ouvrage que Sylvain recommande. D'emblée le
style ne s'embarrasse pas d'efforts pour sinon faire oublier au moins
masquer l'agaçante manie qu'a l'auteur de se sentir obligé de savoir
écrire. Ça n'est ni ce qu'il lui est demandé, ni même le type de
compétence qu'il s'agirait de mobiliser dans ce genre de débat. On
aurait pu souhaiter que Barrau se sente plus obligé envers des exigences
de clarté qu'envers des exigences de style. Les phrases nominales se
suivent les unes aux autres. Sans cesse. Sans arrêt. Sujet verbe. Sans
interruption, pendant parfois des paragraphes entiers, pendant des
lenteurs, pendant de lentes heures, au pendant des anicroches éparses et
entourloupées, aux entournures d'un tour loupé. (On voit bien qu'il
n'est pas très compliqué d'écrire dans ce style et de donner une
apparence de profondeur poético-mystique à un texte qui, si l'on n'est
pas d'emblée permis de douter de la qualité de son contenu, donne au
moins des indices d'une dilution du contenu par la forme --- un style
qui n'est pas sans rappeler celui des auteurs dont Barrau aime à se
recommander, à commencer par Derrida ou Deleuze.)

Un exemple:

> Descartes, c'est l'inventeur du doute. D'un doute très particulier et
> très systématique. Un doute érigé, pourrait-on dire, en méthode.

Ou plus haut:

> Descartes était comme obsédé par la pensée immaculée, prise dans
> l'absoluité de son propre jeu, prisonnière de sa rationalité proclamée
> et presque vénérée, strictement autonome et donc finalement aveugle.
> Fantasme de la transparence et de la clarté, jusqu'à l'aliénation.
> Désir de singularité et d'infinité humaine jusqu'à l'aberration.

Sans doute Barrau a-t-il lu Nietzsche ou Deleuze, mais il n'a jusqu'à
présent pas fait preuve de la légitimité qu'il pourrait avoir à se
sentir autorisé à écrire comme eux. Ce simple niveau de lecture montre
bien avec quel genre d'humilité les paragraphes qui suivent auront été
écris.

On est donc fondé à penser d'emblée que le texte qui suit ne devrait pas
briller par le sérieux de ses propositions ni par la rigueur
d'exposition et la recherce de clarté; ce sont de toutes façons deux
notions qui seraient probablement balayées d'un revers de main par
Barrau comme relevant du style d'un positivisme désuet et d'une
philosophie qu'il est probablement enclin à considérer comme inadéquate
à décrire l'irréductibilité de « la Vie », c'est-à-dire la philosophie
analytique. Quoi qu'il en soit, si le style d'exposition peut déranger,
une lecture même conciliante aura suffisamment à faire avec les erreurs
ou approximations qui parsèment le texte. Mais le style n'est pas
totalement pour rien dans ce genre d'approximation. Par exemple, au
premier chapitre, Barrau nous dit que « Descartes constitue \[...\] un
exemple particulièrement frappant du danger qui accompagne une certaine
ambition totalisante (*et finalement humaniste*) de la pensée. » (Je
souligne.) On aurait aimé que Barrau nous explique à quoi il fait
référence par ce « et finalement humaniste ». Or il semble se sentir
justifié à n'y faire qu'allusion, accordant par là un statut d'évidence
à une proposition qui mériterait pour le moins d'être soutenue.

En fait, dans ce premier paragraphe, Barrau invoque Descartes pour
tenter de définir une solution à la quête de vérité dans les sciences.
Or les paragraphes qu'il écrit à son sujet sont invraisemblablement
creux, et se bornent à décrire des anecdotes éculées concernant
Malebranche battant son chien en vertu d'un cartésianisme portant sur le
rapport à l'animal et à la vision mécaniciste de l'animal par Descartes.
On se demande bien quel genre de rapport cette conception entretient
avec le doute méthodique préconisé par Descartes. Si même elle en
entretient une, il aurait probablement été d'intérêt d'en expliquer la
nature, afin que le lecteur ait a minima une idée même vague des raisons
pour lesquelles Descartes et le cartésianisme pourraient constituer une
tentative d'érection d'un concept de vérité en sciences. Si Barrau
invoque bien le *cogito* et en quoi Descartes y voyait une réponse au
scepticisme radical, on a du mal à comprendre où là dedans Barrau
envisage une piste pour définir un quelconque concept de vérité. Il se
contente d'invoquer Derrida et son concept de « différance », sans qu'on
ne voit très bien quelle espèce de rapport ces deux concepts
entretiennent. On l'a compris, Barrau lit et souhaite qu'on le sache.
Mais les auteurs auxquels il fait référence auraient probablement des
choses à dire sur la façon désinvolte avec laquelle il les utilise.

Et Barrau de conclure: « Contrairement à certaines idées reçues, il est
possible d'être scientifique sans être tout à fait cartésien! » Non
seulement cette phrase vient conclure un paragraphe qui n'a pas le
moindre rapport avec ce qu'elle dit, mais ce qu'elle dit n'est pas non
plus tout à fait clair: quelles idées reçues? qu'est-ce qu'être tout à
fait cartésien? À quoi cela reviendrait-il d'être tout à fait cartésien?
Qui a jamais dit que les scientiques étaient tout à fait cartésiens?

Barrau en vient ensuite à essayer de proposer une définition des
sciences. (On pourrait discuter de la structuration de l'argumentation,
et souligner qu'on a du mal à voir en quoi le paragraphe précédent
contribue à la compréhension de celui-ci, mais ça n'est pas l'objet.) Il
commence par expliquer qu'il est possible de dire « la science » ou
« les sciences », et que selon lui le débat est similaire à « l'art » ou
« les arts »; et d'exposer ensuite toute une série d'arguments qui
montrent qu'on peut dire les deux en esthétique. Barrau croit « qu'il en
va exactement de même en science ». « En contrepoint d'une possible,
quoique déjà contestable, homogénéité des visées, se dessine une immense
hétérogénéité des méthodes. » (On voit bien que Barrau fait un gros
effort d'écriture; j'avoue cependant être totalement imperméable à ses
effets de manche --- quel pourrait bien être le contrepoint d'une
homogénéité de visée?)

Dans les paragraphes suivants, Barrau tente de définir des critères de
scientificité. Le premier serait le recours aux mathématiques; ça ne
semble pas lui convenir puisque certaines sciences ne les utilisent pas
(les sciences humaines). Selon lui, certaines branches de la biologie
comme l'éthologie ou la taxinomie n'utilisent pas non plus les
mathématiques. Soit Barrau est resté coinçé à Lorenz ou Linné, soit il
ment sciemment --- les deux posent problème et n'incitent pas à accorder
une grande confiance à ses propos.

Un autre critère serait que la science entretiendrait un rapport
privilégié avec la raison. Mais ça ne convient pas à Barrau, sous
prétexte que « la raison est un concept très malléable »: « chacun se
croit rationnel! » Mais de ce que chacun se croit rationnel on ne peut
pas déduire que chacun l'est. « Quel tri serait alors opéré? » Et bien
il y aurait déjà un tri évident à faire entre ceux qui sont rationnels
et ceux qui ne le sont pas, indépendamment de ce qu'ils croient être ou
ne pas être. Si « nous sommes tous les irrationnels de nos ennemis », ça
ne l'est que dans une vision absolutiste de la raison, et je veux bien
être l'irrationnel de Barrau.

Barrau finit donc par renoncer à définir ce que sont les sciences, sous
prétexte que:

> Définir, bien que cela puisse être nécessaire, est toujours un acte
> dangereux et parfois même violent. Les dictionnaires sont des
> cimetières ou des prisons.

Il faut bien admettre le cocasse de la situation: Barrau exerce une
violence qu'on pourrait estimer tout à fait hors de propos à l'égard du
concept de définition en le définissant comme un acte dangereux etc.
J'avoue n'avoir pas beaucoup d'estime pour ce genre de stratégie
défaitiste qui consiste à renonce à donner un sens à un concept, sous
prétexte que ce que ce concept dénote est irréductiblement vague. On
peut définir ce que sont les Alpes sans éprouver le besoin d'en donner
les limites géographiques strictes; où donc s'arrête une montagne?

Mais contre toute attente, Barrau se ravise et propose de définir la
science comme cette activité dont l'intention est la Vérité avec un
grand « V ». Mais finalement on nous dit que ça ne peut pas suffire.

« Le chercheur est aux prises avec l'implacable factualité d'un réel qui
n'est jamais purement contractuel. » Si je suis globalement d'accord
avec cela, j'imagine qu'on pourrait l'exposer de la même façon sans
cuistrerie ni néologismes à la noix.

Après quoi Barrau montre qu'en science « tout est toujours sujet au
doute ». Ce qui n'est ni vrai, ni justifié. Ça n'est pas vrai que tout
est *toujours* sujet au doute, il nous faut bien nous en arrêter à
certaines hypothèses ou abduction, ces inférences dont Peirce indique
que l'économie de la recherche préconise d'y arrêter le doute, au moins
pour un temps. Soit Barrau ne comprends pas le processus de recherche,
ce dont je ne le soupçonne guère, soit il fait preuve d'une banale
naïveté en exposant la science comme une activité de doute permanent. Je
dirais même que la quantité de chose qu'il nous faut croire au quotidien
en science pour éviter la paralysie l'emporte de beaucoup sur la somme
de ce que l'on remet parfois en doute. C'est d'ailleurs ce que Barrau
lui-même semble suggérer un peu plus bas, en disant que « ce qui est
vrai ne l'est que relativement à un certain nombre de conventions qui
peuvent s'avérer dangereuses autant que nécessaires, selon les cas. »
C'est bien qu'il semble y avoir quantité de choses que l'on est obligé
d'admettre pour attribuer des valeurs de vérité à d'autres.

Mais Barrau me semble commettre l'erreur classique qui consiste à
confondre les critères que l'on a pour évaluer la vérité d'une
proposition avec la vérité d'une proposition, comme le rappelait
Bouveresse à propos de Foucault
[@BouveresseDesirVeriteConnaissance2013]. Ce que l'on tient pour vrai
n'est pas le vrai.

> \[...\] le diktat de la vérité est aussi une violence, possiblement
> nuisible et souvent trompeuse si celle-ci n'est pas relativisée à la
> contingence des constructions qui la produisent.

La vérité d'une proposition n'est *pas* relative à la contingence des
constructions qui la produisent; ce sont les critères qui permettent
d'évaluer la proposition qui le sont. « La Terre est à 150 millions de
kilomètres du Soleil » est vraie si et seulement si la Terre est à 150
millons de kilomètres du Soleil. Si l'unité de mesure venait à changer,
la proposition serait fausse puisque les critères qui nous permettent de
l'évaluer changent, mais la distance Terre--Soleil n'en changerait pas
pour autant.

Plus bas, Barrau lâche le genre de commentaire qui me semblent condamner
d'emblée toute espèce d'adhésion à la vision écologiste de Barrau; il
fait référence à l'imminence de la chute de la civilisation humaine,
« compte tenu du mépris hallucinant avec lequel nous traitons notre
planète et une grande partie de ses habitants, *en particulier
non-humains*. » (Je souligne.) D'une part il devrait plutôt faire
référence à la chute d'une civilisation humaine, la civilisation
occidentale --- si elle mérite encore le titre de civilisation, ce qui
n'est plus tout à fait évident, si même ça l'a été un jour. D'autre
part, ce genre de commentaire sur le fait que c'est en particulier aux
non-humaines que la civilisation cause du tort exerce toujours sur moi
une capacité à m'exaspérer au plus au point, à la limite de la colère;
si c'est le type d'arguments que Barrau mobilise pour en appeler à une
autre forme de civilisation, la mobilisation qu'il espère devra se
passer de moi. Je suis bien conscient des maux que l'on cause aux
« non-humains », mais la seule évocation des maux biens réels et
facilement évaluables que l'on cause aux humains suffirait ou devrait
suffire à motiver une révolte permanente contre l'ordre établi. Ces
commentaires que Barrau doit probablement penser comme anodins sont le
signe de pensées que l'écologie *doit* laisser derrière elle, si un jour
elle veut faire du non-humain sa priorité; cette priorisation ne passera
que par la prise en compte du fait que les intérêts des combattants en
faveurs des humains que le capitalisme aliène et ceux des non-humains
sont alignés.

De façon générale, il ne m'est pas facile d'avoir une vision claire de
la conception de la vérité que défend Barrau. Tantôt il se fait l'avocat
d'un réalisme quasi-correspondantiste somme toute assez classique,
tantôt il se fait l'avocat de ce qu'il appelle le relativisme exigeant.
Tantôt il définit la vérité comme relevant de conventions contigentes,
tantôt il la définit comme ne se réduisant pas à de simples valeurs
contractuelles. Tantôt il écrit que la vérité est une « invention sous
contraintes », tantôt il semble adopter le point de vue que les vérités
sont susceptibles d'être renversées. Mais encore une fois, les critères
d'évaluation de la vérité d'une proposition ou d'une théorie peuvent
changer, mais sa vérité elle ne change pas. Que l'on croit d'une chose
qu'elle est le cas, et qu'elle soit le cas, ce sont deux choses
différentes.

Tôt dans son essai, il semble se positionner contre le relativisme, mais
en faveur d'un constructivisme --- ce que Boghossian a bien démontré
comme étant finalement équivalent. Plus tard, il défend un « relativisme
cohérent et exigeant ». Il vient de tenter de définir une stratégie pour
combattre le genre d'ineptie que constituent le créationnisme ou le
révisionnisme, en démontrant leurs incohérences internes etc. etc. Il me
paraît beaucoup plus simple de considérer qu'elles sont tout simplement
fausses. S'il y a désaccord quelque part, ça n'est pas sur le contenu
des croyances que ces foutaises impliquent, mais sur la nature des
arguments qu'il faut mobiliser pour motiver l'adoption ou d'un point de
vue scientifique réaliste, ou d'un point de vue créationniste inepte.
Peirce et d'autres pensaient qu'il faut fonder la logique sur l'éthique;
je suis assez d'accord avec ça. S'il faut défaire le créationnisme, ça
n'est pas en démontrant son incohérence interne, c'est en critiquant ses
critères éthiques et les conséquences auxquelles l'adoption de tels
critères éthiques conduisent. Cela devrait suffire.

On pourrait reprocher à Barrau le même genre de tort que Bouveresse
attribue à Foucault dans la conférence déjà citée, toutes proportions
gardées bien sûr; c'est-à-dire que Barrau a la fâcheuse manie sinon
d'être persuadé d'avoir inventé un concept qu'il dit lui-même subversif
(dans l'introduction), au moins de donner l'apparence d'en être
persuadé, là où en vérité il n'a fait que donner un nouveau nom à un
concept déjà bien connu. Bouveresse cite Goethe et son Faust:

> Car justement là où manquent les concepts / Un mot se présente à
> point.[^1]

Qu'il faille fonder le relativisme cohérent sur la possibilité qu'à
chaque énoncé scientifique la probabilité pour que l'on se trompe
l'emporte de beaucoup sur la probabilité pour que l'on ait juste ne
m'apparaît pas comme évident. Ce genre d'attitude s'appelle le
faillibilisme, il n'est pas nouveau, il est déjà bien décrit par Peirce
ou même Popper. Je ne vois pas bien en quoi admettre le fait que l'on
puisse se tromper conduit au relativisme.

J'en arrive à un paragraphe qui s'avère rétrospectivement comique, alors
qu'on en arrive à la dernière partie d'un livre qui s'est employé à
masquer d'une quantité substantielle de métaphores une pauvreté
conceptuelle affligeante:

> Il est légitime de rechercher une certaine clarté et il certainement
> souhaitable de ne pas obscurcir à dessein, par jeu ou par coquetterie.

Que quelqu'un qui recourt aux néologismes d'absoluité ou de factualité
pour parler de l'absolu ou des faits, qui écrit des choses comme « le
monde, à un instant donné \[...\] est alors transitivement diffracté »
nous indique qu'il est souhaitable de rechercher la clarté n'est pas
dépourvu d'une forme de comique.

Plus bas, Barrau nous livre un paragraphe d'inspiration derridienne,
auquel il tient visiblement beaucoup; il est amusant de constater qu'il
a donné à cette partie le titre de « la vérité de la vérité »,
expression que je m'étais au préalable attendu à trouver invoquée
rigoureusement telle quelle! Dans ce paragraphe, on nous dit que:

> L'enjeu est simple et explicité: faire face à la complexité
> performative du vérace sans nier la dimension nécessairement normative
> du vrai.

Si l'enjeu est simple, on pourrait souhaiter un minimum de clarté dans
l'exposition.

Dans cette même partie, Barrau livre des opinions sur l'évolution qui
serait requise pour une forme de progrès à l'égard du rapport à la
vérité, conceptions que je partage, une fois n'est pas coutume.

En guise de conclusion, Barrau nous dit que « la science est une praxis
plus qu'un corpus », ce que je lui accorde. Avant de résumer sa position
en disant que la science « sait qu'elle ne touche pas la Vérité, que nos
savoirs ne sont exacts et justes que relativement à des constructions
toujours déconstructibles ». Ici on pourrait mobiliser l'argument
classique contre le relativisme, et opposer à Barrau que si la science
sait qu'elle ne touche pas à la Vérité, alors elle ne peut pas savoir
qu'elle ne touche pas à la Vérité, puisqu'il faudrait savoir toucher à
la Vérité pour savoir qu'elle ne touche pas à la Vérité. Une façon
beaucoup plus simple de voir les choses à mon sens consiste à admettre
que la science (du moins les scientifiques) n'est effectivement jamais
en mesure de savoir *si* elle touche à la vérité (qui peut très bien se
passer d'un grand « V »), mais que ça ne l'empêche en rien d'y toucher
de temps à autre; sinon comment expliquer les succès qu'elle a
effectivement rencontrés et qu'elle continue de rencontrer
régulièrement?

Si comme le dit Barrau la vérité est innaccessible, alors encore une
fois on ne peut pas savoir si « la vérité est innaccessible » est vraie.
À cela je préfère une vision plus simple qui consiste à adopter une
position humble et dire qu'il est possible, voire même probable, que mes
propositions soient fausses, mais motiver mes propositions par le fait
que je les énonce sur la base d'un processus fiable (le fiabilisme); un
processus fiable est intrinsèquement socialement fondé. Là encore on
retrouve l'intrication étroite entre la démarche scientifique et la
démocratie, et le nécessaire ancrage de la démarche scientifique dans
l'éthique.

Dans l'épilogue, Barrau reprend ses principales conclusions. Selon lui,
et reprenant Goodman, la science « est une manière de faire un monde,
parmi d'autres possibles » et « n'entretient pas nécessairement de lien
privilégié avec la vérité ». On peut probablement lui accorder ce point;
Bouveresse par exemple a beaucoup travaillé sur ce que pourrait être la
« connaissance de l'écrivain ». Il y a probablement des formes de
connaissances que la science est mal placée pour aborder ou acquérir.
Resituer la science dans ce champ et ne lui accorder que le crédit qui
lui est dû est probablement salutaire.

Il en vient ensuite à quelque chose comme la version la plus claire
qu'il nous ait donnée de l'exposition de son « relativisme exigeant ».
Nulle part il ne nous est expliqué en quoi ce qu'il entend *est*
réellement un relativisme. « Un relativisme exigeant et militant, nous
dit Barrau, à l'inverse d'un laxisme, permet d'opérer des choix tout en
demeurant conscient du fait qu'ils ne sont pas nécessairement les seuls
possibles. » Je ne suis pas sûr qu'il soit alors légitime de parler de
relativisme, puisque le relativisme entend que la vérité d'une
proposition est relative à celui ou la communauté de celles qui
l'énoncent. Si je comprends bien la position de Barrau, c'est plutôt du
pluralisme méthodologique qu'il se fait le partisan. Dire qu'il existe
une multitude de description du réel toutes aussi vraies les unes que
les autres n'est pas un relativisme. Mais peut-être que Barrau entend
affecter une position de franc-tireur et se faire le partisan d'une
position dont plus personne ne veut vraiment, le relativisme. Il le dit
d'ailleurs lui-même ensuite:

> Sans doute faudrait-il en fait parler de relationnisme pour éviter la
> connotation souvent péjorative du relativisme. Ou, mieux encore, de
> pluriréalisme: ni l'irréalisme qui nie l'existence d'une réalité hors
> soi, ni le réalisme absolu qui entend tout réduire à un seul mode
> d'être \[...\].

À quel réalisme absolu est-il fait référence ici? Barrau est-il au
courant que d'autres ont déjà pointé les failles de ce genre de
position? On pourrait légitimement attendre d'un scientifique de son
niveau qu'il cherche au moins à se renseigner sur les travaux qui
portent sur ses domaines d'intérêts. Que le nom de Putnam n'apparaisse
jamais dans l'essai est pour le moins surprenant, lui qui avait déjà
dédié deux chapitres à pointer les failles de ces deux types de
positions dans son *Renewing philosophy*.[@PutnamRenewingPhilosophy1995]
Le pire est que Putnam apparaît bien dans la bibliographie, de même que
celui de Tarski --- mais où ces deux auteurs fondamentaux sont-ils
exploités dans le texte?

On en vient au point avec lequel je pense que je ne pourrai jamais
suivre Barrau. Il dit qu'il faut bien sûr combattre les postures
dangereuses, scandaleuses et arbritraires (créationnisme, négationnisme,
racisme, climato-scepticisme, ...), ce avec quoi je suis évidemment
d'accord. Mais selon lui la meilleure façon de s'y prendre est de
démontrer que leur logiques propres sont incohérentes. Je ne pense pas
que ça soit la meilleure façon de procéder. La moins mauvaise façon
d'essayer de sensibiliser aux conséquences du réchauffement climatique
n'est selon moi pas de marteler à qui mieux mieux que lesdites
conséquences sont désastreuses, notamment celles qu'on peut déjà
observer. Ça tout le monde le sait. Le problème se tient plutôt à la
racine; il ne s'agit pas d'un mal logique mais éthique. C'est au
fondement éthique de pareilles incohérences qu'il faut s'attaquer. Comme
le disait Peirce:

> Celui qui ne sacrifierait son âme pour sauver le monde, est, à ce
> qu\'il me semble, illogique dans toutes ses inférences,
> collectivement. La logique est ancrée dans le principe
> social.[@PeirceIllustrationsLogicScience1878]

Et là où j'imagine que Barrau me rejoindrait, c'est que la science n'est
probablement pas la mieux placée pour réaliser ce genre d'attaques ---
encore que l'on pourrait en discuter.

Bouveresse disait dans le *Philosophe et le Réel*:

> Qu'une personne plutôt qu'une autre soit capable d'opposer un refus
> quand presque tout le monde dit « oui », cela dépend d'autre chose qui
> est difficile à définir, mais qui relève certainement plus de
> l'instinct et de la volonté que du raisonnement.

Remarques en conclusion
=======================

Peirce disait qu'il y a, « Dieu merci, des marchands de soupe
philosophique à tous les coins de rue ». Je ne sais à quel coin de rue
Barrau s'est senti légitime de s'installer pour distribuer sa pitance,
mais il est certain que l'humilité que devrait, selon ses dires, nous
inspirer la forme de relativisme cohérent qu'il défend n'a pas eu l'heur
d'innerver ledit défenseur, pour parler d'une façon qu'il a l'air
d'apprécier. S'il me fallait me frayer un chemin dans le maquis
terminologique qu'il dresse entre le lecteur et les quelques conceptions
qu'il semble prêt à défendre, il me semble voir en Barrau un réaliste
relativement classique. Mais pour citer Claudine
Tiercelin[@TiercelinRaisonSensibilite2014]:

> qui ne se dirait pas aujourd'hui « réaliste », voire réaliste «
> spéculatif » ? Certes : mais encore faut-il se donner les moyens de
> ses concepts, bref, être en mesure d'en donner un contenu précis et
> d'en évaluer les effets.

Il resterait à évaluer effectivement si Barrau est bien à la hauteur de
ses concepts. L'invocation superficielle à quelques auteurs clés
(Descartes, Goodman, Foucault, Deleuze, Derrida, Comte, Ockham, Carnap,
...), dont on se demande avec quelle espèce de sérieux Barrau les a
étudiés, ne semble pas venir à l'appui d'une conception dont on aurait
pu attendre un semblant sinon d'originalité, au moins de clarté. La
position qui semble être articulée n'a pas de manière évidente le
caractère subversif que Barrau leur attribue initialement.

On aurait pu attendre de quelqu'un qui multiplie les références qu'il le
fasse de telle sorte qu'on soit en mesure de vérifier si ce qu'il dit
correspond bien aux conceptions qu'il attribue aux auteurs qu'il cite:
un minimum de références bibliographiques n'aurait pas été de trop.
D'aucuns pourraient suspecter Barrau de masquer l'irresponsabilité de
son essai sous l'excuse d'une invocation d'un prétendu caractère
non-formel dans l'introduction.

Wikipédia nous indique que M. Barrau a obtenu un diplôme de philosophie
avec les mentions honorables et les félicitations du jury, à l'issue
d'une thèse portant sur les points communs entre les philosophies de
Derrida et de Nelson Goodman, dont on peut supposer que le présent livre
est en partie issue. La thèse en question n'est malheureusement pas en
accès libre, pour une raison qu'il appartiendrait peut-être à Barrau
d'éclaircir, à l'heure où les écoles doctorales réclament bien
fréquemment que la version numérique de la thèse soit accessible en
ligne. On aurait pu en juger d'un peu plus près de la légitimité de
Barrau. Quoi qu'il en soit, même dans l'éventualité où M. Barrau aurait
pris la peine d'étudier avec le sérieux qu'ils méritent les quelques
auteurs qu'il mentionne, la façon dont il les invoque et les utilise
n'est pour le moins pas propice à ce qu'on lui accorde ce crédit.

Parmi les auteurs qu'il aurait été bienvenue de discuter, mais que les
auteurs qu'il a pour coutume de citer (hormis Goodman peut-être) n'ont
pas beaucoup en affection (Derrida disait que la logique, c'est la
non-pensée, Deleuze qualifiait les wittgensteiniens de méchants et de
dangereux pour la philosophie ...), Wittgenstein aurait probablement
mérité sa place. Toute son activité philosophique est en effet focalisé
sur le genre de question que se pose en dernière analyse Barrau; de quoi
peut-on être sûr (*De la Certitude*), qu'est-ce qu'avoir raison, etc.
etc. Bouveresse dans *Le Philosophe chez les Autophages* disait:

> La force de la critique de Wittgenstein provient en grande partie du
> fait qu'elle renonce délibérément à toutes les prétentions
> universalisantes et totalisantes qui ont pour effet de dépouiller la
> critique elle-même de toute espèce de légitimité, de point d'appui et,
> finalement, de sinification. Et elle ne revendique aucune position
> privilégiée et protégée à partir de laquelle la clarté philosophique
> pourrait être répandue sur la confusion ambiante.

Il est tout de même frappant que le concept de connaissance ne soit que
rarement mentionné; on se contente du concept de vérité, mais ce dont il
est question dans l'ouvrage est moins le concept de vérité en lui-même
que le genre de rapport qu'il nous est possible d'entretenir avec lui.
C'est dire que c'est peut-être moins de la vérité dans les sciences
qu'il serait question que de la nature de la connaissance, de ce qu'il
nous est possible de connaître, de ce qu'est la connaissance
scientifique, si cette dernière a quelque spécificité par rapport à la
connaissance non-scientifique etc. Nulle mention de « croyances vraies
justifiées », nulle mention d'assertabilité garantie, nulle mention de
pragmatisme, nulle mention de Gettier, de tous les apports de la
philosophie de la connaissance de ces quarante dernières années. Si
Barrau cherchait à éclaircir sa conception, ce serait peut-être du côté
de ce genre de littérature qu'il lui faudrait se tourner; il nous y
invite d'ailleurs lui-même. Soit donc il l'a lu et l'a ignorée, soit il
ne l'a pas lu mais estime qu'il serait nécessaire que nous le fassions;
les deux posent problème.

Peut-être mon imperméabilité à ce genre de posture d'inspiration qui se
voudrait philosophique provient d'arrières pensées qu'on pourrait
vaguement réduire à quelque chose comme le soupçon que Barrau investit
dans ce champ moins par intérêt strictement philosophique que par les
profits symboliques que l'investissement dans ce champ est susceptible
de générer. Pas n'est besoin de beaucoup de sociologie pour comprendre
que les profits symboliques que l'on peut espérer retirer d'un champ
aussi technique et concurrentiel que l'astrophysique sont relativement
minces au rapport du nombre et du talent de ceux qui y investissent, et
qu'une stratégie efficace d'investissement symbolique consiste à se
draper de l'hermine philosophique pour donner l'impression de se hisser
au dessus de la masse de ses pairs.

Pascal parlait ainsi de la « montre » des magistrats et des médecins,
texte que Bourdieu cite au début de l'un de ses ouvrages (la
Distinction?):

> Nos magistrats ont bien connu ce mystère. Leurs robes rouges, leurs
> hermines dont ils s'emmaillotent en chats fourrés, les palais où ils
> jugent, les fleurs de lys, tout cet appareil auguste était fort
> nécessaire. Et si les médecins n'avaient des soutanes et des mules et
> que les docteurs n'eussent des bonnets carrés et des robes trop amples
> de quatre parties, jamais ils n'auraient dupé le monde, qui ne peut
> résister à cette montre si authentique. S'ils avaient la véritable
> justice et si les médecins avaient le vrai art de guérir, ils
> n'auraient que faire de bonnets carrés. La majesté de ces sciences
> serait assez vénérable d'elle-même. Mais n'ayant que des sciences
> imaginaires il faut qu'ils prennent ces vains instruments, qui
> frappent l'imagination, à laquelle ils ont affaire. Et par là en effet
> ils s'attirent le respect.

Je me réserve le droit d'attendre de Barrau plus de preuves de son
véritable art de guérir les problèmes philosophiques avant qu'il ne
s'attire mon respect.

Références
==========

[^1]: Denn eben wo Begriffe fehlen, Da stellt ein Wort zur rechten Zeit
    sich ein.

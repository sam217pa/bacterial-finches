---
title: Gaston Bachelard
subtitle: Le Matérialisme Rationnel
author: Samuel Barreto
date: 2018-09-18
---

Quelques notes prises à la lecture de ce livre de Bachelard dans
laquelle il explicite les obstacles épistémologiques franchis par la
chimie depuis qu'elle s'est rationalisée, affranchie de pensées
matérielles primitives comme l'association des matières aux « éléments
primaires ».

# Le matérialisme rationnel

_page 6_: La chimie une science d'avenir parce qu'elle est, de plus en
plus, une science qui déserte son passé. La chimie, dans son effort
moderne, se révèle comme une science ayant été primitivement mal
fondée.

_page 10_: Le projet de Bachelard est d'établir une philosophie directe
de la matière, une philosophie qui cesserait de poser la connaissance
de la matière comme une connaissance subalterne, bref une philosophie
qui ne reconnaîtrait pas les privilèges idéalistes de la forme.

_page 15_:Ce n'est pas en ses premières démarches qu'il faut juger une
pensée qui ne vit que des rectifications qu'elle s'impose.

Il faudrait dans une philosophie complète du matérialisme évoquer une
conscience mélangeante, conscience qui accompagne plusieurs objets,
plusieurs matières, qui participe à tout ce qui se fond, à tout ce qui
s'insinue, conscience qui se trouble devant toute matière qui se trouble.

_page 16_: Dès que les matières sont considérées dans leurs réactions
mutuelles, dès que les matières sont, en quelque sorte, matière l'une
à l'égard de l'autre, apparaît un inter-matérialisme qui est un trait
spécifique de la science de la matière.

La matière se donne une forme, elle manifeste directement ses
puissances de déformation.

_page 18_: Nous défendons un psychisme pris dans la viscosité de son
cosmos en lui donnant un meilleur avenir de pétrissage.^[À rapprocher
de Merleau-Ponty, l'Oeil et l'Esprit.]

_page 53_: Nous donc bien fondés, croyons-nous, à proposer, pour
l'explication de textes comme le *Timée*, en marge des explications
d'intelligibilité, un type d'explication que, faute de meilleurs
termes, nous appelons une psychanalyse matérielle. Autrement dit, les
images matérielles soutiennent et alimentent des convictions
profondes, des convictions qui ont échappé à la discussion
intellectualiste.


_page 56-57_: Mais encore une fois, les idées ne sont pas des images,
les images ne préparent pas des idées ; souvent les idées doivent
lutter contre les premières images, c'est-à-dire rompre l'immobilité
des archétypes conservés au fond de l'âme.

L'alchimie ne prépare nullement la chimie, elle l'entrave.


_page 58_: On va voir en action une sorte de matérialisme idéaliste,
qui se met hors d'expérience en postulant une idéalité invérifiable.
Par ce processus d'idéalisation, l'alchimiste se couvre de tout risque
d'échec devant la réalité. On pourrait donner la pratique alchimique,
en cette occasion, comme une conduite de succès. Cette conduite de
succès est, semble-t-il, la conduite symétrique de la conduite d'échec
bien connue des psychanalystes. Elle consiste à prendre immédiatement
dans la pensée un succès qui compense l'échec qu'on rencontre dans la
réalité. Finalement la conduite de succès nie le réel en prétendant
l'interpréter, en prétendant l'approfondir.


_page 60_: Nous aurons à montrer la longue patience que réclame une
étude matérialiste de la matière, la difficile discipline qui lie
systématiquement les phénomènes d'une matière aux phénomènes d'une
autre matière, bref la difficile institution d'un strict
intermatérialisme.


_page 63_: Le matérialisme peut ordonner et hiérarchiser les
substances avec des coefficients d'assurance comparables aux
coefficients de la connaissance des formes. Le matérialisme instruit
peut donc faire face, comme toute autre doctrine, à la diversité
foisonnante des phénomènes et entreprendre, comme le tente toute
doctrine, une œuvre de clarification.


_page 64_: À première vue, il pourrait sembler que la notion
d'homogénéité fût comme une sorte de catégorie du matérialisme. Elle
est, par bien des côtés, un repos dans le progrès des connaissances de
la matière. Mais ce repos est toujours provisoire ; il est le point de
départ d'une dialectique matérialiste : le chimiste cherche d'abord la
substance homogène, puis il remet en question l'homogénéité, cherchant
à détecter l'autre au sein du même, l'hétérogénéité cachée au sein de
l'homogénéité évidente.


_page 65_: Rares sont les savants qui prennent goût à restituer les
avenues réelles de leur culture ; ils vivent avec trop d'intensité la
culture présente pour s'intéresser à l'obscur passé des notions.


_page 69_: Il apparaît alors, sur ce problème précis des structures,
que les matières strictement minérales et les matières organisées par
la vie n'appartiennent pas, structuralement parlant, à des règnes
absolument séparés.


_page 70_: Y a-t-il un caractère dominant de la vie en général ? Une
telle question paraît aussi vide de sens que la question : y a-t-il un
caractère qui permette de désigner la matière en général ? Le
pluralisme de la matérialité de la vie est si grand qu'il enjoint de
poser une pluralité dans les processus vitaux. Les problèmes
biologiques ne sauraient plus être éclairés, ou même simplement
désignés par la conception d'un fluide vital qui coulerait dans la
matière, qui animerait la matière.


_page 74_: Le chimiste commence sa recherche en multipliant les efforts
de décomposition. La simplicité apparaîtra alors connu une limite
à tout effort de décomposition.

Plus bas, il parle des découvertes de Cavendish sur l'eau et de
Lavoisier sur la composition de l'air. « De telles découvertes brisent
l'histoire. (...) Elles font apparaître la profondeur du chimique sur
la physique --- ou autrement dit, l'hétérogénéité chimique de
l'homogénéité physique. »


_page 77_: En gros, on peut dire qu'il n'y a pas de pureté sans
purification. Et rien ne peut mieux prouver le caractère éminemment
social de la science contemporaine que les techniques de purification.
En effet, les processus de purification ne peuvent se développer que
par l'utilisation de tout un ensemble de réactifs dont la pureté
a reçu une sorte de garantie sociale.


_page 79_: La pureté d'une substance est donc une œuvre humaine. Elle
ne saurait être prise pour une donnée naturelle.


_page 83_: La culture scientifique avance en absorbant quelques
contradictions de ses propres principes.


_page 103_: La pensée scientifique repose sur un passé réformé. Elle
est essentiellement en état de révolution continuée.


_page 111_: En science, toute simplification est provisoire.


_page 114_: Suivant la fatalité caractéristique du réalisme habituel,
même dans la pensée scientifique, le réalisme apparaît trop top, il
est scientifiquement prématuré, il est le signe d'une foi qui n'attend
pas les preuves de sa conviction pour s'affirmer.


_page 115_: Prendre la structure des molécules pour le résultat
définitif de la connaissance, voire comme *but* de la connaissance,
c'est accepter comme moteur de la connaissance la simple dynamique de
la *curiosité*. Cette simple curiosité qui veut savoir des choses « ce
qu'elles sont » est naturellement une excitation indispensable au
début de la connaissance, mais elle ne suffit plus à caractériser le
véritable intérêt pour l'expérience scientifique contemporaine. Sans
doute la curiosité est un intérêt primaire qui reste une composante
des intérêts beaucoup plus complexes de u travail scientifique. Mais
c'est un véritable défaut d'analyse psychologique que de maintenir cet
intérêt primaire comme intérêt *fondamental.*


_page 122_: Mais on ne gagne rien, dans une culture, à dénoncer sans
cesse le *caractère hypothétique des hypothèses*, non plus qu'à se
limiter à une affirmation inconditionné sur la réalité des faits. Il
faut que le problème de l'interprétation des faits soit toujours
ouvert. Et tous les essais d'interprétation doivent laisser une trace
dans la culture.


_page 123_: L'esprit scientifique moderne réalise un juste dosage de
prudence et d'audace ; il est sans cesse animé par une sorte de
dialectique de l'invention et de la réflexion.

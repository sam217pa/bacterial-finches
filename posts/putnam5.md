---
title: Wittgenstein, la Référence et le Relativisme
subtitle: Renewing Philosophy, 1995
author: Hilary Putnam
translator: Samuel Barreto
date: 2018-12-20
draft: false
---

J'ai discuté de la suggestion selon laquelle Wittgenstein pensait que
le langage religieux est non-cognitif (bien qu'il ne le dise pas
explicitement). Mais à qu'est-ce que « non-cognitif » veut dire
lorsqu'on suggère que le « langage religieux est non-cognitif »? La
manière réaliste traditionnelle d'expliquer que le langage religieux
est non-cognitif serait de dire que les termes descriptifs ordinaires
comme « mon frère » ou « Amérique » ou « l'Arc de Triomphe » font tous
référence à quelque chose, mais pas les mots utilisés dans les
contextes religieux dont Wittgenstein discute.

Wittgenstein indique-t-il que lorsque l'on parle de l'Œil de Dieu ou
du Jugement Dernier, c'est _simplement_ une image, c'est-à-dire, qu'on
ne réfère à rien?

Étrangement, Wittgenstein interrompt ce troisième cours pour parler du
phénomène d'une pensée qui serait à propos de « mon frère en
Amérique » (Wittgenstein parle aussi de « référer »; c'est-à-dire, il
dit aussi bien d'une pensée qu'elle est « à propos » de son frère en
Amérique et de mots comme « référant » ou « désignant »). Il n'y a pas
d'indication dans les notes sur la raison pour laquelle Wittgenstein
interrompt un cours sur la croyance religieuse pour discuter de ce
sujet. Les notes de ce cours remplissent huit pages du volume édité
par Cyril Barrett, et presque trois pages sont dédiées à cette
discussion de la référence. La discussion étant séparée du reste du
texte par des espaces vides, les éditeurs eux-mêmes la voyait donc
comme une forme de digression.

De plus, des preuves textuelles suggèrent qu'il s'agit d'une
digression. Ce qui précède la digression est une question sur deux
phrases, « cesser d'exister » et « être un esprit désincarné ».
Wittgenstein dit, « "Lorsque je dis ceci, je me vois comme ayant un
certain ensemble d'expériences."  À quoi cela ressemble-t-il de penser
ceci? » Il revient à cette question après ce que j'appelle la
digression sur la référence. Mais aucun exemples provenant du discours
religieux ou du spiritualisme (que Wittgenstein distingue de la
religion) n'intervient dans cette digression. Le seul exemple utilisé
consiste à penser au frère de Wittgenstein comme étant en Amérique.
Pourtant je ne crois pas que cette digression puisse être un accident.
Cela tient précisément à la crainte que j'ai suggérée être derrière la
remarque de Smythie, la crainte que Wittgenstein soit en train de
pointer vers une différence fondamentale entre le discours religieux
et non-religieux, c'est-à-dire que le discours religieux ne se réfère
à rien (ou ne soit « à propos » de rien). L'inquiétude est que dans le
langage ordinaire nous avons des images (et bien sûr, des mots) et des
utilisations d'images et de mots, et quelque chose au-delà des mots et
des images, alors que dans le langage religieux nous n'avons que des
images et des mots et des usages d'images et de mots.

Je veux suggérer que lorsque Wittgenstein répond « Ridicule » au
commentaire de Smythie, et se hâte d'ajouter ensuite que tout ce qu'il
avait l'intention de faire était une remarque « grammaticale », son
impatience initiale peut être attribuée au fait qu'il a le sentiment
d'avoir déjà traité des problèmes que Smythie soulevait (pas à cette
occasion, mais à de maintes reprises dans ses cours et conversations
philosophiques des années trente), au moins implicitement.

Le premier point que Wittgenstein avance peut sembler étrange
aujourd'hui alors qu'il y a tant de discussions sur les « théories
causales de la référence ». Wittgenstein est étonné du fait qu'il
puisse penser à son frère en Amérique alors même qu'il n'y a pas
d'interactions causales entre lui et son frère actuellement. En effet,
Wittgenstein suppose que nous ne pensions pas même à la référence
comme à une relation causale. Notre tentation naturelle est de penser
que l'intentionnalité de nos mots leur est donnée par l'expérience de
la pensée elle-même. « Si l'on vous demande: "Comment savez-vous que
c'est une pensée de ci ou de ça?" la pensée qui vient immédiatement
à l'esprit est celle d'une image, d'une ombre. Vous ne pensez pas
à une relation causale. Le genre de relation auquel vous pensez est le
mieux exprimé par "image", "ombre", etc. » Et Wittgenstein continue de
parler d'une manière familière aux lecteurs des _Recherches
Philosophiques_ sur la façon que nous avons de tout à la fois tendre
à voir les pensées comme des images mentales et à leur attribuer des
pouvoirs qu'aucune image ne peut avoir.


> Le mot « image » est même parfaitement bon --- dans bien des cas,
> c'est même, au sens le plus ordinaire, une image. Vous pourriez
> transcrire mes propres mots en une image.

> Mais le point est le suivant, supposez que vous dessiniez ceci,
> comment saurai-je que c'est mon frère en Amérique? Qui dit que c'est
> lui --- à moins que ça ne soit là de la similarité ordinaire?

> Quelle est la connection entre ces mots, ou quoi qu'on leur
> substitue, et mon frère en Amérique?

> La première idée [que vous avez] est que vous êtes en train de
> regarder vos propres pensées, et êtes donc absolument certains que
> c'est une pensée quant à ceci ou cela. Vous êtes en train de
> regarder un phénomène mental, et vous vous dites "de toute évidence
> c'est une pensée sur mon frère en Amérique." Cela semble être une
> super-image. Il semble, avec la pensée, qu'il n'y a pas le moindre
> doute. Avec une image, cela dépend encore de la méthode de
> projection, alors qu'ici il semble que vous soyiez débarassé de la
> relation de projection, et êtes absolument certains que c'est une
> pensée de ceci.

Wittgenstein n'est pas, bien sûr, en train de penser à des théories
causales du genre de celles avancées par Fodor, ni aux théories
causales parfois attribuées (à tort) à moi ou à Saul Kripke. Le point
qu'il fait est, je crois, phénoménologique: si je suis une personne en
Angleterre pensant à mon frère en Amérique, alors je ne me conçoit pas
comme ayant une quelconque interaction causale avec mon frère en
Amérique. Que la référence ne soit pas une interaction causale en
cours, est, bien sûr, vrai de toute théorie de la référence. De
l'autre côté, bien que Wittgenstein ne pense pas à ça, il est
causalement connecté à son frère en ce sens qu'il a interagit
causalement avec lui dans le passé, l'interaction causale a laissé des
traces cérébrales qui ont perduré dans son cerveau jusqu'à ce jour, et
ainsi de suite. Et il est « causalement connecté » à l'Amérique (même
s'il n'était jamais allé en Amérique), en ce sens qu'il a acquis le
mot « Amérique » de gens l'ayant acquis de gens l'ayant acquis de gens
... qui étaient en Amérique. Cette histoire est, bien sûr, bien connue
de nos jours.

Je voudrais spéculer quelque peu sur ce que Wittgenstein aurait dit de
tout ceci, parce que je crois que cela nous aidera à comprendre les
points qu'il avance, à la fois dans les brèves remarques sur la
référence transcrites dans ces notes, et en détail dans les
_Recherches philosophiques_. Mais d'abord je vais revoir quelques
points sur la référence qui étaient familiers de Frege, et en fait
aussi familiers de Kant, mais qui ont été négligés, sinon complètement
oubliés, dans les discussions actuelles sur les théories causales de
la référence.

Pour cela, je voudrais ignorer le fait que la théorie de Fodor ne
fonctionne pas. Admettons que la théorie de Fodor marche
parfaitement --- que ses contrefactuels soient tous vrais, et qu'ils
fournissent une condition nécessaire et suffisante pour qu'un mot
réfère à une sorte de chose --- ou alors que quelqu'un d'autre
a réussi à proposer une définition de la référence en terme
d'« attachement causal ». Que peut-il bien nous rester à dire de la
référence?

La première chose à noter est que --- quoique des termes tels que
« théories causales de la référence » puissent suggérer le
contraire --- personne ne croit réellement que _tous_ les cas de
référence soient des cas d'attachement causaux. Il est évident que
nous pouvons faire référence à des choses avec lesquelles nous n'avons
pas interagi causalement. Nous pouvons faire référence à des choses
futures, par exemple, le premier bébé qui naîtra en l'an 3000. Je peux
faire référence à des choses en dehors de mon « cône de lumière », par
exemple, s'il y a des galaxies en dehors de mon cône lumineux, je peux
faire référence à la galaxie la plus proche dans une certaine
direction, et ainsi de suite. Un théoriste causal qui rend
explicitement compte de cela dans diverses publications est Richard
Boyd. Boyd a proposé d'utiliser la distinction de Russell entre la
connaissance par accointance et la connaissance par description, et de
la modifier de la façon suivante: au lieu de parler de deux types de
connaissance, nous devrions parler de deux types de référence, la
référence par connection causale et la référence par description.
L'idée étant que nous puissions faire référence à des choses qui ne
nous sont pas causalement attachés parce que nous pouvons en produire
des descriptions en utilisant des termes qui eux font bien référence
par attachement causal.

Un autre problème --- qui, comme nous le verrons, est étroitement lié
au problème dont traite Boyd --- est que la théorie de la connection
causale rend compte de la référence de mots individuls mais non de la
vérité de phrases. Même si le mot _chat_ tient sa référence du fait
que les chats causent la symbolisation du « chat », _tapis_ tient sa
référence du fait que les tapis causent la symbolisation du « tapis »,
et « sur » tient sa référence du fait que des instance de la relation
spatiale d'une chose étant sur une autre causent la symbolisation de
« sur », comment ceci rend-t-il compte du fait que la phrase complète
« le chat est sur le tapis » ait la valeur de vérité qu'elle a dans
diverses situations?

La relation entre ces deux problèmes est celle-ci: bien que ce soit
les mots individuels qui sont attachés causalement, en ce sens qu'ils
sont les terminaisons du genre de chaines causales dont parle Fodor
(bien sûr, d'autres théoriciens parlent de différentes sortes de
chaines causales), ce sont pourtant d'une manière ou d'une autre les
groupes de mots organisés par leur structure syntaxique qui ont une
valeur de vérité. Dire que si les parties d'une expression faisant
référence font référence, alors évidemment l'expression comme un tout
fait référence, est trop rapide. En un sens en fait, c'est faux. Si
nous disons que dès lors que les parties d'une expression faisant
référence font référence _en ce sens qu'elles sont causalement
attachées à un référent_, alors l'expression faisant référence en tant
que tout fait référence _dans le même sens_ --- c'est-à-dire, en ce
sens qu'elle est causalement attachée à un référent ---, alors ceci
est faux. Dans la phrase « le premier bébé de l'an 3000 », le mot
« bébé » est causalement attaché à une espèce, c'est-à-dire, le
locuteur et sa communauté de langage ont eu des interactions causales
avec des bébé dans lesquelles la « propriété d'être un bébé » jouait
un rôle causal pertinent (il me coûte de parler de cette façon, mais
c'est là le genre de langage métaphysique que ces gens emploient!),
mais la phrase « le premier bébé de l'an 3000 » n'est pas causalement
attachée à son référent de la même manière, nous n'avons pas
causalement interagi avec le premier bébé de l'an 3000, et la
« propriété d'être le premier bébé de l'an 3000 » n'a pas encore eu
d'influence causale sur nous.

D'un autre côté, si quelqu'un dit « le mot _bébé_ fait référence d'une
façon, et cela explique pourquoi la phrase _fait référence d'une autre
façon_ », alors il nous faut une théorie de ces différentes façons de
faire référence. Étrangement, les théoristes causaux ne semblent pas
reconnaître qu'il nous faille là une quelconque théorie. Hartry Field,
par exemple (qui semble avoir ultérieurement adopté une position
agnostique quant aux théories causales de la référence) suggérait il
y a longtemps dans son fameux article sur la vérité que si nous
pouvons construire une théorie causale de ce qu'il appelait la
« référence primitive », c'est-à-dire, la référence dans le cas de ce
que seraient les termes primitifs du français dans une formalisation
du français appropriée, alors la référence dans le cas d'expression
complexes peut être défini par une définition récursive dans le style
de Tarski. Une telle définition suppose simplement que le résultat de
la jonction de deux expressions référantes avec _et_ produit une
expression faisant référence (dans un sens inexpliqué)
à l'intersection de l'extension des deux expressions jointes; que
préfixer un signe de négation à une expression référante résulte en
une expression référante faisant référence (en un sens inexpliqué) au
complément de l'extension de l'expression à laquelle on ajoute le
signe de négation; et ainsi de suite. Mais cela ne peut sûrement pas
faire partie ni de la signification ni de la nature de la référence.
Il n'y a rien dans la nature de la référence qui fasse qu'il soit le
cas que joindre deux expressions par _et_ résulte en quoi que ce soit.
Si la jonction d'expression avec _et_ nous donne une intersection
d'extension, alors cela doit avoir à voir avec le rôle de _et_ dans le
langage. En bref, il nous faut une théorie de la « référence par
description » tout autant qu'une théorie de la référence primitive.

Pour résumer, si nous acceptons la distinction Boyd-Field entre la
référence primitive et la référence par description pour le moment,
alors une théorie de la référence primitive n'est pas une théorie de
la référence du tout. Au mieux c'est une définition d'un néologisme,
c'est-à-dire, c'est une définition d'un concept construit dans
l'espoir qu'il nous aide à donner une théorie de la référence. C'est
seulement s'il l'on avait une définition de la référence primitie _et_
une théorie de la référence par description que nous pourrions
éventuellement dire que nous disposons d'une théorie de la référence,
et jusqu'alors aucune des théories causales n'a réellement tenté de
donner une théorie de la référence par description.

Voici une façon de rendre le problème clair, une façon qui, par
essence, remonte à Frege et même à Kant: la phrase « le chat est sur
le tapis » est constituée d'exactement les mêmes mots que la simple
liste « le », « chat », « est », « sur », « le », « tapis ». Pourtant
la phrase a une valeur de vérité, dans une situation pertinente, quand
la liste n'en a aucune. Sur quoi repose la différence entre une phrase
et une liste? La phrase « le premier bébé de l'an 3000 » a un
référent, quand la liste composée des mêmes mots dans le même ordre ne
fait référence à rien (à moins que l'on ne dise qu'elle fait référence
aux mots listés). Là encore, sur quoi repose la différence? Dans la
terminologie de Kant, un jugement n'est pas qu'une série de
représentations mais leur « synthèse »; c'est précisément ce problème
qui a conduit Frege à donner la priorité au sens des phrases sur le
sens des mots dans sa théorie du langage.

La réponse à ce problème est en un sens presque évidente, ou l'est du
moins après Wittgenstein: ce qui fait qu'il soit le cas qu'une phrase
ait une valeur de vérité ou qu'une phrase complexe ait une référence,
quand une simple liste de mots n'a ni valeur de vérité ni référence,
est que nous _utilisons_ les phrases de nombreuses façons bien
différentes que celles dont nous utilisons de simples listes. Cette
observation sape complètement l'idée d'une théorie de la référence
simplement causale. Faire référence, je le répète, c'est user des mots
de certaines façons (ou, pour anticiper un peu, de l'une des variétés
de façons). Il se pourrait bien que l'usage des mots d'une référence
particulière soit impossible si nous n'étions pas causalement
connectés aux choses auxquelles nous faisons référence; en fait, je
crois même que c'est le cas. Mais cela veut dire qu'il y a des
contraintes causales sur la référence, non que la référence _est_ la
connection causale. Quelle que soit la manière dont _chat_ est
causalement connecté au monde, si je dis « chat, chat, chat, chat,
... » une centaine de fois, je ne fais pas plus référence aux chats,
alors que si j'utilise le mot _chat_ de certaines façons, je fait
référence aux chats.

Dans sa digression sur la référence, Wittgenstein parle de ce que
j'appelle un usage référentiel du langage comme d'une « technique
d'usage », et il suggère que l'illusion de l'intentionalité
intrinsèque, c'est-à-dire, l'illusion que la référence est une chose
mystérieuse existant lorsque nous pensons et dont on ne peut rien
dire, est due au fait que nous ne nous intéressons qu'à notre
expérience subjective et non à la technique d'usage du mot:

> ["Est-ce que penser n'advient qu'à un moment précis, ou diluée parmi
> les mots?" "Cela survient en un éclair ." "Toujours? --- cela
> advient bien parfois en un éclair, mais cela peut aussi bien être
> tout autre espèce de choses."]

> Si cela fait bien référence à une technique, alors il ne peut
> suffire, en certains cas, d'expliquer ce que vous entendez en
> quelques mots; car il y a une chose qui pourrait être vue comme en
> conflit avec l'idée survenant de 7 à 7,5, c'est-à-dire sa la
> pratique d'usage [de la phrase].

> Lorsque nous parlions de: « Ceci ou cela est un automate », le point
> de force de cette idée était due à ce que vous pourriez dire: « Bon,
> je sais ce que je veux dire » ..., tout comme si vous étiez en train
> de regarder quelque chose se produire pendant que vous disiez la
> phrase, totalement indépendant de ce qui arrive avant et après,
> l'application [de la phrase]. Comme si vous pouviez dire comprendre
> un mot, sans la moindre référence à ses techniques d'usage. Comme si
> Smythies disait comprendre la phrase, et que nous n'avions alors
> plus rien à en dire.

À un moment donné, j'ai moi-même eu l'espoir que ce dont Wittgenstein
parle comme l'usage des mots, ou dans ce cours leurs techniques
d'usage, puisse être complètement revu et analysé d'une façon
fonctionaliste; c'est-à-dire, que tous les divers usages de référence
d'un mot puissent être organisés proprement et décris par une sorte de
programme informatique. Dans _Représentation et Réalité_, j'ai
expliqué mes raisons de penser qu'il est hautement probable que cela
ne puisse être fait. (Les difficultés de l'Intelligence Artificielle
que j'ai décrite au chapitre 2 sont liées à certaines de ces raisons.)
Mais si je ne peux exposer tous les usages de référence des mots,
alors en un sens nous n'avons pas du tout de _théorie_ de « la nature
de la référence » (pas même si nous parvenons à montrer que nos mots
sont causalement attachés à ce à quoi ils font référénce de certaines
façons). Si nous ne pouvons donner une sorte de théorie scientifique
de la nature de la référence, c'est-à-dire, des usages de référence de
nos mots, comment sommes-nous censés appréhender la référence?

Dans les _Recherches Philosophiques_, Wittgenstein s'oppose à l'idée
que l'on ne puisse user d'un mot que lorsqu'on possède une condition
nécessaire et suffisante de son application. Il utilise le mot
« jeux » comme un exemple (l'exemple étant désormais fameux), et dit
que dans le cas de ce mot nous n'avons pas une condition nécessaire et
suffisante. Nous avons certains paradigmes --- de différentes sortes,
en fait --- et nous étendons le mot « jeu » à de nouveaux cas parce
qu'ils nous semblent similaires aux cas dans lesquels nous l'avons
utilisé auparavant (il décrit cela comme notre « réaction
naturelle »). Il parle des jeux comme formant une famille, comme ayant
un air de famille, et utilise aussi la métaphore d'une corde. La corde
est faite de fibres, mais il n'y a pas de fibres qui parcourt toute la
longueur de la corde. Il y a des similarités entre jeux, mais il n'y
a pas une similarité unique entre jeux.

Alors que la notion d'air de famille pour les mots est devenue un lieu
commun, nombreux sont ceux ayant manqué le point de Wittgenstein:
comme Rush Rhees le soulignait il y a longtemps, Wittgenstein ne
faisait pas seulement une observation empirique fondamentale du fait
qu'en plus de mots omme _écarlate_, qui s'appliquent à des choses
toutes similaires d'un certain point de vue, il y a des mots comme
_jeux_ qui s'appliquent à des choses qui ne sont pas toutes similaires
du même point de vue. Wittgenstein ne pensait pas d'abord à des mots
comme _jeux_, mais à des mots comme _langage_ et _référence_. Ce sont
précisément ces grandes notions philosophiques auxquelles Wittgenstein
veut appliquer cette notion d'air de famille. À la lecture de Rush
Rhees (et je suis persuadé qu'il a raison), ce que Wittgenstein nous
dit, c'est que les usages de référence n'ont pas d'« essence »; il n'y
a pas une chose unique qui puisse être appelée la référence. Il
y a des similarités chevauchantes entre certaines sortes de référence,
voilà tout. C'est pourquoi, par exemple, Wittgenstein n'est pas
troublé, comme de nombreux philosophes, par le fait que nous puissions
« faire référence » à des entités abstraites. Après tout, nous ne
sommes pas causalement attachés au nombre trois, donc comment
pouvons-nous y faire référence? En fait, savons-nous qu'il y a même un
tel objet? Pour Wittgenstein le fait est que les usages des mots de
nombre sont simplement différents des usages de mots comme _vache_.
Cessons d'appeler trois un « objet » ou une « entité abstraite » et
penchons-nous plutôt sur les façons dont les mots de nombres sont
utilisés, selon lui.

Aussi, la pertinence de ceci dans un cours sur la philosophie de la
religion est la suivante: tout comme j'ai suggéré que Wittgenstein
n'aurait pas considéré le discours d'incommensurabilité utile, pas
plus qu'il n'aurait considéré utile de parler de certains discours
comme étant « cognitifs » et d'autres « non-cognitifs », je suggère
qu'il n'aurait pas non plus considéré la question de la « référence »
du langage religieux comme utile. (Il parle d'un « embrouillamini ».)
L'usage du langage religieux est à la fois similaire et différent des
cas ordinaires de référence; mais se demander si c'est « vraiment » ou
« pas vraiment » de la référence, c'est être dans un embrouillamini.
Il n'y a pas d'essence de la référence. Les penseurs religieux seront
les premiers à dire que lorsqu'ils font référence à Dieu, leur « usage
de référence » n'est guère semblable à l'usage de référence de « son
frère en Amérique ». En bref, Wittgenstein indique là où _n'est pas_
la bonne façon de comprendre le langage religieux. La bonne façon de
comprendre le langage religieux n'est pas de tenter de lui appliquer
une quelconque classification métaphysique des formes possibles de
discours.

# Un relativisme Wittgensteinien?

Nous ne sommes en aucune façon au bout de notre quête interprétative.
Wittgentein a-t-il simplement immunisé le langage religieux de toute
critique? Wittgenstein a-t-il rendu impossible d'être athée?

Pas complètement. D'abord, Wittgenstein se présente comme un
non-croyant dans ces cours. Comme je l'ai souligné, il était très
respectueux de la croyance religieuse; il me semble que, d'une
certaine façon, il aspirait à la croyance religieuse mais n'y parvint
pas; il s'est décrit lors d'une conversation comme ayant eu un
« tempérament religieux ». Pourtant il y a peu de doute que
Wittgenstein dise la vérité lorsqu'il disait qu'il ne dirait jamais
lui-même qu'il croit qu'il y aura un Jugement Dernier. En effet,
Wittgenstein dit --- et je suis certain qu'il était sincère --- qu'il
n'est pas même sûr de _comprendre_ l'homme qui dit qu'il y aura un
Jugement Dernier. (Et lire plus de philosophie ou plus de linguistique
ne va pas aider Wittgenstein à décider s'il le comprend ou non.)

Quelle est l'attitude de Wittgenstein envers l'autre camp, envers ceux
qui combattent la croyance religieuse, qui la combattent ardemment,
qui la dénoncent? Ici nous n'avons que peu à débattre. Mais
permettez-moi de spéculer. Tout d'abord, Wittgenstein dit bien, au
début du premier cours, que la frontière entre croyance religieuse et
croyance scientifique n'est pas toujours nette, que l'on ne peut
penser que les deux sont séparées par un gouffre. Ici Wittgenstein
pense clairement non à nos propres sociétés, dans lesquelles une
distinction entre science et religion a été institutionnalisée, mais
à une société prétendue primitve:

> Nous arrivons à une île et y trouvons des croyances, dont certaines
> que nous serions enclins à dire religieuses ... Ils ont des phrase,
> et aussi des énoncés religieux.

> Ces énoncés ne diffèreraient pas seulement par ce dont ils traitent.
> Des connections entièrement différentes les rendraient croyances
> religieuses, et il peut tout à fait y avoir des transitions dont
> nous ne saurions pas capables de dire si ce sont des croyances
> scientifiques ou religieuses.

Je prend ces commentaires de Wittgenstein comme une justification
liant ce qu'il dit de la croyance religieuse dans ces trois cours à ce
qu'il pensait des prétendus peuples primitifs dans ses notes sur
_Golden Bough_, de Frazer. En particulier, il est clair d'après ces
notes que Wittgenstein pense que nous avons tendance à appréhender des
cultures primitives d'une façon fondamentalement suspicieuse et
auto-élogieuse; au lieu de voir en quoi sont différents les jeux de
langage « primitifs » des notres, nous ne les voyons que comme
inférieurs aux notres. Nous ne parvenons pas à voir la différence
énorme entre celui qui joue à nos jeux de langage et qui commet une
bévue, ou qui est stupide, et celui dont la forme de vie est
entièrement différente. En particulier, Wittgenstein ne voit pas la
magie primitive comme une science ratée ou inférieure. En effet,
Wittgenstein nous taxe d'arrogance, et plus que d'arrogance, de
narcissisme.

Cela n'implique pas que nous ne puissions jamais critiquer une culture
primitive. Je laisserai là les cours de Wittgenstein sur la croyance
religieuse et me tournerai pour le reste de ce chapitre vers _De la
Certitude_. Certaines de ses remarques, plus spécialement les
remarques quant à la possibilité de combattre une autre culture et la
difficulté à donner des raisons du combat, ont conduit certains à voir
Wittgenstein comme un total relativiste culturel. Je crois en fait que
Saul Kripke a lu ces passages en ce sens, et je pense que cela peut
sous-tendre la lecture que Kripke nous propose dans _Wittgenstein on
Rules and Private Language_.

La première chose à noter est que Wittgenstein critique bien,
occasionnellement, une croyance primitive; par exemple, il décrit le
supplice par le feu comme une « absurde » façon d'atteindre un
verdict. Mais ce que Wittgenstein dit dans _De la Certitude_ sur la
lutte contre d'autres formes de vies, d'autres jeux de langages,
pourrait encore donner lieu à une autre interprétation de la position
qu'il a dans les notes sur le _Golden Bough_ de Frazer et les Cours
sur la Croyance Religieuse. Peut-être que Wittgenstein pense seulement
qu'il y a une diversité de jeux de langages possibles, une diversité
de formes de vie humaine, et qu'il n'y a rien à dire quand à la
justesse ou la fausseté des unes par rapport aux autres. En fait,
Wittgenstein ne pense pas même que l'on puisse choisir l'une par
rapport à l'autre, puisqu'il est clair que nous ne _choisissons_ pas
notre forme de vie dans le sens où Wittgenstein utilise « forme de
vie ». Un argument fort en faveur d'une telle lecture pourrait
provenir des suivants:

> §608: Est-il mauvais pour moi d'être guidé dans mes actions par les
> propositions de la physique? Dois-je dire que je n'ai pas de bonnes
> bases pour le faire? N'est-ce pas cela précisément que nous appelons
> des « bonnes bases »?

> §609: Supposons que nous rencontrions des gens qui ne voient pas
> cela comme une bonne base, ni comme une raison concluante. Comment
> l'imaginer? Au lieu du physicien, ils consultent un oracle. Et pour
> cela nous les considérons comme primitifs.
>
> Est-il mauvais pour eux de consulter un oracle et d'être guidés par
> lui? --- En traitant cela de "mauvais", n'utilisons-nous pas nos
> jeux de langage comme une position d'où _combattre_ les leurs?

> §610: Et sommes-nous justifiés de les combattres? Bien sûr il y aura
> toutes sortes de slogans employés pour justifier nos actions.

> §611: Lorsque deux principes se rencontrent et se trouvent être
> mutuellement inconciliables, alors chacun déclare l'autre hérétique
> et idiot.

> §612: J'ai dit que je "combattrais" l'autre homme --- mais ne lui
> donnerai-je pas des _raisons_? Certainement, mais jusqu'où
> iraient-elles? Après les raisons vient la _persuasion_ (Pensez à ce
> qu'il se produit lorsque des missionnaires convertissent des
> locaux.)

Cela ressemble d'assez près à du relativisme. (Je me souviens combien
j'ai été surpris la première fois que j'ai découvert ces paragraphes.)
Mais, si l'on s'y penche d'un peu plus près, l'interprétation
relativiste semble de moins en moins convaincante. Si tout ce dont
nous disposions était §609, alors on pourrait dire que Wittgenstein
prenait ses distances avec ceux qui disent d'une consultation d'oracle
qu'elle est « mauvaise ». Il n'est pas clair que le « nous » dans « En
traitant cela de "mauvais", n'utilisons-nous pas nos jeux de langage
comme une position d'où combattre les leurs? » inclue Wittgensein
lui-même. Mais cette ambiguïté est immédiatement levée par le
paragraphe §612, lorsque Wittgenstein dit « J'ai dit que je
"combattrais" l'autre homme ». Donc ici Wittgenstein n'est pas un
simple spectateur. Wittgenstein lui-même combattrait parfois un autre
jeu de langage. Qui ne le ferait pas? (Quelle personne honnête ne
combattrait pas un jeu de langage impliquant un supplice par le feu,
par exemple?) Nous ne pouvons supposer qu'il ne croirait pas ce qu'il
dirait pour combattre un autre jeu de langage (par exemple, qu'il est
« absurde » de tenter d'atteindre un verdict par le supplice du feu),
ni qu'il l'interpréterait d'une façon métaphysique particulière,
puisque toute le message de _De La Certitude_ est que nous n'avons pas
d'autre choix que de nous tenir dans notre jeu de langage propre. Si
des mots comme « savoir », par exemple, ne peuvent porter un sens
métaphysique, comme il le suggère quelque part, c'est une raison
d'autant plus forte de les utiliser de façon appropriée et sans
emphase métaphysique. Wittgenstein pense simplement qu'il est absurde
de résoudre un problème par un supplice du feu.

Alors, qu'en est-il du reste du paragraphe 612? Je pense que
Wittgenstein nous dit simplement ce qui est le cas: que lorsque nous
tentons de débattre avec, mettons, les Azande, il arrive parfois que
nous ne puissions trouver de raisons qui le soient pour eux; nos
visions du monde sont si totalement différentes qu'il nous arrive dans
un débat avec un Azande intelligent de ne pas pouvoir recourir à des
arguments ordinaires qui soient basés sur des prémisses communes, et
que nous devions recourir à la persuasion.

Mais ce qui m'intéresse bien plus que le paragraphe 612 est le ton
qu'utilise Wittgenstein dans les paragraphes 610 et 611. Wittgenstein
a dit (§605) qu'il combattrait le supplice du feu, et établit ici
clairement qu'il combattrait le recours à un oracle pour la sorte de
prédictions que nous attribuons généralement à la physique. Mais les
références à « toutes sortes de slogans » ["allerlei Schlagworten"] et
l'énoncé au paragraphe 611 que « chacun déclare l'autre hérétique et
idiot » sonnent, à mon sens, comme une preuve d'aversion. Si je suis
dans le vrai en lisant les Cours sur la Croyance Religieuse juxtaposés
avec les notes sur le _Golden Bough_ de Frazer, alors Wittgenstein
attends de nous que nous nous arrêtions et réfléchissions à _quand_
nous devrions combattre un jeu de langage religieux ou primitif qui ne
soit ni le nôtre, ni de notre culture. Si nous voyons d'autres jeux de
langages comme des formes stupides ou ignorantes de nos propres jeux
de langages, nous les combattrons constamment et échouerons toujours
à _voir_. Si nous voyons avec plus de discernement, nous pourrions
voir qu'il y a moins de langages que nous voudrions combattre. Mais
quand bien même nous le devrions, et Wittgenstein se joint parfois
à nous, nous n'avons pas à crier « Hérétique! » ou « Imbécile! ».

Elizabeth Anscombe a une fois demandé à Ludwig Wittgenstein ce qu'il
ferait si l'un de ses amis croyait à la guérison par la foi;
tenterait-il de l'en dissuader? Et Wittgenstein répondit que oui, mais
qu'il ne savait pas pourquoi. (Dans une conversation, Saul Kripke m'a
un jour cité cela comme une preuve claire que Wittgenstein était un
relativiste.) Je crois que Wittgenstein ne voulait pas dire, dans sa
réponse à Elizabeth Anscombe, qu'il ne savait pas si les sulfamides ou
la pénicilline étaient plus efficaces dans le traitement de la
pneumonie bactérienne que la guérison par la foi. C'est plutôt qu'il
est tout à fait inutile de dire ceci à une personne qui croit à la
guérison par la foi (c'est une prémisse qu'il ne partage pas), et
Wittgenstein s'abstient, je pense, d'employer _allerlei Schlagworten_.
Les slogans font partie de nos jeux de langage, mais ils n'en sont
d'aucune façon les meilleures parties ni les plus nobles.

Pourtant, que devrions-nous répondre à celui qui pense que ces
extraits montrent de Wittgenstein qu'il pensait que les jeux de
langages sont des faits de nature arbitraires et irrationnels? Une
telle lecture n'est-elle pas encouragée par la suivante:

> §559: Il vous faut garder à l'esprit qu'un jeu de langage est pour
> ainsi dire imprévisible. Je veux dire qu'il ne repose pas sur des
> motifs. Il n'est pas raisonnable (ou déraisonnable).
>
> Il est --- tout comme notre vie.

En allemand, l'antépénultième phrase est « Nicht vernünftig (oder
unvernünftig) ». Je cite l'allemand ici, parce que _vernünftig_ a un
connotation différente du français « raisonnable ». « Le jeu de
langage n'est pas raisonnable » suggère qu'il n'est pas basé sur des
raisons; bien que ce soit vrai, je ne crois pas que ce soit le point,
ou du moins pas le seul, que Wittgenstein avance ici. En allemand le
mot _vernünftig_ est lié à _Vernunft_, et cette notion particulière de
la Raison a acquis ses lettres de noblesses avec Kant, qui contrastait
la Raison en ce sens avec la simple compréhension, _Vernunft_ avec
_Verstand_. (Dans la tradition qui vient de Hume, de l'autre côté,
_raison_ et _raisonnabilité_ sont contrastés; Hume nous dit que la
raison ne peut montrer que le soleil se lèvera demai, mais que ce ne
serait pas déraisonnable de s'attendre à ce qu'il ne le fasse pas.) Je
suis enclin à penser de Wittgenstein qu'il dise au paragraphe 559 que
les jeux de langages sont comme notre vie en ce que ni nos jeux de
langages ni nos vies ne sont basés sur la _Vernunft_, ce qui constitue
un rejet direct du cœur de la philosophie de Kant. Pour Kant la vie
humaine et le langage humain se distinguent précisément par cette
capacité transcendantale unique que Kant appelle la _Vernunft_.
Wittgenstein ne nie pas que nous comprenions certaines choses et que
nous raisonnions; en fait, _De la Certitude_ est une vaste discussion
de quand nous pouvons dire que nous comprenons quelque chose et de
quand nous ne le pouvons pas, de quand nous pouvons savoir quelque
chose, montrer quelque chose, avoir des raisons pour quelque chose,
être certains de quelque chose, etc … Mais, comme John Dewey, dont je
discuterai des travaux au prochain chapitre, Wittgenstein a une vision
naturaliste (mais non réductionniste) de l'homme. Nous ne sommes pas
de simples animaux, mais nos capacités de compréhension et de
raisonnement proviennent de capacités plus primitives que nous
partageons avec les animaux (dans le même sens, Dewey parlait de
« continuité biosociale »). Cette prémisse naturaliste de Wittgenstein
est affichée explicitement au paragraphe 475: « Je vois ici l'homme
comme un animal. Comme un être primitif doté d'instincts mais non de
ratiocination. Comme une créature à l'état primitif. Toute logique
suffisamment bonne pour servir de moyen primitif de communication n'a
pas besoin d'excuse pour nous. Le langage n'émerge pas d'une forme
quelconque de ratiocination. »

« Je vois ici l'homme comme un animal » fait seulement référence à la
discussion en cours à cet endroit précis de _De la Certitude_; mais ce
qui suit est un énoncé général du naturalisme de Wittgenstein. Sachant
ce naturalisme, la prétention que les jeux de langage ne sont ni
_vernünftig_ ni _unvernünftig_ semble aller de soi. La vie humaine
n'est pas la manifestation empirique d'une capacité transcendantale de
la raison.

Pourtant, _vernünftig_ a un usage courant très similaire à celui du
français _raisonnable_. (Je ne cherche pas ici à ergoter avec les
traductions.) N'y-a-t'il pas un sens à dire que nos jeux de langage,
ou une bonne part de nos jeux de langage, sont « raisonnables »? Le
jeu de langage de la physique n'est-il pas raisonnable, sachant le but
qu'il poursuit? En fait, l'existence même d'un jeu de langage pour
dire à autrui si nous avons faim ou soif n'est-elle pas parfaitement
raisonnable? En un sens, j'ai déjà répondu à cette objection; le
besoin de communiquer sa faim ou sa soif pour les satisfaire est
exactement le genre de besoin auquel pense Wittgenstein au
paragraphe 475. Et le genre de raisonnement qui opère en physique est
un développement postérieur, au sein du langage, non un développement
qui rend le langage possible.

Mais permettez-moi d'à nouveau reformuler la question. Telle que je
souhaiterais la poser, la question est la suivante: Peut-on accepter
ce que Wittgenstein nous dit dans les extraits cités sans aller
jusqu'au relativisme? De ce qui a été dit, la conclusion n'est-elle
pas que lorsque nous « combattons » la tribu recourant à un oracle
pour ses prédictions sur des questions dont la physique traite, ou
celle recourant au supplice du feu, les notions scientifiques que nous
employons sont seulement « vraies dans notre jeu de langage » et non
« dans leur jeu de langage » (ou « garanties dans notre jeu de
langage » et non « dans le leur », ou « raisonnable dans notre jeu de
langage » et non « dans le leur »)? Encore mieux, _nos_ mots
« vrais », « garanti » ou « raisonnable » ne signifient-ils pas
simplement « vrais dans notre jeu de langage », « garanti dans notre
jeu de langage », « raisonnable dans notre jeu de langage »?

La raison pour laquelle cela ne peut être ce que Wittgenstein dit
tient à ce que dire qu'il est vrai dans mon jeu de langage que vous
êtes en train de lire ce livre n'est pas dire que vous êtes en train
de lire ce livre; dire qu'il est vrai dans mon jeu de langage que je
prend mon dîner n'est pas dire que je prend mon dîner; et Wittgenstein
le savait évidemment. Dire qu'une chose est vraie dans un jeu de
langage, c'est se tenir en dehors de ce jeu de langage et en faire un
commentaire; ça n'est pas ce qu'est jouer un jeu de langage. Quels que
soient les motifs qui nous conduisent à remplacer le fait de dire
« c'est vrai », ou « c'est raisonnable », ou « c'est garanti » par
« c'est vrai dans mon jeu de langage », « c'est raisonnable dans mon
jeu de langage » ou « c'est garanti dans mon jeu de langage » (ou qui
nous conduisent à le faire lorsque nous nous apercevons que le jeu de
langage lui-même n'est pas fondé en Raison), ils nous portent à nous
_éloigner_ de notre propre jeu de langages. Tout se passe comme si la
reconnaissance du fait que nos jeux de langage n'ont pas de
justification transcendantale nous incitent à les manier avec des
pincettes, ou depuis un méta-langage. Mais pourquoi le méta-langage
serait-il plus sûr?

Il est important de voir que l'attraction du relativisme ne résulte
pas de ce qu'elle offre une position cohérente d'où faire sens de la
manière dont on peut employer le langage sans fondations
métaphysiques; au contraire, il a maintes fois été souligné que dès
lors que l'on tente d'établir le relativisme comme une _position_,
elle s'effondre dans l'incohérence ou le solipsisme (peut-être un
solipsisme avec un « nous » plutôt qu'un « je »). Que tout ne soit, au
mieux, « vrai seulement dans notre jeu de langage » n'est pas même
cohérent: l'existence propre de notre langage n'est-elle alors pas
seulement « vraie dans notre jeu de langage »? Notre jeu de langage
n'est-il qu'une fiction?

Je crois que nous aurions une meilleure compréhension de la situation
si nous voyions le relativisme non comme un remède ou un soulagement
de la maladie du « manque de fondement métaphysique », mais plutôt le
relativisme _et_ le manque de fondement métaphysique comme les
symptômes d'une même maladie. Il faut répondre au relativiste que
certaines choses sont vraies, d'autres garanties et d'autres
raisonnables, mais que nous ne pouvons _dire_ cela que dans la mesure
où nous avons un langage approprié. Et nous avons bien ce langage, et
nous pouvons dire cela et le disons, quand bien même le langage ne
repose pas lui-même sur une garantie métaphysique comme la Raison.

Sur quoi repose-t-il? Wittgenstein répond de façon étonnamment simple:
la confiance.

> §508: Sur quoi puis-je reposer?

> §509: Je tiens vraiment à dire qu'un jeu de langage n'est seulement
> possible que lorsqu'on a confiance. (Je n'ai pas dit « peut faire
> confiance »).

Nos jeux de langage ne reposent pas sur la preuve ou la Raison mais
sur la _confiance_. Quelque chose en nous trouve cela difficile
à supporter. Dans quelle mesure nous trouvons cela difficile
à supporter, dans quelle mesure nous nous débattons et nous agitons en
quête d'une garantie transcendantale ou d'une échappatoire sceptique,
c'est ce que Stanley Cavell a magnifiquement relaté dans une série
d'œuvres qui commencent avec _The Claim of Reason_. Cavell voit dans
le travail de Wittgenstein une entreprise sur la problématique du
scepticisme, pris dans un sens très large. Le sceptique au sens large
de Cavell peut bien n'être pas du tout un sceptique au sens habituel.
Plutôt que de professer son doute quant à toute chose ou de
relativiser toute chose, il pourrait prétendre avoir une vaste
solution métaphysique à nos problèmes. Mais pour Cavell, la prétention
qu'il pourrait se trouver une vaste solution métaphysique à tous nos
maux comme les échappatoires sceptiques, relativistes ou nihilistes
sont les symptômes d'un même mal. Le mal lui-même est l'incapacité
à accepter le monde et à accepter l'autre, ou, comme le dit Cavell,
à reconnaître le monde et à reconnaître l'autre, sans les garanties.
Quelque chose en nous désire plus que ce que l'on peut avoir, et fuit
les certitudes que nous ayons.

Non que le relativisme et le scepticisme soit irréfutables. Le
relativisme et le scepticisme sont facilement réfutables lorsqu'ils
sont posés comme des positions; mais jamais ils ne meurent, car
l'attitude d'aliénation du monde et de la communauté n'est pas qu'une
théorie, et ne peut être domptée par des arguments purement
intellectuels. En fait, il n'est pas même juste d'y faire référence
comme à un mal; car comme y insiste Cavell, souhaiter se libérer du
scepticisme c'est aussi souhaiter se libérer de son humanité.
L'aliénation fait partie de la condition humaine, le problème est
d'apprendre à vivre avec elle et la reconnaissance.

J'ai dévolu tout ce temps à la pensée de Ludwig Wittgenstein parce que
je crois qu'il nous donne un exemple de la façon dont la réflexion
philosophique peut être plus que des noyades dans des verres d'eau, ou
de chercher d'autres verres d'eau où se noyer. À son faîte, la
réflexion philosophique peut nous fournir un œil étonnamment honnête
et clairvoyant sur notre propre situation. Non une « vision de nulle
part », mais une vision à travers les yeux d'un autre être humain,
sage, faillible et profondément individuel. Si Wittgenstein veut voir
brûler nos vanités philosophiques, ça n'est pas par pur sadisme
intellectuel; si je le lis correctement, selon lui, ce sont elles qui
nous éloignent de la confiance, et, peut-être d'avantage crucial, de
la compassion.

---
title: "Sakoparnig et al, 2019"
subtitle: Whole genome phylogenies reflect long-tailed distributions of recombination rates in many bacterial species
author: "Samuel Barreto"
date: "[2019-06-06 15:33]"
draft: false
---

Je décris ici les travaux de Sakoparnig et al, publié pour l’instant sous forme de preprint.
^[@SakoparnigWholeGenomePhylogenies2019]
Les auteurs se sont attachés à tenter de décrire plus avant le signal qui peut être tiré d’une phylogénie basée sur le _core genome_, c’est-à-dire l’ensemble des séquences communes à un groupe de génomes, et d’essayer de comprendre dans quelle mesure ce signal est le reflet d’une histoire évolutive commune ou seulement un artéfact d’agglomération de signaux, un effet de « moyenne » des histoires évolutives individuelles de chacun des loci génomiques.

Ils partent du constat, à mon sens éminemment discutable, de ce que l’on a coutume de décrire les relations d’ascendance entre individus par un arbre, dont l’objectif « épistémique » serait en quelque sorte de retracer l’histoire de l’ensemble des divisions cellulaires de l’ensemble des êtres vivants, ce qui permettrait de retracer leur évolution depuis leur dernier ancêtre commun.

> Aussi, l’étude de l’évolution biologique correspond en un sens à l’étude de la structure de ce gigantesque arbre de divisions cellulaires.

Il est donc tout naturel de commencer l’analyse de séquences biologiques liées par la reconstruction de l’arbre phylogénétique reflétant, selon eux, l’histoire des divisions cellulaires des séquences, « leur ”phylogénie ancestrale“ ».

Il partent également du constat, selon eux axiomatique de la génétique, qu’il existe quelque chose comme un ensemble d’individus mutuellement interchangeables et partageant un environnement commun, désigné d’ordinaire par le terme de « population ».^[Ils estiment que leur article permet de remettre ce concept en question, ce qui est à mon sens beaucoup moins clair que leur remise en question du premier postulat, qu’ils opèrent sur des bases franchement plus positives, encore que fondées sur des prémisses sinon fausses, au moins relativement partiales.]

Naturellement, leur article vise à remettre en cause ces deux postulats quasi-axiomatiques, en quoi ils n’ont rien de très original, sinon d’appliquer des méthodes de génétique relativement classiquement employées en génétique des populations sexuées, à l’élucidation de l’information phylogénétique véhiculée par un arbre basé sur l’alignement du core-genome.

Ils disposent pour données initiales d’un ensemble de 91 génomes de _E. coli_ environnementaux, échantillonnés sur une période une zone géographiques restreintes.
(Le jeu de données est décrit plus avant dans un papier « compagnon » qui reste à paraître.)
Ils commencent par (re)poser le problème (_puzzle_) de la phylogénie des bactéries et archées: que la topologie de l’arbre obtenu par l’alignement de l’ensemble des locus partagés (_core genome_) rejette essentiellement n’importe quelle topologie obtenue à partir d’un bloc quelconque de 3kb pris au hasard le long de l’alignement.
^[D’abord ils reconstruisent un arbre en maximum de vraisemblance sur la base de l’alignement du core-genome.
Ensuite, pour chaque bloc de 3kb le long de cet alignement, ils reconstruisent deux arbres, l’un sur la base de l’alignement uniquement, l’autre en contraignant la topologie à suivre celle du _core genome_.
Après quoi ils comparent la vraisemblance des deux arbres, ce dont ils déduisent que virtuellement aucune topologie d’arbre provenant de l’alignement d’un bloc de 3kb n’est cohérent avec la topologie de l’arbre entier.]
De plus, la vraisemblance de la topologie de n’importe quel arbre provenant d’un bloc de 3kb est presque toujours supérieure à celle de la topologie d’un arbre que l’on aurait contraint à suivre la topologie d’un arbre provenant d’un autre bloc de 3kb.
Cependant, lorsque suffisamment de bloc de 3kb échantillonés aléatoirement sont concaténés, on retrouve la topologie de l’arbre issu du _core genome_.

Ils reconnaissent ensuite que rien de tout cela n’est bien neuf, c’est même en vérité un fait bien connu depuis une bonne vingtaine d’année.
^[Voir par exemple la thèse de V. Daubin: « Phylogénie et évolution des génomes procaryotes » (Université Claude Bernard-Lyon I, 2002), https://tel.archives-ouvertes.fr/tel-00005208/.]
Voilà qui a néanmoins le mérite de poser le contexte, c’est-à-dire que toute la question se résume à essayer de comprendre ce que traduit cet incohérence surprenante entre les phylogénies « individuelles » et les phylogénies « collectives ».

> Une interprétation, proposés par certaines chercheuses, est que dès lors qu’un grand nombre de segments génomiques sont considérés, les effets des transferts horizontaux sont effectivement moyennés, et que la phylogénie qui en _émerge_ correspond à l’ascendance clonale des souches.
^[Je souligne.]
> ...
> À l’opposé, de récentes études prétendent que la recombinaison est si répandue dans certaines espèces qu’il est impossible de reconstruire sensément l’ascendance clonale des séquences génomiques, donc que l’on doive les considérer comme librement recombinantes (_freely recombining_).

Plutôt que de tenter de résoudre ce problème par l’analyse d’alignements multiples, les auteurs se proposent de considérer des paires de génomes, c’est-à-dire des génomes pris deux à deux dans leur ensemble de 91, soit 4095 paires possibles.
La mesure la plus simple de distance entre deux génomes est leur divergence nucléotidique.
Pour les génomes les moins divergents, la distribution du nombre de sites polymorphes (SNP) le long de l’alignement est relativement uniforme, et correspond pour l’essentiel à la divergence _clonale_ entre les deux souches; ceci à l’exception de quelques régions, de plusieurs dizaines de kilobases, où l’on trouve une densité en SNP beaucoup plus importante, correspondant aux effets d’événements de recombinaison.

> Ces régions de haute densité en SNP résultent presque certainement d’événements de transferts horizontaux au cours duquel un segment d’ADN d’une autre souche d’_E. coli_, par exemple portée par un phage, est parvenue à pénétrer les cellules ancestrales de cette paire, et a été incorporée dans le génome par recombinaison homologue.

De cette distribution, séparant nettement les régions de faible densité en SNP des régions de haute densité en SNP, l’on peut déduire par modélisation les segments recombinants et les segments du cadre clonal (_clonal frame_). (Voir pour cela la figure 2 de l’article, où l’on voit distinctement la rupture dans la distribution du nombre de SNP par région.)
^[Le terme _clonal frame_ n’est nulle part employé dans le papier, bien qu’il fasse selon moi partie du vocabulaire essentiel à qui veut décrire ce genre de patrons évolutifs.]
L’une des figures intéressantes du papier est la figure 2G, qui représente la fraction des SNP du cadre clonal en fonction de la divergence entre deux génomes.
L’on y voit qu’au delà d’une divergence dite _critique_, la part des SNP dans la fraction clonale diminue drastiquement, si bien que passée \num{1e-3} la proportion des SNP dans la fraction clonale est proche de 0.
Au delà d’une divergence de \num{3.6e-2}, la moitié du génome d’un paire est considérée comme dans la fraction recombinante.
On peut dès lors classer une paire de génome soit dans la fraction « essentiellement clonale », soit dans la fraction « essentiellement recombinante » (soit entre elles deux). Parmi toutes les paires, seulement 7.7% sont dans la fraction essentiellement clonale, et 78% dans la fraction essentiellement recombinante.
Ils concluent de cela que les divergences entre souches sont dues _essentiellement à la recombinaison_ et ne reflètent _pas_ leur distance à leurs ancêtres clonaux.
^[Il me semble voir ici poindre quelque chose comme un paradoxe du Bateau de Thésée.
(Celui-ci avait tant pris la mer qu’il n’avait à sa fin plus aucune planche en commun avec celles dont il était constitué au départ.)]

Si l’on laissait évoluer, à partir d’un ancêtre commun qu’on pourrait dire « ancêtre cellulaire » deux lignées cellulaires, dont on connaîtrait par ailleurs l’ensemble de leur histoire de divisions membranaires, de sorte qu’à la fin leurs génomes n’ait en commun qu’un arrière plan clonal, et que tous les locus aient été impliqués dans au moins un événèment de recombinaison, dans quelle mesure devrait-on s’attendre à ce que le taux de _divergence_ reflète leur distance à leur ancêtre cellulaire?

> Pour comprendre de quelle façon une structure phylogénétique cohérente peut encore émerger de la comparaison des _core genome_ entiers, il faut aller au-delà de l’étude de paires.

Leur approche consiste alors à s’intéresser aux SNP de l’alignement du _core genome_ (aux colonnes de l’alignement), qui correspondent à 10% des positions, et 6.7% quand est exclu un groupe extrêmement distant.
Ces positions sont à 95.5% des SNP bi-alléliques, c’est-à-dire qu’on ne trouve dans l’alignement que deux allèles à cette position.
Ils montrent ensuite que, d’après un modèle de substitution simple, ces SNP bi-alléliques ne sont le produit que d’un événement de substitution.
Ils fournissent donc une information importante sur la phylogénie de cette position de l’alignement: « quelle [qu’elle] soit, elle doit contenir une séparation (_split_), une branche partitionnant l’ensemble des souches, de sorte que toutes les souches » se trouvent d’un côté ou de l’autre de cette partition.

On peut alors étudier dans quelle mesure une paire de SNP est cohérente avec une phylogénie commune; cette comparaison deux-à-deux de positions polymorphes bi-alléliques correspond au test des quatre gamètes utilisé chez les eucaryotes à reproduction sexuée.

Ils emploient alors ces SNP bi-alléliques à toutes sortes de mesures.
Pour commencer, ils calculent, pour chaque branche de l’arbre du _core genome_, quelle proportion de SNP bi-alléliques correspondent à cette partition.
En excluant le groupe extrêmement distant, ils comptent 27.4% des SNP en accord avec l’arbre _core_.
De plus, pour la moitié des branches de l’arbre, la fraction de SNP les supportant est inférieure à 5%.

> Aussi, corroborant notre analyse des paires de souches, tous ces résultats montrent que l’arbre _core_ ne capture pas les relations de   séquences entre souches.  
> ...  
> Les phylogénies réelles le long de l’alignement du _core genome_ pourraient bien être totalement différentes de la phylogénie globale « moyenne » représentée par l’arbre _core_.

À la suite de quoi ils cherchent à déterminer le rôle que joue la position relative des SNP.
En clair, ils commencent par déterminer des blocs de liaison: ils sont longs de 200 à 300 bp, soit trois fois moins qu’un gène en moyenne.
Ils montrent ensuite que la distribution du nombre de SNP consécutifs en accord avec une phylogénie commune a un mode autour de 4, rarement plus de 20, c’est-à-dire qu’en moyenne, des segments comprenant plus de 4 SNP tendent à supporter des phylogénies différentes.

Par le test des quatre gamètes appliqué à leurs données, ils sont en mesure de calculer le nombre minimal d’évènement de recombinaison nécessaire pour expliquer les incohérences entre paires de SNP.
Ils en déduise toutes sortes d’indices qui me paraissent plus ou moins discutables.
Par exemple ils déduisent du ratio recombinaison / mutation, inféré à partir de ce nombre minimal, le nombre de fois qu’un locus donné à été impliqué dans un événèment de recombinaison, sachant par ailleurs la taille moyenne d’un événèment de recombinaison.
Bref, ils finissent par dire qu’un locus donné a été recombiné en moyenne 337 fois, sachant qu’il s’agit probablement d’une sous-estimation …

> Ces analyses montrent que l’alignement du _core genome_ consiste en dizaine de milliers de petits segments aux phylogénies propres.

Si rien n’est à mon sens fondamentalement nouveau jusqu’ici, voire repose sur des prémisses quelque peu fragiles, la partie suivante est réellement intéressante, en ce qu’elle cherche à déterminer de quelle manière la recombinaison entre souches permet de donner forme à la structure de la phylogénie du _core genome_.
Leur approche consiste à extraire de l’alignement toutes les positions polymorphes  pour lesquelles deux souches partagent un allèle donné et elles uniquement; les autres souches possèdent un autre allèle.
Ils baptisent ce genre de SNP des 2-SNP (ou _pair-SNP_).
Ils regroupent alors les différentes souches selon un réseau de proximité de 2-SNP, c’est-à-dire regroupant les souches ayant en commun le plus grand nombre de 2-SNP.
Le réseau ainsi formé montre que toute souche est connectée via des 2-SNP à un « nombre substantiel d’autres souches ».
La distribution de la fréquence du nombre de 2-SNP commun entre deux souches suit une loi de puissance (c’est un réseau _scale-free_, en jargon d’analyste des réseaux); autrement dit que certaines souches sont beaucoup plus connectées les unes aux autres.
Ils étendent ensuite l’approche des 2-SNP à des 3, 4 et 12-SNP, et retrouvent essentiellement le même résultat.

Après quoi ils décrivent une approche dite de profil d’entropie phylogénétique, que je serais bien en peine d’expliciter ici, n’étant pas parvenu à comprendre le début d’un morceau du raisonnement qu’ils appliquent.

La discussion est somme toute assez attendue, et se contente pour l’essentiel de souligner les résultats importants du papier, répétant certaines parties de l’introduction. 

Cet article propose différentes approches assez intéressantes pour tenter d’aller un peu plus avant dans l’exploration des alignements de _core genome_.
L’approche qui est d’après moi la plus intéressante est celle qui consiste à déduire la proximité « recombinatoire » pour ainsi dire du profil de partage de SNP discriminants.
C’est d’elle que l’on pourrait tenter de distinguer moins des groupes phylogénétiques aux frontières franches et étanches que des _clusters_ à l’histoire recombinatoire vaguement commune, dans une approche plus probabiliste que typologique.
Cela dit, le matériau initial, un alignement du _core genome_ masque tout un ensemble de données potentiellement utiles pour quantifier l’histoire de proximité recombinatoire des différentes souches, à savoir le contenu en gène du _pan-génome_.
En effet, il serait extrêmement intéressant par exemple de comparer le signal obtenu par la proximité en 2-SNP avec celui que l’on pourrait tirer d’une matrice de présence / absence de gène.
L’on pourrait s’attendre à ce que, sinon des gènes, au moins des séquences d’origines indifférentes, ne soient présentes que chez deux souches, et absentes de toutes les autres, suivant le même _patron_ que les 2-SNP.
Enfin, je reste assez étonné qu’ils n’aient pas tenté de comparer la structure du réseau « recombinatoire » obtenu avec celle de l’arbre du _core genome_.
Car si tout l’argument consiste à vouloir rejeter le modèle de l’arbre du vivant, alors que ce réseau montre qu’à l’évidence les transferts sont sinon contraints, du moins structurés de quelque façon, c’est bien que rejetter l’historicité du phénomène ne fait aucun sens.
Sauf à conclure que l’_histeresis_ n’a aucune forme d’influence sur le système évoluant, et que seule la façon dont il est présentement agencé est déterminante, ce qui serait, il faut bien le convenir, d’un mouvement relativement radical …

Or c’est précisément là je crois que se tient le cœur du problème, et j’attaquerai ainsi la partie la plus spéculative de mon propos.
J’ai le sentiment que ce débat mobilise les mêmes ressources que le débat sur les services épistémiques, pour ainsi dire, rendus par la théorie de l’Acteur Réseau.
Celle-ci, que je connais somme toute assez mal, a été proposée dans les années 1990/2000 par des sociologues comme Michel Callon ou Bruno Latour, et prétend ne considérer le monde social qu’inscrit dans une forme de réseau d’« actants » connectés.
L’on ne doit pas exclure de ce réseau d’actants ceux que Latour appellent les non-humains, comme le fait par exemple Michel Callon dans sa description de la culture de la Saint-Jacques de Saint-Brieuc.
^[Il faut lire cet article, même rapidement, pour voir jusqu’où conduit l’axiomatisation de la méthode sociologique autour de la symétrie de David Bloor, où les Saint-Jacques sont considérées à part égales dans un réseau d’_enrôlement_ rigoureusement symétrique entre les marins-pêcheurs et un trio de scientifiques japonais.
Bourdieu était littéralement exaspéré de cette sociologie ultra-relativiste, comme il le dit dans les premières chapitres de Sciences de la Science et Réflexivité
(@CallonElementsPourSociologie1986, @BourdieuScienceScienceReflexivite2001)]
En gros, la théorie de l’acteur réseau (ANT) veut penser le monde comme inscrit dans un vaste réseau intégré, où finalement toute historicité est déniée au système.
Malgré ses prétentions subversives et hétérodoxes, elle est relativement compatible avec l’ordre néo-libéral, individualisant tout au point finalement de placer la responsabilité du déséquilibre des chances sociales sur les épaules de l’individu considéré comme un point du réseau.
En clair, les déséquilibres de pouvoirs (au sens foucaldien, qu’affectionnent les post-modernistes de l’ANT) sont directement _visibles_ sur le graphe connectant les actants et ceux/ce qu’ils enrôlent: le puissant a su « enrôler » un vaste réseau d’actants.
La géographe Rebecca Lave a bien montré à quel point l’acceptation par les théoriciens de l’écologie politique du modèle ANT pouvait être contre-productive, ôtant finalement aux dominés les outils épistémiques dont ils auraient besoin pour penser leur domination et les moyens de la subvertir.^[@LavePoliticalEcologyActorNetwork2015]

Bref, ce paragraphe pour indiquer que j’ai l’impression que les buts épistémiques poursuivis par ceux qui prétendent rejetter l’arbre du vivant pour ne voir dans l’évolution qu’un vaste réseau de transferts horizontaux s’enracinent dans le même terreau idéologique que les partisans de l’ANT.
Et ce terreau n’est pas nécessairement malsain --- la force de la critique latourienne en témoigne, quand bien même elle se base sur un ultra-relativisme à la limite d’un nihilisme difficilement soutenable, auquel j’avoue avoir du mal à comprendre grand chose ---, mais il est à mon avis voué à l’échec.
Si vouloir rappeler que la structure du monde n’est pas déterminée par sa seule histoire implique de rejetter tout ce qui ressemble de loin ou de près à une historicité quelconque, nous voilà alors contraint de déposer les quelques armes dont on dispose pour essayer de la comprendre.
Pour citer l’introduction au livre de Paul Boghossian,^[@BoghossianPeurSavoirRelativisme2009] « voilà qui menace d’avoir des conséquences profondément conservatrices ».

\newpage
# Références

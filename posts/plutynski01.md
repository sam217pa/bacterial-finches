---
title: La dérive, un aperçu historique et conceptuel
author: Anya Plutynski
translator: Samuel Barreto
date: 2019-03-01
draft: false
abstract: >
    La chance influence le changement évolutif de bien des manières.
    Que l'ensemble de ces processus soient appelée « dérive génétique
    aléatoire » est en partie en raison d'éléments qu'ils ont en communs,
    mais résulte aussi d'emprunts historiques de modèle et de langage,
    à différents niveaux d'organisation de la hiérarchie biologique.
    Une histoire du concept de dérive révélerait les divers contextes
    dans lesquel il joue un rôle explicatif en biologie, et
    éclairerait certaines des controverses philosophiques quant à la
    faculté de la dérive de constituer une cause de changement
    évolutif.
---

Le concept de « dérive génétique », un peu comme le concept de
« gene » ou de « fitness », est devenu un « concept en tension » (pour
paraphraser Falk 2000), ayant déclenché un débat substantiel parmi les
philosophes de la biologie. Ces dix dernières années, les philosophes
ont débattu de la question de savoir si la dérive génétique^[Par
soucis de clarté, « dérive » sera dès lors utilisé indistinctement de
« dérive génétique aléatoire ». Merci à un relecteur d’avoir noté
l’importance de cette distinction.] doit être correctement comprise
comme un processus ou une conséquence (Millstein 2002, 2005; Brandon
2005); si la métaphore de la force est appropriée pour décrire aussi
bien la sélection que la dérive (Mathen et Ariew 2002; Shapiro et
Sober à paraître); et enfin, si la dérive est une cause et dans quel
sens elle l’est (Matthen et Ariew 2002, Walsh et al, 2002, Reisman et
Forber 2005; Shapiro et Sober à paraître). Ce débat peut être vu comme
une noyade dans un verre d’eau; la dérive est un concept tout aussi
bien défini qu’un autre en biologie évolutive mathématique. Cependant,
l’étendue du débat philosophique suggère qu’il y a des enjeux plus
larges. Vraisemblablement, ce débat reflète des questions plus
profondes et plus anciennes en biologie évolutive et en métaphysique.
Dès Ernst Mayr (1959) et sa critique de la « génétique en sacs de
haricots », les évolutionnistes s’inquiètent de la portée et des
limites de la génétique des populations classique, de la relation
entre le formel et l’empirique en biologie, et plus généralement, de
la manière convenable d’intégrer la compréhension des patrons et
processus aux différents niveaux de la hiérarchie biologique, de
l’évolution moléculaire à la spéciation. La dérive est devenue le
dernier point focal autour duquel les philosophes de la biologie
débattent de ces questions. Ce débat est également lié à un autre plus
large en philosophie générale des sciences quant aux niveaux et modes
de causation, ou quant à ce qui peut être dit cause.

L’un des éléments manquant à la discussion philosophique récente est
une dimension historique. La dérive, tout comme « gène » ou
« fitness », est un concept ayant évolué ces 75 dernières années;
comprendre pourquoi et de quelles façons pourrait contribuer
à résoudre certains de ces débats, ou, à tout le moins, pointer les
sources de confusion.

Toute discussion du concept de dérive parmi les philosophes procède
essentiellement sur la base de quelques exemples clés et de
définitions de la dérive issues de manuels. Pourtant, lorsqu’on
consulte les manuels de génétique évolutive, on y trouve diverses
définitions de la dérive, pas nécessairement cohérentes. La dérive
fait référence à « la fluctuation au hasard de la fréquence
allélique », qui a lieu « particulièrement en petites populations,
_résultant_ d’un échantillonnage aléatoire de gamètes » (Hartl 1988:
16). Ou encore, la dérive est « une _force dispersive_ qui supprime la
variabilité génétique des populations », pouvant être due soit à la
« variation dans le nombre de descendants entre individus, soit,
lorsque l’espèce est sexuée et diploïde, des lois de ségrégations de
Mendel » (Gillespie 1998: 19). Enfin, la dérive est « le processus de
changement de fréquence de gène due exclusivement à des effets
aléatoires » (Graur et Li 2000: 48). Ces trois textes décrivent la
dérive à la fois comme le résultat d’un échantillonnage aléatoire et
comme un processus de changement dû à la chance. En d’autres termes,
la dérive est dépeinte de façon interchangeable comme effet ou cause,
patron ou processus. Différentes description des effets majeurs de la
dérive sont proposées, de « fluctuation des fréquences alléliques »
à « suppression de la variabilité génétique ». La dérive est aussi
attribuée à différentes causes --- « ségrégation » aussi bien que
« variation aléatoire du nombre de descendants entre individus. » En
d’autres termes, la dérive est due à différents types de mécanismes en
jeu à différentes échelles et niveaux d’organisation, de la
ségrégation méiotique et la recombinaison aux éléments aléatoires
influençant la fertilisation, en passant par des facteurs
environnementaux tels que les catastrophes naturelles déterminant si
certains individus, et non d’autres, survivent et se reproduisent.

Comme ces exemples l’illustrent, la dérive est devenue une sorte de
concept « pêle-mêle » en biologie --- faisant collectivement référence
aux diverses façons dont la chance, en un sens général, affecte des
populations évoluant dans le temps. De la même façon, tout au long de
l’histoire de l’emploi de ce concept, le terme « dérive » a été
utilisé pour faire référence à toute une gamme de conséquences et de
causes. Comme Beatty y insistait, « la dérive aléatoire est une
catégorie hétérogène de causes et d’effets … les phénomènes collectifs
sont très différents. De plus, certains phénomènes parfois inclus dans
la catégorie de dérive aléatoire n’ont rien à voir avec un
échantillonnage aléatoire » (Beatty 1992: 273). Face à une telle
diversité d’usages et de références, il n’est sans doute pas
surprenant que les philosophes aient si vigoureusement contestés la
nature de la dérive.

En sus du fait que la dérive ait historiquement été utilisée pour
faire référence à une variété de causes et d’effets, il y a un écart
assez important entre le classique modèle « Wright-Fisher » de la
dérive et la diversité de manières dont des événements aléatoires
affectent des populations au cours du temps. Le modèle
d’échantillonnage aléatoire binomial^[Le modèle le plus simple de
dérive est souvent appelé le modèle de Wright-Fisher, en raison de ses
origines dans les travaux de Wright (1931) et Fisher (1992). Ce modèle
traite de la production d’une descendance comme le tirage aléatoire
d’allèle (où il y a deux allèles au « locus » d’un gène), avec
replacement, de parents, ou autrement dit le tirage de gamètes d’un
ensemble infini auquel chaque parent contribue de façon égale. Le
processus d’échantillonnage binomial représente à la fois ce qui est
posé comme (1) un processus aléatoire de ségrégation méiotique et de
fertilisation, et (2) le rôle du hasard dans le nombre de gamètes
(féconds) produits par les parents (e.g. les facteurs aléatoires qui
font que certains individus ont, mettons 10 descendants, quand
d’autres 0 --- la foudre qui frappe, etc.). Conjointement, dans le
modèle Wright-Fisher, ces deux aspects produisent une distribution
binomiale de la descendance. Dans le cas le plus simple, les
générations sont discrètes et il n’y a que deux allèles à un locus.
Aussi, par exemple, considérons deux individus d’une génération
parentale, dont l’un est hétérozygote $Aa$ et l’autre homozygote $AA$.
Étant donné la prémisse que deux allèles à ce locus sont transmis
selon un appariemment Mendélien indépendant, ces deux individus
peuvent avoir des descendants de l’une des deux sortes, soit $AA$ soit
$Aa$, attendus en proportions égales. Seulement par chance, ils
peuvent avoir un nombre égal de descendants $AA$ et $Aa$, ou
autrement, des descendants qui sont tous $AA$ ou tous $Aa$. Additionné
sur l’ensemble de la population, le changement dans la distribution
des génotypes dû à ce processus d’échantillonnage est appelé dérive.
En d’autres termes, la « cause » de la dérive en ce sens n’est guère
qu’une redistribution due à une ségrégation Mendélienne et une
fertilisation aléatoires, ou encore, des accidents
d’« échantillonnage » d’allèles. Ce modèle admet un certain nombre de
présupposés; les organismes sont diploïdes, se reproduisent
sexuellement, les générations sont non-chevauchantes, l’assortiment
est aléatoire au sein des sous-populations échantillonnées, il n’y
a ni migration, ni mutation, ni sélection. Tous ces présupposés ne
sont pas le cas, du moins dans certains contextes. En d’autres termes,
le modèle Wright-Fisher, quoique point de départ théorique et
pédagogique de la plupart des travaux actuels sur la dérive, n’est que
rarement réalisé dans les populations réelles. Les gènes d’un même
chromosome sont liés, et ne sont donc pas transmis indépendamment. Ce
qui implique que lorsque plus d’un locus est considéré, la théorie
doit être étendue pour prendre en compte cette corrélation. Mais il
y a plus. Par exemple, la distribution de la descendance n’est
généralement pas binomiale; il y a souvent des sexes séparés en
effectifs inégaux; la population peut ne pas s’assortir aléatoirement.
L’une des façons de gérer ces complications est d’utiliser le concept
de Wright d’un « coefficient de population efficace ». Il
y a plusieurs façons de définir le coefficient efficace de population:
la plus fréquente est la consanguinité, et la variance de la taille de
population efficace (_variance effective population size_). Le
coefficient de consanguinité efficace est la taille de la population
idéale qui produirait la même probabilité d’identité d’ascendance
entre les individus sélectionnés existants dans la population réelle.
La variance de la taille de population efficace est le taille de la
population de même dispersion de fréquence allélique par dérive, ou de
même variance de fréquence allélique que la population en question. Le
coefficient efficace défini de cette manière est appelé le
« coefficient de variance efficace » (voir Wright 1931 et Crow 1954:
543-556).] traite la dérive comme l’échantillonnage aléatoire
d’allèles (avec replacement) d’une génération à la suivante, où la
conséquence est une forme d’erreur d’échantillonnage. Résultant de ce
processus d’échantillonnage, certains allèles sont fixés, d’autres
perdus. Dans les populations plus petites, ce processus est accéléré;
autrement dit, le temps de fixation d’un allèle sera plus court. Le
modèle mathématique de la dérive permet ainsi de prédire les
conséquences de façons systématiques. Prendre des tailles
d’échantillon plus petites accroît le temps de fixation ou de perte
d’un allèle par la seule action de la chance.

Le modèle de base fut développé dans les années 1920 et 1930, alors
que très peu de choses étaient connues des mécanismes de transmissions
ou des causes de variabilité aux niveaux moléculaires. Le tirage
d’allèle comme similaire au tirage de boules d’une urne était traité
comme une analogie adéquate au processus d’échantillonnage d’allèles
dans une population par la recombinaison génétique, en dépit des
incongruences entre ce processus et les processus réels impliqués dans
l’assortiment aléatoire et l’échantillonnage aux différents niveaux de
la hiérarchie biologique. Néanmoins, ce modèle et l’étiquette de
« dérive génétique aléatoire » devint la boîte noire de ce qui, aux
différents niveaux d’organisation, correspond à différents « moteurs »
du hasard et à leurs conséquences. Bien qu’il y ait des similarités en
termes globaux entre ces différents contextes (l’étendue de la dérive
est contingente de la taille des populations), les facteurs
biologiques en jeu dans la « dérive » des nucléotides, cistrons,
allèles, complexes de gènes, chromosomes, ou, même d’ailleurs,
d’individus et de groupes, sont bien différents. Ces différents
contextes requièrent de subtiles corrections des modèles originels.
Par exemple, les taux de recombinaison intra-cistroniques (les taux
de recombinaison entre sites d’un même gène) sont bien plus faibles
que ceux entre gènes, ce qui suggère que le modèle classique
requièrent de subtiles modifications dans ce cas (Ewens 2004).

Il ne fait aucun doute que le modèle classique d’échantillonnage
aléatoire ait été d’une contribution extraordinairement féconde en
tant qu’outil de compréhension des dynamiques d’évolution au niveau
des populations. Néanmoins, la biologie a tellement changé depuis 1930
que les historiens et philosophes de la biologie ont soulevé des
questions légitimes quant à dans quelle mesure les modèles originels
de dynamique d’évolution sont démodés (Provine), et dans quelle mesure
la façon de parler de la dérive comme d’une « force dynamique » est
problématique (Matthen et Ariew 2002). Dans ce qui suit, je passe en
revue l’histoire de la tradition de modélisation d’évolution ayant
conduit aux controverses philosophiques sur la dérive en tant que
cause et effet, et emploi cette analyse historique comme prétexte à,
sinon résoudre le débat à la satisfaction de tous, du moins clarifier
les questions en jeu.

# Bref aperçu historique: les origines de la dérive

Les prémisses du concept de dérive se trouvent dans les discussions de
Darwin de variations « ni utiles ni délétères » comme traits
polymorphiques « n’étant pas affectés par la sélection naturelle », et
partant perdurant soit en tant qu’« éléments fluctuants » soit
« fixés » (Darwin [1859] 1964: 46, 81). Darwin ne discute ni de la
façon dont ces variations se fixent ou se perdent, ni des raisons pour
lesquelles elles le font. Fleeming Jenkin ([1867] 2001), cependant,
a bien compris la perte de la variabilité à cause de l’isolement de
petites populations (insulaires en particulier). Il traite la perte de
variation par chance comme un défi à la théorie darwinienne de la
sélection naturelle dans son commentaire de l’_Origine_. C’est-à-dire,
si apte que soit un mutant individuel particulier, s’il est isolé dans
une petite population insulaire, les variants avantageux seront
« submergés » par de moins aptes types. (Jenkin l’illustre par le
« lièvre fouisseur », et un autre exemple, outrageusement raciste.) De
plus, le commentaire de Gulick (1889: 209) du rôle de la « destruction
indiscriminée » de certains membres d’une espèce d’escargots
terrestres hawaïens en cours de différentiation fût énormément
influente pour les travaux subséquents sur la dérive, aussi bien qu’en
tant que modèles de spéciation. Gulick pressait que les catastrophes
naturelles peuvent être sources d’élimination indiscriminée de membres
d’un groupe. En d’autres termes, son centre d’intérêt était
l’échantillonnage au niveau de sous-populations entières. Cependant,
les traitements formels antérieurs de dérive génétique aléatoire se
concentraient plutôt sur les événements aléatoires de reproduction
sexuée, la méiose et « l’échantillonnage d’allèles » (Hagedoorn et
Hagedoorn, 1921), ce qui conduit aux modèles de Sewall Wright et R.A.
Fisher.

Fisher (1922) modélise les effets de la dérive dans le contexte d’une
discussion de ce qu’il appelle la thèse Hagedoorn (1921): « la survie
aléatoire est un facteur plus important dans la restriction de la
variabilité d’une espèce que la survie préférentielle. » Fisher voyait
l’argument des Hagedoorn comme une menace à l’évolution darwinienne
par sélection naturelle, et arguait de ce que l’effet Hagedoorn
n’était pas substantiel: « la baisse de variabilité d’une espèce
d’assortiment aléatoire sans sélection ni mutation, est presque
inintelligiblement lente » (Fisher 1922: 323).

Fisher admet que le hasard est un facteur crucial dans les premiers
stades d’apparition d’une mutation dans une population, parce que
« tant qu’elle est rare, sa survie est à la merci du hasard, même
lorsqu’elle est bien apte à survivre » (p. 326). Dans le même sens,
dans de petites populations, la probabilité de survie d’un mutant
isolé est faible. Cependant, Fisher démontra comment la survie
éventuelle d’une nouvelle mutation avantageuse est presque certaine
lorsqu’elle est récurrente (p. 340). Ou encore, Fisher montra que
l’élimination par chance d’allèles est contrecarrée par des mutations
récurrentes, étant donné un taux de mutation suffisant. Cet argument
était central pour la démonstration de Fisher que la mutation et la
sélection peuvent, au cours du temps, engendrer de substantiels
changements dans les populations.

Ce papier de 1922 peut être vu comme une tentative de Fisher de faire
valoir la sélection face aux menaces de perte de variation aléatoire.
En d’autres termes, Fisher se voyait comme défendant Darwin
précisement contre le genre de critique soulevé par Jenkin dans son
commentaire de l’_Origine_. À ceci près que Fisher était beaucoup plus
familier de la statistique, et fut capable d’en tirer une analogie
à une autre discipline traitant elle aussi de changements statistiques
en grandes populations d’entités --- la mécanique statistique.
Employant cette analogie, Fisher dépeint la variation dans une
population comme suivant une distribution normale; la mutation fournit
constamment de nouvelles variations, et le hasard élimine les
nouvelles variations d’une population. Aussi, si la variation d’un
trait est décrite comme la distribution d’une moyenne, la mutation
« étale » continuellement la courbe en cloche, quand le hasard la
« resserre ». De cette façon, et partant de l’analogie qu’il opère
avec la mécanique statistique, Fisher en vint à penser la sélection,
la mutation, la migration et la dérive comme des « forces ». Les
coefficients de sélection décrivent l’avantage relatif d’un gène
donné, donc si la chance de survie d’un gène donné est supérieure ou
inférieure à un; la sélection peut donc déplacer ou biaiser la courbe
dans l’une ou l’autre direction. Le gène augmentera ou diminuera donc
en fréquence dans une population au cours du temps, ou sera perdu par
chance. La sélection mesure sa « vitesse » d’augmentation, relative
à d’autres facteurs, tels que la taille de population et le taux de
mutation. L’objet de Fisher, dans ce papier de 1922 et ceux qui
suivent, fut de considérer le rôle relatif de chacun de ces facteurs
dans la dynamique de l’évolution des populations.

Fisher fait référence aux dynamiques de changement de fréquence d’un
gène comme apparenté à un processus physico-mécanique, déterminé par
des forces, telles que la mutation, la sélection, et la perte
aléatoire: « les effets de la sélection dans la modification des
fréquences de gènes sont … illustrés … par les changements de position
aux vitesses uniformes et proportionnels uniquement à l’intensité de
la sélection » (Fisher 1930: 79). En revanche, Wright (du moins dans
les premiers temps de ses travaux théoriques) préférait parler de la
« labilité » et de la « plasticité » d’une population --- une
population est comme une argile plus ou moins « plastique » en
fonction du degré de consanguinité (_inbreeding_), d’asanguinité
(_outbreeding_), etc. Une population est donc capable d’être modelée
ou « façonnée » par l’élevage ou la nature, quel que soit le cas. La
façon de Fisher de modéliser la dynamique des populations, lorsqu’il
emploi différentes métaphores de « vitesse », a vraisemblablement
influencé la façon de penser de Wright quant à la « balance » optimale
de facteurs en jeu dans une population.

Il y avait une différence importante entre eux deux, ceci dit. Alors
que Fisher voyait la perte aléatoire de variation comme une menace aux
explications sélectives, et donc (à son sens), au « Darwinisme »
lui-même, Wright en vint à voir la chance positivement, comme un
facteur causal d’évolution. Cette vision de la dérive, en tant que
facteur augmentant la « labilité » des populations, devint
éventuellement l’un des nombreux facteurs permettant les
« mouvements » ascendants des populations le long des pics du
paysage adaptif voisin, le long du fameux paysage adaptatif de Wright.
Cependant, dans un premier temps Wright ne fait pas référence à la
dérive per se comme une cause d’évolution distincte. Dans la plupart
de ses premiers papiers sur le rôle de la chance dans l’évolution,
« dérive » était vu plutôt comme _un effet qu’une cause_.

Dans les années 1920 et 1930, Wright parlait essentiellement de la
dérive comme un « effet » de l’extinction, de la subdivision, de
l’isolement et de la consanguinité des populations par hasard. Les
gènes « dérivent » de leur équilibre, et la « dérive » des fréquences
de gènes est due à d’autres causes; mais la _dérive per se n’était pas
décrite comme une cause_. Wright parlait plutôt de la consanguinité ou
de l’« isolement » comme une « cause » de dérive.

Qu’est-ce que la consanguinité? Wright développa une formule de calcul
du coefficient de consanguinité, $F$. $F$ représente la probabilité
que deux allèles homologues soient identiques par ascendance commune,
ou dérivés d’un ancêtre commun. La consanguinité a lieu lorsque les
partenaires sont plus liés que s’ils étaient choisis au hasard; dans
de petites populations, la probabilité que deux partenaires aléatoires
soient apparentés est plus grande que dans de grandes populations.
Ainsi, dans de petites populations, la consanguinité est plus élevée,
comme l’est la vitesse de fixation de gènes due à des facteurs
non-déterministes. Les réflexions précoces de Wright sur la
consanguinité l’ont conduit à considérer le fait que fait que la
taille de population puisse jouer un rôle significatif dans
l’évolution.

En somme, Wright, au moins dans ses premières discussions du rôle du
hasard dans l’évolution, parlait de la dérive non comme d’une cause,
mais d’une conséquence de la consanguinité. Par exemple, l’une des
premières controverses entre Wright et Fisher quant à l’évolution de
la dominance concernait la question de savoir si « l’effet
d’isolement » ou l’effet de la consanguinité dans une population de
petite taille, pourrait compromettre le rôle de la sélection dans
l’évolution de la dominance. À ce stade du débat avec Fisher, Wright
utilise la dérive comme un _verbe_ décrivant le comportement des
allèles. Wright parle moins de la _dérive per se comme une cause
d’évolution_ que d’un « effet d’isolement » (mesuré par 1/2n) comme
l’un des trois « facteurs contrôlant le destin d’un gène ». Les
facteurs génétiques « dérivent jusqu’à leur fixation » comme une
_conséquence_ de l’isolement. Aussi est-il fait référence àla dérive
comme à une conséquence, non une cause de changement évolutif.
L’insistance de Wright sur l’isolement et sa dispute avec Fisher
constituent le premier tour d’un débat au long cours sur la
contribution relative de la dérive et de la sélection dans
l’évolution. À ce stade, au début des années 1930, ce qui était en jeu
n’était pas « dérive » contre « sélection ». C’était plutôt les
différentes causes de perte due au hasard: isolement, consanguinité,
et patrons d’assortiment étaient les « causes » de la « fluctuation »
des fréquences alléliques.

Dans son papier classique de 1931 (dans Wright 1986), lorsqu’il
énumère les facteurs contribuant à l’homogénéité génétique, Wright
n’inclut _pas_ la dérive; il parle plutôt de la dérive comme d’une
_conséquence_ d’une taille de population restreinte. Les allèles
peuvent « dériver » jusqu’à leur fixation, mais, à ce stade, la
« cause » de ce processus que Wright avait en tête était la
consanguinité. En somme, à ce stade, « dérive » fait référence
à l’élimination aléatoire de gènes d’une population par effet de
consanguinité. La cause principale de fixation hormis l’aptitude
différentielle, à cette époque, était selon Wright ([1929], 1986:
279), moins la « dérive » per se que l’isolement et la consanguinité:
« le facteur d’isolement est d’une importance capitale en évolution. »
L’isolement et la consanguinité résultant de patrons d’assortiment au
sein de groupes relativement isolés étaient décrits comme la _cause de
la perte d’hétérogénéité génétique_.

# La dérive extrapolée

Qu’est-ce qui a alors conduit les biologistes à parler de la dérive
comme d’un facteur causal indépendant en évolution, et non comme d’une
conséquence de l’isolement ou d’un « effet de consanguinité »? Il
y a trois principaux facteurs. D’abord, la métaphore de la sélection
et de la dérive comme des « forces » déplaçant les populations le long
d’un paysage adaptatif devint une manière efficace de populariser la
science par ailleurs plutôt complexe et abscons qu’était la génétique
des populations classique. Ensuite, au cours de la synthèse, « la
dérive génétique aléatoire » fut identifiée à l’effet « Sewall
Wright », et le recours à cet effet pour expliquer des différences
non-adaptatives au sein et entre populations se banalisa énormément
(Provine 1986: 405). De plus, avec la polarisation de Wright et Fisher
sur la contribution relative de l’isolement, du hasard contre la
sélection, « dérive » devint le nom de la « force » concurrente du
hasard dans l’évolution. En somme, alors que le modèle du paysage
adaptatif de Wright pour représenter le changement évolutif au cours
du temps se popularisait, comme le recours à lui pour expliquer toute
une variété de phénomènes par ailleurs déroutants, la dérive devint
identifiée à l’un des différents mécanismes causaux de l’évolution.
Les différences non-adaptatives au sein de, et entre populations et
espèces --- des groupes sanguins aux patrons de coquille d’escargot en
passant par les patrons de spéciation --- furent toute expliquées par
la dérive (voir Beatty 1987, 1992).

Par exemple, Dobzhansky recourt à l’effet Sewall Wright pour expliquer
les polymorphismes chez l’Homme tels que le groupe sanguin:

> les polymorphismes chez l’Homme (e.g. l’hétérozygotie ou
> l’homozygotie du groupe sanguin) pourraient … pour autant que l’on
> soit en mesure d’en juger pour l’instant, être expliqués par des
> fluctuations aléatoires de fréquences de gènes en populations
> effectivement petites. À de telles variations aléatoires de fréquences
> de gènes, on fait référence par le terme de dérive génétique, ou
> effet Sewall Wright. (Dobzhansky, 1951: 156)

Dobzhansky développe plus avant, recourant aux graphes classiques de
Wright en forme de "U" ou de cloche pour représenter la contribution
relative de la sélection et de la taille de population dans la
rétention ou la perte d’allèles:

> Plus faible est la taille efficace de population, plus grandes sont
> les variations de fréquences de gènes, et moins efficaces sont les
> faibles pressions de sélections. En petites populations, les allèles
> favorisés par la sélection peuvent être perdus, et d’autres moins
> avantageux peuvent atteindre la fixation. En populations très
> grandes, même de très faibles avantages ou désavantages sélectifs
> peuvent éventuellement être effectifs; mais une selection plus
> stringente doit s’appliquer pour surmonter la dérive génétique en
> petites populations. (1951: 161)

Ainsi Dobzhansky oppose ici la sélection et la dérive moins
explicitement comme « forces » distinctes qu’en tant que concurrentes
à l’explication de la perte ou de la rétention d’un allèle. Dans de
plus petites populations, les allèles sont plus susceptibles d’être
fixés ou d’être perdus en raison de facteurs aléatoires; dans des
populations plus grandes, les effets de coefficients de sélection même
faibles conduisent éventuellement à la fixation d’allèles. Le fait que
la taille de population « contraigne » l’intensité de la sélection se
prête à l’idée que la sélection et la dérive sont des « forces
concurrentes » d’évolution qui peuvent être décomposées. En d’autres
termes, l’importance relative de l’une dépend de celle de l’autre.

Dobzhansky continue en notant un « corollaire biologique riche de
sens », c’est-à-dire, qu’« une espèce, morcelée en colonies isolées,
peut se différencier par le produit de la diminution des tailles
efficaces » (p. 162). Après le commentaire de quelques exemples, e.g.
celui des escargots terrestres hawaïens et celui de la différentiation
non-adaptative chez _Drosophila_, il écrit: « la diminution de la
taille génétiquement effective d’une population naturelle est selon
toute probabilité un agent important de différenciation d’espèces en
groupes locaux possédant différents génotypes » (p. 176).

En d’autres termes, Dobzhansky et d’autres auteurs de la synthèse se
sont appropriés l’effet Sewall Wright pour expliquer tout des
différences adaptatives au sein d’espèces à la spéciation elle-même
(Provine 1986; Beatty 1987). Huxley recourt aussi à l’effet Sewall
Wright comme une explication non seulement des différences adaptatives
entre populations, mais aussi des différences similaires entre
espèces, même entre groupes taxonomiques entiers. Selon ses termes,
l’effet Sewall Wright « d’un seul coup explique bien des faits ayant
troublé les sélectionnistes antérieurs », par exemple le « degré plus
élevé de divergence que l’on rencontre dans les îles », et d’autres
« découvertes taxonomiques récentes » (Huxley 1942: 199-299, 260).

En dépit du fait que Wright ait initialement fait référence à la
dérive comme conséquence et non coause du changement évolutif,
« dérive génétique aléatoire » en vint à supplanter l’effet Sewall
Wright comme somme des rôles à plusieurs étapes et plusieurs facteurs
joués par l’isolement et la consanguinité ainsi que leurs effets sur
la distribution des allèles. De plus, par la banalisation de son
modèle du paysage adaptatif, le rôle de la dérive commençait à être
utilisé pour expliquer le destin de populations et d’espèces entières.

Par exemple, l’« effet fondateur » de Mayr (1942) --- spéciation par
isolement de petites sous-populations, suivie d’une « dérive » jusqu’à
de nouveaux pics adaptatifs --- est vraisemblablement une extension de
l’effet Sewall Wright en remontant dans la hiérarchie jusqu’aux
différences entre espèces. Quoique Mayr nie toute dette envers Wright
sur ce point, le processus emploi le même mécanisme de changement que
celui défendu par Wright dans son papier classique de 1931 (Mayr
1963). Ceci marque le glissement du discours sur la dérive au sein de
populations au discours sur la dérive comme cause du changement entre
espèces.

Wright ([1949, 1955] 1986) brouille les pistes encore davantage en
disant de la fluctuation aléatoire de coefficients de sélection
qu’elle est due à la dérive (commenté par Beatty 1992). Wright étend
ici le concept de dérive encore plus loin, comme expliquant tout
comportement en apparence « aléatoire », même s’il l’est par la
sélection.

Avec le « durcissement » de la synthèse dans les années 1940 (Gould
1983), cependant, des exemples clés de la dérive tels que les groupes
sanguins se trouvèrent avoir des explications sélectives. Néanmoins,
le processus largement réparti et à plusieurs étapes d’échantillonnage
au sein d’une population fut baptisé, en tant que « force » unique, de
« dérive », et la dialectique des contributions relatives de la dérive
et de la sélection était un enjeu majeur, qui perdure au 21e siècle.
Ce fut renforcé par la polarisation de Wright et Fisher sur les
retombées des contributions relatives du hasard et de la sélection
dans l’explication de certains exemples classiques de différences en
apparance non-adaptatives entre espèces.^[Non seulement la dérive fut
employée « plus haut » dans la hiérarchie comme jouant un rôle
significatif dans la spéciation et l’extinction, elle fut aussi
employée « plus bas » au niveau moléculaire. Kimura (1968) et King et
Jukes (1969) proposent indépendamment la théorie neutre de
l’évolution moléculaire. C’est l’idée selon laquelle la majorité des
changements évolutifs au niveau moléculaire sont causés par la dérive
aléatoire d’allèles neutres ou quasi-neutres du point de vue de la
sélection. Selon Kimura, les changements d’acides aminés sont aussi
fixés par « dérive ». Le temps de fixation d’allèles neutres est
déterminé par la taille de population. En ce sens, « dérive » au
niveau moléculaire est un analogue de l’effet fondateur et de l’effet
de consanguinité; l’influence relative de la dérive sur les deux
dépend de la taille de population, même si le « mécanisme »
d’échantillonnage est probablement bien différent.]

# Un pas en arrière

Le concept de dérive génétique aléatoire a été employé dans toute une
variété de contextes biologiques pour expliquer nombre d’observations
ces 75 dernières années. La dérive est invoquée pour expliquer les
polymorphismes « fluctuants », la variation apparemment sélectivement
neutre au sein de, et entre populations ou espèces, la perte de
variabilité dans les populations, les coefficients de sélection
« fluctuants aléatoirement », et enfin, la variation apparemment
neutre au niveau moléculaire. Ce que tous ces emplois ont en commun
est la notion que le changement aléatoire des fréquences de gènes (de
paires de bases, de gamètes entiers, et même de populations) peuvent
être attribués à une chose similaire à un échantillonnage --- où
échantillonnage est seulement la fixation ou l’élimination de gènes,
de gamètes, etc. indépendamment de leur avantage ou désavantage
sélectif.

Les modèles initiaux de la dérive n’y faisait pas référence comme
cause; elle était plutôt une conséquence de l’« effet de
consanguinité ». La dérive, comme la « fluctuation » de Darwin, était
employée comme métaphore du comportement des allèles dans diverses
formes d’échantillonnage aléatoire. Pendant et après le début de la
synthèse, l’effet Sewall Wright fut fréquemment invoqué pour expliquer
les patrons de différences apparemment non-adaptatives entre espèces.
Finalement, le terme « dérive génétique aléatoire » servit à décrire
génériquement le rôle du hasard dans des populations au cours du temps
à tout niveau d’organisation.

Que révèle cette enquête historique de la dérive? On peut en tirer
quatre conclusions clés:

1. Le modèle classique Wright-Fisher de la dérive comme un processus
   d’échantillonnage binomial est un artéfact d’un temps où les
   mécanismes de transmissions étaient mal compris, et modélisent donc
   la diversité de manières dont le hasard affecte le changement
   évolutif de façon indirecte et idéalisée. Les attributs partagés
   par tous les cas de dérive sont que la taille de population est
   pertinente à la prédiction d’effets du hasard dans le temps. Les
   particularités des modes d’échantillonnage à différents niveaux
   d’organisation sont plus ou moins placés dans des boîtes noires par
   ce modèle.
2. Qu’il soit fait référence à la dérive comme cause et conséquence
   n’est pas nouveau. La dérive était originellement une métaphore du
   comportement d’allèles en populations consanguines isolées. C’est
   seulement récemment qu’on a fait à la dérive une place parmi les
   nombreuses « causes » du changement évolutif.
3. Que Fisher ait modelé la théorie de la génétique des populations
   sur la mécanique statistique et que Wright ait adopté la métaphore
   des paysages adaptatifs pour le changement évolutif a contribué
   à renforcer la vision selon quoi la dérive, comme la sélection, la
   mutation et la migration, est une « force » de changement évolutif.
4. Tester les arguments quant à la contribution relative de la dérive
   fut difficile dès le départ, comme l’illustrent bien les débats sur
   le groupe sanguin autant que les questions contentieuses qui ont
   tant divisé Fisher et Wright.

Cet aperçu historique peut aider à l’évaluation de récents et
vigoureux échanges philosophiques sur la question de savoir si la
dérive est une cause de changement évolutif, et dans quelle mesure
elle l’est. En partie, le débat s’est articulé autour la question de
s’il est plus approprié de décrire la dérive comme effet ou cause du
changement évolutif dans le temps. Comme nous l’avons vu, cette
question a des antécédents historiques. Car le concept de dérive a lui
même évolué d’une métaphore pour décrire le comportement d’allèles dû
au hasard dans les populations à une « cause » ou « force » distincte
de changement évolutif.

Cependant, le débat philosophique a porté plus loin que la question de
la façon la plus appropriée de décrire la dérive, comme conséquence ou
processus. En faits, il y a ici toute une famille de questions
entremêlées en jeu qui sont, malheureusement, toutes confondues.
D’abord, il y a ce que j’appellerai la question métaphysique: la
dérive (ou même, d’ailleurs, la sélection naturelle) est-elle un
épiphénomène, ou un processus causal « au niveau des populations »
survenant véritablement? Plus généralement, y a-t-il même un
« processus » opérant au niveau des populations dans l’évolution?
Ensuite, il y a ce que j’appellerai la question épistémologique:
comment sont empiriquement substantiés les arguments en faveur de la
dérive (ou de la sélection)? Si de nombreux témoignages des rôles
relatifs de la dérive et de sélection sont empiriquement
sous-déterminés --- i.e., on observe une variation par rapport à une
moyenne, un écart à l’attendu, et l’étiquetons simplement
« dérive » ---, par quoi sont alors soutenus les arguments en faveur
de ce qu’opère un « processus » distinct générant cet état de chose?
Pourquoi « dérive » n’est-il pas un terme « poubelle » où l’on met
l’inconnu --- signe de ce que l’on manque d’informations quant au
processus causal, plutôt qu’argument sur un processus
d’échantillonnage indiscriminé dont nous savons qu’il opère? Il semble
que ces deux questions distinctes aient été confondues dans le débat
sur la dérive comme cause de changement évolutif.

L’échange peut être retracé jusqu’au texte influent de Sober sur la
nature de la sélection. Sober (1984: 141) décrit la théorie évolutive
comme une « théorie des forces »: « Dans la théorie évolutive, les
forces de la mutation, migration, sélection et dérive constituent des
causes qui propulsent une population le long d’une séquence de
fréquence de gène. Identifier les causes de l’état présent … implique
de décrire quelles forces évolutives ont une incidence. » Shapiro et
Sober l’appellent plus tard la « vision conventionnelle » de
l’évolution:

> La vision conventionnelle, à laquelle s’opposent WALM [Walsh, Ariew,
> Lewens et Matthen], veut que la sélection naturelle, comme la
> dérive, la mutation, la migration et le patron d’assortiment, soient
> des causes possibles d’évolution. Ces causes affectent une
> population, parfois changeant son état, parfois causant qu’elle
> reste dans le même état.^[L’état d’une population peut être
> caractérisé en spécifiant les fréquences de ses différents traits.]
> Les causes de l’évolution se comportent en un certain sens comme des
> forces Newtoniennes. Si deux forces promeuvent l’évolution d’un
> trait, il augmentera en fréquence à une vitesse plus rapide que si
> l’une d’elle seulement était en place. Et une population peut être
> à l’équilibre parce que des forces opposées s’annulent mutuellement
> … par exemple, la sélection pousse à l’augmentation de la fréquence
> quand la pression de mutation pousse à son déclin. WALM appelle ceci
> la vision dynamique. (Shapiro et Sober, 2007)

Shapiro et Sober arguent que la vision de la dérive génétique comme
force ou cause (tout comme le sont la sélection, la migration, la
mutation) n’a du sens que pour autant que la réduction de la taille de
population (qu’ils identifient à la dérive) est un facteur associé au
changement de fréquence d’allèles dans une population au cours du
temps (p. 38). En d’autres termes, la dérive « supprime » la
variabilité génétique des populations. La dérive et la mutation
(comme la sélection et la migration) sont donc qualifiées de
« forces » aux tendances opposées; la dérive supprime la variabilité,
la mutation la restaure. Ces métaphores sont assurément problématiques
de certaines façons; pourtant, elles capturent bien un aspect
important du changement évolutif au niveau des populations.
L’isolement et la consanguinité réduisent la variabilité génétique; de
petites populations sont plus susceptibles de contenir moins de
variation que de grandes populations panmictiques, e.g. les espèces
dans leur globalité. En ce sens, la dérive, ou échantillonnage au
« hasard », compris comme tout processus aléatoire impliquant la
réduction du nombre d’individus panmictiques dans quelque population
en question, est une « force » de changement dans le sens (en moyenne)
d’une réduction de la variation génétique.

Ceci est décrit (à raison) comme une vision « consensuelle », dans la
mesure ou nombre de biologistes de nos jours décrivent la dérive comme
s’il s’agissait d’une « force » de réduction de la variation génétique
au cours du temps en raison de quelque chose comme une erreur
d’échantillonnage. (Comme nous l’avons vu, néanmoins, ça n’était en
aucun cas le sens de la dérive auquel il était fait uniformément
référence historiquement.) Comme la sélection ou la mutation, la
dérive peut métaphoriquement être décrite comme contrecarrée par des
« forces » concurrentes --- une population est à l’équilibre lorsque
ces forces « s’annulent » mutuellement. Aussi, de très petites
différences sélectives ne seront guère effectuées dans de petites
populations, à cause des « effets » de la dérive (voir aussi Stephens
2004).

En réponse à Sober, Shapiro et Stephens, Walsh (2000), Matthen et
Ariew (2002) et Walsh et al. (2002) ont argué de ce que la théorie de
l’évolution n’est pas une théorie de forces. Ils identifient cette
vision à une vision « dynamique » de la théorie évolutionniste, et lui
oppose leur vision « statistique ». Au cœur de ce débat est la
question de savoir si les naissances et morts individuelles, les
interactions des organismes avec leurs environnements peuvent ou
doivent être supplantés par des explications au niveau populationnel,
ou si ces dernières ne sont guère que des descriptions statistiques
sans réel poids causal.

Matthen et Ariew (2002) prétendent qu’il y a deux concepts d’aptitude
(_fitness_) opérant en biologie évolutive: l’aptitude « vernaculaire »
ou « écologique » --- l’aptitude comme adaptation relative ou comme
meilleure « solution de dessein » --- contre l’aptitude comme « mesure
prédictive », mesure statistique du « taux relatif attendu
d’accroissement d’un gène (ou d’un trait), dans les générations
futures. » La première ne fait pas même partie de la théorie
évolutionniste, puisqu’il n’y a pas de lois réelles décrivant comment
sont causées les différences d’aptitudes entendue en ce sens. La
seconde est simplement une mesure statistique, et donc, à leur sens,
pas du tout un paramètre causal.

Walsh (2000) argue de façon similaire que la sélection naturelle n’est
guère que l’« ombre » de processus causaux véritables; « il n’y a nul
besoin de recourir à une force _distincte_ [de sélection naturelle]
opérant sur les populations », quand, au niveau des organismes
individuels, on dispose déjà des nombreuses causes de naissance et
mort individuelle (p. 139; italique dans l’original; voir aussi p.
150). Dans le même sens, Walsh et al. (2002) défendent
l’interprétation « statistique » contre l’interprétation « dynamique »
de la théorie évolutionniste: « Sélection et dérive ne sont pas des
forces agissant sur les populations; ce sont des propriétés
statistiques d’assemblages d’évènements de « tri »: naissances, morts,
et reproduction. Les seules forces de l’évolution opèrent au niveau
des individus … et aucune d’elle ne peut être identifiée à la
sélection ou à la dérive » (p. 453). Walsh et al. (2002) disent que la
dérive a lieu si et seulement si une population montre une fréquence
de trait s’écartant de la fréquence que l’on prédirait si la sélection
seule agissait. C’est-à-dire, la « dérive » ou « fluctuations » de
fréquence de gène sont simplement des écarts à l’attendu, où l’attendu
se base sur ce que WALM appelle l’aptitude « prédictive ». Dans la
même veine, mais plus récemment, Pigliucci et Kaplan (2006) écrivent:

> La dérive n’est d’aucune façon un processus; la meilleure façon dont
> on peut faire sens du concept ressort non d’une propriété que des
> populations particulières partagent, mais plutôt de la fréquence
> relative des types de changement dont les populations ont fait
> l’expérience. Dans le cas où les changements observés sont proches
> des changements attendus statistiquement … on peut dire que l’issue
> reflète nos attendus de la sélection prédictive; dans le cas où les
> changements sont plus distants de la moyenne, on peut dire que
> l’issue ne reflète pas ces attendus --- c’est-à-dire, nous pourrions
> choisir de l’appeler un exemple de dérive. Mais cela n’implique pas
> qu’aucun genre de processus ait eu lieu dans la seconde population
> qui n’ait pas aussi eut lieu dans la première (Pigliucci et Kaplan
> 2006: 28)

Il semble y avoir deux questions en jeu dans le passage ci-dessus.
D’abord, Pigliucci et Kaplan prétendent que la dérive n’est pas un
processus. Ceci fait plus ou moins écho au commentaire précédent de
WALM, selon quoi « formellement » la sélection et la dérive ne sont
pas des processus causaux distincts. Ensuite, ils prétendent que ce
que l’on appelle dérive n’est souvent qu’un écart à l’attendu. Cela
ressemble plus à une critique empirique de la pratique de la biologie
qu’un argument métaphysique sur la nature de la dérive.
Vraisemblablement, ces deux questions peuvent être séparées; qu’il
y ait ou non des processus causaux au niveau populationnel dans
l’évolution, et que les les biologistes fournissent suffisamment de
preuves de leurs évaluations du caractère opératoire de la dérive et
de la sélection sont deux questions distinctes, qui pourraient bien
avoir différentes réponses. En d’autres termes, l’un des diagnotic du
débat est que les deux questions ont été confondues.

Ceci posé, les critiques de la vision « consensuelle » sont exactes
par bien des aspects. Les modèles formels de sélection et dérive sont
des mesures statistiques du changement dans les populations au cours
du temps. Ils traitent la survie moyenne et le succès reproductif de
groupes de traits comme causaux, et, dans la mesure où les résultats
s’écartent de l’attendu, l’issue est souvent expliquée par « dérive ».
La dérive est souvent identifiée avec la variété de facteurs hasardeux
inconnus affectant la survie et le succès reproductif. En effet, Sober
serait sûrement d’accord avec cette description d’une bonne partie de
la pratique biologique.

Cependant, il ne s’ensuit pas, du moins pas sans argumenter plus
avant, que la dérive et la sélection ne sont pas des causes, ou
qu’elles ne sont qu’épiphénomènes. Toutes deux sont véritables,
lorsqu’elles sont des processus causaux « survenants », selon Sober et
Shapiro. Leur argument pour cet argument dépend d’un présupposé clé:
« enquêter sur le fait que X cause Y implique de comprendre si le fait
de faire varier X en tenant constante toute autre cause qu’X et
Y puissent avoir en commun est associé à une variation de Y. Il n’est
pas pertinent de se demander se qu’il advient si l’on fait varier X en
gardant constante la base micro-survenante^[Les conditions suffisantes
d’existence de X (NdT).] de X. » Ce présupposé s’inspire du modèle
« manipulationniste » de causation de Woodward (2003: 55), selon quoi
une « condition nécessaire et suffisante pour qu’X soit une cause
directe de Y eut égard à un ensemble de variable V est qu’il y ait une
intervention possible sur X qui change Y ou la distribution de
probabilité de Y lorsqu’on garde constante toute valeur d’autres
variables V. »

Pour Woodward, des conditions très précises doivent être remplies pour
qu’il soit le cas que I est une intervention sur X par rapport à Y.
Une intervention sur X doit changer la valeur de Y; doit être une
sorte d’« interrupteur » de Y, en gardant toute autre variable
constante. De plus, X doit être une variable bien définie, telle qu’il
est clair en quoi consiste une variation de la valeur de cette
variable. En somme, toute prétention causale est essentiellement une
prétention sur la façon dont la manipulation d’une variable, ou
changement dans sa valeur, est capable de changer la valeur d’une
seconde variable.

Shapiro et Sober arguent que la sélection et la dérive sont des causes
survenantes qui remplissent ces conditions. La « base supervenante »
de la sélection, selon eux, est la mort et la naissance individuelle
d’organismes dans une population. Dans le même sens, la base
supervenante de la dérive est la taille de population effective. On ne
peut « garder constante » la taille effective de population et changer
les effets de la dérive. Les arguments en faveur de ce que la
sélection naturelle n’est pas une cause parce qu’elle n’est pas
distincte des interactions causales individuelles constituant sa base
supervenante sont proches de ceux en faveur de ce que les croyances,
les désirs, etc. ne sont pas des causes, parce qu’indistinctes d’états
neurologiques du cerveau. Shapiro et Sober résument:

> Nous répondons que, même s’il est vrai que la sélection naturelle
> n’est pas distincte de sa base de supervenance dans un processus de
> sélection donné, ça n’est pas une raison de nier que la sélection
> soit une cause. De la même façon, on voit la température, la
> pression, le volume de gaz d’un contenant comme des causes bien
> qu’elles surviennent sur les états des molécules constituant le gaz.
> Walsh demande que la sélection contribue à l’évolution par delà les
> contributions des processus causaux affectant les organismes
> individuels … Bien sûr que la sélection ne le peut, mais ça n’est
> pas un argument contre son effectivité causale. En évaluant si
> X cause Y, on ne devrait pas avoir à garder constante la base
> micro-survenante de X tout en faisant varier X.

En réponse à WALM sur la question de la dérive, Shapiro et Sober
arguent qu’ils confondent issue et processus:

> Walsh, Lewens et Ariew (2002) disent que la dérive a lieu lorsque et
> uniquement lorsqu’une population montre une fréquence de trait qui
> s’écarte de la fréquence prédite si seule la sélection opérait …
> Pour WALM, la dérive est une issue possible; un produit, non un
> processus.^[Brandon et Carson (1996: 324-325) voient la dérive de
> la même façon.]

> La dérive fait partie du processus évolutif dès lors que la taille
> de population est finie, tout comme la sélection en fait partie dès
> lors qu’il y a variation dans l’aptitude d’un trait. Nous ne voyons
> aucun inconvénient à voir ces deux « parties » du processus évolutif
> comme processus en eux-mêmes … La sélection et la dérive sont des
> processus distincts dont l’amplitude est représentée par des
> paramètres populationnels distincts (l’aptitude d’un côté, la
> taille effective de population de l’autre). Les variations de
> chacun de ces paramètres sont associés à des variations dans la
> probabilité des différentes issues. Si l’on intervient sur la valeur
> de l’aptitude en gardant constante la taille de population, cela
> sera associé avec un changement de la probabilité de différentes
> fréquences de traits dans la génération suivante. C’est aussi vrai
> si l’on intervient sur la taille de population en gardant constante
> l’aptitude. La sélection et la dérive sont des causes parce qu’elles
> font la différence (_difference-makers_). Les valeurs d’aptitude et
> de taille de population ne sont en rien comme le baromètre dans sa
> relation au temps.

En somme, réduire la taille de population réduit en moyenne le temps
de fixation d’allèles rares. C’est là tout ce qu’implique d’asserter
que la dérive est une cause, selon Shapiro et Sober. Des facteurs
aléatoires jouent un rôle dans la fixation d’allèles en petites
populations plus rapide qu’en grandes populations, de la même façon
que lancer moins de fois une pièce est davantage susceptible de
produire uniquement des piles ou des faces. C’est vrai de n’importe
quel ensemble de populations; de plus petites populations montrent
davantage d’écart à l’attendu, à cause d’erreurs d’échantillonnage, où
l’écart à l’attendu peut être dû à quoi que ce soit, de la foudre à la
recombinaison génétique.

Reisman et Forber (2005) s’inspirent de l’« expérience » de dérive de
Dobzhansky et Pavlovsky pour illustrer cet argument. Dobzhansky et
Pavlovsky (1957) expliquent comment la variance des fréquences de
gènes dans un ensemble de population peut accroître ou décroître,
résultant de choix de tailles d’échantillon différentes. Pour autant
qu’il soit possible de manipuler la variance génétique, en
sélectionnant de plus petites ou plus grandes populations, la dérive
est une cause de changement évolutif au sens de Woodward. Shapiro et
Sober arguent ainsi que la dérive « survient » de changements de $Ne$,
la taille effective de population. La dérive n’est donc ni
épiphénomènale, ni causalement inerte, disent-ils, parce que l’on ne
peut faire varier la taille effective de population sans faire varier
la dérive d’autant.

Il semble que même Pigliucci et Kaplan l’accordent à des ensembles de
populations dans leur commentaire. Ils écrivent:

> Au sens formel, la sélection naturelle peut être explicative
> à l’échelle des changements moyens de fréquence d’attributs
> héréditaires dans les populations --- c’est-à-dire, à l’échelle
> d’ensemble de populations … Assurément, s’il y a des différences en
> aptitude prédictive, il doit y avoir des processus différents au
> niveau individuel. Cependant, les différences particulières en
> aptitude prédictive que l’on trouve au niveau formel ne sont pas
> nécessairement le reflet de ces différents processus de quelque
> façon évidente que ce soit. (p. 32)

En d’autres termes, ils semblent accorder que les modèles formels
capturent bien une forme de relation explicative; ils ne vont pas
jusqu’à admettre qu’il y ait une relation causale. Et cela semble
avoir essentiellement trait aux _bases empiriques_ des arguments en
faveur du caractère opératoire de la sélection, « les différences
particulières en aptitude prédictive que l’on trouve au niveau formel
ne sont pas nécessairement le reflet de ces différents processus de
quelque façon évidente que ce soit. »

Autrement dit, il semble que les deux camps se divisent, d’abord sur
le point de l’existence de choses telles que des processus causaux
survenant à l’échelle des populations, ensuite sur la question de si
les biologistes sont empiriquement fondés dans leurs jugements de
façon générales. C’est-à-dire, d’un côté, toutes les parties semblent
s’accorder sur le fait que la génétique des populations est une
description « formelle » adéquate des patrons de distributions
statistique au cours du temps. Là où ils divergent, c’est sur la façon
dont les modèles de sélection traitent des distributions de propriétés
comme causes de leurs conséquences. Dans le cas de la sélection, ces
propriétés aboutissent à des différences d’aptitudes; i.e. des
propriétés telle que la vitesse de course ou la résistance à un
antibiotique affectent la survie et la reproduction. Ainsi,
à l’échelle des distributions d’allèles dans le temps, les
coefficients d’aptitude associés à la possession de tel ou tel trait
en prédisent les conséquences. Si l’on peut accorder que de telles
représentations indirectes, ou le calcul de la moyenne des variétés
d’interactions causales réelles que « aptitude » représente sont une
sorte de modèle causale, alors on peut accorder joyeusement que la
sélection est une cause. Cependant, tout le monde n’accorde pas que de
telles représentations indirectes soient véritablement causales, ou
qu’il y ait même un « processus » de sélection distinct. Au moins en
partie, cela a trait au fait que les arguments en faveur de tels
processus sont bien souvent sous-déterminés par les preuves.

Ainsi, au cœur de cette division se tiennent non seulement une
question de métaphysique de la causalité, mais aussi une question de
consistence des preuves en faveur de patrons ou processus
populationnels. Du point de vue de la question métaphysique, le
problème est entièrement ouvert; de sérieux débats subsistent dans la
littérature philosophique quant à ce que les causes soient un ou
plusieurs types d’objet, ou à ce qui interagit causalement avec autre
chose comprend tous des événements particuliers, et uniquement eux, ou
inclut aussi des types d’événements (Hausman 2005; Godfrey-Smith
2007). D’un côté, Shapiro et Sober prétendent que la dérive est une
cause, de la même façon qu’un faible nombre de lancers d’une pièce non
biaisée cause une série de face uniquement. Selon WALM cependant, il
semble que seulement tous les événèments particuliers, et eux
uniquement, soient des causes; naissance et mort individuelle, ou
interactions physiques entre organismes individuels et leurs
environnements sont causaux; les prétentions à la « survenance » de la
sélection ne sont sans doute guère plus que redondantes. Une
inquiétude quant à l’adoption de cette vision restrictive, néanmoins,
est que nous aurions à abandonner toute prétention causale de
processus populationnels. Ceci impliquerait pourtant que de prendre de
petites tailles d’échantillons n’est pas une cause de distribution
biaisée; c’est donc exclure nombre d’explications en statistique, en
biologie et en sciences sociales. Il est relativement commun, par
exemple, d’arguer qu’une taille d’échantillon faible est une cause de
résultats expérimentaux fallacieux; en fait, cette prémisse joue même
un rôle central dans nombre d’arguments de Pigliucci et Kaplan (2006).
Selon le critère de WALM, le jeu ne cause pas de perte d’argents;
c’est plutôt une main particulière ou un lancer de dé qui en est
responsable. Cela semble exclure indûment une large gamme
d’explications causales dépendant de relation de survenance.

En somme, ce que semblent arguer les critiques de la vision
« consensuelle » n’est pas simplement que l’« aptitude prédictive » ou
la dérive ne soient pas causales, mais plus fondamentalement, que
l’évaluation des rôles respectifs de la sélection et de la dérive sont
mal corroborés par les preuves. C’est-à-dire, (1) dans leur pratique
réelle, les biologistes supposent simplement que le modèle est vrai du
système d’intérêt, et (2) fréquemment, la dérive est appliquée à toute
déviation de l’attendu trouvé dans les données, avec ou sans preuves
suffisantes que c’est le hasard qui en est jeu plutôt que d’autres
facteurs non-déterministes.

Sur ce cet argument empirique, ils ont ma sympathie, car les
biologistes ont assurément manqué de cohérence et de précautions quant
au sens de la dérive et à ses emplois. Évaluer si la sélection a opéré
dans toute population requiert plus que la connaissance de
distributions statistiques au cours du temps; cela requiert des
informations sur l’écologie etc., pas seulement des corrélations
populationnelles. Cependant, Shapiro et Sober seraient (probablement)
enclins à accorder ce dernier point épistémologique. Il peut bien être
le cas que beaucoup d’arguments quant à l’importance relative de la
sélection et de la dérive soient mal corroborées; mais ça n’est pas
suffisant pour montrer que la dérive et la sélection sont
épiphénoménales.

# Conclusions

Walsh, Ariew, Lewens, Matthen, Pigliucci et Kaplan ont raison
lorsqu’ils disent que les biologistes ont réifiés des variables de
leurs modèles comme variables causales, quand ce que ces variables
mesurent sont des taux de survie ou de fécondité, des sommations d’une
gamme d’interactions causales à la micro-échelle. Dit autrement, les
événements « réellement » causaux dans ce contexte sont les
interactions entre organismes individuels discret et leurs
environnements. Les coefficients de sélection sont une sommation
statistique de nombreuses interactions causales « réelles » entre
organismes et leurs environnements. Kaplan et Pigliucci, d’un autre
côté, semblent arguer non seulement que la sélection est
« épiphénoménale », mais aussi qu’une explication évolutionniste plus
complète impliquerait d’enquêter sur les détails écologiques. En
d’autres termes, leur propos n’est pas strictement métaphysique, en ce
sens que la sélection et la dérive sont correctement entendus comme
processus causaux ou non, mais plutôt épistémologique, en ce sens que
suffisamment de preuves sont avancées ou non pour corroborer les
arguments quant à l’importance relative de la sélection et de la
dérive. Ces deux points sont bien différents, peuvent donc être
traités séparément, et doivent l’être.

De l’autre côté, Shapiro et Sober, et Reisman et Forber ont raison (en
supposant que le modèle Woodward de la causalité ne pose pas problème,
et que de traiter la dérive comme survenant sur la taille effective de
population ne demande pas plus de plaidoirie particulière) lorsqu’ils
affirment que la dérive est une cause de changement évolutif. En
somme, si les prétentions causales ressortent essentiellement de la
manière dont la manipulation d’une variable, ou changement de la
valeur de cette variable, en moyenne, est capable de faire varier la
valeur d’une seconde variable, alors en ce sens au moins, la dérive
est une cause.

Cependant, le débat concernant la généralité du modèle Woodward de
causalité est loin d’être clos (Cartwright 2003; Hausman 2005). Que
l’influence causale nécessite une influence physique, que la causalité
nécessite des relations entre particules et seulement elles, ou aussi
entre types, sans parler de la manière dont nous somme censés évaluer
les questions d’intermédiaires causaux, tout cela requiert des
explorations plus avant. Il semble n’y avoir aucune place à la
pluralité des formes de causalité; avec Cartwright (2003) j’arguerais
que nous ne devrions pas exclure qu’il puisse y avoir diverses formes
d’explications causales.

Les critiques de la vision « consensuelle » croient que le traitement
de la dérive comme un processus causal survenant est problématique. En
quoi? Une difficulté potentielle est qu’il est difficile de « situer »
la dérive à la fois historiquement et conceptuellement. Sur quoi
survient la dérive, et est-elle correctement comprise comme cause ou
effet des processus aléatoires distribués qui réduisent collectivement
l’hétérogénéité génétique? Est-elle la réduction de la taille de
population, de l’échantillonnage de gamète, de la méiose et de la
recombinaison, les processus aléatoires affectant la survie et la
fécondité, ou chacun de ces cas? La taille de population finie est
sans doute plus correctement décrite comme _une condition de
possibilité_ d’erreurs d’échantillonnage qu’une cause d’hétérozygotie
réduite. Assurément, réduire la taille de population accroît la
possibilité d’échantillonnage aléatoire, mais il est vrai aussi que
les causes et conséquences de l’échantillonnage sont distribuées et
différentes aux différents niveaux d’organisation. Il est donc
difficile de « situer » la dérive à un stade précis de ce processus
étendu dans le temps.

Il y a de bonnes raison d’être inquiet ici, car historiquement il
a été fait référence à la dérive dans chacun des cas ci-dessus --- à
la fois la diversité des conditions et des conséquences de
l’isolement, la consanguinité _et_ la chance en évolution. La dérive
est souvent le terme fourre-tout du biologiste pour tout changement de
fréquence de gène due à des événements stochastiques,
non-déterministes.^[Bien sûr, tous les auteurs ne sont pas d’accord
qu’il existe même des événements indéterministes en évolution.
Certains (Rosenberg 1994; Graves et al. 1999) ont défendu le fait que
la dérive, si elle est même un processus, doit être déterministe, et
nié que la dépendance de la théorie évolutionniste à des explications
statistiques soit due à autre chose qu’à des raisons purement
épistémologiques. Pour les fins de cet article, cet ensemble plus
large de question sera mis de côté.] À différents niveaux
d’organisation, et à différentes échelles de temps, cependant, de
bien différents « moteurs » ou sources de stochasticité existent. Le
brassage d’allèle au cours de la reproduction sexuée est une « cause »
ou moteur de dérive différente des manières aléatoires dont la survie
et la fécondités sont affectées. Ainsi, bien qu’il soit vrai que les
mêmes modèles de bases sont employés, du moins dans un premier temps,
pour modéliser la dérive à différents niveaux d’organisation et
différentes échelles de temps, les processus causaux « réels » sont
vraisemblablement différents à ces différents niveaux d’organisation
et temporels.

Si l’on peut accorder que des modèles statistiques hautement idéalisés
représentent de façon légitime des causes, alors une causalité
populationnelle, survenant de la sélection et la dérive ne pose pas
problème. Cependant, si vous soutenez que toutes les interactions
écologiques entre organismes et leurs environnements, et seulement
elles, sont causales, traiter les taux de naissance et mort, ou les
tailles de population comme des causes, n’est au mieux que de peu
d’intérêt intuitif. La réponse de Sober suggère que les explications
de génétique évolutives ne sont pas distinctes; les modèles, comme les
théories populaires de l’esprit, surviennent sans doute de détails
biologiques bien plus compliqués, et sont de bons raccourcis pour des
relations de dépendances populationnelles. Dans la mesure où nos
modèles mathématiques éliminent quelque contingence apparente et
démontre en quoi et pourquoi cette observation dépend de régularité
mathématiques plus basiques et mieux comprises, les modèles sont
explicatifs. De cette façon, les modèles de dérivent sont explicatifs
en vertu de la loi des grands nombres; de plus petites tailles de
population engendrent des distributions différentes. Pourtant ce
modèle explicatif est lui-même contestable; ce que Walsh et al. et
Lewens semblent contester n’est pas simplement que la dérive et la
sélection soient des causes ou de quelle façon elles le seraient, mais
que l’explication en biologie suffise si elle repose sur des modèles
idéalisée employant des paramètres statistiques. Ils semblent suggérer
que nous devons mieux comprendre la biologie moléculaire, l’écologie,
le développement etc. de l’organisme afin d’expliquer l’évolution de
façon convaincante.

Si nous étions en mesure de consulter les auteurs de la génétique des
populations théorique, sans doute que Wright serait plus proche de
WALM, et Fisher de la vision consensuelle défendue par Shapiro et
Sober. Wright était parfaitement conscient de ce que ses modèles
avaient d’idéalisé, de ce qu’ils n’étaient qu’indirectes
représentations d’interactions biologiques compliquées, posant de
nombreux postulats faux à strictement parler. Assurément, Wright
aurait soutenu que la compréhension plus avant de l’écologie, du
développement etc. est crucial à la fois à l’explication de
l’évolution et à l’évaluation d’arguments en faveur de l’adaptation.
Pourtant, dans une veine Fisherienne, les modèles sont d’utiles outils
(même s’ils ne sont qu’idéalisés) pour comprendre la dynamique
populationnelle.

Cependant, il y a assurément de la place à la fois pour des stratégies
explicatives ascendantes (_bottom-up_) et descendantes (_top-down_) en
biologie. C’est-à-dire, nous avons besoin à la fois des perspectives
Wrightiennes et Fisheriennes sur les dynamiques évolutives au cours du
temps. La clé est de trouver un terrain commun entre les deux qui
intègre le meilleur de notre compréhension non seulement de dynamiques
populationnelles, mais aussi des bourbeux détails biologiques.

# Références

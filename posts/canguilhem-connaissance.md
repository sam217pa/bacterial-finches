---
title: Canguilhem, la connaissance de la vie
author: Samuel Barreto
date: 2018-08-08
---

L'Histoire des sciences montre que le concept de Vie est
historiquement inscrit dans un processus que Canguilhem nomme
oscillatoire entre des visions mécanistiques ou *mécanicistes* de la
vie et des visions *vitalistes*.

Peu de praticiens de l'évolution tendraient aujourd'hui à avoir
recours à cette vision *vitaliste* — la tendance actuelle étant au
réductionnisme génétique. Elle est souvent rattachée à la description
par Bergson de ce qu'il appelait « élan vital, » une conception de
l'évolution orientée par ce que d'autres appelaient Entéléchie
— notamment Driesch après Leibniz. Elle implique d'avoir recours à une
sorte de dualisme entre l'âme et la matière, entre l'entéléchie et la
monade[^1], comme si l'« âme » était *au principe* de la matière ; comme
si la matière était orientée par l'âme.

[^1]: Non pas que je considère ici apparenter l'âme et l'entéléchie d'une part, la matière et la monade d'autre part. Je ne suis pas assez familier des écrits de Leibniz pour m'aventurer sur ce terrain là, mais c'est ce que j'ai cru en comprendre à la lecture de Canguilhem.

Canguilhem montre en quoi le rejet de cette vision des choses est
dépendante de la dynamique sociale et politique du champ scientifique
à un moment donné de son histoire ; ce que certains qualifient de
*Weltanschauung*. Il décrit les développements de la conception de la
vie dans les travaux de physiologistes et médecins, essentiellement
entre 1600 et 1900, en l'affrontant aux conceptions aristotéliciennes.

L'un des chapitres les plus intéressants selon moi concerne la théorie
cellulaire, la façon dont elle est venue à être relativement largement
acceptée, les conceptions auxquelles elle s'est heurtée, les
« pré-jugés » scientifiques, sociaux ou politiques des praticiens ou
théoriciens qui l'ont accueillie. Il fait écho, il me semble, à de
nombreux débats contemporains sur le réductionnisme, la notion
d'individualité biologique, l'élan vital, le caractère processuel de
l'individu biologique …

Certains considéraient en effet le corps comme un agrégat de cellules
*libres*, dont le « comportement » serait identique si elles n'étaient
pas agrégées justement. D'autres proposaient au contraire que la
« nature » respective des cellules et du corps qu'elles « composent »
sont dans une relation de causalité réciproque : les cellules sont
structurées par les *tissus* --- et Canguilhem montre en quoi le
vocabulaire témoigne de la *Weltanschauung* des scientifiques qui
l'emploient --- dans lesquelles elles sont *inscrites*. En forçant le
trait, les premiers considèrent le corps, partant, la Vie, comme un
agrégat de monades libres, les seconds considèrent que le principe du
corps est à trouver dans l'agrégation même, que l'on ne peut réduire
la Vie à une somme de partie. Canguilhem montre en quoi ces deux
visions — j'en expurge ici toutes les nuances et les balancements
qu'opèrent ceux qui les proposent, ceux qui s'opposent — correspondent
aux visions socialement déterminées de ce qu'est l'individualité
sociale. L'époque politique opposait deux visions de la société
relativement analogues, entre une vision libérale, atomiste voir
monadiste de la société et une vision disons holiste.

Sans employer ce vocable austinien, il décrit comment les
pré-conceptions sociales ont un effet *performatif* : elles contribuent
à déterminer la vision du monde de ceux sur lesquels elles opèrent,
et donc contribuent à générer un monde social qui se conforme à elles.
(Je crois qu'on peut voir ici l'influence de Canguilhem sur Bourdieu ou
Foucault.)

Je pense qu'on trouverait matière à de nombreux débats actuels sur la
notion d'individu, la théorie de l'*holobionte*  — [tous entrelacés ?](https://livres-et-lectures.com/70-eric-bapteste/73-eric-bapteste-tous-entrelaces) —
le caractère discret, essentialiste ou processuel de l'individu dans
la construction historique de la théorie cellulaire, sa réception par
les médecins et physiologistes.

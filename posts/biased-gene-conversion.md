---
title: Biased Gene Conversion
date: 2015-10-17
author: Samuel Barreto
---

Base composition of genomes is affected by many major process, being
either neutral or selective. The neutral model describes mutations as
following the genetic drift, which depends on population effective
size. It does not represent the genome reality very well, but it is
a useful model as a null hypothesis. One must reject the neutral
hypothesis before affirming that a genomic trait is under selection
constraints.

# Natural Selection hypothesis

> A curious aspect of the theory of evolution is that everybody thinks
> he understands it. *Jacques Monod*

The natural selection hypothesis discriminate three cases of a genomic
trait being under selective constraints. The positive or directional
selection leads to the gradual erasing of variation on a given
position of the genome, if this position has a positive impact on the
fitness. The purifying, or negative selection, act against all
variation, nearly all mutation are deleterious. Typically, a mutation
on conserved region of 16S rDNA in Bacteria are deleterious : they
does not spread in the population.

Recently, a new process that can confound selection tracks with
a neutral process has been discovered : the biased gene conversion.

# Gene conversion

A gene conversion event takes place when the resolution of the
intermediate poroduct of homolog recombination leads to the
uni-directional—non-reciprocal—exchange of genetic information from
one donor to a receptor sequence. It is a key process of the first
meiosis division, an obligate step in eukaryotes gametogenesis.

Nevertheless, if one allele has a greater chance to be the donor, the
process is biased.

# Biased Gene Conversion

There are two main scenarios to explain a biased gene conversion
event.

## Initiation bias

The initiation bias appears when a region on one double stranded DNA
is more often victim of double strand breaks than its homolog region
on the sister chromosome. This region is thus called a recombination
hotspots, whereas its homolog on the sister chromosome is called
a recombination coldspots. Paradoxically, on the lifespan of the
hotspot, the coldspot will more often be the donor of gene conversion
events, leading to the gradual death of the corresponding hotspot.
There is an initiation bias on the gene conversion event.

## GC-biased gene conversion

The other bias is called GC-bias gene conversion. Indeed, it has been
shown in many eukaryotes, including yeast and human, that the G or
C alleles of a gene are statistically significantly more often the
donor of a gene conversion event than its A or T corresponding allele.

> It looks exactly as directed selection, but is *not* an adaptative
> process.

One can find traces of GC-bias gene conversion in a region if all
positions are affected, being either neutral or _even deleterious_.
Typically, gBGC is associated with regions of high rate of
recombination, actual or past. While selection only act on non neutral
regions[^3], gBGC also acts on nearby introns and non-functional
sequences. Moreover, selection can act on a very large genomic scale,
when linkage is taken into account.


----------

gBGC has been shown to be responsible for the large scale variations
of GC-content, the so called genomic isochores; it can lead to the
fixation of deleterious mutations; and it can confound traces of
selection.


[^3]: exons or regulatory sequences

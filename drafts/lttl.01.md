# Introduction

Au début de l'année 2003, le Président Bush prétendit que l'Irak tentait de se fournir les matériaux nécessaires à la fabrication d'armes nucléaires.
Bien que des officiels de la Maison Blanche aient plus tard admis qu'il leur manquait les preuves appropriées pour croire que c'était vrai, divers membres de l'administration ont balayé cette question, notant le fait que, que tout argument du président soit juste ou non, l'important étant que l'invasion subséquente de l'Irak parvienne à stabiliser la région et à libérer le pays.

Nombre de citoyens américains étaient apparemment d'accord.
Après tout, il y avait d'autres raisons de vouloir déposer le régime de Hussein.
Et la croyance que l'Irak constituait une menace nucléaire imminente nous a réunis et a fournis une justification commode à ceux qui doutaient de la noblesse de notre cause.
Qu'est-ce que cela peut-il alors bien faire que cela ne soit pas réellement vrai?
À beaucoup, il semblait naïf de s'inquiéter d'une chose aussi abstraite que la vérité ou la fausseté de ces prétentions lorsqu'on pouvait s'inquiéter de choses qui comptent réellement --- telles que nous protéger du terrorisme et s'assurer un accès au pétrole.
Pour paraphraser Nietzsche, la vérité peut bien être bonne, mais pourquoi pas de temps en temps la non-vérité (_untruth_) si elle mène où l'on veut aller?

Ce sont d'importantes questions.
Au bout du compte, est-il toujours mieux de croire et de dire la vérité?
La vérité elle-même compte-t-elle réellement?
Pour dangereuse que soient les généralisations, les réponses précédentes à l'affaire des armes Irakiennes indiquent que bien des gens regarderaient ces questions d'un mauvais œil.
L'on est plutôt cynique quant à la valeur de la vérité.

La politique n'est pas le seul endroit où l'on trouve ce genre de scepticisme.
Une attitude similaire est courante parmi certains de nos intellectuels publics les plus éminents.
En effet, sous la bannière du post-modernisme, le cynisme face aux notions de vérité et de notions liées comme l'objectivité et la connaissance sont devenue les marques semiofficielles de la posture philosophique de nombreuses disciplines académiques.
Pour le dire vite, l'idée est que la vérité objective est une illusion et que la « vérité » n'est qu'un autre nom du pouvoir.
En conséquence de quoi, si la vérité a même une valeur, elle n'en a guère --- comme le pouvoir --- qu'en tant que moyen.

Stanley Fish, un critique littéraire éminent et Doyen de l'Université d'Illinois à Chicago, a récemment poussé la rhétorique anti-vérité encore plus loin.
Non seulement la vérité objective est une illusion, selon Fish, mais s'inquiéter de la nature de la vérité est déjà une perte de temps.
Débattre d'une idée abstraite comme la vérité, c'est comme débattre de ce que Ted Williams était un meilleur frappeur que Hank Aaron: c'est amusant, mais n'est d'aucun intérêt pour le match du jour.

Paradoxalement, les raisons que Fish a de penser ceci découlent de sa théorie particulière de la vérité.
Selon Fish, la discussion philosophique sur la vérité est une perte de temps parce qu'il pense que la vérité n'a pas de valeur.
De plus, il pense que c'est ce que nous croyons déjà.
Assurément, l'on peut _dire_ que l'on veut croire la vérité, mais ce que nous désirons réellement, dit-il, c'est croire ce qui est utile.
Les bonnes croyances sont des croyances utiles, celles qui nous conduisent où l'on veut, que l'on veuille des vêtements plus élégants, moins d'impôts, ou une source stable de pétrole pour nos SUV.
Au bout du compte, la vérité de ce que l'on  croit ou dit n'a pas d'importance.
Ce qui compte, ce sont les conséquences.

Le pragmatisme grossier et commode de Fish puise à l'une de nos plus profondes veines intellectuelles.
Il fait appel à l'image de soi collective des États Unis d'Amérique comme un héros d'action à la mâchoire carrée.
Et cela peut expliquer partiellement pourquoi la protestation contre la tromperie de la Maison Blanche quant à la guerre en Irak a fait long feu.
Ça n'est pas seulement que nous croyons que « nous faisons face, unis » (« _united we stand_ »), c'est surtout qu'au fond de nous, beaucoup sont enclins à penser que ce sont les résultats, non les principes, qui comptent.
Comme Fish et Bush, certains d'entre nous jugent ennuyeux de s'inquiêter de principes abstraits comme la vérité, que c'est un pinaillage sans importance qu'il vaut mieux laisser aux binoclards qui regardent Arte et s'inquiêtent de ce que la peine de mort est « juste ».

Bien sûr, de nombreux autres intellectuels _sont_ prêts à défendre volontier l'idée que la vérité compte.
Malheureusement cependant, certaines défenses de la valeur de la vérité aboutissent seulement à grever cette valeur de façon différente.
Il y a une tendance chez certains à croire, par exemple, que _se soucier de vérité implique de se soucier des vérités « absolument certaines » d'antan_. 
Cette idée a toujours été une mélodie familière de la droite politique, entonnée avec ferveur par des auteurs comme Allan Bloom ou Robert Bork, mais dont le volume n'a commencé à monter qu'après le 11 Septembre.
Les citoyens américains ont perdu leur « boussolle morale » et ont besoin d'aiguiser leur vision avec de la « clarté morale ».
Le relativisme d'inspiration libérale affaiblit la résolution de la nation, nous dit-on; afin de l'emporter (contre le terrorisme, les « assauts » que subissent les valeurs familiales, et autres du même accabit), il nous faut redécouvrir l'accès que Dieu nous fournit à la vérité.
Et cette vérité, semble-t-il, c'est que nous avons raison, et que tout le monde a tort.
Voilà ce que dit William Bennett:

> Sous une forme ou une autre, un relativisme commode, à la fois moral et culturel, constitue notre sagesse commune de nos jours.
> Mais les choses n'ont pas toujours été ainsi.
> Il fut un temps où les enfants de ce pays étaient élevés dans la révérence de ses institutions et de ses valeurs, dans l'identification à ses coutumes et traditions, dans l'orgueil de ses extraordinaires réussites, dans la vénération de ses symboles nationaux. …
> La valeur (_goodness_) supérieure du mode de vie Américain, de la culture Américaine au sens large, étaient le message déclaré et tacite de cette instruction à la citoyenneté.
> S'il était parfois exagéré, parfois édulcoré, il était néanmoins soutenu par l'archive de l'histoire et par l'évidence accessible même aux yeux d'un enfant. 

Aussi dans les jours bénis d'antan, alors que les relativistes ne s'étaient pas encore lançés à l'assaut des murs du paradis, la vérité était si limpide qu'elle pouvait être saisie « même aux yeux d'un enfant.
C'est le genre de vérité qui compte vraiment, vraisemblablement selon Bennett. 
Se soucier de vérité objective, non-relative, c'est se soucier de ce qui est simple et tangible. 

En tant que défense de la valeur de la vérité, voilà qui est auto-destructeur.
D'abord, une allégeance inconditionnelle à ce que l'on croit ne témoigne guère d'un attachement à la vérité.
C'est un signe de dogmatisme.
Se soucier de la vérité n'implique pas de ne jamais admettre que l'on a tort.
Au contraire, cela implique d'être ouvert à l'éventualité que ses propres croyances sont erronnées.
Ensuite, cette attitude dogmatique, ce sense de la « supériorité » que Bennett encourage, confond se soucier de la vérité avec se soucier de ce que l'on croit certain.
Il résulte que cela tend plus à grever qu'à soutenir un souci de vérité. 
Car en tant qu'adultes, notre vision, contrairement à celle d'un enfant, est plus encline à discerner des nuances de gris.
En conséquence, la plupart d'entre nous sait bien qu'il est peu probable que l'on soit absolument certain de grand chose.
Mais si l'on confond la quête de vérité avec l'impossible quête d'un sentiment de certitude, alors la vérité semble du même coup elle aussi hors de portée.
Elle devient une cible dont on ne sait jamais qu'on l'atteint, donc un but qu'il n'est pas même bon de viser en premier lieu.


Le cynisme quant à la vérité est largement répandu, mais il n'est pas inévitable.
On est porté au cynisme moins parce qu'on le trouve merveilleux que parce que l'on est pas clair quant à ce qu'est la vérité et quant à sa valeur.
Et du même mouvement, cela cause notre adhésion aux éternels poncifs quant à la vérité.
Des philosophes tels que Fish disent, puisque la confiance que l'on accorde aux vieilles certitudes absolues est naïve, la vérité n'a pas de valeur.
Des auteurs comme Bennett défendent que, puisque la vérité a une valeur, l'on aurait intérêt à commencer à se rappeller de ces certitudes absolues.
Le présupposé implicite commun à ces deux conceptions est que deux choix seulement s'offrent à nous: soit la vérité avec un grand “V”, de type Certitude Absolue, soit pas de vérité du tout.
Avec de telles options, pas étonnant que l'on soit enclin à baisser les bras.

Ce livre est une exploration philosophique et une défense de l'idée que la vérité compte.
J'essaierai de vous convaincre que si vous vous souciez de vérité, vous avez intérêt à ne pas vous souciez de dogme; qu'une bonne part, mais pas tout, de ce qui porte l'étiquette de « relativisme » est creuse; que vous n'avez cependant pas à croire à une seule version vraie de l'histoire du monde; qu'être vrai à l'égard de soi est difficile mais en vaut la peine; qu'être prêt à défendre ce que l'on croit est important pour être heureux; et que si vous vous souciez de vos droits, vous devriez vous souciez de vérité.
Il nous faut trouver le moyen de contourner nos confusions et nous débarasser de notre cynisme quant à la valeur de la vérité.
Sans quoi nous ne serons pas capable d'agir intègrement, de vivre authentiquement, et de dire ses vérités au pouvoir.

Spécifiquement, ce livre peut être vu comme une défense de quatre arguments: que la vérité est objective; qu'il est bon de croire ce qui est vrai; que la vérité est un but d'enquête louable; et que la vérité mérite que l'on se soucie d'elle pour elle-même.
L'argumentation procède par parties.
Dans la première, je tente d'identifier et réfuter les confusions et les présupposés erronés les plus communs qui nous portent au cynisme quant à la vérité.
J'ai déjà mentionné certaines d'entre elles: la vérité n'a pas d'importance parce qu'on ne peut l'atteindre; la vérité n'a pas d'importance parce qu'elle est relative; la vérité n'a pas d'importance parce que le mensonge est souvent plus utile.
Chacune de ces idées est compréhensible; mais toutes sont, à mon sens, erronnées.
La partie II traite de certaines théories de la vérité.
Le cynisme quant à la vérité n'est pas simplement le résultat de confusion et d'incompréhension.
Il résulte aussi de la prévalence de certaines théories philosophiques sur ce qu'est la vérité.
Je discute et critique trois des plus importantes.
La troisième partie de ce livre explique _pourquoi_ la vérité compte.
Mon approche est une sorte d'ouverture interne, pour ainsi dire.
Je commence par quelques raisons profondément personnelles de me soucier de vérité; je soutiens que, étonnamment peut-être, cela fait partie d'une vie heureuse.
Je discute ensuite de ce pourquoi le souci de vérité compte dans nos relations personnelles à autrui.
Et je termine en examinant la valeur politique de la vérité, en particulier la connexion entre un souci de vérité et un souci de droits de l'Homme.

Déterminer les raisons qui nous portent à nous soucier d'une chose peut contribuer à une meilleure compréhension de ce qu'elle est.
Penser aux raisons pour lesquelles on devrait se soucier de vérité nous indique deux choses quant à elle: d'abord que la vérité est, en partie, une propriété profondément normative --- c'est une valeur.
Et ensuite, que c'est un fait dont doit tenir compte toute théorie adéquate de la vérité.
À la lumière de ce fait, je suggère que la vérité, comme d'autres valeurs, doit être comprise comme dépendant de, mais ne se réduisant pas à, des propriétés sous-jacentes.
De quelles propriétés dépend la vérité, ou sur quelles propriétés survient-elle, cela peut changer avec le type de croyance en jeu.
Voilà qui ouvre la voie d'un type de pluralisme: la vérité en éthique peut se trouver réalisée différemment de sa réalisation en physique.

Les questions qui comptent sont vitales, ce sont celles que l'on rencontre chaque jour.
« Pourquoi se soucier de vérité? » est une telle question.
Car malgré notre aversion pour l'illusion, l'on ne se soucie pas de vérité autant qu'on le devrait: on ruse, on s'abrite derrière l'ambiguïté, on s'abstient de prendre la parole, on tourne les talons, cessons de poser des questions, ignorons des objections raisonnables, falsifions les données, et fermons nos esprits.
Ne pas se soucier de vérité est une forme de lâcheté.
Et le fait même que cela advienne implique que la question de pourquoi l'on devrait se soucier de vérité est la fois importante et terriblement personnelle. 

Cela implique aussi que nos réponses doivent être des réponses humaines.
En conséquence, la valeur de la vérité que je défend dans ce livre n'est pas une idée abstraite, absolue.
Je n'ai pas d'intérêt à placer la vérité sur un piédestal.
Il est bon de se soucier de vérité, il n'est pas bon de l'adorer.
Elle est seulement une valeur parmi d'autres, et n'est pas même la plus importante --- si tant est qu'il y ait une telle chose.
Si la vérité compte, elle doit compter _pour nous_, pour nos confuses et conflictuelles vies d'êtres humains réels.
Si l'argumentaire qui suit est correct, on peut avoir confiance qu'elle le fera.

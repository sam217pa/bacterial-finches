\documentclass[12pt,oneside]{memoir}

% \KOMAoptions{parskip=half}

\usepackage[usenames, dvipsnames]{xcolor}
\usepackage{hyperref}
\hypersetup{colorlinks, breaklinks, urlcolor=Maroon, linkcolor=Maroon}

\usepackage{ccicons}
\usepackage{fontawesome}

\usepackage{scrlayer-scrpage}
\ifoot{\href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{\textcolor[gray]{0.70}{\ccbyncsa}}}
\ohead{\thepage}
\cfoot{} % remove page number
\ofoot{%
  \footnotesize
  \color{Gray}%
  samuel barreto\quad\faMale\\
  \href{mailto:samuel.barreto8@gmail.com}{samuel.barreto8@gmail.com}\quad\faEnvelope \\
  \href{https://sam217pa.gitlab.io/bacterial-finches/}{bacterial-finches.gitlab.io}\quad\faExternalLink
}

\newcommand{\tabbed}[1]{{{\hskip 2em}#1}}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{microtype}
\usepackage[sc, osf]{mathpazo}
\usepackage{amsmath, amssymb}
\usepackage{graphicx}
\usepackage{etoolbox}

\AtBeginEnvironment{align*}{\scriptsize}
\AtBeginEnvironment{align}{\scriptsize}

\title{Illustrations de la Logique de la Science}
\providecommand{\subtitle}[1]{}
\subtitle{Quatrième Papier --- La Probabilité de l'Induction}
\author{Charles S. Peirce}
\date{}

\begin{document}

\maketitle

\fancybreak{* * *}

Nous avons trouvé que tout argument tire sa force de la vérité générale de la classe d'inférence à laquelle il appartient; également que cette probabilité est la proportion d'arguments porteurs de vérité parmi ceux d'un genre (\emph{genus}) donné.
Voilà qui s'exprime plus commodément dans la nomenclature des logiciens médiévaux.
Ils appelaient le fait exprimé par une prémisse un \emph{antécédant}, et ce qui s'ensuit de lui son \emph{conséquent}; du principe général, selon lequel tout (ou presque tout) antécédant de la sorte est suivi d'un conséquent de la sorte, ils disaient qu'il était la \emph{conséquence}.
En employant ce langage, l'on pourrait dire que la probabilité appartient exclusivement aux \emph{conséquences}, et que la probabilité de n'importe quelle conséquence est le nombre de fois où antécédant et conséquent adviennent tous deux divisé par le nombre total de fois où l'antécédent advient.
De cette définition sont déduites les règles suivantes d'addition et multiplication des probabilités.

\emph{Règle d'Addition des Probabilités}.
--- Soient les probabilités de deux conséquences ayant le même antécédant et des conséquents incompatibles.
Alors la somme de ces deux nombres est la probabilité de la conséquence, dont suivent du même antécédant l'un ou l'autre de ces conséquents.

\emph{Règle de Multiplication des Probabilités}.
--- Soient les probabilités distinctes de deux conséquences, «~Si \emph{A} alors \emph{B}~», et «~Si \emph{A} et \emph{B}, alors \emph{C}~».
Alors le produit de ces deux nombres est la probabilité de la conséquence, «~Si \emph{A}, alors et \emph{B} et \emph{C}~».

\emph{Règle spéciale de Multiplication de Probabilités Indépendantes}.
--- Soient les probabilités distinctes de deux conséquences ayant les mêmes antécédents, «~Si \emph{A}, alors \emph{B}~» et «~Si \emph{A}, alors \emph{C}~».
Supposons que ces deux conséquences soient telles que la probabilité de la seconde est égale à la probabilité de la conséquence, «~Si et \emph{A} et \emph{B}, alors \emph{C}~».
Alors le produit des deux nombres est égal à la probabilité de la conséquence, «~Si \emph{A}, alors et \emph{B} et \emph{C}~».

Afin de montrer le fonctionnement de ces deux règles, l'on peut examiner les probabilités de lancers de dés.
Quelle est la probabilité d'obtenir un six avec un seul dé?
L'antécédant est ici l'événement du lancer de dé; le conséquent, l'obtention d'un six.
Comme le dé a six faces, qui tombent toutes avec une fréquence égale, la probabilité que l'une d'entre elle tombe est \(\frac{1}{6}\).
Supposons que deux dés soient lancés, quelle est la probabilité d'obtenir des six?
La probabilité que l'un des deux tombe sur un six est évidemment la même quand les deux sont lancés que quand l'un seulement est lancé ---~c'est-à-dire, \(\frac{1}{6}\).
La probabilité que l'un des deux tombe sur un six quand l'autre tombe également sur un six est aussi la même que celle qu'il tombe sur un six, que l'autre tombe ou non sur un six aussi.
Partant, les probabilités sont indépendantes; et, selon notre règle, la probabilité que les deux évènements adviennent conjointement est le produit de leurs probabilités, ou \(\frac{1}{6} \times \frac{1}{6}\).
Quelle est la probabilité d'obtenir un as et un deux (\emph{deuce-ace})?
La probabilité que le premier dé tombe sur un as et le second sur un deux est la même que la probabilité que tous deux tombent sur un six ---~c'est-à-dire, \(\frac{1}{36}\); la probabilité que le \emph{second} tombe sur un as et le \emph{premier} sur un deux est de la même façon \(\frac{1}{36}\); ces deux événements ---~d'abord, un as, ensuite un deux; et ensuite un as, d'abord un deux~--- sont incompatibles.
Aussi la règle d'addition est ici valable, et la probabilité que l'un tombe sur as et l'autre sur deux est \(\frac{1}{36} + \frac{1}{36}\), soit \(\frac{1}{18}\).

De cette manière tous les problèmes de dès, etc., peuvent être résolus.
Quand le nombre de dés lancés est supposé très grand, les mathématiques (qui peuvent être définies comme l'art de distinguer des groupes pour faciliter la numération) viennent à notre aide équipées d'instruments réduisant les difficultés.

\fancybreak{* * *}

La conception de la probabilité comme une question de \emph{faits}, i.e.~comme la proportion de fois où l'occurence d'un type s'accompagne de l'occurence d'un autre, est appelé par M. Venn la vision matérialiste du sujet.
Mais la probabilité a souvent été regardée comme étant seulement le degré de croyance qui doit être attribué à une proposition; et ce mode d'explication d'une idée est appelé par Venn la vision conceptualiste.
La plupart des auteurs ont confondu les deux conceptions.
D'abord, ils définissent la probabilité d'un évènement comme la raison que l'on a de croire qu'il s'est produit, ce qui est conceptualiste; mais tout de suite après ils établissent que c'est le ratio du nombre de cas favorables à l'événement sur le nombre total de cas favorable ou défavorable, tous également possibles.
En dehors du fait que cela introduit l'idée profondément obscure de cas également possibles au lieu de cas également fréquents, c'est une formulation acceptable de la vision matérialiste.
La théorie purement conceptualiste a été la mieux exposée par M. De Morgan dans sa «~Logique Formelle: ou le Calcul de l'Inference, du Nécessaire et du Probable~».

La différence majeure entre ces deux analyses tient en ce que les conceptualistes font référence à la probabilité d'un événement, quand les matérialistes en font le ratio de fréquences d'évènements d'une \emph{espèce} (\emph{species}) sur celui d'un genre (\emph{genus}) de cette \emph{espèce}, partant \emph{lui accordant deux termes plutôt qu'un}.
L'on peut rendre cette opposition manifeste comme suit:

Supposons que l'on dispose de deux règles d'inférence, de sorte que, à toutes questions solubles par elles deux, la première fournisse des réponses correctes à \(\frac{81}{100}\) et incorrectes aux \(\frac{19}{100}\) restants; et que la seconde fournisse des réponses correctes à \(\frac{93}{100}\) et incorrectes aux \(\frac{7}{100}\) restants.
Supposons, de plus, que les deux règles soient totalement indépendantes du point de vue leur vérité, de sorte que la seconde réponde correctement à \(\frac{93}{100}\) des questions auxquelles la première répond correctement, ainsi qu'à \(\frac{93}{100}\) des questions auxquelles la première répond incorrectement, et réponde incorrectement les \(\frac{7}{100}\) restants de questions auxquelles la première règle répond correctement, ainsi qu'à \(\frac{7}{100}\) des réponses auxquelles la première règle répond incorrectement.

Ainsi, de toutes les questions solubles par elles deux ---

\begin{align*}
  \text{Toutes deux répondent correctement: }                           & \frac{93}{100} \text{ de } \frac{81}{100} \text{, ou } \frac{93 \times 81}{100 \times 100} \text{;}\\
  \text{La seconde répond correctement et la première incorrectement: } & \frac{93}{100} \text{ de } \frac{19}{100} \text{, ou } \frac{93 \times 19}{100 \times 100} \text{;}\\
  \text{La seconde répond incorrectement et la première correctement: } & \frac{7}{100} \text{ de } \frac{81}{100} \text{, ou } \frac{7 \times 81}{100 \times 100} \text{;}\\
  \text{Toutes deux répondent [in]correctement: }                         & \frac{7}{100} \text{ de } \frac{19}{100} \text{, ou } \frac{7 \times 19}{100 \times 100} \text{;}\\
\end{align*}

Supposons à présent que, en référence à n’importe quelle question, les deux fournissent la même réponse.
Alors (les questions étant toujours telles qu’on puisse y répondre par \emph{oui} ou par \emph{non}), celles en référence auxquelles leurs réponses concordent sont les mêmes que celles auxquelles toutes deux répondent correctement et celles auxquelles toutes deux répondent faussement, ou \(\frac{93 \times 81}{100 \times 100} + \frac{7 \times 19}{100\times100}\) d’entre elles.
Les proportions de celles auxquelles toutes deux répondent correctement parmi celles dont leurs réponses concordent est, partant ---

\[
  \frac{\frac{93 \times 81}{100 \times 100}}{\frac{93 \times 81}{100 \times 100} + \frac{7 \times 19}{100\times100}}
  \text{, ou, } \frac{93 \times 81}{(93 \times 81) + (7 \times 19)} \text{.}
\]

Ceci est donc la probabilité que, si les deux modes d’inférences fournissent le même résultat, ce résultat soit correct.
L’on peut ici faire un usage commode d’un autre mode d’expression.
La \emph{probabilité} est le ratio des cas favorables parmi tous les cas.
Plutôt que d’exprimer nos résultats en termes de ce ratio, l’on peut en employer un autre ---~le ratio des cas favorables par rapport aux cas défavorables.
Ce dernier ratio peut être appelé la \emph{chance} d’un événement.
Ainsi la chance d’une réponse vraie selon le premier mode d’inférence est \(\frac{81}{19}\) et \(\frac{93}{7}\) par le second; et la chance d’une réponse correcte des deux, lorsqu’ils concordent, est~---

\[\frac{81\times93}{19\times7} \text{ ou } \frac{81}{19} \times \frac{93}{7} \text{ , }\]

ou le produit des chances que chacune d’elles fournissent une réponse vraie.

\end{document}

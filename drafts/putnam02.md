---
title: Le projet de l'Intelligence Artificielle
author: Hilary Putnam
translator: Samuel Barreto
date: 2019-02-15
draft: true
---

Traditionnellement, les Leçons de Gifford traitent de questions liées
à la religion. Ces dernières années, bien que le rapport à la religion
n'ait jamais été totalement absent, elles ont parfois été données par
des scientifiques ou des philosophes des sciences, qui se sont penchés
sur les dernières avancées en cosmologie, en physique des particules
élémentaires, etc … Il ne fait pas de doute que ce changement reflète
un changement culturel, en particulier un changement de la culture
philosophique. Mais ces faits quant aux Leçons de Gifford --- leur
intérêt historique pour la religion et plus récent pour la science ---
me parlent tous deux. En tant que juif pratiquant, la dimension
religieuse de la vie m'est devenue de plus en plus importante, bien
que ce ne soit une dimension dont je ne sais philosopher autrement que
par tâtonnement; et l'étude de la science a pris une grande ampleur
dans ma vie. En fait, lorsque j'ai commencé à enseigner la
philosophie, dans les années 1950, je me voyais comme un philosophe
des sciences (quoique j'incluais dans ma généreuse interprétation de
la phrase « philosophie des sciences » la philosophie du langage et la
philosophie de l'esprit). Les familiers de mes écrits de cette période
doivent se demander de quelle façon j'ai concilié mon élan religieux,
qui d'une certaine façon a toujours existé, même alors, et ma vision
scientifique du monde globalement matérialiste de l'époque. La réponse
est que je ne l'ai pas fait. J'étais athée pur et dur, et j'étais
croyant. J'ai simplement gardé ces deux parties de moi distinctes.

Pour l'essentiel cependant, c'était le matérialiste scientifique qui
dominait en moi dans les années cinquante et soixante. Je croyais que
tout ce qui est peut être expliqué et décrit par une théorie unique.
Bien sûr, nous ne saurons jamais le détail de cette théorie, et
resterons même dans d'une certaine façon l'erreur quant à ses
principes généraux. Mais je croyais voir dans l'état présent de la
science les grandes lignes de ce à quoi une telle théorie devait
ressembler. En particulier, je croyais que la meilleure métaphysique
est physique, ou plus précisément, que la meilleure métaphysique était
ce que les positivistes appelaient une « science unifiée », une
science décrite comme basée sur l'application des lois de la physique
fondamentale et unifiée par elles. De nos jours, Bernard Williams
prétend que nous avons à tout le moins une esquisse d'une « conception
absolue du monde » dans la physique actuelle. De nombreux philosophes
analytiques souscrivent aujourd'hui à ce genre de vision; pour eux la
tâche de la philosophie en revient en gros à commenter et spéculer des
résultats des progrès de la science, particulièrement lorsqu'ils
pèsent ou semblent peser sur les problèmes traditionnels de la
philosophie.

Quand j'étais jeune, une conception bien différente de la philosophie
était représentée par les travaux de John Dewey. Dewey soutenait que
l'idée d'une théorie unique capable de tout expliquer était un
désastre de l'histoire de la philosophie. La science elle-même, comme
Dewey y insiste, n'a jamais consisté en une théorie unifiée unique,
pas plus que les diverses théories coexistantes à un moment donné
n'ont jamais été totalement cohérentes. Bien que nous ne devrions
jamais cesser de tenter de rendre nos théories cohérentes --- Dewey ne
voyait pas l'incohérence comme une _vertu_ ---, en philosophie nous
devrions abandonner l'illusion d'une seule conception absolue du
monde, comme le pensait Dewey. Au lieu de chercher une théorie
finale --- qu'elle s'appelle « conception absolue du monde » ou
non --- qui expliquerait tout, nous devrions voir la philosophie comme
une réflexion sur la façon dont les êtres humains peuvent résoudre la
variété de « situations problématiques » qu'ils rencontrent, que ce
soit en science, en éthique, en politique, en éducation, où que ce
soit. Ma propre évolution philosophique est parti d'une conception
comme celle de Bernard Williams à une conception beaucoup plus proche
de celle de John Dewey. Dans ce livre, je tente d'expliquer et, dans
la mesure du possible dans l'espace qui m'est dévolu, de justifier ce
changement d'attitude philosophique.

Dans les trois premiers chapitres, je commence par un tour d'horizon
de certaines des façons dont les philosophes ont suggéré que la
science cognitive moderne explique le lien entre le langage et le
monde. Celui-ci traite de l'Intelligence Artificielle. Le chapitre
deux traite de l'idée que la théorie de l'évolution est là clé aux
mystères de l'intentionnalité (c'est-à-dire, de la vérité et de la
référence), et le chapitre trois discute des prétentions du philosophe
Jerry Fodor que l'on peut définir la référence en termes de notions
causales/contrefactuelles. En particulier, je voudrais suggérer que la
psychologie cognitive ne se réduit pas à la science du cerveau
similaire à une science informatique, contrairement à ce que tant de
monde attend d'elle (y compris la plupart des pratiquants de la
« science cognitive »).

Je viens de mentionner cette conception particulière de ce qu'est la
vision scientifique du monde, que la science se réduit _in fine_ à la
physique, ou du moins qu'elle est unifiée dans la vision physique du
monde. L'idée de l'esprit en tant que « machine calculatrice » remonte
à la naissance de cette « vision scientifique du monde » aux
dix-septième et dix-huitième siècle. Par exemple, Hobbes suggèrait
qu'il est approprié de voir en la pensée un « calcul », parce qu'elle
n'est vraiment qu'une manipulation de signes suivant des règles
(analogues à des règles de calcul), et La Mettrie scandalisait son
temps par l'idée que l'Homme n'était qu'une machine (_L'Homme
Machine_). Ces idées, sans surprise, furent associées au matérialisme.
Et à tous ceux qui de près ou de loin ont affaire à l'Intelligence
Artificielle, il revient toujours cette question du « pensez-vous
qu'une machine de calcul puisse être intelligente, consciente, et
ainsi de suite, de la même manière que les humains? » Parfois elle est
entendue comme un « qu'elle le puisse en principe », parfois comme un
« qu'elle le puisse, en pratique » (à mon sens, la question de loin la
plus intéressante.)

L'histoire de l'ordinateur, et du rôle d'Alan Turing dans la
conception de l'ordinateur moderne a maintes fois été contée. Dans les
années trente, Turing formula la notion de calculabilité en des termes
directement liés aux ordinateurs (qui restaient alors à inventer). EN
fait, l'ordinateur digital moderne est une réalisation de l'idée d'une
« machine de Turing universelle ». Une vingtaine d'année plus tard,
des matérialistes tels que celui que j'étais alors en sont venu
à penser que l'« esprit est une machine de Turing ». Il est
intéressant de comprendre pourquoi cela me semblait si évident (et
semble encore si évident à de nombreux philosophes de l'esprit).

Si le corps humain entier est un système physique obéissant aux lois
de la physique Newtonienne, et si tout système de ce genre, y compris
l'univers physique entier et jusqu'à lui, est au moins
métaphoriquement une machine, alors le corps humain entier est au
moins métaphoriquement une machine. Et les matérialistes croient qu'un
être humain n'_est_ qu'un corps humain vivant. Donc, dès lors qu'ils
supposent que la mécanique quantique ne s'applique pas à la
philosophie de l'esprit (tout comme je le supposais lorsque je fis
cette suggestion), les matérialistes sont engagés envers l'idée qu'un
être humain est --- au moins métaphoriquement --- une machine. On peut
comprendre que l'idée d'une machine de Turing puisse être vue comme
une façon de préciser l'idée matérialiste. C'est compréhensible, mais
insuffisamment réfléchi.

Le problème est celui-ci: au sens d'un système physique obéissant aux
lois de la physique newtonienne, qu'une « machine » le soit n'implique
pas qu'elle soit une machine de Turing. (Pour la défense de mes
conceptions antérieures, il faut ajouter que cela n'était pas su au
début des années soixante lorsque j'ai formulé ma description
soi-disante fonctionaliste de l'esprit.) Car une machine de Turing ne
peut calculer une fonction que si celle-ci fait partie d'une classe
précise de fonctions, les dénommées fonctions générales récursives.
Mais il a été prouvé qu'il existe des systèmes physiques potentiels
dont l'évolution temporelle n'est pas descriptible par une fonction
récursive, même lorsque les conditions initiales du systèmes le sont.
(Les équations d'ondes de la physique classique ont été démontrées
comme fournissant des exemples de ce type.) En termes moins
techniques, cela veut dire qu'il existe des appareils analogues
physiquement possibles pouvant « calculer » des fonctions
non-récursives. Que de tels appareils ne puissent être réellement
préparés par un physicien (et Georg Kreisel a souligné qu'aucun
théorème n'a été montré comme _empêchant_ la préparation d'un tel
appareil), il ne s'ensuit pas qu'ils n'ont pas cours dans la nature.
De plus, il n'y a aucune raison que les nombres réels décrivant les
conditions d'un système physique naturel à un temps donné soient
« récursifs ». Ainsi, pour plus d'une raison, un système physique
naturel pourrait bien présenter une trajectoire « calculant » une
fonction non-récursive.

Vous pourriez vous demander alors, pourquoi j'ai supposé qu'un être
humain puisse, du moins dans un processus d'idéalisation raisonnable,
être vu comme une machine de Turing. Une raison était que le
raisonnement suivant m'est apparu. Un être humain ne peut vivre
éternellement. Un être humain est fini dans le temps et l'espace. Et
les mots et actions --- les « sorties », en jargon informatique ---
d'un être humain, dans la mesure où ils sont perceptibles par les
seuls sens d'autres être humains (et nous pourrions plausiblement
supposer que c'est là le degré de précision recherché par la
psychologie cognitive), peuvent être décris par des paramètres
physiques, spécifiés à un degré de précision seulement macroscopique.
Mais cela implique que les « sorties » puissent être prédites au cours
du temps fini de la vie humaine par une approximation convenable de la
trajectoire continue réelle, et une telle « approximation convenable »
peut être une fonction récursive. (Toute fonction peut être approximée
à un degré de précision donné par une fonction récursive sur un
interval de temps fini.) Puisque nous pourrions supposer que les
valeurs limites possibles des paramètres sont aussi restreintes à un
interval fini, un ensemble fini de telles fonctions récursives rendra
compte du comportement d'un être humain sous toutes les conditions
dans l'interval spécifié à la précision voulue. (Puisque les lois du
mouvement sont continues, les conditions limites ne doivent être
connues que dans un Δ approprié pour prédire la trajectoire du système
à la précision spécifiée.) Mais si c'était le cas, les
« sorties » --- ce que l'humain dit ou fait --- pourraient être
prédites par une machine de Turing (en fait la machine de Turing n'a
qu'à calculer les valeurs que prend toute fonction récursive
correspondant aux valeurs des conditions limites de l'ensemble fini),
et une telle machine de Turing pourrait, en principe, simuler le
comportement en question, tout comme elle pourrait le prédire.

---
title: Retour sur conclave
author: Samuel Barreto
---

Il serait inutile de chercher à décortiquer chaque cas individuellement,
d’interroger chaque personne et de chercher à comprendre dans quelle
mesure ce dont j’ai parlé au conclave est vrai pour elle. C’est à mon
sens une erreur de croire que les questions sociales peuvent trouver
leurs fondements dans l’individu, ou dans la somme d’individus
sérialisés comme aurait dit Sartre. Croire que les questions sociales
se réduisent à des sommes de questions personnelles, « ne sont en
dernière analyse que » du ressort de facteurs individuels, c’est ne pas
voir que les « individus » ne se construisent que socialement, et que
leur état d’esprit individuel ne leur est qu’assez peu
propre.[@HouserSocialMindsFixation2016] C’est-à-dire, là où l’on est
souvent enclin à s’attribuer une conception personnelle, (« _je_ crois
que … »), bien souvent cette conception est socialement construite et ne
nous est propre que parce que l’on « baigne » en quelque sorte dans le
jeu de langage qui rend cette conception intelligible. C’est le propre
d’un champ social.[@BourdieuSociologieGeneraleCours2015]

En d’autres termes, chercher à, sinon résoudre, du moins comprendre un
problème social par essence diffus par le « traitement » individuel de
chaque cas, c’est passer à côté du fait qu’il correspond à ce à quoi en
écologie il est souvent fait référence sous le nom de propriété
émergente (quoi que ce terme ait de métaphysiquement flou ou dévoyé).
Encore une fois, ce serait comme tenter d’arrêter la pluie en cherchant
à se protéger de chaque goutte d’eau (réductionnisme). Il paraît plus
pragmatique de résoudre le problème de la pluie en le traitant comme un
phénomène diffus. C’est à mon sens l’erreur (ou l’impasse) de la
psychologie (positive) de ne finalement vouloir « traiter » que les cas
individuels séparément, dans l’espoir que le problème collectif s’en
résoudra d’autant — si louable que soit l’effort par ailleurs. C’est
aussi l’erreur qui consiste à ne voir dans les quatre cas d’abandon de
thèse que le résultat de processus externes au laboratoire, indépendants
entre eux, et sur lesquels il n’y a d’autres prises qu’individuelles.
C’est-à-dire, s’il ou elle a abandonné, c’est parce qu’il ou elle
avait un terrain psychologique (dé)favorable, ou c’est parce que son
encadrante ou son encadrant le ou la traitait comme de la chaire à
pipette. N’importe quoi.

Aussi bien qu’il est malvenu de vouloir comprendre ces cas uniquement
individuellement, il est malvenu de croire que des causes externes sont
seules responsables de cet état de choses: « Voici les faits, ils ne
dépendent pas de nous, nous n’y pouvons donc rien » (positivisme).
Exemples: 1) Si elle abandonne, c’est parce qu’elle a changé de voie. 2)
S’il abandonne, c’est parce qu’il n’était pas fait pour la recherche. 3)
Si elle abandonne, c’était pour des raisons personnelles.

Peut-être alors qu’on peut aller un peu plus loin que de s’arrêter aux
faits et tenter de comprendre ce qui fait qu’ils sont? « Le fait traduit
non pas ce qu’on fait, mais ce qu’on ne fait pas » (Canguilhem). C’est à
mon sens l’attitude critique attendue d’une savante ou d’un savant, un
sain doute qui cherche à comprendre le fond des choses: pourquoi les
choses sont ce qu’elles sont? Exemples: 1) Si elle abandonne, c’est
peut-être parce que certaines choses l’ont conduite à changer de voie.
2) S’il abandonne, c’est peut-être parce que la recherche telle qu’elle
est désormais n’était pas faite pour lui. 3) Si elle abandonne, c’est
peut-être parce que des raisons personnelles l’ont conduite à voir dans
le laboratoire un environnement peu propice à son rétablissement.

J’espère qu’il est clair dans quelle mesure je me considère, et partant
je nous considère, pour partie responsables dans l’abandon de toutes ces
personnes, dans le dégoût de bien d’autres qui dès après leur soutenance
se sont enfuies loin de la science, et dans tout ce qui fait que la
science telle qu’elle se pratique actuellement ne constitue pas
l’environnement idéal à l’accomplissement de ses principes fondateurs.
Car enfin je suis d’avis que la science devrait être un beau métier, que
l’enseignement de la science devrait être un beau métier, un métier
auquel il est justifié de se consacrer, et justifié par autre chose que
les seules courses aux publications, aux facteurs d’impact, encore moins
aux brevets, aux « évaluations », à la « visibilité », au
« positionnement », à l’« affichage » et à la « stratégie
scientifique ». D’accord, ces éléments là ne dépendent pas uniquement
d’éléments « internes » au laboratoire, mais peut-être que c’est
justement parce qu’ils ne dépendent pas de nous qu’ils sont facteurs
d’angoisse; et que de se saisir de ces questions collectivement
contribuerait à faire en sorte que l’on soit conscient et du fait qu’ils
font problème et des solutions qu’on peut leur apporter.

Si j’insiste autant sur l’aspect collectif, c’est parce que je ne vois
pas très bien comment il serait possible à une personne seule d’être
en mesure de faire quoi que ce soit dans le sens d’un progrès, ni même
s’il est souhaitable qu’elle le soit. C’est aussi parce que je ne suis
pas certain que le courage éthique que ces démarches impliquent,
à travers les prises de risques institutionnelles, les engagements
symboliques ou concrets qu’elles demandent, puisse émerger d’attitudes
individuelles. En dehors de celles et ceux qui n’ont rien à perdre
(parce que proches de la retraite, d’un autre laboratoire, parce que
prix Nobel[^1]), les autres sont pris et prises par l’_illusio_, cette
idée (encore une fois socialement construite, mais pas moins réelle)
qui veut que le jeu en vaille la chandelle.

#

[^1]: Voir cet article où Randy Schekman déclare ne plus vouloir publier ni dans Nature, ni dans Cell ni dans Science. How convenient quand on est éditeur en chef d’eLife, avec une liste de publications dans lesdites revues longue comme le bras, et quand sa réputation n’est tellement plus à asseoir que l’on en reçoit un prix Nobel … <https://www.theguardian.com/science/2013/dec/09/nobel-winner-boycott-science-journals>

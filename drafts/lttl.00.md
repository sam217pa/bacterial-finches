# Preface

Ce livre traite de l'importance de la vérité dans nos vies personnelles et politiques.
Ces cent dernières années ont vu couler beaucoup d'encre sur la vérité par les philosophes, mais qui pour l'essentiel  s'inquiétait de questions formelles de définitions et de paradoxes.
Pour important qu'il soit, ce travail peut laisser l'homme de la rue perplexe; il ne traite que rarement des questions qui font que l'on se préoccupe de vérité en premier lieu.
L'un des buts de ce livre est de corriger cet état de fait.

Un autre but est d'être clair.
J'aime que ma philosophie soit évidente et qu'elle ne s'encombre pas de de technicités académiques.
Même les problèmes philosophiques les plus profonds peuvent être appréciés par tout un chacun prêt à retrousser ses manches intellectuelles et à se creuser les méninges.
C'est ce que j'ai tenté de faire dans ce livre: m'affronter aux problèmes de façon à ce que le lecteur me suive.
En conséquence, j'ai essayé de réduire les aspects techniques à leur minimum absolu.

Je me suis penché sur ces questions pendant plusieurs années, et suis redevable à de nombreuses personnes pour leur aide, bien qu'aucune d'elles ne doive être rendue responsable du résultat.
Le travail sur ce livre a été rendu possible grâce à un congé sabattique généreusement supporté par le Connecticut College au cours de l'année académique 2002-2003 et un Bogliasco Fellowship au printemps 2003 au Liguria Study Center.
Des versions de diverses parties du livre ont bénéficié de commentaires qui m'ont été fais suite à des présentations à l'Université de St. Andrews, au Moral Sciences Club à Cambridge, à l'Université de Galles à Cardiff, à l'Université de Gênes, à l'Université de Turin, à l'Université de l'État de Floride, à l'Université de Californie Fullerton, à l'Université de Cincinnati, à l'Université du Mississipi et à la Tufts University. 
À l'automne 2002, j'eu la chance d'être un Membre Visiteur du centre Arché à l'Université de St. Andrews, où j'ai pu bénéficié de conversations avec de nombreuses personnes, en particulier Crispin Wright, ainsi que J.C. Beall, John Haldane, et Patrick Greenough.
De plus, nombreux sont ceux qui ont souffert d'anciennes versions de diverses parties du manuscrit, et m'ont aidé à éviter bien des problèmes.
Merci à Robert Barnard, Eddy Nahmias, Bridget Lynch, Patty Lynch, Tom Bontly, Derek Turner, Chase Wrenn, et Chris Gauker. 
Heather Battaly, Tom Polger et Paul Bloomfield, vieux amis et frères d'armes, méritent une mention spéciale pour leurs nombreux et secourables commentaires de diverses versions du manuscrit.
William Alston, professeur et ami, dont les travaux ont fortement influencé les (bonnes) idées de ce livre, m'a également fourni de précieux commentaires du manuscrit à une étape plus tardive.
Plus important, je remercie Terry Berthelot, dont l'insistence tenace sur la clarté, et le criticisme pénétrant n'ont d'égal que le soutien sans faille, et l'incroyable tolérance à l'égard des excentricités de son mari.
L'influence de Terry sur ce livre est profonde.
Je n'aurais littéralement pas pu l'écrire sans elle.

Des portions du chapitre 7 sont basées sur des éléments de « Minimalism and the Value of Truth », _Philosophical Quarterly 55_ C 2005, les éditeurs du _Philosophical Quartely_.

Merci aussi à mes éditeurs July Feldmann, et à mon éditeur Tom Stone, qui savent ce qui compte.

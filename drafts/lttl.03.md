# Peut-on atteindre à la vérité?

## Cauchemars

L'ancien philosophe Chinois Chuang Tzu revât une fois qu'il était un papillon.
Se réveillant soudainement, il se demanda: était-il un papillon rêvant qu'il était un homme ou un homme rêvant qu'il était un papillon?
^[Chuang Tzu, de Chan, Wing-tsit, éd. et trad., _A Source Book in Chinese Philosophy_ (Princeton, N.J.: Princeton University Press, 1963), p. 190.]

L'inquiétude de Chuang Tzu soulève une question troublante.
Et si tout ce que vous aviez vécu jusqu'alors n'était qu'une illusion?
Et si la vie n'était vraiment qu'un rêve?
L'idée que l'on pourrait avoir tort au sujet de, et bien, tout, se retrouve de partout --- pas seulement dans les différentes religions et traditions philosophiques du monde, mais dans la littérature, les poèmes, et, comme je l'ai souligné plus haut, dans de nombreux films Hollywoodiens.
Dans la traduction intellectuelle occidentale, son expression la plus fameuse a été formulée par le mathématicien et philosophe du seizième siècle René Descartes.
Descartes s'est aussi demandé s'il pouvait différencier le rêve de la réalité.
Il opta pour le scénario « se pourrait-il que je rêve? ».
Pour ce que j'en sais, dis Descartes, une intelligence toute-puissante et malveillante me leurre de sorte que je crois qu'il n'y a pas de monde physique du tout.
^[Le démon de Descartes: voir la Première Méditation dans _Meditations on First Philosophy_; traduction J. Cottingham (Cambride: Cambridge University Press, 1986).]
Voilà un cauchemar plus terrible.
Après tout, pour réalistes que soient parfois les rêves, ils ont une tendance caractéristique à manquer de cohérence.
Mais un malin génie tout-puissant pourrait vous faire ressentir ce livre entre vos mains, la lumière de la pièce, votre souffle, la sensation du dossier de votre chaise, le paysage à travers la fenêtre, et ainsi de suite, aussi cohérent que cela vous chante, et vous n'en seriez pas moins le plus sage.

Point n'est besoin du démon de Descartes pour donner lieu à cette pensée paranoïaque.
La vie moderne dans la plupart des pays occidentaux est pleine de technologies dont le seul but est de confondre des apparences en réalités.
Pensez aux machines de réalité virtuelle, aux jeux vidéos, aux hologrammes, aux cinémas I-Max, et ainsi de suite.
La préoccupation de notre culture consistant à « s'échapper de la réalité » nous rend bien prompts à partager l'inquiétude de Descartes, et d'en proposer diverses variations, telles que la machine à sensation dont nous avons parlé à la fin du précédent chapitre.
Aussi, bien que la trame de fond varie, l'inquiétude de base reste identique à celle de Chuang Tzu ou Descartes: comment peut-on être _sûr_ que ce que l'on croit vrai le soit vraiment?

L'une des racines de cette étrange inquiétude sceptique est la simple platitude selon laquelle la vérité est objective.
Comme je l'ai souligné plus haut, accepter l'objectivité de la vérité c'est simplement accepter l'évidence que l'on peut se tromper.
Pourtant, cette acception innocente peut conduire à un doute radical: _si l'on peut se tromper, comment savoir que l'on ne se trompe pas toujours?_
Et si l'on ne peut jamais être sûr, alors à quoi bon chercher à atteindre des croyances vraies en premier lieu?
Voilà qui résume une raison très courante pour que les gens deviennent cyniques au sujet de la vérité et de sa poursuite: il est si difficile de savoir ce qui est réellement vrai qu'il semble vain de tenter de le faire.

Il peut sembler qu'une partie du problème vient de ce que nos truismes au sujet de la vérité soient en conflit mutuellement.
Plus la vérité est objective, moins elle semble un but qu'il faut viser.
Comme le dit le philosophe et critique Richard Rorty, la vérité objective n'est pas un but, parce que « l'on ne peut viser une chose, travailler à l'atteindre, sans être en mesure de la reconnaître lorsqu'on l'a ».
^[« Universality and Truth », in _Rorty and His Critics_, éd. R. Brandom (Cambridge: Blackwell, 2001), p. 2.]
Le philosophe Donald Davidson ajoute, « Les Vérités ne portent pas “d'insigne”, comme la date au coin de certaines photographies, qui les distingueraient de faussetés.
Le mieux que l'on puisse faire, c'est tester, expérimenter, comparer, et garder l'esprit ouvert. …
Puisqu'elle n'est ni visible en tant que cible, ni reconnaissable lorsqu'on l'atteint, il est vain d'appeler la vérité un but. »
^[« Truth Rehabilitated », in _Rorty and His Critics_, p. 67.]

Le fait qu'il soit difficile de reconnaître le vrai signifie-t-il qu'il ne faut pas chercher à l'atteindre?
La réponse courte est: bien sûr que non.
Le fait que la vérité soit difficile à déterminer n'implique pas qu'elle n'est pas un but; cela implique seulement que la quête de vérité n'est pas aisée.
Mais le raisonnement derrière ce genre d'inquiétude mérite qu'on s'y intéresse, car il se fonde sur des mythes et erreurs que notre culture intellectuelle doit laisser derrière elle.

## Une carte du monde

En 1507, le cartographe allemand Martin Waldseemüller plaça l'Amérique sur une carte --- littéralement.
^[La carte de Waldseemüller: la Library of Congress à Washington, D.C, possède une copie de _Cosmographie Introductio_.
Pour en savoir plus sur Waldseemüller et Vespucci, voir _Letters from a New World: Amerigo Vespucci's Discovery of America_, éd. L. Formisano, traduction par D. Jacobson (New York: Marsilio, 1992).]
Les cartes du monde étaient rares à cette époque, mais la _Cosmographie Introductio_ de Waldseemüller était spéciale nonobstant, car elle était la première à attester de ce que le Nouveau Monde était précisément --- un nouveau monde et non l'Asie, et qu'elle se composait de deux grandes régions terrestres.
Waldseemüller nomma « l'île » la plus au sud d'après l'explorateur Amerigo Vespucci.

La carte de Waldseemüller est sans aucun doute un succès majeur.
Mais elle est aussi très imprécise.
Deux erreurs sont particulièrement frappantes: d'abord, les deux régions terrestres du « Nouveau Monde » sont _extrêmement_ restreintes, bien loin de leur taille réelle.
Ensuite, la carte évoque à tort un passage clair jusqu'à l'Asie au Nord de la masse terrestre du nord --- une vaste mer ouverte séparant l'Amérique du Nord du Pôle Nord.
Ces erreurs sont en partie le reflet des limites de l'exploration d'alors.
Mais elles sont aussi le reflet d'une chose bien différente --- la tendance de l'esprit humain à teinter la perception de préconception.
Au temps de la conception de la carte, la pensée Européenne était empreinte de l'idée qu'il devait y avoir une route plus ou moins directe par l'océan de l'Europe à l'Asie.
La découverte de masses terrestres sur le chemin n'a pas éloigné les géographes du seizième siècle --- ou les explorateurs --- de cette vision, pour la simple raison qu'en suivant d'anciens calculs Grecs, ils avaient sous-estimé la taille de la Terre.
Aussi, plutôt que de montrer des espaces blancs sur la cartes représentant le manque d'information, Waldseemüller a simplement dessiné les masses terrestres telles qu'il --- ainsi que l'essentiel de ses collègues instruits --- escomptait qu'elles soient.

Lorsque l'on dit qu'une croyance est objectivement vraie ou fausse, on les voit comme similaires à des cartes, à certains égards.
Comme les cartes, les croyances représentent le monde d'une certaine façon, et comme les cartes, elles sont précises, ou vraies, lorsqu'elles représentent le monde tel qu'il est.
Mais, comme le montre l'exemple de la carte de Waldseemüller, nos croyances, comme nos cartes, ne sont pas distinctes du reste de nos préjugés et suppositions.

Un phénomène bien étudié en psychologie de la perception est que nos croyances d'arrière plan affectent notre expérience visuelle quotidienne du monde physique.
La majorité d'entre nous ont fait l'expérience d'un "changement de Gestalt" (_gestalt switch_) en regardant la fameuse image ambivalente du « canard/lapin » de Wittgenstein ou le dessin de la jeune/vieille femme (voir figure 2.1).
Étant données nos croyances d'arrière plan et nos dispositions, l'on voit initialement la figure sur la page _comme_ un canard ou _comme_ un lapin, et l'on peut éprouver de grandes difficultés à « s'empêcher » de continuer à les voir de telle façon, quand bien même l'autre perspective nous a été soulignée par ailleurs.
Et bien sûr, ce qui est vrai de pareils cas « triviaux » est bien plus évident dans d'autres cas impliquant des concepts plus complexes.
Lorsqu'elles croisent un jeune homme noir dans la rue, par exemple, certaines personnes blanches le « voit » simplement comme dangereux, en dépit du fait qu'elles n'ont aucune raison de croire que cette personne particulière est dangereuse.
Les gens du Nord perçoivent parfois l'accent du Sud comme traduisant l'ignorance, les hommes perçoivent fréquemment les femmes comme des objets sexuels, et ainsi de suite.
En bref, il semble que nos sensations et croyances au sujet du monde sont comme la carte de Waldseemüller: elles reflètent nos propres préconceptions et croyances autant qu'elles reflètent le monde tel qu'il est réellement.

Emmanuel Kant fut peut-être le premier philosophe à avancer que l’expérience normale, pleinement consciente ne nous est pas simplement « donnée » par le monde, mais est en partie informée par notre esprit.
^[Kant, _Critique de la Raison Pure_.]
Nous supposons que notre esprit perçoit plus ou moins le monde tel qu’il est.
Pourtant, dans les faits, nos habitudes de pensée, les concepts que l’on emploie, colorent notre perception du monde.
Tout se passe comme si nos croyances étaient un voile ou une tenture à travers lequel on ne voit, dans le meilleur des cas, qu’assez vaguement.
Pourtant, contrairement à Dorothée dans _le Magicien d’Oz_, l’on ne peut simplement écarter ce voile et voir la réalité « en elle-même ».
Le mieux que l’on puisse faire est d’en faire l’expérience depuis notre propre perspective humaine, croyait Kant.
Car notre expérience du monde n’est jamais pure et directe.
Elle est modifiée par les structures et les habitudes de nos esprits propres.

Une façon spécifique de comprendre l’argument de Rorty et Davidson contre le truisme selon lequel la vérité est l’un des buts de l’enquête s’inspire de celui de Kant et l’applique aux croyances.
Supposons, par exemple, que je forme la croyance qu’il y a un ordinateur en face de moi.
Il est tout à fait clair que je ne peux dire si cette croyance est vraie en sortant de mon corps et en vérifiant s’il y a réellement un ordinateur là-dehors.
Toute tentative pour vérifier et voir s’il est bien là, selon notre réflection ci-dessus, implique quelques croyances d’arrière-plan et de préconceptions de ma part.
Aussi, que je le veuille ou non, le mieux que je puisse faire, semble-t-il, est de me reposer sur mes autres croyances --- mes croyances quant à la lumière dans la pièce, le bureau, l’état de ma vue, et ainsi de suite qui confirment ma croyance originelle à propos de l’ordinateur.
Partant, puisque je ne peux jamais me confronter aux faits réels bruts (_naked facts_) mais seulement à mes autres croyances, je ne peux dire si l’une quelconque de mes croyances particulières est réellement vraie ou si elle paraît seulement l’être.

En somme, l’argument de Rorty et Davidson semble celui-ci.
Si la vérité objective est réellement un but de l’enquête, alors nous devrions être en mesure de reconnaître lorsqu’on l’atteint, c’est-à-dire, de reconnaître lorsque nos croyances sont vraies.
Mais la seule manière dont on pourrait être en mesure de le faire serait de comparer nos croyances aux faits sans fard.
Or on ne le peut.
Donc la vérité ne peut être un but.

Le présupposé central au cœur de cet argument est que l’on ne peut reconnaître si une croyance est vraie ou fausse à moins de la comparer aux faits bruts.
Mais pourquoi au juste devrions-nous penser cela?
L’une des raisons pourrait être que l’on accepte une vision désuète de la vérité et de la représentation que l’on pourrait appeler la _vision de l’esprit miroir_.
Selon cette théorie, notre esprit est une sorte de miroir --- nos croyances et idées sont vraies seulement lorsqu’elles « reflètent » ou « ressemblent » à certains aspects du monde.
C’est une vieille idée, remontant au seizième siècle, et adoptée par divers philosophe, notamment John Locke.
Shakespeare l’expose le mieux:

> | Mais l’homme, l’homme vaniteux !
> | drapé dans sa petite et brève autorité,
> | connaissant le moins ce dont il est le plus assuré
> | sa fragile essence, il s’évertue, comme un singe en colère,
> | à faire à la face du ciel des farces grotesques
> | qui font pleurer les anges [...].^[« Man, proud man - Drest in a little brief authority - Most ignorant of what he’s most assured - His glassy essence, like an angry ape - Plays such fantastic tricks before high heaven - As makes the angels weep. ». William Shakespeare, Mesure pour Mesure, Acte II, Scène 2, Oeuvres Complètes, tome 10, traduction François-Victor Hugo, 1872.]

Les miroirs distordent bien souvent ce qu’ils reflètent.
Aussi, si nos pensées sont comme de petites images miroir, elles peuvent elles aussi nous jouer des « farces grotesques », et refléter ce qui n’est pas le cas.
Avec de vraies images en miroir, ou des images comme les photographies, l’on vérifie si elles sont fidèles en les comparant à la chose réelle.
Mais s’il en va ainsi de nos pensées, l’on pourrait croire que l’on ne peut savoir ce qui est vrai qu’en parcourant d’une façon ou d’une autre le miroir de nos pensées et en ayant une sorte d’« immaculée perception » --- c’est-à-dire, en vérifiant de quelque façon directement que nos croyances ressemblent au monde extérieur, sans reposer sur l’une quelconque de nos autres croyances.
Et bien sûr on ne le peut; contrairement à Alice, l’on ne peut traverser le miroir.

Si cette vision de l’esprit réfléchissante est matière à poésie, elle l’est moins pour une théorie de la vérité.
^[Qui adhérait à cette vision de l’esprit réfléchissant? George Berkeley semblait croire que John Locke y adhérait: voir Berkeley, _Principes de la Connaissance Humaine_ (traduction Charles Renouvier, Armand Colin, 1920) et _Dialogues entre Hylas et Philonoüs_, (traduction Jean-Paul de Gua de Malves, 1750).
Mais bien que Locke semble quelquefois soutenir une vision de la vérité miroir ou « copie », il est mieux compris, à mon sens, comme soutenant une forme précoce de réalisme causal. Voir le chapitre 6 ci-dessous.]

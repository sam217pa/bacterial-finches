---
title: "Introduction et Aperçu"
author: "Peter Godfrey-Smith"
date: 2019-03-26
translator: "Samuel Barreto"
draft: true
---

# Science, Philosophie des Sciences, Philosophie de la Nature

> [_Organisation de la biologie évolutionnaire; discussions fondatrices de la sélection naturelle; science, philosophie des sciences, philosophie de la nature._]

Ce livre traite de l’évolution par sélection naturelle --- du
processus lui-même, et de nos tentatives pour le comprendre à travers
la théorie scientifique. Ces deux sujets s’éclairent l’un l’autre.

La biologie évolutionnaire dans son ensemble peut être vue comme
structurée autour de deux ensembles d’idées centrales. L’un peut se
résumer par « l’arbre de la vie ». C’est l’hypothèse selon laquelle
tous les organismes sur Terre sont liés les uns aux autres par une
ascendance commune; si l’on regarde de loin la cartographie de cet
ensemble de relation d’ascendances et de descendances, les relations
généalogiques entre espèces prennent grossièrement l’aspect d’un
arbre. La seconde est notre description de la façon dont le changement
advient au sein d’une population ou espèce. C’est là que l’on trouve,
entre autres choses, l’évolution par sélection naturelle. Les
variations émergent au sein des populations, d’une façon aléatoire et
non-dirigée. Certaines des caractéristiques de ces variants conduisent
l’individu qui les porte à produire plus de descendants que les
autres. Lorsque ces caractéristiques avantageuses sont transmises
à travers les générations, la population change.

L’histoire des tentatives de description des attributs essentiels de
ce processus est désormais une longue histoire. La description de
Darwin dans _l’Origine des Espèces_ (1859) était pour l’essentiel
relativement concrète. C’est-à-dire, elles avaient pour but de
capturer la manière dont le processus opère dans des organismes et
environnements bien réels. Assez rapidement néanmoins, advint une
transition vers un traitement plus abstrait de ses idées centrales,
dont la tradition perdure. Il semblait à beaucoup que Darwin avait
identifié un patron global, une sorte de machine schématique, que l’on
devait pouvoir trouver dans de nombreux domaines et ne dépendait pas
d’aspects contingents des systèmes biologiques présents ici sur Terre.
Cette tradition comprend ce que j’appelerai l’approche « classique »
de la description abstraite du Darwinisme, pour qui la variation,
l’hérédité, les différences d’issues reproductives sont vues comme
comprenant une sorte de recette du changement d’un vaste ensemble de
système. Plus récemment, cette tradition inclut des descriptions de
l’évolution en termes de « réplicateurs », des structures fidèlement
copiées perdurant sur de longues périodes et construisant le reste du
monde biologique autour d’eux.

Les raisons derrière cette tentative de proposer une description
abstraite de ce type sont globalement de deux types. L’un est la quête
d’une pure compréhension du processus évolutionnaire lui-même. L’autre
est la quête d’un outil théorique susceptible d’applications
à de nouveaux domaines. Ce livre s’inscrit dans ce projet. On dispose
désormais d’une histoire substantielle de pareilles descriptions,
comme souligné plus haut, mais j’ai l’espoir et d’améliorer les
tentatives antérieures, et de conduire le projet plus avant. Cela doit
se faire en partie en poursuivant des discussions existantes et en
partie en utilisant de nouvelles sources. Ce qui comprend un regard du
problème par la philosophie des sciences récente.

Ce livre se veut dans le même temps philosophique et biologique. Ce
dont il traite est vu par le prisme de la philosophie, mais ses
arguments ont également pour but de porter à conséquence en biologie.
Pour décrire ce mélange, je distinguerai trois types de projets et de
questions en jeu: la science, la philosophie de la science, et ce que
l’on pourrait appeler la « philosophie de la nature ».

La science se concentre sur le monde naturel. La science parcourt le
monde par le biais moins d’une « méthode » réglée que de quelque chose
comme une _stratégie_. Les idées sur la façon dont le monde fonctionne
se développent et se justifient d’une manière conçue pour les rendre
à la fois mutuellement cohérentes et capables de répondre de
l’observation. Une partie de ce processus implique d’explorer la
« logique interne » des idées théoriques --- leurs ressources, leur
pouvoir explicatif en principe, dans quelle mesure elle sont
connectées à d’autres pans théoriques, et quelle sorte de données leur
fournirait une base ou les contredirait.

La philosophie des sciences se concentre sur la science elle-même, sur
le processus précédemment décrit. Son but est de comprendre de quelle
façon fonctionne la science et ce à quoi elle parvient. Ici l’on se
demande quelle sorte de contact ont les théories avec le
monde --- comment fonctionnent-elles en tant que représentations, et
comment génèrent-elles la compréhension. L’on enquête sur le rôle de
buts louables mais controversés tels que la vérité, la simplicité,
le pouvoir explicatif, et sur la nature de la preuve [_evidence_],
l’épreuve et le changement scientifique. Une telle entreprise peut
déployer un large filet, tentant de capturer l’ensemble de la science,
ou plus restreint, pour n’en comprendre qu’une petite partie, comme la
biologie évolutive.

Des travaux de ce type contribuent utilement en retour à la science,
dans la mesure où il est sain pour une science d’être consciente
d’elle-même. Tout ne s’accomplit pas mieux en pleine conscience, et il
n’est pas pas évident que ce soit le cas pour la science. Dans la
description de Thomas Kuhn du changement scientifique par exemple, la
science opère le mieux lorsque le « scientifique normal » est en
quelque sorte inconscient de ce qu’il ou elle fait réellement, et de
la marche au long cours de la science (Kuhn 1962). C’est une vision
intriguante, presque macabre, exagérée à tout le moins. Mais l’opinion
de Kuhn, certainement cohérente, indique qu’il n’est ni évident ni
inévitable que la philosophie des sciences soit d’une aide quelconque
à la science. Une telle contribution en retour mise à part, la
compréhension philosophique de la science est un but en soi.

Enfin, il existe un troisième type de travail philosophique. Ici le
point de mire est à nouveau sur le monde naturel. Mais alors le monde
naturel est vu à travers la lunette des instruments de la science.
C’est le projet qui veut adopter la science telle que développée par
les scientifiques, et déterminer ce qu’en sont les réels messages,
particulièrement quand il en vient aux grandes questions sur notre
place dans la nature. Aussi tentons-nous d’employer les travaux
scientifiques à l’information de notre vision du monde, mais sans la
déterminer par un emploi de la science « brute ». Nous étudions plutôt
la science d’un sujet donné et élaborons, philosophiquement, ce
qu’elle en dit exactement. Pour ressusciter un vieil usage, ce projet
peut être appelé « philosophie de la nature ».

D’aucuns pourraient se demander si pareil travail est bien nécessaire.
Si l’on est d’humeur scientifique, pourquoi ne pas modeler sa vision
du monde d’après la science brute? Si, d’un autre côté, l’on ne se fie
pas à ce que les scientifiques prétendent, ne devrions-nous pas
chercher d’autres sources?

L’attitude personnelle vis-à-vis de ce troisième projet dépend de la
manière dont on pense que la science opère --- elle dépend de ses
conceptions en philosophie des sciences. Voici un ensemble d’idées au
sein desquelles ce troisième projet prend tout son sens. La science
est un outil incroyablement puissant pour enquêter sur ce qu’il en est
du monde. Mais les idées se développent au sein de communautés
scientifiques selon les inquiétudes de la science elle-même. Il en
résulte des concepts dont les contours s’adaptent aux buts pratiques
du travail scientifique --- l’exigence de questions intelligibles, de
travaux coopératifs (et compétitifs), et de contrastes généralement
nets entre différentes options. L’on se trouve également face à un
langage infusé de subtiles --- presqu’invisibles --- métaphores, face
à des catégories modelées autant par les outils que le phénomène, et
face à des simplifications graissant les roues du travail quotidien.
Lorsqu’une image du monde issue du contexte immédiat de la science est
exportée vers une discussion plus générale, les attributs de la
description scientifique dont l’origine se tient dans ces fins
pratiques portent potentiellement à confusion. Les « discussions plus
générales » peuvent être ouvertement philosophiques (en éthique ou en
philosophie de l’esprit par exemple), ou même encore plus générales,
et moins académiques. Mais l’information scientifique doit
généralement être digérée avant de nourrir ce genre de débats. Les
travaux de cette sorte tentent souvent de synthétiser les résultats de
divers domaines scientifiques, de déterminer leur consistance --- ou
leur absence de consistance --- en un ensemble cohérent.

Aussi la philosophie de la nature affine, clarifie et explicite
l’image que la science nous propose du monde naturel et de la place
que l’on y occupe. L’appeler « philosophie » n’implique nullement
qu’il s’agisse d’un travail de philosophe uniquement. Nombre de
scientifiques, dont beaucoup de ceux mentionnés dans ce livre,
entreprennent ce genre de travaux. Mais c’est une activité distincte
de la science elle-même.

Cet ouvrage se déplace constamment entre ces trois types d’enquêtes.
Le point d’ancrage est celui de la philosophie des sciences --- un
regard sur la théorie évolutionnaire du point de vue du philosophe.
J’examine en quoi la biologie évolutionnaire tente de représenter et
de comprendre un certain ensemble de phénomènes naturels. Ce type
d’entreprise, ai-je déjà dit, ne mène pas nécessairement à un
retour productif à la science, mais j’ai ici l’espoir qu’il
y conduise. Certains problèmes deviennent plus clairs en se penchant
sur le rôle de différents types de « véhicules » théoriques employés
dans ce domaine, et j’argue en faveur de nouvelles conceptions des
relations entre certains concepts évolutionnaires centraux. L’image
résultante est également employée à l’induction d’une attitude
réflexive quant à nos habitudes psychologiques, lorsque nous pensons
le monde biologique. Ces habitudes, qui affectent la façon dont nous
conduisons science et philosophie, découlent des relations entre la
façon dont fonctionne notre monde Darwinien et la façon dont opèrent
nos esprits évolués.

# Un exposé des thèmes centraux

> [_Plans et réplicateurs; le concept minimal; cas marginaux et
> paradigmes; outils spatiaux; niveaux et transitions._]

La littérature existante présente deux traditions principales de
descriptions abstraites de la sélection naturelle. J’appelerai l’une
d’elle l’approche « classique ». C’est un ensemble de résumés qui
prennent grossièrement la forme suivante: l’évolution par sélection
naturelle s’ensuit dès lors qu’il se trouve une population avec de la
variation entre individus, qui conduit à ce que lesdits individus
aient une descendance plus ou moins nombreuse, et qui est héréditaire
dans une certaine mesure. Ces énoncés sont souvent exprimés comme une
sorte de recette: si vous êtes en présence de ces ingrédients, s’en
suit le changement évolutif. Cette recette est applicable, en
principe, à toute entité douée d’une forme quelconque de reproduction.

Cette approche « classique » constitue le point de départ de ma propre
analyse, mais ces énoncés standards font face à un certain nombre de
problèmes. Il peut se trouver que les ingrédients soient présents mais
qu’il n’advienne aucun changement, ou que des changements (en
apparence) du bon type adviennent sans que les ingrédients ne soient
présents. Parfois aussi le contenu d’un résumé semble aller
à l’encontre des commentaires qu’en donne son auteur.

Ces problèmes surviennent pour des raisons philosophiquement
intéressantes. Les recettes standards du changement par sélection
naturelle sont les produits de compromis, souvent inavoués, entre le
souhait de capturer tous les cas véridiques de sélection naturelle par
une description résumée, et le souhait de décrire une machine simple
et causalement limpide. Ces deux motifs correspondent aux deux sortes
distinctes de compréhension que s’efforce d’atteindre toute enquête
théorique. Une approche consiste à tentre de donner une description
littéralement vraie de tous les cas du phénomène d’intérêt. Une autre
consiste à décrire directement une classe de cas, simple et
intelligible, et de l’employer comme base d’une compréhension plus
indirecte d’autres classes de cas. L’on atteint la compréhension par
des relations de similarité entre les cas simples ayant été détaillés,
et l’essaim de questions plus compliquées.

Les cas « simples » en pareil entreprise peuvent n’être qu’un ensemble
plus simples de cas réels, ou un ensemble de cas fictifs auxquels on
parvient par une modification imagée de cas réels. Dans tous les cas,
par cette seconde approche, l’on ne parvient à la compréhension
qu’indirectement, à travers des modèles, plutôt que par des
descriptions directes de la masse de phénomènes empiriques. Des
passages méconnus d’une approche à l’autre s’opèrent dans bien des
discussions fondatrices de la sélection naturelle. L’on pourrait
objecter à ce stade: assurément il n’est nul besoin de choisir entre
ces deux approches, aussi ne devrait-on pas les appliquer
régulièrement toutes deux? C’est en effet ce à quoi je procède ici,
ce qui requiert néanmoins de poser les choses de façon nouvelle.

La seconde tradition de description abstraite de la sélection
naturelle est plus récente; c’est l’approche des « réplicateurs »,
développée d’abord par Richard Dawkins (1976) et David Hull (1980).
Brièvement, un réplicateur est toute chose produisant des copies
d’elle-même, ou induisant une production desdites copies. L’image
globale qui nous est proposée ressemble à la suivante. Les premiers
réplicateurs surviennent à l’origine de la vie elle-même, et ne sont
rien de plus que de simples molécules. Ces simples réplicateurs se
livrent alors une compétition évolutive féroce; se répandent ainsi
ceux qui se répliquent plus vite et plus fidèlement, et sont capables
de rester intacts plus longtemps. Mais éventuellement, ils s’engagent
dans des activités de coopérations à grande-échelle --- lorsque c’est
dans leur intérêt ---, y compris la fabrique de « véhicules » ou
d’« interacteurs ». Ce sont de plus grandes unités abritant les
réplicateurs et (particulièrement dans la version de Dawkins) servant
les fins des réplicateurs. Les réplicateurs, ici sur Terre, sont les
gènes, ainsi que (possiblement, depuis peu) quelques entités
transmises culturellement.

Selon cette vision, les réplicateurs sont des ingrédients essentiels
de tout processus Darwinien. La logique interne de la théorie
évolutionnaire est structurée en premier lieu autour de l’idée que les
réplicateurs évoluent de façon à accroître leur réplication, et que
les autres structures vivantes n’existent que comme produits de
l’action des réplicateurs. Il nous faut alosr appréhender le monde
vivant en traitant tout gène ou autre réplicateur comme le « centre
d’une toile de puissance radiative » (Dawkins 1982a: vii).

Je vais être beaucoup plus critique de la conception des réplicateurs.
Il n’est pas vrai que l’évolution par sélection naturelle requiert des
réplicateurs. Une partie de l’attrait de cette conception tient au
fait qu’elle est conçue pour aller de pair avec une façon particulière
de concevoir l’évolution, que j’appellerai « agentielle ». L’évolution
doit alors être pensée en termes de conflits entre entités qui
poursuivent un agenda, des fins et des stratégies. Je vois la vision
agentielle de l’évolution comme une sorte de piège. Elle a une vraie
force heuristique en certains contextes, mais aussi une tendance forte
à nous induire en erreur, particulièrement sur des questions
fondamentales. Dès lors que l’on commence à penser en termes d’agents
poursuivant un agenda --- même dans un esprit résolument
métaphorique --- il peut être difficile de s’arrêter.

D’aucuns se demandent sans doute si l’on a réellement _besoin_ d’une
description verbale abstraite de la sélection naturelle qui en
couvrirait tous les cas. Peut-être que les mots n’ont pas ce pouvoir.
Lorsque la théorie évolutionnaire était encore au berceau, il fut
possible à Darwin d’en exprimer les principes dans un anglais simple
(et élégant). Mais la tendance en théorie évolutionnaire depuis une
centaine d’années maintenant fut à la formalisation. Plutôt qu’une
description verbale, peut-être devrions-nous chercher une équation
maîtresse, une $E = mc^2$ ou $F = MA$ de l’évolution. Le tranchant des
mots, pourrait-on dire, s’émousse lorsqu’une théorie scientifique
parvient au stade du néo-Darwinisme moderne.

Quoique ce livre soit écrit dans un style très informel, le débat en
viendra souvent aux modèles formels. J’argue que ces modèles, qui
embarquent des idéalisations, servent des fins différentes que celles
des descriptions verbales, et ne s’y substituent pas. L’essentiel de
la discussion d’une « équation maîtresse » de l’évolution sera
reportée en Appendice, ceci dit, afin que ce livre reste aussi peu
technique que possible.

Le cadre conceptuel employé par le reste de ce livre est présenté au
chapitre 2. Le concept central, comme le suggère le titre, est celui
d’une « Population Darwinienne ». Il s’agit d’une population --- un
ensemble de particules --- douée de la capacité d’évoluer par
sélection naturelle. Tout membre de pareille population est un
« indidividu Darwinien ». Les individus Darwiniens seront discutés par
le menu, mais l’idée est que le niveau population vient d’abord. La
première étape principale est la description de ce que j’appellerai le
« concept minimal » d’une population Darwinienne, et une catégorie de
changement correspondante. Ce concept minimal arbore trois aspects
familiers de l’approche classique: la variation des caractères
individuels, qui affecte l’issue de la reproduction, et est
héréditaire. Ce concept minimal fonctionne comme une sorte de jalon,
dont le rôle s’efface peu à peu à mesure que s’étoffe l’entièreté du
cadre conceptuel.

La deuxième étape est une sorte de fragmentation, la reconnaissance
d’une famille de différents types de processus Darwiniens. Le concept
minimal est très permissif, comprenant à la fois des cas
biologique pertinents et d’autres qui paraissent presque triviaux. Si
l’on dépeint la sélection naturelle seulement à partir du concept
minimal, il peut sembler difficile de comprendre en quoi le Darwinisme
est-il si important. Mais le processus englobé par le concept minimal
ne doit pas être posé comme « le » processus Darwinien. Les processus
Darwiniens significatifs ont d’autres aspects, qui peuvent dans
certains cas être décrits de manière abstraite. Aussi pouvons-nous
distinguer au sein de la région délimitée par le concept minimal une
catégorie de populations Darwiniennes _paradigmatiques_. Il s’agit du
type de systèmes qui produisent des organismes nouveaux et complexes,
finement adaptés aux circonstances qui se présentent à eux.

À l’autre bout du spectre, l’on peut identifier une catégorie de
populations Darwiniennes _marginales_. Ce ne sont pas des cas qui
ressortent du concept minimal; ce sont plutôt des populations qui ne
satisfont pas clairement les prérequis minimaux, mais s’en approchent.
Ils ont un caractère partiellement Darwinien.

Aussi avons-nous, à ce stade, un concept _minimal_ (global et
permissif), un concept de cas _paradigmatiques_ (beaucoup plus
restreints) et un concept de cas _marginaux_ (s’approchant
vaguement des critères minimaux). Mais il est évident que le status de
« paradigme » n’est pas une catégorie discrète, et qu’une population
Darwinienne, par bien des façons --- bien des dimensions --- peut
l’être de façon beaucoup plus significative qu’une autre, qui le
serait de façon plus triviale. Je présenterai un cadre spatial
représentant certaines de ces possibilités. Cette partie de l’argument
est plus spéculative, mais le but est de montrer que les cas
paradigmatiques occupent une région de l’espace, quand les cas
marginaux en occupent une autre. Le critère minimal délimite une vaste
région, comprenant les paradigmes, et dont les frontières sont vagues,
se mêlant d’abord aux cas marginaux puis aux cas sans même de
caractéristiques Darwiniennes.

Pour lors un concept clé est tenu pour acquis: la reproduction. La
reproduction est au cœur du Darwinisme, mais elle est perclue de
mystères. Les cas les plus nets et les plus familiers de reproduction
impliquent la génération sexuée d’un nouvel individu, biologiquement
semblable à ses parents mais génétiquement distinct d’eux, et dont la
vie se développe à partir d’un état unicellulaire. Mais il existe bien
des cas moins clairs. Si une plante développe un stolon, conduisant
à une nouvelle structure semblable à l’ancienne et capable de vivre
indépendamment d’elle, est-ce de la reproduction ou guère plus que la
croissance de l’ancien individu? D’autres problèmes sont posés par les
entités « collectives », tels que les groupes sociaux ou les
associations symbiotiques. Les essaims d’abeilles se reproduisent-ils,
autant que les abeilles elles-mêmes? Un troupeau de bison se
reproduit-il, autant qu’un bison lui-même? Les entités symbiotiques
hautement entremêlées, comme les lichens, comprennent-elles des
individus Darwiniens propres? L’entité reproductrice est-elle tout ou
partie?

Cette question ne doit pas être vue comme du type « ou bien… ou
bien »; tous peuvent se reproduire, partant contribuer à un processus
Darwinien à leur niveau propre. Dans les chapitres suivants je traite
la reproduction elle-même par le bias d’un gradient, et emploie
à nouveau des outils spatiaux pour imposer un ordre à la variété de
cas non réglés. Le but est de défendre la vision suivante: la
« reproduction » advient par formes plus claires ou plus marginales,
parfois mal distinguables d’un croissance, parfois peu claires en
raison de ce que le statut d’individu des entités reproductrices
elles-mêmes peut être contestable. L’une des façons dont une
population Darwinienne peut être marginale est d’être composée
d’individus dont les relations reproductives sont elles-mêmes
marginales. Les cas de reproduction « marginaux » ne sont pas ceux qui
_semblent_ étranges, sachant ce dont on a coutume ou sachant le sens
quotidien du terme. Les cas de reproduction « marginaux » ont un rôle
évolutionnaire différent de cas plus francs.

Cela nous dote d’une façon neuve de traiter de vieilles apories --- en
partie biologiques, en partie philosophiques --- quant aux « niveaux »
ou « unités » de sélection. Nombre de processus sélectifs semblent
pouvoir être potentiellement décrits à plus d’un niveau, il peut alors
être difficile de déterminer si l’on doit choisir entre ces
descriptions, ou comment le faire. Par exemple, quand y a-t-il (le cas
échéant) évolution au cours de laquelle les unités sélectionnées sont
les groupes sociaux ou organismes, plutôt que les individus qui les
composent? Si la réponde est « jamais », doit-on alors voir la
sélection d’organismes entiers réellement comme des cas de sélection
sur des gènes individuels? Lorsque l’on est face à des objets
biologiques disposés comme une hiérarchie de tout et parties, comment
sommes-nous censés décider du niveau hiérarchique auquel opère la
sélection?

Mon approche de toutes ces questions est de demander séparément,
à chaque niveau, dans quelle mesure les entités qui composent ce
niveau satisfont le critère d’une population Darwinienne. Si le
système contient des gènes, des chromosomes, des cellules, des
organismes et des groupes, par exemple, l’approche est appliquée de
façon uniforme à chacun d’eux. Cela peut sembler la chose à faire
évidente, mais il est devenu relativement courant de procéder
différemment. Je discuterai des raisons pour qu’il en soit ainsi.
J’argue ensuite que les idées des chapitres antérieurs nous permettent
d’appréhender de vieilles questions quant aux niveaux d’une façon
neuve. Ce qu’il nous faut dès lors traiter, lorsque l’on touche à ces
questions, est le rôle des cas marginaux et partiels de populations
Darwiniennes, et les gradations qui les séparent des cas
paradigmatiques.

Les outils spatiaux développés dans ces chapitres ont deux rôles.
D’abord, comme précédemment souligné, celui de fournir une façon
concise de catégoriser différents cas. Les populations Darwiniennes
diffèrent à bien des égards, aussi l’une des façons de leur imposer un
ordre est d’en déterminer quelques paramètres pouvant être quantifiés
au moins grossièrement, et de représenter les populations dans un
espace abstrait où chacun des paramètres correspond à l’une de ses
dimensions. Mais les représentations spatiales ont aussi une
application plus dynamique. Lorsqu’une population évolue, elle ne
change pas seulement les caractéristiques des organismes la composant.
Elle change en conséquence, _comment_ elle évolue, la manière dont le
changement ultérieur doit advenir. Les organismes peuvent évoluer des
mode de reproduction plus, ou moins, fidèles; une intégration plus, ou
moins, fine des membres de la population; des limites plus, ou moins,
franches entre les générations. La relation parent--descendant
elle-même peut être accentuée, brouillée, transformée, voire perdue.
Aussi pouvons nous tout aussi bien voir les populations comme occupant
des points de l’espace Darwnien que représenter un certain type de
changement comme déplacements dans ledit espace.

En sus de mouvements d’une population unique, il y a la création de
nouvelles populations Darwiniennes à partir d’anciennes. Cela nous
permet de penser les nombreuses « transitions majeures » de
l’évolution, spécifiquement celles au cours desquelles des
entités de niveaux inférieurs s’associent, coopèrent et forment
éventuellement une population d’individus de niveaux supérieurs. Les
deux transitions les plus manifestes de ce type sont l’évolution de la
cellule eucaryote depuis l’association (par englobement) entre
diverses cellules procaryotiques (semblables aux bactéries), et
l’évolution de la multicellularité depuis la vie unicellulaire.

Le cadre conceptuel ici présenté se veut utile à l’appréhension de
pareilles transitions, particulièrement leurs étapes intermédiaires.
Ainsi faisons-nous fréquemment face à des populations au statut
Darwinien marginal --- par exemple, des entités collectives semblables
dans une certaine mesure à des organismes, quoique pas totalement. Un
grand nombre d’entre elles peuvent être observées dans la vie
aquatique, où l’on trouve une variété de formes d’integrations
_partielles_ de cellules et d’organismes simples en entités
collectives, comprenant les algues, les coraux et les éponges.

Alors que se déroule une transition de ce type, une population peut
apparaître d’abord comme un cas marginal, d’un point de vue
Darwinien --- un ensemble d’entités collectives dont l’on peut dire
qu’elles se reproduisent seulement en un sens très général, comptant
à peine comme des individus. Mais il peut se produire des
accroissements d’intégrations successifs, si bien que les entités
finissent par montrer un mode de reproduction bien défini au niveau
supérieur, la variation des traits du dit niveau étant héréditaire. Le
collectif devient un cas paradigmatique. Et alors que cela se produit,
les populations de niveaux inférieurs originelles tendent
à _s’éloigner_ du statut paradigmatique. C’est un un thème récurrent
que la maintenance aboutie d’une organisation à plus haut niveau
implique que la compétition entre les entités inférieures (les
cellules, par exemple) doit être supprimée. La suppression de la
compétition opère par la mise au pas des activités évolutionnaires
indépendantes desdites entités inférieures. Une population Darwinienne
peut en « dé-Darwiniser » d’autres.

Dans les derniers chapitres, je me penche plus longuement sur certains
sujets controversés: l’évolution biologique du « point de vue du
gène » [_gene’s eye view_] et la perspective Darwinienne sur le
changement culturel. Dans les deux cas je procède en appliquant, là
encore directement et uniformément, la conception défendue auparavant.
Cela implique que les gènes soient traités de façon matérialiste,
comme petites parties d’organismes, aux propriétés causales
spécifiques doués de la capacité de se répliquer fidèlement. Ils
comprennent des individus Darwinien très inhabituels, des entités dont
les activités reproductrices et dont le statut d’individu Darwnien
dépendent d’activités au niveau de la cellule et de l’organisme, en
particulier le sexe. Je m’oppose à l’idée que les gènes sont les
unités de base de l’évolution Darwinienne en un sens strict ou
définitif, mais discute de phénomènes spécifiques pour la
compréhension desquels l’adoption du point de vue du gène est une clé.

Je me tourne ensuite vers le changement culturel. L’attrait d’un
traitement de la culture en termes Darwiniens est presqu’aussi ancien
que le Darwinisme en biologie. Penser le changement culturel en termes
Darwiniens requiert d’explorer et de scruter plus avant le concept de
reproduction. Nombreux sont ceux ayant tenté d’appliquer rigidement un
cadre Darwinien à phénomènes culturels, mais il s’agit là encore d’un
domaine peuplé de cas partiaux et marginaux. Le changement culturel
est Darwinien sous certains aspects, et ne l’est pas sous beaucoup
d’autres. J’essaye de situer la ligne de démarcation. L’essentiel de
l’Appendice, qui conclue ce livre, est un supplément aux chapitres
antérieurs. La dernière section fait exception, en ce qu’elle
formalise certaines parties de l’image globale et est auto-suffisante.

# La pensée populationnelle et l’attrait des Agents et des Essences

> [_Pensée populationnelle; de laquelle l’on est aisément diverti; le
> modèle des organismes de la biologie populaire; la psychologie de
> l’explication; processus évolutionnaires comme produits de
> l’évolution._]

La section précédente résumait les principaux thèmes positifs de ce
livre. La présente introduit certains des arguments les plus
critiques, et leurs liens à une description plus générale englobant le
traitement de la sélection naturelle.

Je commencerai par en dire plus sur l’une de mes critiques de
l’approche des réplicateurs. Dans la section précédente j’ai dit que
cette approche est, dans nombre de ses apparitions, conçue pour aller
de pair avec une une manière « agentielle » de penser l’évolution.
L’évolution est vue comme un conflit entre entités douées de buts, de
stratégies, poursuivant un agenda. Ce genre de description peut être
appliquée à de nombreuses entités biologiques; les organismes, par
exemple, peuvent être vus comme « s’efforçant d’accroître la
représentation de leurs gènes dans les générations futures ». Mais est
elle est maintenant souvent appliquée aux gènes et autres réplicateurs
eux-mêmes. Les réplicateurs agissent de façon à accroître leur propre
réplication.

La perspective agentielle de l’évolution a toujours été composée d’un
mélange maladroit de métaphore et de littéral. Différentes formes de
cette façon de penser seront discutées plus loin, mais toute
conception en termes de bénéfices ou d’agenda porte une force
psychologique particulière. Ce mode de pensée embarque un ensemble de
concepts et d’habitudes: nos outils cognitifs pour traverser le monde
social. David Haig, un biologiste qui s’enthousiasme de l’approche
« stratégique » des gènes et de l’évolution, argue qu’elle est
littéralement pour nous une manière d’aborder les questions
évolutionnaires intelligemment. Le domaine socio-stratégique est l’un
de ceux où notre esprit est le plus puissant, comme les psychologues
évolutionnaires le défendent (Cosmides et Tooby 1992). Lorsque l’on
pense aux agents et à leur agenda, nous pensons différemment et avec
plus d’acuité que nous ne pensons la logique abstraite ou les
relations causales. La perspective stratégique de l’évolution est une
façon de mettre scientifiquement à profit cette puissante
caractéristique de notre esprit.

J’accorde à Haig que l’on pense de façon distincte dans un cadre
agentiel, mais je ne crois pas que ça soit souhaitable.
Particulièrement lorsque l’on pense fondamentalement à l’évolution, la
perspective agentielle peut être susceptible d’induire en erreur. Et
une fois adoptés ou enclenchés, ce sont des outils psychologiques
difficiles à mettre de côté; ils ont un attrait narratif séduisant,
addictif presque, et tendent à nous conduire sur des voies bien
spécifiques. Ceci vaux là encore pour toute vision agentielle de
l’évolution, pas seulement celles impliquant des réplicateurs. Elles
peuvent conduire à une forme aiguë de ce que Richard Francis (2004)
a appelé --- un peu théâtralement mais justement --- la « paranoia
Darwinienne ». La paranoia Darwinienne est la tendance à ne concevoir
l’évolution qu’en termes de raisons, de manigances et de stratégies.
Le terme d’agenda est puissamment explicatif. Dès lors qu’est
introduite la possibilité de la compréhension d’un phénomènes dans les
termes d’une grandiose logique, l’on rechigne à se satisfaire de
moins. Un agenda peut se substituer à un autre, mais cela devient le
_genre_ de compréhension que l’on cherche à atteindre. L’application
d’un agenda aux faits empiriques peut être contrainte et indirecte,
mais un agenda « fait sens » pour nous des choses d’une manière
que ne peut atteindre aucun catalogue de causes efficientes.

L’image d’une pensée agentielle dont j’use ici peut être opposée
à celle développée par Robert Wilson dans un contexte similaire
(2005). Pour Wilson, tout individu causalement actif et physiquement
délimité est un « agent »; un atome de carbone ou une brique peuvent
y prétendre, aussi bien qu’un gène ou un organisme. Et ce concept peut
être employé, pour Wilson, pour analyser comment les description
causales fonctionnent de façon générale en science. Wilson suggère ici
que les métaphores ont un rôle « cognitif » caché dans _toute_
catégorie générale de pensée causale. Ceci s’applique même aux
descriptions causales de choses inanimées, quoiqu’avec assurément
moins de force dans ce cas ci. En revanche, l’image que j’évoque
entends la pensée « agentielle » en un sens plus fort que Wilson, et
la voit comme spécifique à des domaines particuliers. Elle ne fait pas
partie de tout discours causal, mais j’argue qu’elle est source de
confusions en pensée évolutionnaire.

Aussi l’un des thèmes de ce livre concerne la manière dont nos
habitudes intuitives de catégorisation et d’explication sont liées aux
descriptions du monde que la théorie Darwinienne nous propose. Les
concepts Darwiniens centraux ont été développés de façon à ce que l’on
soit capables de traiter scientifiquement d’un domaine particulier.
Mais il existe des façons plus anciennes de penser ce domaine qui
exercent une influence forte --- en contextes informels, en
philosophie, et en biologie.

Dans une discussion classique, Ernst Mayr (1976) prétend que l’une des
contributions majeures de Darwin fut de « substituer à une pensée
typologique une pensée populationnelle ». La pensée populationnelle
est une idée controversée et délicate (Sober 1980, Lewens 2007, Ariew
2008), mais je crois qu’une chose très importante est contenue dans
l’argument de Mayr.

L’essentiel de la discussion de Mayr se concentre sur le rôle de la
variation au sein des espèces. Les visions « typologiques »
antérieures, dit Mayr, tendaient à ne voir la variation au sein de
l’espèce que comme imperfections de la réalisation matérielle d’un
« type » idéel. Il retrace ces conceptions jusqu’à Platon. La pensée
populationnelle, en revanche, ne traite guère les « types » que comme
outils conceptuels, et les populations elles-mêmes comme la base des
groupes et des espèces, et s’occupe de l’importance causale de la
variation en leur sein.

L’emphase mise sur la variation individuelle ne constitue pas un recul
défaitiste par rapport à la volonté de théoriser (« tout est unique,
aussi les théories générales faillissent toujours »), mais une
reformulation du sujet de la biologie en accord avec de meilleurs
théories sur la manière dont fonctionnent réellement les systèmes
biologiques. Une population est un objet physique, contraint par son
ascendance et d’autres relations causales, présentant toujours une
variété interne et changeant au cours du temps. Dans la mesure où les
organismes s’inscrivent dans des « espèces » bien distinctes et
distinguables, auquels on peut donner un nom simple, il s’agit d’une
conséquence dépendant de processus populationnels. Une espèce
distincte peut se diviser et se dissoudre, dès demain, si les
conditions l’y conduisent.

La pensée populationnelle, prise grossièrement au sens de Mayr, est
effectivement la façon dont on a réussi à avoir prise sur les
structures à grande échelle du monde vivant. Mais c’est une chose dont
on est aisément distrait. Elle est venue tardivement dans le
développement de la pensée, et pas seulement pour des raisons
historiques locales. Mayr a dit que Darwin a substitué à la pensée
typologique la pensée populationnelle, mais il serait plus correct de
dire que Darwin a _montré comment_ le faire. La pensée typologique est
tenace, est c’est une bataille qui doit constamment être à nouveau
livrée. La pensée populationnelle procède à l’encontre d’habitudes
psychologiques, d’une panoplie conceptuelle que l’on a naturellement
tendance à employer pour traiter du monde vivant.

En prétendant cela, j’use d’éléments de différentes branches de la
psychologie empirique récente, particulièrement les travaux sur la
« biologie populaire » [_folkbiology_] (Medin et Atran 1999,
Griffiths 2002), combinés à des observations plus familières sur
l’histoire des idées. Cette vieille panoplie conceptuelle comprend au
moins trois éléments, parfois explicitement défendus, parfois opérant
de façon plus implicite.

Le premier élément un _modèle causal essentialiste des organismes_.
Selon ce modèle, chaque organisme possède un type, ou une nature
sous-jacente. La nature est _exprimée_ par ses attributs observables.
Les rôles de la nature sous-jacente et de la contigence de son
expression prennent souvent une connotation normative; lorsque la
nature interne [_the inner nature_] n’est pas fidèlement exprimée,
quelque chose ne tourne pas rond.

Le deuxième élément est ce que Sober (1980) a baptisé un _modèlement
naturelle statique_ de la variation populationnelle et du changement.
Les espèces sont vues comme des collections d’individus exprimant un
type. Des perturbations de la population en raisons de variations
accidentelles conduisent à une force ou une tendance restauratrice,
qui incline à la pleine réalisation du type. D’un point de vue
Darwinien en revanche, la composition génétique et phénotypique d’une
population est toujours la base de variations et changement plus
avant. Si la population s’éloigne d’un état antérieur sous l’action de
causes locales, elle n’est pas conduite par une tendance intrinsèque
à retourner à la forme antérieure. Ceci fait de l’évolution des
espèces une question ouverte, non délimitée par une « sphère close de
possibilité ».

Le troisième élément est une _vision téléologique de l’activité
biologique_. Elle est liée au cadre agentiel discuté plus tôt, mais
est d’une portée plus générale.

La téléologie constitue un pan entier de la philosophie de la
biologie; par là j’entends le statut de notre tendance à traiter les
activités biologiques en termes de buts, de fins, et de fonctions. Il
est généralement supposé que les intentions d’un concepteur ou
utilisateur intelligent d’un objet peuvent servir de base à sa
description téléologique de façon évidente. La question est de savoir
si, et de quelles manières, ces termes peuvent être employés en
l’absence de ce rôle avoué de l’intelligence. Chez Aristote, un mode
de pensée téléologique était la base d’un traitement complet du monde
naturel. Cette vision fut largement supplantée par la philosophie
« mécaniciste » au cours de la révolution scientifique, du moins en ce
qui concerne la physique. Tout naturellement, ces idées ont perduré
plus longtemps en biologie. Leur relation avec le point de vue
Darwinien est incertaine. Parfois le Darwinisme est vu comme
démolissant les derniers fondements d’une vision téléologique, mais
parfois comme domestiquant ces idées de manière constructive, en
montrant qu’elles ont une application limitée mais bien réelle aux
processus biologiques.

La forme la moins agressive de ces inclinations se rencontrait souvent
dans la philosophie de la biologie de la fin du vingtième siècle
(Buller 1999). Une forme mince de description téléologique peut être
fondée sur une vision Darwinienne. Par exemple, le Darwinien peut dire
que la fonction d’une partie du corps est la tâche qu’elle accomplit
ayant été favorisée par la sélection naturelle. En ce sens mince, la
fonction est ce que cette structure est « censée » faire. Il s’agit
d’un sens très déflationniste de « censée ». Tout autre discours de
but ou de fins, sauf lorsqu’elle est basée sur les intentions d’un
concepteur ou utilisateur intelligent, est vue dans cette vision
Darwinienne simplement comme métaphore.

Récemment, certains philosophes ont souhaité rendre à la vision
téléologique des activités biologiques un statut plus significatif et
plus robuste (e.g. Thompson 1995). Souvent, cela repose sur l’idée
qu’une façon téléologique de voir les choses génère un type spécifique
de compréhension. Par l’emploi de concepts de buts et de nature,
embarquant une connotation normative, l’on est à même de « faire
sens » des phénomènes naturels. J’accorde à ces philosophes le fait
que les _intuitions_ avec lesquelles ils travaillent sont réelles;
nous possédons des habitudes distinctes de catégorisation et
d’explication que nous appliquons au monde vivant, incluant des formes
de compréhension essentialistes et téléologiques. Mais ces intuitions
font partie d’un ensemble d’idées et d’habitudes qui nous induisent en
erreur depuis des siècles, en ce qui concerne la théorie, et doivent
être surmontées pour développer la vision Darwinienne. La _sensation_
qu’une façon particulière de voir les choses génère de la
compréhension ne devrait pas être acceptée d’emblée, et ne marque donc
pas la fin du débat.

Aussi une partie de l’arrière-plan critique de ce livre est l’idée que
certains aspects profondément ancrés de notre psychologie continuent
d’affecter la façon dont on pense le monde vivant, même lorsque l’on
fait de la science et de la philosophie. Lorsque je parle d’« aspects
profondément ancrés de notre psychologie », je fait référence à un
mélange _quelconque_ d’un habituel profil psychologique humain
transculturel, ainsi qu’une sur-couche d’attributs contingents de
l’histoire de la pensée Occidentale. Ça n’est guère une surprise: nous
avons avec le domaine biologique une longue histoire de relations
pratiques immédiates et pressantes. C’est aussi un domaine qui rejoint,
et empiète sur, le domaine social où les concepts de la psychologie
populaire d’intentions et de buts sont applicables de façon plus
évidente, et il se trouve être suffisamment complexe pour que l’on
favorise des schémas et modèles compacts avec lesquels on peut lui
imposer un ordre. En conséquence, un but additionnel de ce livre est
d’étendre et de ré-affirmer la puissance de la perspective sur le
monde vivant que Mayr avait partiellement exprimée par le biais de son
concept de pensée populationnelle. Cela inclut des extensions que Mayr
lui-même, et d’autres dans sa tradition tels que David Hull,
pourraient ne pas accepter. Je me pencherai également, vers la fin de
l’ouvrage, sur les endroits où la pensée populationnelle est en
défaut; une vision naturaliste n’implique pas de traiter _toute_
collection comme une population. Plus souvent dans les chapitres qui
suivent je soulignerai ses forces. Et pour finir ce chapitre, je
complèterai cette perspective plus en détail, en plaçant l’emphase sur
les contrastes avec l’ancien ensemble d’habitudes décrit ci-dessus.

Permettez-moi de revenir au point de Mayr sur la distinction entre
façons populationnelles et typologiques de penser la variation au sein
des populations et espèces. D’un point de vue typologique, cette
variation reflète une imperfection de la manifestation d’un type dans
le monde empirique et chaotique. La variation _entre_ espèces est la
base de la catégorisation et de l’explication; la variation _dans_
l’espèce reflète l’imperfection de la réalisation matérielle d’une
forme. Mais, comme le dit Lewontin, l’évolution Darwienne fonctionne
comme une machine qui change le second type de variation en le
premier. Toutes les différences étonnantes entre espèces d’organismes,
et les traits qui rendent certaines espèces si distinctes, ont leurs
origines dans la variation au sein des populations, filtrée et
magnifiée de manière telle qu’elle génère un changement plus général.

La machine Darwinienne à laquelle il est fait allusion ici est un
ensemble de facteurs générant des changements dans les populations,
combiné à leur fragmentation et leur division. Par division, une
population devient deux choses aux chemins évolutifs différents,
accumulant leurs propres réponses aux accidents et aux circonstances
locales. Ceci génère un ensemble de populations. Supposez maintenant
que l’on « s’éloigne » de sorte que la structure interne de chaque
population s’efface, et que l’on voit ces objets comme étendus dans le
temps. Ceci engendre l’autre idée Darwinienne centrale soulignée dans
la première partie de ce chapitre: l’arbre de la vie. C’est l’idée
d’un réseau d’ascendance et de descendance reliant tous les
organismes --- aussi bien tous les individus que toutes les
espèces --- et remontant à une racine unique.

Le fait d’une racine unique est contingent, et l’assertion d’une
structure arborée est une approximation, spécifiquement par rapport
aux premiers stades de la vie. Mais lorsqu’une telle structure est
découverte et décrite, elle devient une idée incroyablement puissante.
Supposez que vous contempliez une colline. Vous apercevez un chêne, un
cyprès, une fourmi, un oiseau, une grenouille et une étendue d’herbe.
Vous pouvez immédiatement en inférer leur ascendance commune: le chêne
et l’herbe ont un ancêtre commun plus proche qu’aucun des deux n’a
avec le cyprès; l’oiseau et la grenouille ont d’abord un ancêtre en
commun avant d’en avoir un avec la fourmi; et finalement un ancêtre
commun, il y a un milliard d’années, entre le chêne, la grenouille, et
vous-mêmes. Les organismes forment des lignées qui s’étirent de cette
façon, comme le font les cellules qui les composent, chaque cellule
survenant de la division d’une autre (ou d’une fusion de cellules
sexuées), et toute membrane cellulaire survenant d’une membrane
antérieure, par une chaîne continue de croissances et divisions.

Les gens parlent parfois de l’arbre du vivant comme d’une simple
métaphore, mais ça n’est pas rendre justice à l’idée. Les
représentations de la vie comme un s’approchant d’un arbre peuvent
être traitées comme des représentations abstraites de faits réels,
comme le sont les cartes de métro. Comme une carte de métro, une
représentation en arbre n’est pas seulement un outil utile, mais
fonctionne _via_ cette correspondance abstraite avec les choses
réelles qu’elle est censée représenter. Cette représentation est
partielle; il y a des anomalies, des transferts et des exceptions. La
vie ne décrit que grossièrement un arbre, mais il s’ensuit beaucoup de
ce qu’elle le décrive.

Le point de mire de ce livre est l’évolution au sein des populations.
Mais l’évolution au sein des populations est vue dans le contexte d’un
arbre. L’état d’esprit qui en résulte peut être illustré là encore en
regardant le message de la pensée populationnelle quant aux espèces et
autres catégories biologiques.

La surprise évidente par rapport aux espèces biologiques que réserve
la théorie évolutionnaire est que les espèces elles-mêmes ne sont pas
des types fixés. Mais une autre surprise est qu’il y a de la variation
eut égard aux _types_ de similitudes et distinctions que l’on trouve.
Certains organismes, comme les humains et les chimpanzés, se classent
dans des espèces manifestement nettement distinctes. Ce fait --- quel
type d’ensemble est formé, pas seulement à quoi ressemblent les
organismes de cet ensemble --- découle de facteurs Darwiniens locaux.
D’autres organismes ne forment pas des espèces en ce sens. Chez les
chênes, les barrières d’espèces se dissolvent par des réseaux
sophistiqués d’hybridisation --- les botanistes parlent d’essaims
hybrides [_hybrid swarms_] de chênes. De nombreux types d’organismes
forment des « espèces circulaires », dans lesquelles une chaine
d’organismes localement similaires et interféconds engendre d’autres
extrémités de la chaîne, différentes des premières de façon marquée et
avec lesquelles elles ne sont pas interfécondes. Les organismes
asexués sont différents encore, parce qu’ils décrivent des lignées
individuelles en principe isolées, mais qui forment néanmoins quelque
chose _comme_ des espèces --- d’où le terme de « quasi-espèces ».
Aussi alors que l’on se déplace d’une partie à l’autre de l’arbre de
la vie, on y trouve non seulement des organismes dissemblables, mais
aussi des espèces dissemblables, chacune dotée de son histoire, de son
écologie et de sa génétique propre. Il y a donc variation et évolution
de l’espècéité [_kind-hood_], autant que des organismes eux-mêmes
(Dupré 2002, Wilkins 2003).

Le même mode de pensée peut être amené à porter sur l’évolution par
sélection naturelle elle-même. Les processus évolutionnaires, et leurs
différences, peuvent être caractérisés de manières abstraites. Mais la
forme que prennent ces processus sur Terre sont les conséquences des
organismes (et des environnements) que l’évolution a engendrés. En ce
sens, les processus évolutifs sont eux-mêmes des produits de
l’évolution. Ils varient le long de l’arbre de la même façon que les
organismes et les espèces. Glenn Adelson (à paraître) a très justement
appelé cette attitude envers les processus évolutionnaires « le
Darwinisme du Darwinisme ».

De tout cela, l’on retire une image du monde particulière. Ce monde
contient une large diversité de populations Darwiniennes. Celles-ci
sont de bien des sortes, se trouvent à différents niveaux, et
comprennent des populations paradigmatiques, triviales et marginales.
D’elles découle l’arbre, mais elles évoluent en présence des
organismes qui le composent, étant à la fois éliminées, modifiées, et
multipliées, à la fois par des changements endogènes et par
l’évolution d’autres populations. L’image Darwinienne qui en ressort
à nouveau, va à l’encontre de vieilles habitudes psychologiques
humaines, un ensemble ancien de réponse aux phénomènes biologiques,
qui sont le produit de notre histoire d’interactions pratiques avec le
vivant --- le produit de notre propre insertion dans les processus
Darwiniens.

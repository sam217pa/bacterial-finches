---
title: Les pan-genomes sont-ils adaptatifs?
author: Samuel Barreto
date: 2018-12-13
---

L'une des découvertes majeures depuis la « démocratisation » des
technologies de séquençage est l'existence d'une grande hétérogénéité
de contenu en gènes entre les représentants de ce que l'on qualifie
d'espèce chez les procaryotes. Dit autrement, deux individus de la
même espèce bactérienne diffèrent d'abord par le polymorphisme des
gènes qu'ils partagent, mais aussi par la nature des gènes qu'ils ne
partagent pas.

Certaines espèces bactériennes d'intérêts médical, agricole ou
académique^[_Staphylococcus aureus_, _Pseudomonas aeruginosa_,
_Pseudomonas syringae_, _Escherichia coli_] ont désormais un nombre de
génomes publiquement disponibles important (2085 pour _Escherichia
coli_, d'après @LandInsights20Years2015). Cela permet de comparer le
contenu en gène à large échelle. On peut alors définir trois
catégories de gènes: les gènes partagés par une proportion proche de
1 des individus font partie du _core-genome_; les autres du génome
_accessoire_ (certains disent dispensable, mais qu'il soit le cas est
bien l'enjeu du débat), qu'on peut diviser en gènes
_flexibles_ --- présents chez au moins deux individus --- et gènes
_uniques_ --- présents chez seulement un représentant de l'espèce.
Pour donner un exemple, chez _Pseudomonas aeruginosa_, 1.2% (665) des
gènes constituent le _core-genome_, 48.7% et 50.1% constituent
respectivement des gènes flexibles et uniques du
pan-génome.[@FreschiPseudomonasAeruginosaPangenome]

D'une matrice de présence/absence de gène, on peut déduire un arbre de
proximité en contenu en gène entre individus. Pour l'essentiel, cet
arbre est cohérent avec la phylogénie qu'on peut obtenir à partir de
l'alignement des gènes du _core-genome_, à quelques exceptions près
qu'on peut imputer à la proximité de style de vie, bref
à l'écologie.[@DillonRecombinationEcologicallyEvolutionarily2017]
Autrement dit, en tout cas dans le modèle _Pseudomonas syringae_, le
signal phylogénétique d'héritabilité verticale n'est pas brouillé par
le transfert horizontal au point de changer radicalement la topologie
de l'arbre résultant.^[De quoi mettre un peu d'eau dans le vin des
partisans d'une évolution d'abord rhizomatique, en réseau. Il est vrai
que le transfert horizontal est l'un des processus majeurs des
dynamiques évolutives procaryotes (mais pas seulement); il n'empêche
qu'aux échelles de temps contemporaines au moins, la structure des
relations entre espèces d'abord, au sein des espèces ensuite, est
d'abord arborescente.]

Le fait même que l'on observe des pan-génomes a suscité la question de
leur origine évolutive. Comment se fait-il qu'il y ait une telle
hétérogénéité en contenu en gène entre individus par ailleurs
suffisamment « cohésifs » pour qu'on continue de les classer
ensembles? C'est la question que pose cet article paru dans Nature
Microbiology en 2017,^[@McInerneyWhyProkaryotesHave2017] qui a généré
un débat par correspondance interposée à ladite
revue.^[@ShapiroPopulationGeneticsPangenomes2017;
@VosArePangenomesAdaptive2017; @McInerneyReplyPopulationGenetics2017]

Dans ce qui suit, je propose de replacer dans l'ordre logique les arguments avancés par cet article et les réponses qui lui ont été opposées.

Cet article propose d'évaluer les différents candidats permettant
d'expliquer l'observation de cette diversité de contenu en gène. Le
premier, le plus « évident », est la dérive génétique. En effet, selon
eux, le « transfert horizontal de gène est une forme de mutation et
peut dès lors être traité comme tel dans les modèles d'évolution du
pan-génome ». Pour les citer:

> La théorie évolutive nous indique que le destin d'un allèle dans une population de taille $N$ aura une fréquence initiale de $1/N$. Si le taux d'acquisition de nouveaux allèles est $\mu$, alors le taux de fixation de nouveaux allèles purement par dérive est $N\mu \times 1/N = \mu$. Cela implique que la probabilité de fixation d'un nouvel allèle est indépendante de la taille de population et égale au taux d'introduction d'un nouvel allèle. Le temps de fixation d'un allèle neutre est, en moyenne égal à $2N$, ce qui implique qu'un allèle neutre puisse rester longtemps à l'état polymorphique et à basse fréquence dans une population. Ainsi, ce modèle pourrait potentiellement expliquer la présence de larges pan-génomes.

Mais selon eux, un transfert de gène n'a pas le même résultat en terme
de fitness qu'une mutation ponctuelle seule, parce qu'il en coûte une
certaine somme d'énergie pour être répliqué, transcrit, traduit…^[Foin
du comportement du transfert de gène comme d'un polymorphisme que l'on
peut traiter comme tel dans les modèles d'évolution.] De plus, pour
qu'un allèle se comporte en allèle neutre, le coefficient de sélection
qui lui est imparti doit être très proche de zéro pour que l'allèle ne
soit pas contre-sélectionné dans de très grandes populations.
D'ailleurs, si lesdits coûts étaient proches de zéro, on pourrait
s'attendre à ce que les génomes augmentent en taille, mais on sait que
la taille des génomes bactériens est contrainte dans l'interval entre
1 et 8 Mb. De plus, des preuves empiriques semblent montrer que les
génomes procaryotes sont biaisés vers la délétion d'ADN, et que donc
ce biais devrait conduire à l'élimination de gènes neutres.

Vos et Eyre-Walker rétorquent que cela implique que le biais de
déletion « soit indépendant de la taille du génome, ce qui paraît peu
probable puisqu'on peut s'attendre à ce que des génomes en expansion
deviennent progressivement plus instables, favorisant des délétions
plus grandes ». Cela implique aussi que les transferts de gènes
conduisent plus à des gains qu'à des pertes de gènes, ce qui pourrait
bien n'être pas le cas.[@CroucherHorizontalDNATransfer2016]

McInerney et al avancent ensuite un autre candidat, auquel les
théoriciens de l'expansion des génomes eucaryotes pourraient
spontanément penser: l'invasion par des éléments génétiques mobiles au
comportement « égoïste ». Il existe de nombreuses preuves empiriques
selon lesquelles ces éléments sont relativement nombreux dans le
génome accessoire; on les trouve sur des plasmides, intégratifs ou
non, sur des prophages, des transposons, des intégrons, bref toute une
zoologie d'éléments génétiques typiquement parasitaires. Ils citent
cependant à charge le fait qu'ils ne se classent « que » troisièmes en
nombre de gènes dans le génome accessoire de 228 souches de _E. coli_
ST131, derrière les protéines aux fonctions inconnues et les gènes
associés au métabolisme. Néanmoins, deux autres travaux célèbres
montrent que ces éléments constituent bien la première catégorie
d'élements du génome accessoire, y compris chez
_E. coli_.[@TouchonOrganisedGenomeDynamics2009;
@NakamuraBiasedBiologicalFunctions2004] Un travail tout récent
utilisant une méthode astucieuse de capture des gènes transférés par
séquençage de la matrice CRISPR d'une souche « enregistreuse » de _E.
coli_ dans le microbiote digestif humain montre que ce sont
effectivement des éléments génétiques mobiles qui sont
préférentiellement capturés, de là
transférés?[@MunckRealtimeCaptureHorizontal2018] De plus, comme le
notent Vos et Eyre-Walker, ça n'est pas en soi un argument pour que la
majorité des variations en contenu en gène ne soit pas délétère.
McInerney soulignent que s'il est vrai que la résultante d'un
transfert de gène peut être délétère, si le transfert de gène l'était
toujours, on n'observerait pas de pan-génome. Vos et Eyre-Walker
répondent que ce pourrait être le cas si le coût de la capture de
gènes généralement délétères était compensé par l'avantage occasionnel
de la capture d'un gène bénéfique; ou si la sélection contre l'afflux
de gène est soit contrainte soit même impossible.

## Références
\footnotesize

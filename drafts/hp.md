---
title: "Après la déconstruction"
subtitle: "L'engagement rationaliste"
date: 2019-06-27
author: Samuel Barreto
draft: true
---

Ce billet pour tenter de mettre en forme les différentes critiques que m’ont inspiré le discours d’un phylogénéticien dont, les « faits d’armes en phylogénie moléculaire, remarquait V. Daubin, ne se présentent même plus », à savoir Hervé Philippe, venu au laboratoire ce jeudi 20 juin 2019.

L’intervention portait sur le rôle de la science (et des scientifiques) dans la catastrophe écologique en cours --- rôle qu’elle a joué, qu’elle joue et celui qu’elle doit jouer.
Une bonne part du discours a été consacrée à ce qu’on pourrait appeler une déconstruction du rôle social de la science.
Pour bénéfique qu’elle soit parfois, ce genre de déconstruction a une certaine tendance à me hérisser le poil, surtout par ces temps troublés où il se pourrait bien que la raison soit la dernière arme des faibles.
J’essaierai dans ce billet de coucher par écrit ma conviction de plus en plus profonde qu’il nous faut prendre des _engagements rationalistes_, titre que Canguilhem avait donné à ce recueil de textes de Gaston Bachelard.

\vspace{1em}

Hervé Philippe a commencé, dans mon souvenir, par exposer différents arguments chiffrés en faveur de ce que l’on connaît en fin de compte déjà, c’est-à-dire la montée du niveau de gaz à effet de serre dans l’atmosphère, la relative stabilité sinon diminution du rayonnement solaire (par quoi on comprend qu’il ne peut pas s’agir de ça), et l’absence d’un quelconque effet observable après quarante ans de sommets mondiaux sur le climat (Rio 1992, Kyoto, ...).

Il a continué, tentant de définir ce qu’est la science, en citant les premières lignes de l’entrée « Science » dans [Wikipédia](https://fr.wikipedia.org/wiki/Science), où il est dit qu’elle est « l'ensemble des connaissances et études d'une valeur universelle, caractérisées par un objet et une méthode fondés sur des observations objectives vérifiables et des raisonnements rigoureux. »
Ce à quoi il a vivement réagi, rappelant en substance que la science est moins une activité qui vise à la vérité qu’une construction « sociologique » (là où il faudrait dire « sociale », une construction sociologique étant plus un objet construit par la science du monde social qu’un objet construit par le monde social lui-même) visant à _produire_ de la connaissance (je n’ai malheureusement plus la définition exacte en tête, mais c’était celle-là en substance).

En fait, s’appuyant sur une citation d’Erwin Chargaff dans _Heraclitean Fire_, il en est venu à comparer la Science (il faudrait souligner cette majuscule) telle qu’elle se pratique désormais à une sorte de religion, dont les scientifiques seraient les grands prêtres, assénant aux profanes (aux laïcs) des vérités hors-sol, détachées depuis longtemps de tout arrière-plan qui aurait quoi que ce soit à voir avec le réel (si d’ailleurs il n’est pas tout à fait vain de faire emploi d’un terme comme « réel »).

Après quoi suivait une partie assez douloureuse à entendre sur le fait (s’il est encore permis d’en parler) que la Science a historiquement été encouragée parce qu’elle servait des intérêts militaires.
Il a rappelé l’exemple de Galilée, qui, contrairement à Giordano Bruno, brûlé vif par l’inquisition italienne, fut finalement autorisé à poursuivre ses travaux sur l’héliocentrisme parce qu’il permettait de faire avancer la technologie de la lunette, avantage crucial en temps de guerre maritime fréquente.
(Je suis à peu près convaincu qu’une analyse historique plus fine montrerait que ce furent de tout autres facteurs en ligne de compte, par exemple parce que Galilée était implanté à Venise, cité dont on sait que l’inquisition et l’Église en général y étaient de relativement peu de poids, contrairement à Rome où fut condamné Bruno.)
Plus globalement, était assez prégnante l’idée que la science n’est « au service » que d’une frange de la société (l’armée) ou d’élans sociaux peu avouables, voire carrément condamnables comme l’égocentrisme, le narcissisme, le culte de l’ego ou d’un _ethos_ dans la plus pure veine capitaliste.

S’il est vrai qu’on peut reconnaître à cela un fond de vérité (toute personne de science en activité a dû se reconnaître, elle, lui ou quelque pair célèbre ou anonyme), je crois reconnaître dans ce discours ce que Bourdieu appelle la _réduction sociologique_, par quoi l’on réduit toute action à des motifs vaguement sociaux, une sorte de déconstruction des intérêts à l’œuvre, forme au combien jouissive du radicalisme de celle ou celui qui explique aux autres ce à quoi ils sont _vraiment_ à l’œuvre, partant du principe qu’ils en sont totalement inconscient.
(L’on croit y voir une forme de psychanalyse du social, par laquelle on projette l’arrière-plan de valeurs de l’analyste sur celles du patient, construisant par là une théorie vaguement explicative dont la cohérence peut sembler rendre compte des phénomènes à l’œuvre, en grande partie parce que leur complexité échappe largement à quelque analyse réductrice que ce soit.
C’est précisément à ce genre d’analyse qu’il faut échapper, pour y substituer une socio-analyse beaucoup plus émancipatrice, en ce qu’elle pose explicitement comme « sujet » de l’analyse une vision sociale du monde, dissipant d’un seul coup toute dualité sujet/groupe, tout « mythe de l’intériorité » pour reprendre Bouveresse, quand la réduction sociologique concentre sur l’individu tout le pouvoir explicatif, perdant par là toute puissance subversive qu’elle s’évertue pourtant à poursuivre.)

Les motifs d’Hervé Philippe sont à mon sens tout à fait louables: son but est d’encourager le monde scientifique à une plus grande réflexivité sur la question de savoir si son rôle dans la crise écologique en cours n’est pas de construire l’exemplarité, puisqu’on sait que c’est largement par la science qu’a été rendue possible la dégradation relativement avancée de l’état global de la planète.
À mon sens, vouloir construire ce besoin de réflexivité sur un rejet des valeurs cardinales de la science --- le désintérêt, la curiosité, le recours délibéré à la raison, à un rationalisme avoué ou latent ---, c’est se contraindre à un relativisme à tout crin, dont on sait à quel point il peut frayer avec les nihilismes de tout bord qui nous ont conduit aux pires atrocités par le passé.

## La science et l’intérêt au désintérêt

Bourdieu, qui s’est intéressé une bonne partie de sa carrière à la compréhension du social, et notamment de certains des champs sociaux qui lui sont propres, la science et l’univers académique, avait donné pour titre à l’un des chapitres de _Raisons Pratiques_: « Un acte désintéressé est-il possible? »
Je reprendrais volontiers chacune des lignes de ce chapitre pour tenter de fonder une critique rationaliste de la « réduction sociale », qui, je le rappelle, veut expliquer tous les motifs des scientifiques par la poursuite d’intérêts narcissiques ou égoïstes.

## Plus de morale, moins d’éthique?

Un aspect sur lequel Hervé Philippe est longuement revenu est son impression d’une disparition de la morale, et d’un envahissement de l’éthique.

## La science construit-elle son objet?

C’est tout un pan de la sociologie des sciences à quoi il faut ici s’opposer, un pan construit entre autres par des sociologues français comme Michel Callon, Bruno Latour ou Isabelle Stengers.
S’appuyant sur une méthode d’appréhension du social, l’ethnométhodologie, ce type de sociologie prétend que toute connaissance est _construite_.
L’un des ouvrages phares de cette mouvance est _La Vie de Laboratoire_, dans lequel Bruno Latour retranscrit son immersion dans un laboratoire d’endocrinologie de l’Institut Salk dans les années 1970, au sein du laboratoire de Roger Guillemin.
Les travaux que Bruno Latour a commentés ont vallu quelques années plus tard (1977) à Roger Guillemin un prix Nobel de médecine pour la découverte de la TRH, l’une des premières neurohormones à avoir été purifiée puis décrite.

Dans ce livre, Latour montre toutes les transactions qui s’opèrent notamment entre l’humain et la technique, et entre humains d’un même univers scientifique.
Il mobilise pour cela un vocabulaire tiré de la _Grammatologie_ de Jacques Derrida, l’un des fondements philosophiques du déconstructionnisme: pour Derrida, il n’y a rien « d’autre que le texte », autrement dit --- à la fois pour faire simple et parce que j’ai beaucoup de mal à me frayer un chemin dans ce maquis philosophique --- il n’y a dans le monde rien qui n’existe autrement que par ce que l’on en dit.
Latour applique à la lettre ce précepte, et retranscrit ainsi l’interaction entre un technicien et un appareil de chromatographie comme une « traduction », une opération sur des signes, qui n’ont de valeur et de _signification qu’inscrite dans ce contexte social_.
(L’expression a d’ailleurs été employée telle qu’elle par Hervé Philippe également.)

En clair, que rien n’existe dans le monde autrement que par ce que l’on en dit implique qu’il n’est rien que l’on ne construise pas.
Latour explique ainsi quelque part qu’il est bien impossible que ce soit le bacille de Koch qui ait été responsable de la mort de Ramsès II, sachant qu’il _n’existait rien de tel_ que le bacille de Koch il y a 3000 ans: c’est Koch et la microbiologie post-Pasteur qui a _construit_ le bacille de Koch.
C’est un pur anachronisme, tout aussi étrange que de dire que Ramsès II est mort d’une rafale de mitrailleuse.
La puissance subversive de ce genre d’approche n’est pas à sous-estimer: en explicitant les intérêts sous-jacents d’une entreprise comme la science, on met au jour des catégories de pensée et tout ce qu’elles peuvent embarquer comme valeurs dominantes.

Il faudrait beaucoup plus que ce billet pour expliquer jusqu’où mène l’adhésion franche à ce genre de précepte méthodologique.
La moindre conséquence n’est pas qu’elle conduise à un relativisme extrême, par lequel tout n’est qu’une question de point de vue, de perspective: « vous vous prétendez opprimés, mais c’est votre point de vue, subjectif par essence; du mien votre situation est tout à fait souhaitable pour l’ordre social. »

En outre, le déconstructionnisme débridé qui a pu se pratiquer en sciences humaines dans les années 1990 (pour ce qui est de la biologie moléculaire, il faut se reporter aux travaux de Pnina Abir-Am, exemplaires de cette littérature) n’est pas sans son lot de problèmes qu’ils soient d’ordre méthodologique ou proprement philosophique.
^[Je renverrai pour cela au chapitre qu’Hilary Putnam a consacré dans _Renewing Philosophy_ à l’irréalisme et au déconstructionnisme, dont j’ai pu faire une ébauche de traduction disponible [ici](https://sam217pa.gitlab.io/bacterial-finches/putnam3.html).
Il y explique en quoi le déconstructionnisme porte sa part de responsabilité dans l’irresponsabilité avec laquelle certains l’ont interprété et « enrôlé » dans le sens de la contre-révolution conservatrice des années 1990.]
Souvent l’adhésion à la mode du déconstructionnisme va de pair avec un refus ouvert du positivisme et de son exclusion hors du domaine du connaissable de tout ce qui ne relève pas de l’empirique.
Mais, « dans la haine du positivisme, écrit Jacques Bouveresse, qui n’est souvent pas très différente de celle du mode de pensée scientifique lui-même, on peut aisément percevoir la peur de la vérité et de ses conséquences. »
Non pas que je sois particulièrement positiviste, pas plus d’ailleurs que ne l’est Bouveresse, mais il est symptomatique que le rejet en bloc des valeurs cardinales de la science, ou du moins le sourire désabusé qui suit inévitablement leur simple mention, aille souvent de pair avec cette haine catégorique du positivisme; un sourire cynique témoin s’il en est que l’on se débat encore bien malgré nous avec de vieilles catégories largement dépassées, que l’on n’est plus dans le coup, pas assez radical, voire franchement inconscient de notre propre ignorance de la réalité intéressée du monde social.

Jean-Jacques Rosat, dans sa préface à la traduction française de Paul Boghossian, « La Peur du Savoir, sur le relativisme et le constructivisme de la connaissance », écrivait:

> Les idées du constructivisme de la connaissance sont étroitement liées à des courants progressistes comme le postcolonialisme et le multiculturalisme : elles fourniraient des armes philosophiques pour protéger les cultures opprimées.
> Mais, même d’un point de vue strictement politique, ce n’est pas très judicieux.
> Car, si les puissants ne peuvent plus critiquer les opprimés parce que les catégories du savoir sont inévitablement liées à des perspectives particulières, il s’ensuit également que les opprimés ne peuvent plus critiquer les puissants.
> Voilà qui menace d’avoir des conséquences profondément conservatrices.

## La science n’est-elle qu’une forme contemporaine de religion?

<!-- TODO: la fixation de la croyance, Peirce. -->

Une analogie filée tout au long du discours d’Hervé Philippe était que la science a pour le profane actuel le statut d’une religion.
On croit en la science comme l’on croyait autrefois en Dieu; on croit ce que disent ses prêtres, les scientifiques, comme l’on croyait autrefois les prêtres et les pasteurs.

Voilà qui procède d’un principe pour le moins contestable, selon lequel la méthode scientifique et la méthode théologique reposent sur les mêmes valeurs fondamentales.

J’ai entendu pendant le débat que la théologie était finalement relativement débattue, que les croyants doutent, remettent en question régulièrement les principes mêmes de leur foi.
Ce avec quoi je ne suis pas en désaccord, à ceci près que la nature du débat est bien différente de celle du débat scientifique.
Car celui-ci tend à se fonder sur l’interrogation du monde, dans une démarche d’enquête (_inquiry_) qui procède par essais et erreurs, avec au cœur de la démarche un faillibilisme foncier, selon lequel on est toujours en position de se tromper, quand bien même on prétendrait à la vérité.
Le débat scientifique est par nature intrinsèquement réaliste; il postule qu’il y a là-dehors un monde indépendant de ce que l’on en pense et de ce que l’on en dit.
Toute opposition à ce principe se fonde sur l’adhésion à d’autres catégories de pensées, nominalistes celles-là, selon lesquelles le monde n’est autre que ce que l’on dit de lui.
Tout discours sur des faits, sur le réel, sur le vrai, sur la vérité prend pour base l’idée qu’il y a quelque chose là-dehors indépendant de nous, qu’on peut le découvrir et l’interroger, entre autres méthodes par la science.
Ça n’est pas dire que la science est purement objective; ça n’est pas dire que la science dit la vérité; simplement que le but de toute science est d’atteindre à un idéal tel que le vrai.

C’est là un point de départ important avec le débat théologique, qui par nature ne peut se reposer sur des faits.
Lorsqu’il y a débat, c’est plus sur la force de persuasion des arguments que sur leur éventuelle « correspondance » à des faits (j’ai bien conscience que ce correspondantisme est largement débattu en ce qui concerne la vérité, mais ça n’est pas l’objet ici).

On rejoint là l’engagement rationaliste.
Prétendre atteindre à la vérité, voilà qui implique pour base des idéaux pour lesquels il faut se battre.
S’il faut l’atteindre, ça ne peut être seul; on ne peut parvenir à démêler le vrai du faux par la seule entreprise individuelle.
C’est donc qu’il faut s’engager dans l’idée qu’il est possible que d’autres partagent les mêmes valeurs, qu’il soit possible de les convaincre sur une base rationnelle, en leur donnant des _raisons_ de ne pas douter, ou au contraire des raisons de douter.
C’est l’espoir de la démocratie deweyenne comme un _espace des raisons_, dans lequel l’on se donnerait les moyens de construire un espace démocratique rationnellement fondé, basé sur l’expérience, sur l’observation, sur la critique rationnelle.

Ce sont toutes catégories proprement réactionnaires pour les partisans de la déconstruction, pour lesquels ou bien il n’existe rien de tel que la raison, ou bien, lorsqu’ils l’admettent, constitue-t-elle une catégorie de domination et de pouvoir des dominants sur les dominés.
Pourtant c’est bien par la raison que j’ai l’espoir que l’on puisse fonder la subversion de la domination actuelle, dans un monde empêtré dans l’ère de la post-vérité, des fake-news (autrement dit des mensonges), bref d’une vérité comme catégorie évanescente qui ne sert les intérêts que des puissants, des oligarques.

Dans le contexte de la crise écologique, il me semble particulièrement important de rappeler que la science repose sur des valeurs différentes de la religion, que ces valeurs sont peut-être les seules qui restent aux dominés pour essayer de fonder une critique subversive, les seules par lesquelles on peut avoir espoir de fonder une démocratie objectivement construite de façon à minimiser les inégalités, la domination, l’asservissement et l’abrutissement des dominés par les dominants (et l’on peut inclure les écosystèmes dans la catégorie des dominés).
Dire que le discours scientifique n’a pas plus de valeur que le discours religieux, puisque finalement la science n’est rien de plus qu’une forme de religion contemporaine, c’est précisément donner des armes à ceux qui prétendent que la crise écologique n’est qu’une vérité construite, notamment par ces scientifiques qui poursuivent un [« agenda politique »](https://www.bbc.com/news/world-us-canada-45859325).

Ça n’est pas dire que la science telle qu’elle se pratique correspond à ce à quoi elle devrait se conformer si elle était pensée réellement selon ses valeurs cardinales.
C’est bien pourquoi il faut également se battre pour que le monde scientifique ne soit pas asservi aux intérêts franchement utilitaristes d’une gouvernance défendant au moins implicitement un modèle où la science permettrait d’insuffler, par l’innovation technologique, un nouvel élan économique à des pays occidentaux ayant depuis longtemps délégué quasiment toute ambition industrielle à d’autres régions aux codes du travail résolument plus « souples ».
En tant que champ, l’activité scientifique encourage de plus en plus au productivisme généralisé, et l’on sait bien les dérives de la bibliométrie, du fonctionnement de la recherche en projets (ANR), de l’incitation par l’avancement de carrière à la productivité à tout crin, si tant est qu’elle soit mesurable.
Encore un pan entier de la lutte qu’il s’agit de mener, mais que l’on ne pourra mener si l’on abandonne tout espoir de fonder le discours sur la raison, sur l’argument, sur le débat rationnel, et qu’on refuse cyniquement à l’autre toute prétention à la vérité parce qu’il ou elle n’est mue que par ses intérêts propres ou ceux de sa classe.

## Pour un rationalisme engagé

Georges Canguilhem, l’un des grands noms de la philosophie de la biologie et de l’histoire des sciences, maître d’une liste interminable d’autres grands noms, incluant entre autres Bourdieu, Lecourt, Foucault, Dagognet, Gayon, Limoges, disait dans la brève préface qu’il a écrit pour ce recueil de texte de Gaston Bachelard:

> Pour Bachelard il s'agit d'un engagement pour la raison, contre cette forme de rationalisme, sorte de superstition scientifique, expression béate d'un premier succès de rationalisation.
> Il est si vrai que le rationalisme de Gaston Bachelard est la contestation d'un rationalisme euphorisant qu'il invente un terme pour l'en distinguer, celui de « surrationalisme », qu'il fait appel à l'agressivité de la raison, systématiquement divisée contre elle-même.
> Le rationalisme polémique est autrement radical que la polémique rationaliste, souvent limitée par un compromis inconscient avec l'objet de sa critique.
> Pour espérer devenir rationaliste il faut plus qu'un souci de dévalorisation des préjugés, il faut la volonté de valoriser la dialectique du déjugement.

À mon sens il est temps de dépasser ce stade euphorisant où l’on pratique une forme d’auto-flagellation étrangement jouissive, où l’on rappelle à quel point nos intérêts sont peu avouables, à quel point l’on est mû par des motifs égoïstes ou narcissiques, à quel point le savoir que l’on mobilise est _construit_ et détaché de tout lien avec une réalité qui existerait indépendamment de nous.
Je crois reconnaître en cela ce que Wittgenstein disait de la psychanalyse: qu’elle est moins la science qu’elle voudrait être qu’un discours auquel on reconnaît après coup une forme de cohérence, cohérence à laquelle on attribue une force explicative systématiquement réductrice.

D’ailleurs si l’on voulait retourner leur cynisme contre eux, il suffirait de souligner que cette prétention à vouloir réduire les conduites scientifiques à la poursuite d’intérêts égoïstes n’est qu’une tentative de prise de pouvoir symbolique sur les agents auxquels elle s’adresse: « je vais vous dire ce à quoi vous êtes _vraiment_ à l’œuvre »; au nom de quoi serais-je bien censé croire au relativisme, si le relativisme lui-même n’est qu’une histoire de perspective?
C’est le fameux caractère auto-réfutatoire du relativisme extrême.
Je suis certain qu’il y a de la place pour une autre vision du monde social, telle que proposée entre autres par Bourdieu, qui dissipe d’un coup toute réduction de l’explication des conduites à une fausse dichotomie individu/société.

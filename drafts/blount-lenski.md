---
title: Contingence et déterminisme dans l'évolution
subtitle: Rejouer la piste de la Vie
author: Blount, Lenski & Losos
translator: Samuel Barreto
date: 2018-11-09
drafts: true
---


Les processus historiques sont « contingents » dans une certaine
mesure, ce qui signifie que leurs résultats sont sensibles à des
évènements en apparaissent anodins qui peuvent changer
fondamentalement le futur. La contingence est ce qui rend les
résultats de l'histoire imprédictibles. Contrairement à la plupart des
phénomènes naturels, l'évolution est un processus historique. Le
changement évolutif est souvent le produit des forces déterministiques
de la sélection naturelle, mais la sélection opère sur des variations
qui surviennent de façon imprédictible au cours du temps par la
mutation aléatoire, et même les mutations bénéfiques peuvent être
perdues aux hasards de la dérive génétique. De plus, l'évolution a eu
lieu dans un environnement planétaire avec son histoire propre. Cette
tension entre le déterminisme et la contingence fait de la biologie
évolutive une sorte d'hybride entre la science et l'histoire. Alors
que les philosophes des sciences scrutent les nuances de la
contingence, les biologistes ont réalisé de nombreuses études
empiriques de répétabilité et contingence évolutive. Ici, nous passons
en revue les preuves expérimentales et comparatives de ces travaux.
Les populations répliquées dans les expériences de « replay »
évolutive montrent souvent des changements parallèles,
particulièrement en performances globales, quoique des résultats
idiosyncratiques montrent que les particularités de l'histoire d'une
lignée peut affecter le chemin évolutif emprunté. Les biologistes
comparatifs ont trouvé de nombreux exemples notables d'adaptations
convergentes à des conditions similaires, mais il est difficile de
quantifier la fréquence à laquelle de telle convergences surviennent.
En somme, les preuves indiquent que l'évolution tend à être
étonnamment répétables entre des lignées proches, mais que ses
résultats diffèrent est plus probable lorsque l'emprunte de l'histoire
se fait plus profonde. Les recherches en cours sur la structure des
paysages adaptatifs montrent de nouvelles découvertes quant
aux relations complexes entre destin et hasard dans le processus
évolutif.

Le monde dans lequel nous vivons --- dans toute sa splendeur, ses
tragédies et ses étrangetés --- est le produit d'une vaste toile
entremêlée d'événements que nous appelons l'histoire. Si l'histoire
avait pris une autre route, le monde d'aujourd'hui serait différent.
En effet, la trace historique est pleine d'accidents et de
coïncidences qui façonnent le cours des évènements, de tournants
critiques du destin comme de mauvais virages ou des voitures en panne
ayant contribué au début de guerres, des chutes de cigares en ayant
changé l'issue, ou de mutations qui contribuent à la chute d'empires.
Ces exemples illustrent une propriété de l'histoire appelée
« contingence », qui en rendent l'issue sensible aux détails des
évènements en interaction qui conduisent à elle. La contingence est la
raison pour laquelle, bien que certaines tendances soient prédictibles
sur le long terme et que le passé soit explicable, le futur ne peut
être connu.

Contrairement à la plupart des phénomènes naturels, l'évolution est un
processus historique, et la biologie évolutive est un domaine dans
lequel la science et l'histoire viennent nécessairement de pair. Tout
comme les historiens débatent de la mesure dans laquelle certains
évènements historiques étaient inévitables, des débats similaires ont
fait rage en biologie évolutive. Une personne fut particulièrement
influente à encourager les biologistes à appréhender le rôle de
l'histoire dans l'évolution: Stephen Jay Gould. Dans nombre de ses
écrits, et dans sa pleine mesure dans son livre _La vie est belle_ de
1989, Gould avançait que la contingence historique est au cœur de
l'évolution. Il assertait que le monde vivant est le produit d'une
histoire particulière, et que si cette histoire s'était déroulée
différemment, le monde d'aujourd'hui aurait été totalement différent
de celui que nous connaissons.

Dans _La vie est belle_, Gould illustre cela par une désormais célèbre
expérience de pensée de rejouer la cassette de la vie et d'y voir si
l'issue en aurait été la même. La conclusion de Gould était: « Rejouer
la piste un million de fois… et je doute que quoi que ce soit de tel
qu'_Homo sapiens_ évoluerait à nouveau. » Mais Gould déplorait, « la
mauvaise nouvelle est que nous ne pouvons envisager de réaliser cette
expérience ». Récemment, cependant, les biologistes de l'évolution ont
montré que l'expérience de Gould peut, en fait, être réalisée, au
moins à de petites échelles. En effet, un sous-domaine vibrant de
l'évolution expérimentale a réalisé de nombreuses expérience de
« replay » au laboratoire comme sur le terrain. De plus, de nombreux
paléontologistes et biologistes comparatifs soutiennent que
l'évolution naturelle a conduit de nombreuses expériences naturelles
qui peuvent être interprétées comme des expériences de « replay ». Ces
études empiriques offrent de nombreux aperçus des relations entre la
contingence et le déterminisme au cœur de l'évolution.

# "Rejouer la piste de la vie" et le sens de la « contingence »

Toute tentative de revue du corpus de la recherche empirique sur le
rôle de la contingence dans l'évolution doit d'abord traiter de deux
sources de confusion que Gould a lui-même introduit. La première vient
des incohérences dans la façon dont Gould décrit la métaphore du
« replay ». Comme souligné par le philosophe John Beatty, dans _La vie
est belle_, Gould décrit d'abord son expérience de pensée comme un
rembobinage strict de la cassette de la vie, à partir de conditions
initiales identiques, mais plus loin Gould s'interroge sur la façon
dont de légères variations auraient altéré l'issue. On peut discuter
de l'idée que Gould avait réellement en tête, mais nombre de citations
de _La vie est belle_ semblent suggérer qu'il pensait plutôt au
second scénario qu'au premier. Dans tous les cas, des chercheurs
différents ont conçu des tests de l'hypothèse de « replay » pour
chacun de ses deux versions alternatives, qui à la fois compliquent et
enrichissent la synthèse de leurs découvertes.

Gould a aussi introduit une confusion quant au sens de la contigence
elle-même. En dépit de sa centralité dans sa pensée, Gould ne définit
jamais formellement la contingence. Il en a donné diverses
descriptions informelles, mais elles tendent à être incomplètes ou
circulaires. De plus, il confond souvent les sens communs de
« contingence » comme « dépendre de quelque chose d'autre » et « un
évènement accidentel ou du fait du hasard ». D'autres ont tenté de
définir la contingence en se basant sur leurs interprétation des
travaux de Gould, et différents chercheurs ont, là encore, conçu des
travaux basés sur les différentes notions de contingence. Ces
définitions se réduisent largement à deux alternatives qui
correspondent aux différentes versions de la métaphore du « replay »:
imprédictibilité de l'issue à partir de conditions initiales
identiques, et dépendance causale de l'histoire conduisant à une
issue.

Les philosophes des sciences ont travaillé à clarifier et formaliser
le concept de contingence. Beatty souligne que la contingence signifie
en dernière analyse qu'une issue dépend d'une histoire dont il n'était
pas nécessaire qu'elle se passe ainsi. Desjardins a identifié cette
propriété a des systèmes dépendants du chemin, dans lesquels il
y a plusieurs chemins possibles depuis un état initial, plusieurs
issues possibles, et une « dépendance causale probabilistique » liant
les deux.

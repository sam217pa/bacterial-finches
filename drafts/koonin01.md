---
title: "The fundamentals of evolution"
subtitle: "Darwin and Modern Synthesis"
author: Eugene V. Koonin
translator: Samuel Barreto
date: 2019-03-07
draft: true
---

Dans ce chapitre et le suivant, j’entends fournir un bref résumé de
l’état de la biologie évolutive à l’aube de la génomique comparative
en 1995. Assurément, la tâche consistant à concentrer un siècle et
demi de pensée et de recherche en évolution, en deux brefs chapitres,
quasiment non-techniques est effarante, c’est le moins qu’on puisse
dire. Ceci dit, je crois que l’on peut commencer par se poser une
question évidente: Quel est le message à retenir de toutes ces
décennies d’études? L’on peut en rassembler une synthèse concise et
raisonnable de la synthèse évolutive prégénomique même si l’on omet
inévitablement la plupart de ses spécificités.

J’ai tenté de concilier histoire et logique dans ces deux premiers
chapitres, quoiqu’une dose d’arbitraire soit inévitable. Dans ce
chapitre, je retrace le développement conceptuel de la biologie
évolutive de _De l’Origine des Espèces_ de Charles Darwin à la
consolidation de la Synthèse Moderne dans les années 1950. Le chapitre
2 traite des concepts et découvertes ayant affecté la compréhension de
l’évolution entre l’achèvement de la Synthèse Moderne et la révolution
génomique des années 1990.

# Darwin et la première synthèse évolutive: grandeurs, limites et difficultés

Il est parfois étrange de contempler le fait que l’on ait juste
célébré le 150e anniversaire de la première publication de _de
L’Origine des Espèces_ de Darwin (Darwin, 1859) et le 200e jubilé de
Darwin lui-même. Compte tenu de la trace profonde et indélébile
laissée par l’_Origine_ dans l’ensemble de la science, la philosophie,
et la pensée humaine en général (bien au-delà des confins de la
biologie), 150 ans semble une durée bien courte.

En quoi exactement les changements de notre vision du monde
occasionnés par Darwin étaient-ils si remarquables et importants?
Darwin n’a pas découvert l’évolution (comme il est parfois
ouvertement, plus souvent implicitement, prétendu, particulièrement
dans le parler populaire et les débats publics). De nombreux érudits
avant lui, sommités d’alors y compris, croyaient que les organismes
changeaient au cours du temps de façon non-aléatoire. En dehors même
des grands philosophes grecques (quelque peu légendaires), Empédocle,
Parménide et Héraclite, et leurs contemporains indiens aux idées
étrangement prescientes (même si elles sont, chose étrange à nos yeux,
mélangées à la mythologie) sur le processus de changement dans la
nature, Darwin eut de nombreux prédécesseurs aux dix-huitième et
dix-neuvième siècles. Dans les éditions plus tardives de l’_Origine_,
Darwin reconnaît leurs contributions avec la candeur et la générosité
qui le caractérisent. Le propre grand-père de Darwin, Erasmus, et le
fameux botaniste et zoologiste français Jean-Baptiste Lamarck
(Lamarck, 1809) étudient l’évolution dans de volumineux tomes. Lamarck
présentait même une conception cohérente des mécanismes qui, selon
lui, perpétuent ces changements. De plus, le héros favori de Darwin,
son professeur et ami, le grand géologue Sir Charles Lyell, écrivit
sur la « lutte pour l’existence », au cours de quoi les plus féconds
l’emportent toujours. Et, bien sûr, il est bien connu que le jeune
contemporain de Darwin, Alfred Russell Wallace, simultanément proposa
essentiellement le même concept d’évolution et ses mécanismes.

Cependant, nonobstant les succès de ces évolutionnistes précoces, ce
fut Darwin qui posa les fondations de la biologie moderne et changea
à tout jamais la vision scientifique du monde dans l’_Origine_.
Pourquoi ses travaux sont-ils si originaux et décisifs? En analysant
rétrospectivement son tour de force, à 150 ans d’écart, trois
généralisations majeures semblent ressortir:

1. Darwin présenta sa vision de l’évolution dans une perspective
   totalement naturaliste et rationaliste, sans invoquer quelque force
   téléologique ou élan de perfection (ou carrément un créateur), ce
   que les théoriciens de son époque avaient coutume de faire.
2. Darwin proposa un mécanisme d’évolution spécifique, évident, et
   facile à comprendre, c’est-à-dire l’entre-jeu entre la variation
   héréditaire et la sélection naturelle, décrit conjointement comme
   la survie du plus apte.
3. Darwin étendit courageusement la notion de l’évolution à toute
   l’histoire du vivant, qu’il pensait pouvoir être représentée
   adéquatement par un grand arbre (la fameuse unique illustration de
   l’_Origine_), et postula même que toutes formes de vie partagent un
   commun ancêtre.

Ce puissant concept général vint en contraste net avec les idées
évolutionnistes des prédécesseurs, spécifiquement Lamarck et Lyell,
qui étudiaient d’abord, sinon seulement, le changement évolutif au
sein des espèces. Le quatrième succès de Darwin ne fut pas strictement
scientifique, mais plutôt pédagogique. En grande partie à cause de son
sentiment d’urgence causé, à raison, par la compétition avec Wallace,
Darwin présenta son concept d’un bref et lisible volume (même pour le
profane averti), quoique méticuleusement et précautionneusement
argumenté. Grâce à ces avancées majeures, Darwin est parvenu à changer
la face de la science, plutôt que simplement publier un autre livre.
Immédiatement après que l’_Origine_ fut publiée, la plupart des
biologistes et même le grand public éduqué y reconnurent une
description naturaliste crédible de la façon dont la diversité du
vivant observée a pu voir le jour, ainsi qu’une prometteuse fondation
sur laquelle s’appuyer.

En considérant le travail de Darwin du plan d’abstraction plus élevé
central à cet ouvrage, il faut souligner que Darwin semble avoir été
le premier à établir l’interaction crucial entre la chance et l’ordre
(la nécessité) dans l’évolution. Selon le concept Darwinien, la
variation est (quasiment) complètement aléatoire, alors que la
sélection introduit de l’ordre et crée la complexité. Sous ce rapport,
Darwin est diamétralement opposé à Lamarck, dont la vision du monde
exclut essentiellement le hasard. Nous reviendrons sur ce conflit de
perspectives clé à travers cet ouvrage.

En effet, avec tout le crédit dû à ses prédécesseurs géologues et
biologistes évolutionnistes, Darwin fut vraisemblablement le premier
savant à appliquer la possibilité de changement évolutif (et, par
conséquent, la possibilité de l’origine évolutive) de l’univers entier
au règne des phénomènes naturels sujets à l’étude rationnelle. En
d’autres termes, Darwin initia l’étude scientifique de _la flèche du
temps_ --- c’est-à-dire, de processus temporellement asymétriques,
irréversibles. Ce faisant, il prépara le terrain non seulement de tout
développement ultérieur en biologie, mais aussi de l’avènement de la
physique moderne. Je crois que le grand physicien Ludwig Boltzmann,
fondateur de la thermodynamique statistique et père du concept moderne
d’entropie, avait de bonnes raisons d’appeler Darwin « un grand
physicien », aussi paradoxal que cela puisse sembler, sachant que
Darwin ne savait quasiment rien de la physique et de la mathématique.
Le philosophe contemporain Daniel Dennett n’a peut-être pas tort
lorsqu’il suggère que l’idée de sélection naturelle de Darwin est sans
doute la meilleure idée jamais proposée (Dennett, 1996).

Assurément, la conception de l’évolution de Darwin au moment où fut
publiée l’_Origine_, et au moins pour le reste du dix-neuvième siècle,
se heurtait à de nombreux problèmes ayant grandement inquiété Darwin,
et apparaissaient parfois insurmontables à bien des scientifiques. La
première difficulté substantielle était l’estimation basse de l’âge de
la Terre prévalent à l’époque. En dehors des mythes de la création,
les meilleures estimations des physiciens du dix-neuvième siècle
(spécifiquement, Lord Kelvin) étaient proches de 100 million d’années,
un temps jugé insuffisemment long pour l’évolution du vivant par la
voie Darwinienne d’accumulation progressive de changements mineurs.
Clairement, c’était un jugement fondé --- la période de 100 million
d’années est bien trop courte pour que la diversité actuelle du vivant
ait évoluée, bien que personne n’ait alors une estimation quantitative
du taux d’évolution darwinien. Le problème fut résolu 20 ans après la
mort de Darwin. Au début du vingtième siècle, alors que la
radioactivité était découverte, les scientifiques calculèrent que le
refroidissement de la Terre à partir de son état chaud initial
prendrait des milliards d’années, précisément le temps que Darwin
pensait être requis pour l’évolution du vivant par sélection
naturelle.

Le deuxième problème, plus important celui-là, a trait au mécanisme de
l’hérédité, appelé « cauchemar de Jenkin ». À cause de ce que le
concept de déterminants héréditaires discrets n’existait pas
à l’époque de Darwin (en dehors d’obscurs articles de Mendel), il
n’était pas clair de quelle façon une variation bénéfique émergente
peut persister à travers les générations et être fixée dans les
populations en évolution sans être diluée et perdue. Darwin
apparemment n’a pas pensé à ce problème au moment d’écrire
l’_Origine_; un lecteur inhabituellement incisif, un ingénieur du nom
de Jenkin, l’informa de ce défi à sa théorie. Rétrospectivement, il
est difficile de comprendre pourquoi Darwin (ou Jenkin ou Huxley) n’a
pas pensé à une solution Mendélienne. À la place, Darwin en vint
à conception de l’hérédité plus extravagante, appelée pangénèse, que
lui-même ne semblait pas prendre très au sérieux. Ce problème fut
résolu par la (re)naissance de la génétique, bien que les conséquences
initiales pour le Darwinisme étaient inattendues (voir la section
suivante).

Le troisième problème dont Darwin était pleinement conscient et qu’il
a étudié brillamment était l’évolution de structures complexes (des
_organes_, dans les termes de Darwin) qui requièrent l’assemblage de
parties multiples pour accomplir leur fonction. Pareils organes
complexes posent l’énigme classique de la biologie évolutionniste au
vingtième siècle, dénommée de façon évocatrice « complexité
irréductible ». En effet, il n’est pas clair immédiatement en quoi la
sélection peut permettre l’évolution de pareils organes sous
l’hypothèse que les parties individuelles ou les assemblages partiels
sont inutiles. Darwin traite de ce problème frontalement dans l’un des
plus fameux passage de l’_Origine_, le scénario de l’évolution de
l’œil. La solution qu’il propose était logiquement impeccable,
plausible et ingénieuse: Darwin postula que les organes complexes
évoluent bien par une série d’étapes intermédiaires, dont chacune
accomplit une fonction partielle liée à la fonction éventuelle de
l’organe complexe évolué. Ainsi, l’évolution de l’œil, selon Darwin,
commence par un simple tissu photosensible et procède par structures
oculaires primitives d’utilité croissant discrètement jusqu’aux yeux
pleinement développés, complexes des arthropodes et vertébrés. Il faut
souligner que des structures photosensibles primitives semblables
à celles que postule Darwin sur des bases générales ont été
découvertes par la suite, validant au moins partiellement son scénario
et montrant que, dans ce cas, l’irréductibilité de l’organe complexe
est illusoire. Néanmoins, nonobstant le brio du schéma Darwinien, il
doit être pris pour ce qu’il est: un scénario spéculatif partiellement
soutenu pour l’évolution d’un organe complexe spécifique.
L’explication de Darwin propose une trajectoire possible pour
l’évolution de la complexité mais ne résout pas ce problème en
général. L’évolution de la complexité à différents niveaux est au cœur
de la compréhension de la biologie, aussi nous la reverrons
à plusieurs occasions dans cet ouvrage.

La quatrième difficulté pour le Darwinisme, est peut-être la plus
profonde. Ce problème majeur a trait au titre et au présumé sujet
central du livre de Darwin, l’origine des espèces et, plus
généralement, les événements évolutifs à grande échelle, qui sont
maintenant collectivement dénotés sous le terme de macroévolution.
Dans un écart assez frappant au titre du livre, tous les exemples
irréfutables d’évolution que Darwin présente impliquent l’émergence de
nouvelles variations au sein d’une espèce, non de nouvelles espèces,
encore moins de taxons supérieurs. Cette difficulté perdure bien après
la mort de Darwin et existe toujours maintenant, quoiqu’elle ait été
atténuée d’abord par les progrès de la paléontologie, ensuite par les
développements des théories de la spéciation soutenus par les données
biogéographiques, et ensuite, de façon plus convaincante, par la
génomique comparative (voir chapitres 2 et 3). À porter à son crédit,
et contrairement aux détracteurs de l’évolution jusqu’à ce jour,
Darwin a tenu bon face à ces difficultés, grâce à sa confiance
inébranlable en ce que, si incomplète que puisse être sa théorie, elle
n’a pas d’alternative rationnelle. Le seul signe de vulnérabilité que
Darwin montre est l’inclusion de l’improbable pangénèse dans les
éditions ultérieures de l’_Origine_, une mesure paliative au cauchemar
de Jenkin.

# La génétique et les « jours sombres » du Darwinisme

Une légende urbaine raconte que Darwin avait lu l’article de Mendel
mais l’a trouvé sans intérêt (peut-être en partie à cause de sa
connaissance limitée de l’Allemand). Il est difficile d’estimer dans
quelle mesure l’histoire de la biologie aurait été différente si
Darwin avait perçu le message de Mendel, qui nous semble si
élémentaire aujourd’hui. Cela ne devait toutefois pas être.

Sans doute de manière plus suprenante, Mendel lui-même, alors qu’il
était évidemment familier de l’_Origine_, n’a pas du tout inscrit ses
découvertes dans un contexte darwinien. Cette connexion vitale a du
attendre non seulement la redécouverte de la génétique à l’aube du
vingtième siècle, mais aussi l’essor de la génétique des populations
dans les années 1920. La redécouverte de l’hérédité mendélienne et la
naissance de la génétique aurait du constituer un formidable élan pour
Darwinisme, parce qu’en révélant le caractère discret des déterminants
de l’hérédité, ces découvertes éliminaient le cauchemar de Jenkin. Il
est donc totalement paradoxal que la réaction originelle de la plupart
des biologistes à la découverte des gènes fut de dire qu’elle ôtait
toute pertinence au concept de Darwin, bien qu’aucun scientifique
sérieux n’aurait l’idée de nier la réalité de l’évolution. La
principale raison à ce que la génétique soit jugée incompatible avec
le Darwinisme était que les fondateurs de la génétique, spécifiquement
Hugo de Vries, le scientifique le plus productif parmi les trois
redécouvreurs des lois de Mendel, voyait les mutations de gènes comme
des changements héréditaires abrupts, saltatoires, à l’encontre du
gradualisme Darwinien. Ces mutations étaient considérées comme un
attribut inaliénable du Darwinisme, en accord total avec l’_Origine_.
Par conséquent, de Vries voyait sa théorie mutationnelle de
l’Évolution comme « anti-darwinienne ». Aussi le centenaire de la
naissance de Darwin et le cinquantième anniversaire de l’_Origine_ en
1909 ne furent guère triomphaux, alors même que la recherche en
génétique déferlait et que Wilhelm Johansson introduisit le terme de
_gène_ cette même année.

# Génétique des populations, théorème de Fisher, paysages adaptatifs, dérive et brouillons

Les fondements de la synthèse d’importance critique entre le
Darwinisme et la génétique furent posées à la fin des années 1920 et
au début des années 1930 par le trio de brillants théoriciens de la
génétique: Ronald Fisher, Sewall Wright et J.B.S Haldane. Ils
employèrent un raisonnement mathématique et statistique rigoureux au
développement d’une description idéalisée de l’évolution de
populations biologiques. Le grand statisticien Fisher fut à l’évidence
le premier à voir que, loin de condamner le Darwinisme, la génétique
fournissait une fondation naturelle solide à l’évolution Darwinienne.
Fisher résume ses conclusions dans le livre fondateur de 1930 _La
Théorie Génétique de la Sélection Naturelle_ (Fisher 1930), un volume
dont l’importance en biologie évolutionniste ne cède peut-être qu’à
l’_Origine_. Ce fut le début d’un retour spectaculaire du Darwinisme
qui fut connu plus tard sous le nom de _Synthèse Moderne_ (un terme
essentiellement employé aux États-Unis) ou de _néo-Darwinisme_ (dans
les traditions britanniques et européennes.)

Il n’est ni nécessaire ni pratiquement réalisable de présenter ici les
bases de la génétique des populations. Toutefois, certaines
généralisations pertinentes à l’étude de la biologie évolutive
contemporaine peuvent être présentées succinctement. Pareil résumé,
quoique superficiel, se trouve en essence ici. Essentiellement, les
fondateurs de la génétique des populations ont pleinement réalisé le
fait que l’évolution n’affecte pas les organismes isolés ou
d’abstraites espèces, mais plutôt des groupes concrets d’individus
interféconds, appelés populations. La taille et la structure des
populations évoluant détermine grandement la trajectoire et l’issue de
l’évolution. En particulier, Fisher formule et prouve le théorème
fondamental de sélection natruelle (communément désigné sous le terme
de théorème de Fisher), qui établit que l’intensité de la sélection
(et partant, le taux d’évolution dû à la sélection) est proportionnel
à l’amplitude de la variation génétique présente dans une population
en évolution, qui à son tour est proportionnelle à la taille de
population effective.

L’encadré 1-1 fournit les définitions de base et les équations qui
déterminent les effets de la mutation et la sélection sur
l’élimination ou la fixation d’allèles mutants, dépendant de la taille
de population effective. L’essentiel d’un point de vue qualitatif est
que, à taux de mutation égal, dans une population de grande taille
effective, la sélection est intense. Dans ce cas, même des mutations
à coefficient de sélection faiblement positif (mutations
« légèrement » bénéfiques) parviennent rapidement à fixation. De
l’autre côté, même des mutations à faible coefficient de sélection
négatif (légèrement délétères) sont promptement éliminées. Cet effet
s’est vu rigoureusement démontré par le théorème de Fisher.

> **Encadré 1-1** Les relations fondamentales définissant le rôle de
> la sélection et de la dérive dans l’évolution des populations:
>
> Évolution quasi-neutre dominée par la dérive:
>
> $1/Ne >> \abs{s}$
>
> Évolution dominée par la sélection:
>
> $1/Ne << \abs{s}$
>
> Régime mixte, où dérive et sélection sont importantes:
>
> $1/Ne \approx \abs{s}$
>
> $Ne$: taille de population effective (typiquement,
> substantiellement moins que le nombre d’individu d’une population
> puisque tous ne produisent pas de descendance viable)
>
> $s$: coefficient de sélection ou effet en _fitness_ d’une mutation:
>
> $s = F_{A} - F_{a}$
>
> $F_{A}$, $F_{a}$: valeur en _fitness_ de deux allèles d’un gène
>
> $s > 0$: mutation bénéfique
>
> $s < 0$: mutation délétère

Un corollaire du théorème de Fisher est que, en supposant que la
sélection naturelle guide toute évolution, _la _fitness_ d’une
population ne peut décroître au cours de l’évolution_ (c’est-à-dire si
la population est censée survivre). C’est probablement par l’image
d’un _paysage adaptatif_ que l’on peut le mieux se le représenter,
lequel fut d’abord introduit par un autre père fondateur de la
génétique des populations, Sewall Wright. Lorsque ses mentors lui
demandèrent de présenter les résultats de ses analyses mathématiques
de la sélection sous une forme accessible aux biologistes, Wright en
vint à proposer cette image particulièrement heureuse. La force et la
simplicité de la représentation en paysage de l’évolution de la
_fitness_ a survécu jusqu’à ce jour et a stimulé de nombreuses études
ultérieures en ayant engendré des théories et versions beaucoup plus
sophistiquées et beaucoup moins intuitives, y compris
multidimensionnelles (Gavrilets, 2004). Selon le théorème de Fisher,
une population évoluant seulement par sélection (techniquement, une
population de taille infinie --- les populations infinies n’existent
assurément pas, mais c’est une abstraction pratique employée de façon
routinière par les généticiens des populations) ne peut jamais
descendre d’une colline du paysage adaptative (voir Figure 1-1). Il
est facile de comprendre qu’un paysage adaptatif, tout comme un
paysage réel, peut prendre plusieurs formes. Dans certaines
circonstances particulières, le paysage peut être extrêmement lisse,
avec un pic unique correspondant au maximum global de _fitness_
(parfois poétiquement appelé un paysage du Mont Fujiyama; voir Figure
1-1A). Plus souvent toutefois, le paysage est vallonné, avec de
multiples pics de hauteurs variées séparées par des vallées (voir
Figure 1-1B). Comme le théorème de Fisher le décrit formellement (et
tout à fait dans la veine de Darwin), une population évoluant par
sélection peut se déplacer dans le paysage seulement de façon
ascendante et dont atteindre le pic local, même si sa hauteur est bien
moindre que celle du pic global (voir Figure 1-1B). Selon Darwin et la
Synthèse Moderne, le mouvement à travers les vallées est exclu parce
qu’il implique une composante descendante. Néanmoins, le développement
de la génétique des populations et ses conséquences pour le processus
évolutif a changé cette agréable image par le concept de _dérive
génétique_, un concept clé de la biologie évolutive que Wright
introduisit également.

Comme souligné plus tôt, Darwin reconnaissait à la chance un rôle
crucial dans l’évolution, mais le restreignait à une partie seulement
du processus évolutif: à l’émergence de variations (mutations, dans le
parler actuel). Le reste de l’évolution était vu comme un domaine de
nécessité déterministe, où la sélection fixe les mutations
avantageuses et les autres sont éliminées sans plus de conséquences
à long terme. Toutefois, quand la dynamique des populations est entré
en jeu, la situation a totalement changé. Les fondateurs de la
génétique quantitative des populations capturèrent par des formules
simples la dépendance de l’intensité de la sélection à l’égard de la
taille de population et du taux de mutation (voir encadré 1-1 et
figure 1-2). Dans une grande population au taux de mutation élevé, la
sélection est efficace, et même une mutation légèrement avantageuse
est presque certainement fixée (dans une population infinie, une
mutation au coefficient de sélection positif infinitésimal est fixé de
façon déterministe). Wright compris qu’une petite population,
particulièrement lorsqu’elle a un faible taux de mutation, est bien
différente. Ici la dérive génétique aléatoire joue un rôle crucial en
évolution, par laquelle des mutations neutres ou même délétères (mais,
bien sûr, non-léthales) sont souvent fixées par pur hasard.
Assurément, par dérive, une population évoluant peut violer le
principe d’un mouvement dans le paysage adaptatif uniquement
ascendant, et peut glisser le long d’un pic (voir figure 1-2). La
plupart du temps, ceci résulte en un mouvement descendant et une
extinction subséquente, mais si la vallée séparant le pic local d’un
autre, possiblement plus élevé, est étroite, alors deviennent
possibles la traversée d’une vallée et l’ascension d’un sommet
possiblement plus élevé (voir figure 1-2). L’introduction de la notion
de dérive dans le discours évolutionniste est central à mon récit. Ici
la chance fait son entrée en scène à une échelle nouvelle: Bien que
Darwin et ses successeurs immédiats aient perçu le rôle de la chance
dans l’émergence de variations héréditaires (mutations), la dérive
introduit la chance à l’étape suivante --- c’est-à-dire, la fixation
de ces variations --- et ôte une part de responsabilité à la
sélection. J’explore à quel point le rôle de la dérive est important
dans diverses situations de l’évolution à travers ce livre.

John Maynard Smith, et plus tard, John Gillespie développèrent la
théorie et le modèle informatique démontrant l’existence d’un mode
d’évolution neutre distinct, qui n’est que faiblement dépendant de la
taille de population effective et donc pertinente même en populations
infinies où la sélection est forte. Cette forme de fixation neutre de
mutations devint célèbre sous le nom d’_ébauche génétique_ (_genetic
draft_) et fait référence aux cas où une ou plusieurs mutations
neutres ou même faiblement délétères se répandent dans une population
et sont éventuellemetn fixées en raison de leur liaison avec une
mutation bénéfique: L’allèle neutre ou délétère se répand par
auto-stop avec l’allèle avantageux lié (Barton 2000). Certaines
données et modèles de génétique des populations semblent suggérer que
l’ébauche génétique est même plus importante pour l’évolution de
populations sexuées que la dérive. À l’évidence, l’ébauche génétique
est causée par les effets conjoints de la sélection naturelle et de la
variation neutre à différents sites génomiques, et, contrairement à la
dérive, peut se produire même dans de grandes populations infinies
(Gillespie, 2000). L’ébauche génétique pourrait permettre même à de
grandes populations de fixer des mutations faiblement délétères, et
partant, de les douer du potentiel de traverser les vallées du paysage
adaptatif.

# Sélection positive et purificatrice (négative): classifier les formes de sélection

Darwin pensait la sélection naturelle d’abord en termes de fixation de
variations bénéfiques. Il compris que l’évolution éliminait les
variations délétères, mais ne l’a pas interprété sur le même plan que
la sélection naturelle. Au cours de l’évolution de la Synthèse
Moderne, la notion de sélection fut étendue de façon à inclure la
sélection « purificatrice » (négative); à certains stades d’évolution,
elle s’avère être plus fréquente (de plusieurs ordres de grandeurs en
fait) que la sélection positive « Darwinienne ». En essence, la
sélection purificatrice est le processus par défaut d’élimination des
inaptes. Néanmoins, définir ce processus comme une forme spéciale de
sélection semble fondée et importante en ce qu’elle souligne le rôle
crucial de l’élimination dans le modelage (la contrainte) de la
diversité biologique à tous les niveaux. Dit simplement, la variation
n’est permise que si elle ne confère pas d’inconvénient significatif
à tout variant survivant. Dans quelle mesure ces contraintes limitent
réellement l’espace accessible à l’évolution est une question
intéressante et toujours ouverte, dont je dirai quelques mots plus
loin (voir en particulier les chapitres 3, 8 et 9).

Une différence subtile mais substantielle existe entre la sélection
purificatrice et la _sélection stabilisatrice_, laquelle est une forme
de sélection définie par ses effets sur la distribution en fréquence
des valeurs de traits. Cette forme comprends la sélection
stabilisatrice reposant principalement sur la sélection purificatrice,
la sélection directive portée par sélection positive (Darwinienne), et
les régimes quelques peu exotiques de sélection pertubatrice et
équilibrante résultant de combinaisons de contraintes multiples (voir
Figure 1-3).

# La Synthèse Moderne

L’unification de l’évolution Darwinienne et de la génétique aboutie
d’abord dans les travaux séminaux de Fisher, Wright et Haldane prépara
le terrain de la Synthèse Moderne de la biologie évolutionniste. Le
terme lui-même provient du livre éponyme de 1942 par Julian Huxley
(Huxley, 2010), mais on considère que le cadre conceptuel de la
Synthèse Moderne ne s’est rigidifié qu’en 1959, à l’occasion du
centenaire de l’_Origine_. Cette nouvelle synthèse fut elle-même le
produit de nombreux scientifiques de renom. Les architectes en chef de
la Synthèse Moderne furent vraisemblablement le généticien
expérimental Theodosius Dobzhansky, le zoologiste Ernst Mayr, et le
paléontologue George Gaylord Simpson. Le domaine de compétence
expérimentale de Dobzhansky, la mouche du vinaigre _Drosophila
melanogaster_, fournit le crucial matériau soutenant la théorie de la
génétique des populations et fut la première validation expérimentale
à grande échelle du concept de sélection naturelle. Le livre de
Dobzhansky _Genetics and the Origin of Species_ (Dobzhansky, 1951) est
le principal manifeste de la Synthèse Moderne, dans lequel il définit
l’évolution de façon restrictive au « changement de la fréquence
d’allèle d’un ensemble de gènes. » Dobzhansky déclara également cette
fameuse idée que _rien en biologie ne fait sens en dehors de
l’évolution_^[_Nothing in biology makes sense except in the light of
evolution_, NdT] (voir aussi l’appendice A pour aller plus loin sur
« faire sens »). Ernst Mayr, plus que tout autre scientifique, doit
être crédité d’une tentative pénétrante et extrêmement influente de
proposer un cadre théorique à la quête originelle de Darwin, l’origine
des espèces. Mayr formule le dénomme concept d’espèce biologique,
selon quoi la spéciation se produit lorsque deux populations (sexuées)
sont isolées l’une de l’autre par suffisamment longtemps pour que
l’incompatibilité génétique soit irréversible (Mayr, 1963). Simpson
reconstruisit l’image la plus exhaustive (en son temps) de l’évolution
du vivant basée sur les archives fossiles (Simpson, 1983).
Étonnamment, Simpson reconnaît la prévalence de la stase dans
l’évolution de la plupart des espèces et le remplacement subit
des espèces dominantes. Il introduisit également le concept
d’évolution quantique, qui présage du concept d’équilibre ponctué de
Stephen Jay Gould et Niles Eldredge (voir chapitre 2).

Le renforcement de la Synthèse Moderne dans les années 1950 fut un
processus quelque peu étrange qui compris un remarquable
« durcissement » (selon l’expression de Gould) des idées principales
de Darwin (Gould, 2002). Ainsi, le doctrine de la Synthèse Moderne
dans les faits a mis de côté le concept de dérive génétique aléatoire
de Wright et son importance évolutive, et resta sans compromis
possible pan-adaptationniste. De façon presque similaire, Simpson
lui-même abandonna l’idée d’une évolution quantique, de sorte que le
gradualisme resta l’un des piliers non controversés de la Synthèse
Moderne. Ce « durcissement » a modelé la Synthèse Moderne en un cadre
conceptuel relativement restreint, dogmatique à bien des égards.

Avant de continuer la discussion de l’évolution de la biologie
évolutionniste plus avant et sa transformation à l’âge de la
génomique, il semble nécessaire de récapituler succinctement les
principes fondamentaux de l’évolution que Darwin formule d’abord, que
la première génération de biologistes de l’évolution amende ensuite,
et que la Synthèse Moderne codifie finalement. Nous revenons à chacun
de ces points cruciaux à travers le livre.

1. La variation non-dirigée, aléatoire est le principal processus qui
   fournit le matériau de l’évolution. Darwin fut le premier
   à accorder à la chance le statu de facteur majeur de l’histoire du
   vivant, et ce fut probablement l’une de ses plus grandes
   découvertes. Darwin reconnaissait aussi un rôle subsidiaire
   à la variation dirigée, de type Lamarckienne, et tendit à accorder
   plus de poids à ces mécanismes dans les éditions tardives de
   l’_Origine_. La Synthèse Moderne, toutefois, est inflexible dans
   son insistance sur le fait que la mutation aléatoire est la seule
   pertinente source de variabilité évolutive.
2. L’évolution procède par fixation de rares variations bénéfiques et
   l’élimination de variations délétères. C’est le processus de
   sélection naturelle, qui, avec la variation aléatoire, est la
   principale force directrice de l’évolution, selon Darwin et la
   Synthèse Moderne. La sélection naturelle, qui est évidemment proche
   de, et inspirée de, la « main invisible » du marché gouvernant
   l’économie selon Adam Smith, est le premier mécanisme d’évolution
   jamais proposé qui était tout à la fois simple et plausible et ne
   requérait pas de tendances innées mystérieuses. Partant, ce fut la
   seconde découverte majeure de Darwin. Sewall Wright souligna que le
   hasard peut jouer un rôle substantiel dans la fixation de
   variation au cours de l’évolution plutôt que seulement à leur
   émergence uniquement, par la dérive génétique entraînant la
   fixation aléatoire de variations neutres ou même modérément
   délétères. La théorie de la génétique des populations indique que
   la dérive est particulièrement importante dans les petites
   populations qui traversent des goulot d’étranglement. L’ébauche
   génétique (l’auto-stop) est une autre forme de fixation
   stochastique de mutations non-bénéfiques. Cependant, _la Synthèse
   Moderne dans sa forme « rigide » rejetta en fait le rôle des
   processus aléatoires dans l’évolution au-delà de l’origine de
   variation et adhère à une vision pûrement adaptationniste
   (pan-adaptationniste) de l’évolution_. Ce modèle conduisit
   inévitablement au concept de « progrès », l’amélioration graduelle
   d’« organes » au cours de l’évolution. Darwin adopte cette idée
   comme une tendance générale, en dépit de la pleine conscience qu’il
   avait du fait que les organismes sont moins que parfaitement
   adaptés, comme l’exemplifient étonnement les organes rudimentaires,
   et en dépit de son aversion pour toute ce qui ressemble à un élan
   inné vers la perfection, dans une veine Lamarckienne. La Synthèse
   Moderne bannit le progrès en tant que concept anthropomorphique
   mais maintient néanmoins que l’évolution, en général, procède des
   formes simples aux formes complexes.
3. Les variations bénéfiques fixées par la sélection naturelle sont
   infinitésimaux (dans le parler moderne, les mutations pertinentes
   du point de vue de l’évolution sont supposée avoir des effets en
   _fitness_ infinitésimaux), aussi l’évolution se produit-elle par
   accumulation graduelle de petites modifications. Darwin insistait
   sur le _gradualisme strict_ comme un élement incontournable de sa
   théorie: « La sélection naturelle ne peut agir que par préservation
   et accumulation de variations héréditaires infinitésimales,
   profitant chacune à l’être préservé. … S’il pouvait être démontré
   qu’un organe complexe existait, qui n’ait pas été le produit de
   nombreuses modifications successives, ma théorie s’effondrerait
   absolument. » (_de l’Origine des Espèces_, chapitre 6). Même
   certains de ses contemporains pensait qu’il s’agissait d’une
   condition par trop stricte de la théorie. En particulier, les
   objections précoces de Thomas Huxley sont célèbres: Avant même la
   publication de l’_Origine_, Huxley écrit à Darwin, « Vous vous
   embarasser d’une difficulté indue en adoptant sans réverve le
   _Natura non facit saltum_ » (http://aleph0.clarku.edu/huxley). Sans
   tenir compte de ces alertes précoces ni même du concept d’évolution
   quantique de Simpson, la Synthèse Moderne embrassa le gradualisme
   sans compromission possible.
4. Un aspect de la biologie évolutive classique qui a trait à, mais
   aussi se distingue du principe du gradualisme est
   l’_uniformitarisme_ (emprunté par Darwin à la géologie de Lyell).
   C’est la croyance que le processus évolutif est essentiellement
   resté le même au cours de l’histoire de la vie.
5. Ce principe clé est logiquement lié au gradualisme et
   à l’uniformitarisme: La _macroévolution_ (l’origine des espèces et
   des taxons supérieurs) procède des même mécanismes que la
   _microévolution_ (l’évolution au sein des espèces). Dobzhansky, par
   sa définition de l’évolution comme changement des fréquences
   d’allèles d’une population, fut le principal partisan de ce
   principe. Darwin n’emploi pas les termes de _microévolution_ ou
   _macroévolution_; toutefois, que les processus intraspécifiques
   suffisent à expliquer l’origine des espèces et, plus généralement,
   toute l’évolution du vivant peut être considéré comme central
   à l’axiome Darwinien (ou peut-être un théorème fondamental, mais
   dont Darwin n’avait pas le moindre début de preuve). Il paraît
   raisonnable de qualifier de principe d’« uniformitarisme
   généralisé »: _Les processus de l’évolution sont les mêmes non
   seulement au long de l’histoire du vivant, mais aussi aux
   différents niveaux de transformation évolutive, y compris les
   transitions majeures_. Le dilemme de la microévolution contre la
   macroévolution est, à certains égards, le pivot de la biologie
   évolutive, aussi nous le reverrons à maintes reprises à travers ce
   livre.
6. L’évolution du vivant peut être représentée fidèlement par un
   « grand arbre », comme le souligne l’unique illustration de
   l’_Origine_ (au chapitre 4). Darwin introduit le concept de l’arbre
   du vivant (_Tree of Life_, TOL) seulement comme un concept générale
   et n’a pas tenté de chercher l’ordre réel des branches. L’arbre fut
   peuplé de formes de vie réelles, au mieux de la connaissance
   d’alors, par le partisan majeur du Darwinisme allemand, Ernst
   Haeckel. Les fondateurs de la Synthèse Moderne n’étaient pas
   particulièrement intéressés par l’arbre du vivant, mais l’ont
   assurément adopté comme une représentation de l’évolution des
   animaux et des plantes que la trace fossile soutient amplement au
   vingtième siècle. Par contre, les microbes qui étaient de plus en
   plus reconnus comme de majeurs agents écologiques restèrent en fait
   en dehors de la portée de la biologie évolutive.
7. Un corollaire de l’arbre du vivant unique mérite le statut de
   principe distinct: Toute l’étendue de la diversité du vivant évolue
   d’un ancêtre commun unique (ou très peu de formes ancestrales,
   selon la formule précautionneuse de Darwin au chapitre 14 de
   l’_Origine_; voir Darwin, 1959). Bien des années plus tard, cet
   ancêtre fut nommé le dernier ancêtre commun (cellulaire) universel
   (_Last Universal Common Ancestor_, LUCA). Pour les architectes de
   la Synthèse Moderne, l’existence de LUCA n’était guère en doute,
   mais ils ne semblent pas avoir considéré la résolution de sa nature
   comme un but scientifique important ou réaliste.

# Résumé

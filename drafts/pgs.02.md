---
title: "La sélection naturelle et sa représentation "
author: "P. Godfrey-Smith"
translator: "Samuel Barreto"
date: 2019-04-15
draft: true
---

# Résumés et Recettes

> [_La tradition « classique »; le rôle des abstractions; Weismann, Lewontin et Ridley; résumés sous forme de recettes; les deux chemins de la compréhension._]

Une manière d’appréhender l’évolution par sélection naturelle est de tenter de donner un résumé abstrait de ce qui lui est essentiel.
Cette tradition remonte aux discussions originelles de Darwin, mais est récemment devenue plus prégnante.
Je m’attèle à décrire et poursuivre cette tradition.

Darwin n’a pas débuté l’_Origine des Espèces_ de cette façon.
Il commence plutôt par les phénomènes empiriques, puis s’achemine graduellement vers ses énoncés théoriques.
Mais vers le milieu, et particulièrement à la fin de l’_Origine_, il commence à se résumer.
Le résumé le plus explicite se trouve au dernier paragraphe du livre.
Darwin y voit le changement par sélection naturelle comme conséquence de quelques « lois » naturelles simples.

> Ces lois, prises dans leur sens le plus large, sont: la loi de croissance et de reproduction; la loi d’hérédité qu’implique presque la loi de reproduction ; la loi de variabilité, résultant de l’action directe et indirecte des conditions d’existence, de l’usage et du défaut d’usage; la loi de la multiplication des espèces en raison assez élevée pour amener la lutte pour l’existence, qui a pour conséquence la sélection naturelle, laquelle détermine la divergence des caractères, et l’extinction des formes moins perfectionnées.^[Charles Darwin, _L’origine des espèces au moyen de la sélection naturelle_, trad. par Edmond Barbier, 1876.]

Dans de tels résumés, Darwin souligne deux choses.
L’une est la nature largement abstraite des prérequis de l’évolution.
Il n’est fait nulle mention de la machinerie donnant lieu à la reproduction et l’hérédité, par exemple.
L’autre est cette sorte d’inéluctabilité du processus.
Si certaines conditions sont rencontrées, alors le changement évolutif s’ensuit, inexorablement.

Les résumés de Darwin dans l’_Origine_ sont cependant plus détaillés que les résumés modernes, qui aspirent souvent à une grande simplicité.
Une version précoce d’un résumé de ce style a été donné par August Weismann (1909:50):^[Je dois à Lukas Rieppel d’avoir porté ce résumé de Weismann à mon attention.
Gould (2002: 223) cite un extrait dans lequel Weismann identifie son « extension du principe de sélection à toute échelle d’unité vitale » comme son idée la plus importante.]

> L’on peut dire que le processus de sélection s’ensuit comme nécessité logique de la satisfaction des trois postulats préliminaires de la théorie: variabilité, hérédité et lutte pour l’existence, avec son énorme taux d’élimination dans toutes les espèces.

Bien d’autres en ont été donnés depuis.
Sans doute que la formulation la plus souvent citée est celle que l’on doit à Richard Lewontin (1970: 1).

> Vu par les évolutionnistes contemporains, le schéma général de Darwin comporte trois principes …:
>
> 1. Différents individus d’une population ont des morphologies, physiologies et comportements différents (variation phénotypique).
> 2. Des phénotypes différents ont des taux de survie et de reproduction différents dans des environnements différents (_fitness_ différentielle).
> 3. Il existe une corrélation entre la contribution des parents et celle de leur descendance aux générations futures (hérédité de la _fitness_).
>
> Ces trois principes donnent corps au principe de l’évolution par sélection naturelle.
> Tant qu’ils tiennent, une population subit des changements évolutifs.

En voici une autre que Lewontin a proposé plus tard, qui constitue d’une certaine façon un meilleur point de départ (1985: 76).

> Un mécanisme suffisant à l’évolution par sélection naturelle est contenu dans ces trois propositions:
>
> 1. Il y a variation des traits morphologiques, physiologiques et comportementaux des membres d’une espèce (principe de variation).
> 2. Cette variation est en partie héréditaire, de sorte que les individus ressemblent à ceux qui leur sont apparentés plus qu’ils ne ressemblent à d’autres, et en particulier, les descendants ressemblent à leurs parents (principe d’hérédité).
> 3. Différents variants produisent un nombre de descendants différents, que ce soit dans la génération immédiate ou plus lointaine (principe de _fitness_ différentielle).
>
> Ces trois conditions conjointes sont des conditions nécessaires et suffisantes à l’évolution par sélection naturelle. …
> On peut attendre de tout trait auquel s’appliquent les trois principes qu’il évolue.

Dans les discussions contemporaines, comme celles de Lewontin, la recherche d’abstraction est particulièrement prégnante.
Le théoricien commence par décrire la façon dont fonctionne le processus évolutif dans les populations d’organismes individuels, mais note ensuite que les ingrédients de l’évolution peuvent se trouver dans d’autres domaines.
Des entités beaucoup plus petites que les organismes, tels que les chromosomes ou les gènes, ainsi que des entités beaucoup plus larges que les organismes, tels que les groupes sociaux, peuvent satisfaire les réquisits de la théorie.
Weismann lui-même entrevit ce genre de possibilité, et l’employa dans sa théorie du fonctionnement du changement au sein d’organismes individuels au cours du développement (1896).
Herbert Spencer (1871) et d’autres ont conçu des possibilités plus extravagantes d’applications des idées Darwiniennes.
L’idée de l’extension du Darwinisme au-delà de ses frontières originelles est presqu’aussi vieille que le Darwinisme lui-même.

Les résumés de Lewontin soulignent également la fiabilité interne du processus en un sens particulier.
Ils fournissent un _résumé_ du processus évolutif sous la forme d’une _recette_ du changement.
La variation, l’héritabilité et les différences de _fitness_ sont présentés comme des ingrédients.
S’ils sont mélangés, le changement évolutif résulte.
L’intention de Darwin et Weismann est la même; le but est de montrer qu’une machine simple se tient au cœur du Darwinisme.
Cette machine a une sorte de transparence causale, et son fonctionnement, lorsqu’elle est correctement alimentée, inéluctable.
Le changement s’ensuit par une sorte de ce que Weismann appelle « nécessité logique ».

L’on peut trouver des points de contrastes entre les formulations précoces et modernes.
La « lutte pour l’existence », si prégnante chez un Darwin ou un Weismann, est absente chez un Lewontin.
Les vagues mentions d’« hérédité » et de « transmission » des premiers font place à un discours statistique.
L’hérédité a toujours été le point faible de la théorisation de Darwin, ainsi qu’on le voit dans son résumé plus haut.
Il n’est pas vrai que l’hérédité, en un sens pertinent, est « presque impliquée par la loi de reproduction ».
Il est tout à fait possible qu’il y ait reproduction et réapparition fiable de variation à chaque génération, sans que les parents ne ressemblent à leur descendance eu égard à cette variation.

Des résumés de ce genre ne surgissent pas seulement dans des discussions théoriques.
Ils sont aussi employés pour véhiculer certaines idées Darwiniennes centrales dans des présentations introductives, et de faire montre de la cohérence de la théorie évolutionnaire en réponse aux attaques qu’elle subit.
Je clôt ici cet échantillon de résumés par celui que l’on trouve dans le manuel _Évolution_, de Mark Ridley (1996: 71-2).

> La sélection naturelle est la plus aisément comprise, de manière abstraite, comme un argument logique, allant des prémisses aux conclusions.
> Celui-ci, sous sa forme la plus générale, requiert quatre conditions:
>
> 1. Reproduction. Les entités doivent se reproduirent pour former une nouvelle génération.
> 2. Hérédité. La descendance doit tendre à ressembler à ses parents: dit grossièrement, « les semblables produisent des semblables ».
> 3. Variation des caractères individuels parmi les membres de la population. …
> 4. Variation de la _fitness_ des organismes eu égard à l’état d’un caractère héréditaire.

> Dans la théorie évolutionnaire, la _fitness_ est un terme technique, signifiant le nombre moyen de descendants laissés par un individu relatif au nombre moyen de descendants laissés par un membre de la population. …
>
> Si ces conditions sont satisfaites par une propriété quelconque d’une espèce, la sélection naturelle s’ensuit automatiquement.
> Si l’une de ces conditions ne l’est pas, la sélection naturelle n’a pas lieu.

Je ferai référence à des formulations de ce genre comme à la tradition « classique » de résumé de l’évolution par sélection naturelle.
Ces résumés tendent à comporter trois ingrédients: variation, hérédité et fécondité différentielle, bien qu’ils soient parfois détaillés plus précisément, comme chez Ridley.
Ils visent à la transparence causale et sont souvent exprimés comme des recettes d’un changement.
Ils décrivent un mécanisme qui, à cours terme, ne fait rien de plus que changer la distribution des caractéristiques d’une population.
Ils accordent que la fiabilité de la transmission peut être faible --- il peut n’y avoir guère qu’une tendance à ce que les parents ressemblent à leur descendance --- mais que le changement surviennent néanmoins.
Et, dans les résumés modernes, non dans ceux de Darwin, rien n’est dit du changement à long terme, ni de ce que ce changement prédit conduise à des organismes « mieux adaptés » à leurs environnements.

Ces résumés sont sur la bonne voie.
Ceci dit, les formulations existantes ne sont pas sans failles.
Il ne serait d’aucun but de passer ces formulations au chalumeau de la critique alors qu’elles sont intentionnellement conçues comme des résumés compacts de discussions plus exhaustives, mais leur examen plus avant constitue un bon point de départ, et leurs erreurs sont éclairantes.
Leur visée habituelle, encore une fois, est de décrire le processus Darwinien en en listant un ensemble d’ingrédients, et de noter de quelles façons ils interagissent pour produire le changement.
Mais, tels qu’ils sont, les résumés standards ne couvrent pas tous les cas, et ne suffisent pas à prédire le changement.
Dans la section suivante, et plus loin à l’Appendice, je décrit divers cas illustrant ce point.
Je propose également un diagnostic de la situation.
Les résumés standards ont des faiblesses parce qu’ils tentent d’accomplir deux tâches théoriques d’un jet.
Ces tâches sont (i) décrire _tout cas véritable_ d’évolution par sélection naturelle, et (ii) décrire un _mécanisme causalement transparent_.
Elles méritent toutes deux d’être accomplies, mais il est difficile de les satisfaire toutes deux par une formulation unique.
Rétrospectivement, l’on peut dire des résumés existants qu’ils s’étalent à travers elles deux.

## Naissances, morts et idéalisation

> [_Questions causales et constitutives; _fitness_ et générations discretes; interaction entre sélection et hérédité; idéalisation et compréhension_]

Avant de se pencher sur les résumés plus en détail, il convient de lever quelques ambiguïtés quant au rôle qu'ils sont censés jouer.
Premièrement, ce qu'ils visent habituellement est de fournir les « conditions nécessaires et suffisantes », ou seulement des « conditions suffisantes » à l'évolution par sélection naturelle.
Mais cela peut aussi bien vouloir dire que la tâche consiste à décrire les conditions qui vont _produire_ l'évolution par sélection naturelle (où l'on sait ce qu'est l'évolution par sélection naturelle), ou qu'elle est de donner les conditions pour qu'une chose _soit un cas_ d'évolution par sélection naturelle.
L'on vise alors à répondre à une question _causale_ sur l'évolution (comment cela se passe-t-il?) ou à une question _constitutive_ (qu'est-ce que c'est?).
Une fois la distinction faite, l'on voit que les résumés tentent généralement de répondre aux deux questions simultanément.
Ils décrivent une situation dans laquelle un certain type de changement advient, et le processus entier est identifié à l'évolution par sélection naturelle.
Les résumés sont souvent donnés sous forme d'une recette du changement.

Il existe une ambiguïté plus profonde, lorsque l'on commence à penser en termes de recettes.
Ces formulations sont généralement interprétées comme disant que dès lors qu'il y a variation, héritabilité et _fitness_ différentielle d'un trait particulier d'une population, le changement de ce trait advient.
Mais d'autres peuvent être lus comme disant que dès lors qu'une population fait preuve d'une tendance _générale_ à la variation, l'héritabilité et la _fitness_ différentielle, alors _certains_ traits changent.
La conception que je défendrai finalement, à la fin de ce chapitre, est plus proche de cette seconde façon de penser, bien que ça ne soit pas celle dont ces résumés sont généralement perçus.
Je commencerai par les interpréter dans le sens premier, spécifique d'un trait.

Il y a pourtant une autre incertitude.
Si d'aucuns disent qu'ils résument _l'évolution_ par sélection naturelle, ils visent clairement à décrire un processus de changement.
Mais un résumé de la _sélection naturelle_ doit pouvoir inclure des cas où il n'y a pas de changement, parce qu'une population est contrainte par la sélection en un point particulier --- elle _aurait_ changé, en l'absence de sélection, mais ne l'a pas fait.
Là encore, l'interprétation habituelle des recettes est la première, et parfois même le langage est très clair, mais à certains égards il serait souhaitable de commencer par la seconde.

Je discuterai de deux familles de problèmes dans cette section.
La première traite de reproduction et « fitness »; la seconde d'hérédité.

Assurément la sélection naturelle a quelque chose à voir avec des différences dans la mesure où les individus se reproduisent.
Nombres de résumés et d’autres discussions spécifient une manière de mesurer les différences reproductives; la _fitness_ d’un individu est assimilée au nombre de descendants qu’il produit.
Tantôt la _fitness_ est dite correspondre au nombre « attendu » plutôt que réel, tantôt elle est mesure relative plutôt qu’absolue.
Ces distinctions n’ont pas d’importance dans cette première discussion.
Parfois, comme dans le résumé de Lewontin 1985, les descendants plus distants sont également pris en compte, mais je les mettrai ici de côté, et traiterai d’abord la _fitness_ d’un individu comme mesurée par le nombre de descendants qu’il produit, et la _fitness_ d’un type comme le nombre moyen produit par des individus de ce type.

Voilà qui ressemble à une approche évidente, mais bien souvent l’énumération des descendants n’est pas suffisante.
En commençant par l’example le plus simple possible, supposons que l’on ait une population d’individus de type $A$ et $B$ présent en nombre égaux à quelque temps initial.
Tout individu se divise pour produire deux descendants du même type que le parent.
Après quoi, tout individu réitère les mêmes étapes, et ainsi de suite.
Mais les individus $A$ accomplissent ce cycle deux fois plus rapidement que les individus $B$, en raison de leur métabolisme plus efficient.
Aussi plus d’individus $A$ sont produits, et les fréquences des types changent.
Bien qu’il y ait changement, il n’y a pas de différences du point de vue du _nombre_ de descendant produits par les individus, ni produit par la moyenne des différents types.
Les différences concernent le taux auquel de nouveaux individus sont produits par unité de _temps_.

L’on pourrait objecter qu’il s’agit d’un cas vraiment inhabituel, en ce que la population s’accroît sans contrainte.
Cette situation ne durera pas longtemps.
Mais cette caractéristique n’est pas essentiel à l’argument.
Supposons maintenant que l’on ait une population croissant de la même manière, vivant puis se divisant, sauf qu’à mesure que la population s’agrandit elle épuise ses ressources et s’accroît plus lentement.
Tout individu fait face à la possibilité de mourir avant de se reproduire, dont la probabilité est la même pour les types $A$ et $B$, mais qui augmente avec la densité globale de la population.
Il y aura alors un changement évolutif dans une telle situation.
Mais il n’y a pas de différences entre les types eu égard ni au nombre de descendants qu’un individu produit s’il survit, ni à la probabilité qu’un individu né à un temps donné survive suffisamment longtemps pour se reproduire.
^[Voici un modèle simple d’un pareil cas.
La _fitness_ attendue de chaque type est $W = 2(k - N)/k$, ou $k$ est une constant et $N$ (toujours inférieure à $k$) est la taille de la population totale à la naissance d’un individu.
Cette formule décrit à la fois le type $A$ et $B$, et $N$ est le même pour pour des individus $A$ et $B$ né au même moment.
Mais étant donnée la différence de vitesse de reproduction, la fréquence des types changera, au moins jusqu’à ce que $N$ atteigne une valeur d’équilibre où elle est égale à $k/2$, auquel stade le changement évolutif ralentit puisque les événements reproductifs n’ont pas d’effet sur les valeurs globales.
Si la population s’accroît depuis le point où $N = k/2$, la fréquence de $A$ augmente jusqu’au « ralentissement ».
Si la population se réduit au delà de ce point, la fréquence de $A$ décroît.]

Tout résumé de la sélection naturelle qui mesure les différences de _fitness_ en terme de nombre de descendants produits par les individus excluent des cas de genre.
Ça n’est pas un problème pour la théorie évolutionnaire elle-même.
Il existe des modèles élaborés de situations de ce genre.
Ce sont des modèles de « populations structurées en âge (_age-structured populations_) », et dans ces modèles la survie et la reproduction sont décrites de manière détaillée.
Dans un cas simple comme mon premier exemple ci-dessus, chaque type ($A$ et $B$) est décrit par un « programme $l(x)$ », qui spécifie la susceptibilité d’un individu de ce type de survivre jusqu’à l’âge $x$, et un « programme $m(x)$ », qui spécifie le nombre de descendants qu’un individu de ce type aura à l’âge $x$.
À partir de ces valeurs il est possible de calculer les différents « taux de croissance » de chaque type, et de décrire pourquoi $A$ augmente relativement à $B$.
^[Pour trouver le taux de croissance, $\lambda$, d’un type (une fois que la population est parvenue à une distribution d’âge stable), l’on résout l’équation suivante pour ce type: $l = \sum_{x}\lambda^{-x}l(x)m(x)$.
Supposons, par exemple, que le temps soit mesuré en jours, et que les types $A$ vivent toujours pendant un jour et se divise en deux à la fin de ce jour, quand les types $B$ vivent toujours pour deux jours et se divisent ensuite.
Alors le taux de croissance par jour pour $A$ est de $2$ et celui de B est de $\sqrt{2}$.
Ces nombres peuvent être utilisés pour prédire le taux auquel la fréquence de $A$ augmentera relativement à $B$ dans la population totale (Crow 1986: ch. 6).]

L’on y voit que bien des discussions admettent une idéalisation tacite.
Elles traitent des cas de sélection naturelle comme s’ils advenaient dans des situations où les générations sont _non-chevauchantes_ et _synchronisées_ à travers la population.
Ceci est généralement appelé un modèle d’évolution à « générations discrètes ».
Ce sont les cas les plus simples à analyser.
Et certains organismes ont réellement des générations non-chevauchantes synchronisées à travers la population.
Cela comprends des plantes annuelles comme le basilic, de nombreux insectes, et quelques autres.
Mais l’essentiel des organismes ne se reproduisent pas ainsi; dont les humains à l’évidence.
L’évolution est, assurément, un processus qui s’incrit dans le temps.
En certains cas, le rôle joué par le temps est rendu si simple par le cycle de vie des organismes qu’il n’est pas besoin de le mentionner explicitement.
Mais un résumé qui ignore le rôle du temps ne peut être vu que comme décrivant un processus _imaginaire simple apparenté_ au processus d’évolution dans la majorité des cas.

Cet argument est le plus limpide lorsque l’on pense la _fitness_ d’un individu comme son nombre de descendants immédiats.
Il l’est moins lorsque les descendants « lointains » y sont inclus, comme dans le résumé de Lewontin 1985.
Les descendants lointains peuvent servir d’une sorte de témoin (_proxy_) du temps.
Dans mes exemples ci-dessus, par exemple, la fréquence du type $A$ augmentait relative à celle du type $B$ en raison de son taux de reproduction plus rapide.
Ce qui signifie que dans tout intervalle de temps spécifié, les individus initialement présents du type $A$ finiront par avoir plus de descendants lointains (arrière-arrière-...-petits enfants) que les individus initiaux du type $B$, parce que les individus $A$ accomplissent plus de générations dans le temps disponible.
Dans mon second exemple j’ai supposé que la susceptibilité d’une mort prématurée augmente avec le temps.
Aussi, bien que toute paire d’individus $A$ et $B$ nés au même moment aient le même nombre attendu de descendants, ceux de l’individu $B$ naîtront dans des temps plus durs que ceux de l’individu $A$, de sorte que l’individu $B$ devrait avoir moins de petits-enfants (et d’arrière-petits-enfants) que l’individu $A$.

La mention des petits-enfants dans les discussions de _fitness_ est généralement censée capturer ces cas spécifiques pour lesquels l’avantage associé à un trait n’est pas apparent dans le nombre de descendants immédiats, mais l’est dans le nombre de petits-enfants.
Un exemple en est proposé par l’explication de Fisher (1930) de la raison pour laquelle les _sex ratios_ sont généralement autour de $1:1$.

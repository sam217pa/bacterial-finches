---
title: Illustrations de la Logique de la Science
subtitle: Troisième Papier --- La Doctrine des Chances
author: Charles S. Peirce
translator: Samuel Barreto
---

# 

C'est une observation fréquente que la science ne commence d'être exacte que lorsqu'elle est quantitativement traitée.
Ne sont dites exactes que les sciences mathématiques.
Les chimistes raisonnaient confusément avant que Lavoisier ne leur montre comment employer la balance à la vérification de leurs théories, et que la chimie ne se hisse soudainement à la position de la plus parfaite des sciences classificatoires.
Elle est devenue depuis si précise et assurée qu'on la pense d'ordinaire au même rang que l'optique, la science thermique ou électrique.
Mais celles-ci étudient les lois générales, quand la chimie ne considère guère que les relations et les classifications de certains objets; aussi appartient-elle, en réalité, à la même catégorie que la botanique et la zoologie systématique.
Comparez-la à ces dernières, néanmoins, et l'avantage qu'elle tire de son traitement quantitatif est tout ce qu'il y a d'évident.

Les échelles numériques les plus grossières, telles que celles par lesquelles le minéralogiste distingue différents degrés de dureté, sont utiles.
Le simple dénombrement de pistils et d'étamines suffit à sortir la botanique d'un chaos total à un semblant de forme.
C'est moins, cependant, du _comptage_ que de la _mesure_, moins de la conception du nombre que de celle d'une quantité continue, que provient l'avantage d'un traitement mathématique.
Les nombres, après tout, ne servent qu'à atteindre à un degré précision de nos pensées qui, pour bénéfique qu'il soit, conduit rarement à d'augustes conceptions, et s'abaisse fréquemment à l'insignifiance.
Des deux facultés dont parle Bacon, celle qui marque les différences et celle qui note les ressemblances, le recours aux nombres n'est d'assistance qu'à la moindre des deux; et leur usage excessif tend à restreindre les pouvoirs de l'esprit.
Mais la conception d'une quantité continue à de grands services à rendre, indépendamment de toute recherche de précision.
Loin de tendre à exagérer les différences, elle est l'instrument direct des généralisations les plus profondes.
Lorsqu'un naturaliste cherche à étudier une espèce, il collecte un nombre considérable de specimens plus ou moins similaires.
Par leur contemplation, il en remarque certains plus ou moins semblables à certains égards particuliers.
Ils ont tous, par exemple, une marque caractéristique en forme de S.
Il observe qu'ils ne sont pas _rigoureusement_ semblables à cet égard; le S n'a pas précisément la même forme, mais les différences sont telles qu'elles le conduisent à croire que l'on pourrait trouver des formes intermédiaires, entre, mettons, les deux qu'il possède.
Après quoi, il découvre d'autres formes en apparence bien dissemblables --- mettons une marque de la forme d'un C --- et la question est s'il est capable de trouver des formes intermédiaires qui relieraient ces deux dernières avec les autres.
Il parvient à cela dans bien des cas dont on aurait pu croire d'emblée que c'était impossible; pourtant, il découvre parfois de cas qui, au premier abord, semblaient bien moins différer, qu'ils sont séparés dans la Nature par l'absence de formes intermédiaires.
Ce faisant, il construit par l'étude de la Nature, un conception générale nouvelle du caractère en question.
Il obtient, par exemple, l'idée d'une feuille incluant toutes les parties de la fleur, et l'idée d'une vertèbre incluant le crâne.
Point n'est assurément besoin de montrer quel appareil logique opère ici.
C'est l'essence de la méthode du naturaliste.
De quelle manière il l'applique d'abord à un caractère, puis à un autre, pour finalement obtenir la notion d'une espèce d'animaux, au sein de laquelle les différences entre membres, pour importantes qu'elles soient, sont contenues par des limites, ça n'est pas la question qui nous concerne ici.
Toute la méthode de classification doit être considérée plus tard; pour l'instant, je souhaite seulement souligner que c'est par le biais de l'idée de continuité, du passage d'une forme à l'autre par degrés insensibles, que le naturaliste construits ses concepts.
Assurément, les naturalistes sont de grands élaborateurs de concepts; dans nulle autre branche de la science n'est accompli autant de ce genre de travail qu'en la leur; aussi nous faut-il, à bien des égards, les prendre pour professeurs en cette part importante de la logique.
L'on verra partout que l'idée de continuité est d'une aide formidable à la formation de concepts vrais et fructueux.
Par elle, les différences les plus grandes sont démantelées et résolues en différences de degrés, et son application incessante est du plus grand intérêt à l'élargissement de nos conceptions.
Je me propose d'employer grandement cet idée dans la présente série d'articles; et la série particulière de sophismes, qui, survenant de sa négligence, ont dévasté la philosophie, doit plus tard être étudiée de près.
Pour l'instant, j'attire seulement l'attention du lecteur sur l'utilité de cette conception.

Dans l'étude des nombres, l'idée de continuité est si indispensable qu'elle est sans cesse introduite, quand bien même il n'y ait pas de continuité en fait, comme lorsque l'on dit qu'il y a aux États-Unis 4,1 habitants par kilomètre carré, ou qu'à New York 14,72 personnes vivent dans un bâtiment moyen.
^[Ce mode de pensée est si intimement associée aux considérations numériques exactes que la phrase qu'on lui attribue est imitée par des écrivains peu profonds afin de produire une apparence d'exactitude où il n'y en a aucune.
Certains journaux affectant d'un ton instruit parlent de l'« homme moyen », quand ils veulent simplement dire _la plupart des hommes_, et n'ont aucune envie d'estimer une moyenne.]
Un autre exemple est celui des lois de distribution d'erreurs que Quêtelet, Galton et d'autres ont appliqué avec tant de justesse à l'étude de questions biologiques et sociales.
Cette application de la continuité à des cas où elle n'existe pas réellement illustre également un autre point, qui dès lors exige une étude distincte, c'est-à-dire la grande utilité qu'ont parfois les fictions en science.

#

La théorie des probabilités n'est que la science de la logique traitée quantitativement.
Il y a deux certitudes concevables quant à une hypothèse quelconque, la certitude de sa vérité et la certitude de sa fausseté.
Les nombres _un_ et _zero_ sont d'usage approprié, en ce calcul, pour dénoter ces extrêmes de la connaissance; les fractions, elles, lorsqu'elles sont de valeur intermédiaire entre ces deux extrêmes indiquent, comme on pourrait le dire vaguement, le degré d'inclination des données probantes (_evidence_) envers l'un ou l'autre.
Le problème général des probabilités est, à partir d'un état de faits donné, de déterminer la probabilité numérique d'un fait possible.
Cela revient à rechercher dans quelle mesure les faits donnés sont probants, en tant que preuves du fait éventuel.
Ainsi le problème des probabilités est simplement celui plus général de la logique.

La probabilité est une quantité continue, de sorte que l'on peut espérer tirer de grands profits de ce mode d'étude de la logique.
Certains auteurs sont allés jusqu'à tenir que, par le calcul des chances, toute inférence solide puisse être représentée par des opérations arithmétiques légitimes sur les nombres donnés par les prémisses.
Si cela s'avérait vrai en effet, le grand problème de la logique, comment se fait-il que l'observation d'un fait nous fournit la connaissance d'un autre fait indépendant, se réduit à un simple problème d'arithmétique.
Il semble approprié d'examiner cette prétention avant d'entreprendre de chercher une résolution plus avant de ce paradoxe.

Mais, malheureusement, les écrits sur les probabilités ne s'accordent pas tous sur ce résultat.
Cette branche des mathématiques est bien la seule, je crois, où de bons auteurs obtiennent régulièrement des résultats totalement erronés.
En géométrie élémentaire le raisonnement est fréquemment fallacieux, mais les conclusions erronées sont évitées; pourtant l'on peut douter de ce qu'il existe un unique traité exhaustif des probabilités qui ne contienne pas de solutions absolument indéfaisables.
Cela provient en partie d'un désir de quelque méthode de procédure régulière; car le sujet implique trop de subtilités pour qu'il soit possible de formuler ses problèmes en équations sans une telle assistance.
Mais, au delà de ça, les principes fondamentaux de ses calculs sont plus ou moins débattus.
Eu égard à cette catégorie de questions pour lesquelles les probabilités sont communément appliquées à des fins pratiques, il n'y a proportionnellement que peu de doutes; mais eu égard à d'autres questions auxquelles on a songé à les étendre, l'opinion est passablement ambivalente.

Cette dernière classe de difficultés ne peut être surmontée qu'en rendant l'idée des probabilité parfaitement claire dans nos esprits, de la façon proposée par notre dernier article.

#

Afin d'avoir une idée claire de ce que l'on entend par probabilité, il nous faut considérer quelle différence réelle et sensible il y a entre un degré de probabilité et un autre.

Le caractère de probabilité appartient en premier lieu, sans aucun doute, à certaines inférences.
Locke l'explique ainsi:
Ayant noté que le mathématicien sait positivement que la somme des trois angles d'un triangle est égale à deux angles droits parce qu'il en comprend la preuve géométrique, il continue:
« Mais un autre homme qui n’a jamais pris la peine de considérer cette Démonstration, entendant affirmer à un Mathématicien, homme de poids, que les trois Angles d’un Triangle sont égaux à deux Droits, y donne son _consentement_, c’est-à-dire, le reçoit pour véritable: auquel cas le fondement de son Assentiment, c’est la Probabilité de la chose, dont la preuve est pour l’ordinaire accompagnée de la vérité, l’homme sur le témoignage duquel il reçoit, n’ayant pas accoûtumé d’affirmer une chose qui soit contraire à sa connoissance ou au dessus de sa connoissance, et surtout dans ces sortes de matières. » 
Le fameux « Essai Concernant l'Entendement Humain » contient de nombreux passages qui, comme celui-ci, gravissent les premières marchent d'analyses profondes qui ne sont pas développées plus avant.
Il a été montré dans le premier de ces articles que la validité d'une inférence ne dépend pas de quelque tendance de l'esprit à l'accepter, si forte que soit cette tendance; elle consiste plutôt dans les faits réels qui, quand les prémisses pareilles à celles des débats en questions sont vraies, les conclusions qui leur sont liées, pareilles à celle de ce débat, sont vraies aussi.
Il a été noté qu'à un esprit logique un argument est toujours conçu comme membre d'un _genre_ (_genus_) d'arguments tous construits de la même manière, et de telle sorte que, quand leurs prémisses sont des faits réels, leurs conclusions le sont aussi.
Si l'argument est démonstratif, alors il l'est toujours; s'il n'est que probable, alors il l'est pour l'essentiel.
Comme le dit Locke, l'argument probable est « _tel que_, pour l'essentiel, il porte la vérité. »

À cette lueur, cette différence réelle et sensible entre l'un et l'autre degré de probabilité, en quoi se tient le sens de la distinction, est que dans l'emploi récurrent de deux modes d'inférence différents, l'un porte avec lui la vérité plus souvent que l'autre.
Il est évident qu'il s'agit de l'unique différence qu'il y ait dans le fait existant.
De certaines prémisses, un homme tire une certaine conclusion, et en ce qui concerne uniquement cette inférence la seule question pratique possible est de savoir si cette conclusion est vraie ou non, et entre l'existence et la non-existence il n'y a pas de terme médian.
« L'être est, et le non-être n'est pas », disait Parménide; ce qui est en accord strict avec l'analyse de la conception de la réalité donnée dans le dernier papier.
Car l'on y voyait que la distinction entre réalité et fiction dépend du postulat que des investigations suffisantes entraîneraient l'adhésion universelle à une opinion et au rejet de toutes les autres.
Cette présupposition, impliquée dans la conception même de la réalité et du fantasme nécessite un anéantissement complet de ces dernières.
Cette idée constitue le ciel et la terre du domaine de la pensée.
^[_It is the heaven-and-hell idea in the domain of thought_.]
Mais, en moyenne, il y a un fait réel qui correspond à l'idée de probabilité, qui est qu'un mode donné d'inférence s'avère parfois fructueux, parfois non, ceci suivant un ratio fixe en dernière analyse.
Alors que l'on tire inférence après inférence d'une espèce donnée, au cours des dix ou cent premiers cas, on peut s'attendre à ce que le taux de succès fluctue considérablement; mais lorsque l'on arrive aux milliers et aux millions, ces fluctuations se réduisent toujours d'avantage; et si l'on y insiste suffisamment longtemps, le ratio s'approchera d'une limite fixe.
L'on peut dès lors définir la probabilité d'un mode d'argument comme la proportion de cas où il est porteur de vérité.

L'inférence de la prémisse, A, à la conclusion, B, dépend, comme nous l'avons vu, du principe directeur selon lequel, si un fait de la classe A est vrai, un fait de la classe B est vrai.
La probabilité consiste en la fraction dont le numérateur est le nombre de fois où et A et B sont vrais, et dont le dénominateur est le nombre total de foix où A est vrai, que B le soit ou non.
Plutôt que de parler de cela comme la probabilité de l'inférence, il n'y a pas la moindre espèce d'objection à l'appeler la probabilité que, si A survient, B survient.
Mais de parler de la probabilité de l'événement B, sans en nommer les conditions, n'a vraiment aucun sens.
Il est vrai que, lorsqu'il est parfaitement évident à quelles conditions il est fait allusion, l'ellipse peut être autorisée.
Mais l'on devrait éviter de prendre l'habitude d'employer le langage de cette manière (pour universelle que soit cette habitude), parce qu'elle conduit à une manière de penser imprécise, comme si l'action de la causation pouvait ou bien déterminer l'advenue d'un événement, ou bien déterminer sa non-advenue, ou bien le laisser plus ou moins libre d'advenir ou non, de telle sorte qu'elle génèrerait une chance _inhérente_ eu égard à l'occurence de l'événement.
Il est tout à fait clair à mon sens que certaines des pires erreurs et parmi les plus tenaces liées à l'emploi de la doctrine des chances proviennent de cette sournoise forme d'expression.
^[La conception de la probabilité mise en avant ici est en substance celle développée par M. Venn, dans sa « Logic of Chance ».
Assurément, une appréhension vague de l'idée a toujours existé, mais la question était de la rendre parfaitement claire, et c'est de l'avoir fait le premier que l'on doit le créditer.]

#

Mais il reste un point important à aborder.
D'après ce que l'on vient de dire, l'idée de la probabilité appartient essentiellement à une sorte d'inférence qui est indéfiniment répétée.
Une inférence individuelle doit être ou fausse ou vraie, et ne peut montrer aucun effet de probabilité; partant, en référence à un cas isolé considéré en lui-même, la probabilité ne peut avoir aucun sens.
Pourtant, si un homme devait choisir entre tirer une carte d'un paquet contenant vingt-cinq cartes rouges et une noire, ou d'un paquet contenant vingt-cinq cartes noires et une rouge, et si le tirage d'une carte rouge était censée le conduire à la félicité perpétuelle, quand le tirage d'une carte noire le condamnerait à d'éternels malheurs, ce serait folie que de nier qu'il doive préférer le paquet contenant une majorité de cartes rouges, quand bien même, du fait de la nature de l'essai, le tirage ne puisse être répété.
Il n'est pas simple de réconcilier ceci avec notre analyse du concept de chance.
Supposez alors qu'il choisisse le paquet rouge, et tire la mauvaise carte, quelle consolation aurait-il?
Il pourrait bien dire qu'il a agi en suivant la raison, mais cela ne ferait que démontrer que sa raison n'était d'aucun secours.
Et s'il devait choisir la bonne carte, comment pourrait-il ne pas le voir comme autre chose qu'un heureux hasard?
Il ne pourrait pas dire que s'il avait choisi l'autre paquet, il aurait pu tirer la mauvaise carte, parce qu'une proposition hypothétique telle que, « si A, alors B », ne signifie rien eu égard à un cas unique.
La vérité consiste en l'existence d'un fait réel correspondant à la vraie proposition.
Correspondant à la proposition, « si A, alors B », il peut y avoir le fait que _dès lors_ qu'un événement tel que A se produit, un événement tel que B se produit.
Mais dans le cas supposé, qui n'a aucun parallèle en ce qui concerne cet homme, il ne peut y avoir aucun fait réel dont l'existence confère quelque vérité à l'énoncé que, s'il avait choisi l'autre paquet, il en aurait tiré une carte noire.
En fait, puisque la validité d'une inférence consiste en la vérité de la proposition hypothétique selon laquelle _si_ les prémisses sont vraies les conclusions le seront aussi, et puisque l'unique fait réel qui puisse correspondre à pareille proposition est que dès lors que l'antécédent est vraie le conséquent l'est aussi, il suit qu'il ne fait aucun sens de raisonner d'un cas unique.

Ces considérations semblent, à première vue, nous débarasser des difficultés mentionnées.
Pourtant, le cas de l'autre bord n'a pas encore été traité.
Que la probabilité manifesterait probablement ses effets en, mettons, un millier d'essai, par une certaine proportion entre le nombre de succès et le nombre d'échecs, cela revient, comme nous l'avons vu, à dire qu'elle le fera certainement, à la longue.
Mais le nombre d'essais, le nombre d'inférences probables qu'un homme peut tirer au cours de sa vie, est un nombre fini, et il ne peut pas être absolument _certain_ que le résultat moyen suive bel et bien les probabilités.
Considérant l'ensemble de ses essais collectivement, alors, il ne peut être certain qu'ils n'échouent pas, et son cas ne diffère alors en rien, sinon en degré, du cas précédemment imaginé.
C'est un résultat indubitable de la théorie des probabilités que tout joueur, s'il continue suffisamment longtemps, doive être ruiné en dernière analyse.
Supposez qu'il tente la martingale, que certains croient infaillible, et qui, pour autant que je le sache, est interdite dans les maisons de jeu.
Selon cette méthode de jeu, il parie d'abord 1 dollar; s'il perd il en parie 2; s'il perd il en parie 4; s'il perd il en parie 8; si maintenant il gagne il aura perdu 1 + 2 + 4 = 7, et a gagné 1 dollar de plus; et quel que soit le nombre de pari qu'il perd, le premier qu'il gagne l'enrichit d'un dollar de plus qu'il n'avait au départ.
De cette façon, il gagnera probablement dans un premier temps; mais, à la longue, le temps viendra où le cours de la chance sera tellement contre lui qu'il n'aura pas suffisamment d'argent pour doubler la mise, et qu'il doive donc perdre son pari.
Cela adviendra _probablement_ avant qu'il n'ait gagné autant que ce qu'il avait initialement, de sorte que ce manque de chance le laisse plus pauvre qu'il n'était au départ; cela arrivera à un moment ou un autre.
Il est vrai qu'il y a toujours une possibilité qu'il emporte toute somme que la banque peut payer, et l'on rencontre ici un fameux paradoxe qui est que, bien qu'il soit sûr d'être ruiné, la valeur de ses revenus calculés selon les règles habituelles (excluant cette considération) est élevée.
Mais, qu'un parieur joue de cette façon ou non, la même chose reste vraie, c'est-à-dire, que s'il joue suffisamment longtemps il sera certain à un moment de manquer de tellement de chance qu'il n'en épuise toute sa fortune.
La même chose est vraie d'une compagnie d'assurance.
Que les directeurs prennent le plus grand soin d'éviter les grandes pestes et incendies, leurs actuaires peuvent leur dire que, selon la doctrine des chances, le temps viendra où, finalement, leurs pertes les conduiront à leur perte.
Ils peuvent bien surmonter pareille crise par d'extraordinaires moyens, mais s'en trouveront affaiblis d'autant, et l'histoire s'en répétera d'autant plus vite.
Un actuaire pourrait être enclin à nier ceci, parce qu'il sait que les revenus de sa compagnie sont importants, voire même (sans tenir compte de l'intérêt sur l'argent) infinis.
Mais les calculs de revenus ne tiennent pas compte des circonstances que l'on considère ici, ce qui renverse toute l'histoire.
Ceci dit, que l'on ne me comprenne pas comme disant que l'assurance est, sur cette base, bancale, plus que d'autres genres d'affaires.
Toutes les affaires humaines reposent sur la probabilité, aussi la même chose est-elle vraie partout.
Si l'Homme était immortel il pourrait être parfaitement sûr de voir le jour où tout ce en quoi il croyait trahisse sa confiance, et, en bref, de parvenir enfin à la misère la plus désespérée.
Il s'effondrerait, enfin, comme toute grande fortune, toute dynastie, toute civilisation.
À la place de quoi nous avons la mort.

Mais ce qui, sans la mort, arriverait à tout homme, avec elle arrive à certains.
Dans le même temps, la mort rend le nombre de nos essais, de nos inférences, fini, et partant leur résultat moyen incertain.
L'idée même de probabilité et de raisonnement repose sur le postulat que ce nombre est infiniment grand.
Nous voilà conduits à la même difficulté qu'auparavant, et je n'en peux voir qu'une solution.
Il me semble que nous sommes conduits à cela, que le raisonnement logique requiert inexorablement que nos intérêts _ne_ soient _pas_ limités.
Ils ne doivent pas s'arrêter à notre destin propre, mais embrasser toute la communauté.
Cette communauté, là encore, ne doit pas se limiter, mais doit s'étendre à toutes les races d'êtres avec lesquels on peut entrer immédiatement ou médiatement en relation intellectuelle.
Elle doit porter, de quelque façon que ce soit, au delà de cette ère géologique, au delà de toute frontière.
Celui qui ne sacrifierait son âme pour sauver le monde, est, à ce qu'il me semble, illogique dans toutes ses inférences, collectivement.
La logique est ancrée dans le principe social.

Pour être logiques les hommes ne devraient pas être égoïstes; et, du point de vue des faits, ils ne sont pas si égoïstes qu'on le croit.
La poursuite intentionnelle de ses désirs est une chose bien différente de l'égoïsme.
L'avare n'est pas égoïste; son argent ne lui est d'aucun bien, d'autant qu'il se soucie de ce qu'il en adviendra après sa mort.
L'on parle constamment de _nos_ possessions dans le Pacifique, de _notre_ destin en tant que république, quand aucun intérêt personnel n'est en jeu, par quoi l'on voit que nos intérêts sont plus larges qu'on les dit.
L'on discute anxieusement de l'épuisement possible du charbon dans quelques centaines d'années, ou du refroidissement du Soleil dans quelques millions d'années, et voyons dans la plus populaire de toutes les croyances religieuses que l'on peut concevoir l'éventualité d'un homme descendant aux enfers pour le salut de ses prochains.

Ceci dit, il n'est pas nécessaire pour l'esprit logique qu'un homme soit lui-même prêt à l'héroïsme de son sacrifice.
Il suffit qu'il en reconnaisse la possibilité, perçoive que seules les inférences de l'homme qui en est capable sont réellement logiques, et par conséquent ne voie les siennes propres comme valides que dans la mesure où elles seraient acceptées par le héros.
Dans la mesure où il réfère ses inférences à cette norme, on l'identifie à un tel esprit.

Voilà qui place l'esprit logique suffisamment à portée.
Parfois l'on peut personnellement parvenir à l'héroïsme.
Le soldat qui s'élance à l'assaut d'un mur sait qu'il sera probablement touché, mais ça n'est pas tout ce dont il se soucie.
Il sait aussi que, si tout le régiment, avec lequel il a le sentiment de s'identifier, s'élance de concert, le fort sera pris.
En d'autres cas l'on peut seulement imiter cette vertu.
L'homme que l'on a imaginé devoir tirer une carte de l'un des deux paquets, et qui, s'il n'est pas logicien, tirerait du paquet rouge par simple habitude, verra, s'il est suffisamment logique, qu'il ne peut être logique tant qu'il s'intéresse seulement à son destin propre, mais que quelque homme qui se soucierait autant de ce qu'il se produirait en tout pareil cas possible et pouvant agir logiquement, choisirait de tirer du paquet avec le plus de cartes rouges, et donc, bien qu'incapable lui-même d'une telle sublimité, ce logicien imiterait l'effet du courage de cet homme afin de partager son esprit logique.

Mais tout ceci requiert que l'on conçoive ses intérêts propres comme identifiés à ceux d'une communauté sans limites.
En fait, il n'y a aucune raison, et une discussion ultérieure montrera qu'il ne peut y avoir de raison, de penser que la race humaine, ou toute race intellectuelle, existe éternellement.
De l'autre côté, il n'y a aucune raison de ne pas penser l'inverse;
^[Je ne présume pas ici d'un inconnaissable absolu.
Les preuves pourraient nous montrer ce qui serait probablement le cas après un intervalle de temps donné; et bien qu'une période subséquente puisse ne pas être intégrée par ces preuves, elle le serait par d'autres plus avant.]
et fort heureusement, puisque l'unique réquisit est qu'il nous faut avoir certains sentiments, il n'y a dans les faits rien qui nous empêche d'avoir _espoir_, un souhait serein et enjoué, en ce que la communauté dure par delà n'importe quelle date assignable.

Il pourrait sembler étrange que j'avance trois sentiments, c'est-à-dire, l'intérêt à une communauté indéfinie, la reconnaissance de ce que la possibilité de cet intérêt puisse être conçu comme suprême, et l'espoir en une prolongation illimitée de l'activité intellectuelle, comme prérequis indispensables de la logique.
Pourtant, lorsque l'on comprends que la logique ne dépend guère que d'une lutte pour échapper au doute, qui, tout comme elle trouve sa fin dans l'action, doit commencer par l'émotion, et que, de plus, la seule cause de notre adossement à la raison tient en ce que d'autres méthodes pour échapper au doute échouent eu égard à l'élan social, pourquoi s'étonner de trouver le sentiment social présupposé par la logique?
Quant aux deux autres sentiments que je trouve nécessaires, ils ne le sont qu'en tant que supports et compléments de tout ceci.
Il me faut souligner que ces trois sentiments semblent être à peu de choses près les mêmes que le fameux trio de Charité, Foi et Espoir, qui, de l'avis de St. Paul, sont les dons spirituels les plus grands et les plus purs.
Ni l'Ancien ni le Nouveau Testament ne sont des manuels de la logique de la science, mais ce dernier est assurément l'autorité la plus haute qu'il y ait eu égard aux dispositions de cœur qu'un homme devrait posséder.

#

De nombres moyens statistiques tels que le nombre d'habitants par kilomètre carré, le nombre moyen de décès par semaine, le nombre de condamnation par mise en examen, ou, de façon générale, le nombre de _x_ par _y_, où les _x_ sont une classe de choses dont certaines ou toutes sont liées à une autre classe de choses, leur _y_, je dis qu'ils sont des _nombres relatifs_.
Des deux classes de choses auxquelles un nombre relatif fait référence, celle dont il est un nombre peut être appelée son _relat_, et celle _par_ quoi la numération est faite son _corrélat_.

Une probabilité est un genre de nombre relatif; c'est-à-dire, elle est le ratio du nombre d'arguments d'un certain genre qui portent la vérité sur le nombre total d'arguments de ce genre, et les règles du calcul des probabilités sont très facilement dérivée de cette considération.
Elles peuvent toutes être données ici, puisqu'elles sont extrêmement simples, et qu'il est parfois pratique de connaître une chose telle que les règles élémentaires de calcul des chances.

RÈGLE I. _Calcul direct_. --- Pour calculer, directement, tout nombre relatif, mettons par exemple le nombre de passagers par voyage moyen d'un tramway, il faut procéder comme suit:  

Comptez le nombre de passager par voyage; ajoutez tous ces nombres, et diviser par le nombre de voyage.
Il y a des cas où une telle règle peut être simplifiée.
Supposons que l'on souhaite connaître le nombre d'habitants par logement à New York.
La même personne ne peut habiter deux logements.
S'il partage son temps entre deux logements il doit être compté comme semi-habitant des deux.
En ce cas il nous faut seulement diviser le nombre total d'habitants à New York par le nombre de logements, sans qu'il soit nécessaire de compter séparément chacun des habitants de tous les logements.
Une démarche similaire s'appliquera dès lors que chaque relat individuel appartient à un corrélat individuel exclusivement.
Si l'on cherche le nombre de _x_ par _y_, et que nul _x_ n'appartient à plus d'un _y_, il suffit de diviser le nombre total de _x_ d'_y_ par le nombre d'_y_.
Pareille méthode ne serait, assurément, d'aucune aide lorsqu'appliquée au cas du nombre moyen de passager par tramway par voyage.
L'on ne pourrait pas diviser le nombre total de passager par le nombre de voyage, puisque beaucoup d'entre eux auraient effectué plusieurs passages.

Pour trouver la probabilité que, d'une classe de prémisse donnée, _A_, une classe de conclusion donnée, _B_, s'ensuive, il est seulement nécessaire d'asserter de quelle proportion des fois où les prémisses de cette classe sont vraies, les conclusions correspondantes sont vraies aussi.
En d'autres termes, c'est le nombre d'occurences des deux évènements _A_ et _B_, divisé par le nombre total d'occurences de l'évènement _A_.

RÈGLE II. _Addition de Nombres Relatifs_. ---
Sachant deux nombres relatifs de même corrélat, mettons le nombre de _x_ par _y_, et le nombre de _z_ par _y_; il faut trouver le nombre de _x_ et de _z_ par _y_.
S'il n'y a rien qui soit en même temps un _x_ et un _z_ par _y_, la somme des deux nombres donne le nombre cherché.
Supposons, par exemple, que l'on ait le nombre moyen d'ami qu'un homme ait, et le nombre moyen d'ennemies, la somme des deux est le nombre moyen de personnes qui se soucient de lui.
D'un autre côté, cela ne fait aucun sens d'ajouter le nombre moyen de personnes ayant des maladies graves par nombre de personnes au delà de la limite d'âge militaire, et le nombre moyen de personnes exemptés chacun par une cause particulière du service militaire, afin d'obtenir le nombre moyen d'exemptés, puisque beaucoup sont exemptés de deux ou plus de deux façons simultanément.

Cette règle s'applique directement aux probabilités.
Sachant la probabilité que deux évènements différents et mutuellement exclusifs se produisent dans les mêmes ensembles de circonstances supposées, sachant par exemple, la probabilité que si _A_ alors _B_, et la probabilité que si _A_ alors _C_, alors la somme de ces deux probabilités est la probabilité que si _A_ alors ou _B_ ou _C_, tant qu'il n'y a pas d'évènement qui appartiennent aux deux classes _B_ et _C_.

RÈGLE III. _Multiplication de Nombres Relatifs_. ---
Supposons que l'on sache le nombre relatif de _x_ par _y_; que l'on sache aussi le nombre relatif de _z_ par _x_ de _y_; ou pour prendre un exemple concret, supposons que l'on sache, premièrement, le nombre moyen d'enfants par familles vivant à New York; deuxièmement, le nombre moyen de dents par dentition d'enfant de New York --- alors le produit de ces deux nombres donnerait le nombre moyen de dent d'enfant par famille de New York.
Mais cette méthode de calcul ne s'applique en général que sous deux conditions.
En premier lieu, elle ne serait pas vraie si le même enfant pouvait appartenir à différentes familles, car alors dans le cas où des enfants qui appartiennent à plusieurs familles différentes auraient un nombre de dents exceptionnellement faible ou élevé, cela affecterait le nombre moyen de dents d'enfant par famille plus que le nombre moyen de dents par enfant.
En second lieu, la règle ne serait pas vraie si plusieurs enfants pouvaient partager une même dent, le nombre moyen de dents d'enfant étant en ce cas à l'évidence bien différent du nombre moyen de dent par enfant.

Afin d'appliquer cette règle aux probabilités, il nous faut procéder comme suit:
Supposons qu'il nous soit donnée la probabilité que la conclusion _B_ suive de la prémisse _A_, _B_ et _A_ représentant comme à l'accoutumée certaines classes de propositions.
Supposons que l'on sache aussi la probabilité d'une inférence dont _B_ serait la prémisse, et une proposition d'un troisième type, _C_, la conclusion.
Dans ce cas, alors, nous avons tout les matériaux pour appliquer cette règle.
Nous avons, d'abord, le nombre relatif de _B_ par _A_.
Nous avons, ensuite, le nombre relatif de _C_ par _B_ suivant de _A_.
Mais la classe de proposition sélectionnée de sorte que la probabilité que _C_ s'ensuive de quelque _B_ en général est précisément la même que la probabilité que _C_ s'ensuive de ces _B_ déductibles de _A_.
Les mêmes restrictions qu'auparavant s'appliquent.
Il pourrait se produire que la probabilité que _B_ s'ensuive de _A_ soit affectée par ce que certaines propositions de classe _B_ s'ensuivent de plusieurs propositions différentes de classe _A_.
Mais, en pratique, toutes ces restrictions n'ont que peu de conséquences, et il est d'ordinaire admis comme principe universellement vrai que la probabilité que, si _A_ est vrai, _B_ l'est aussi, multipliée par la probabilité que, si _B_ est vrai, _C_ l'est aussi, donne la probabilité que, si _A_ est vrai, _C_ l'est aussi.

Il existe une règle supplémentaire à celles-ci, dont il est fait grand usage.
Elle n'est pas universellemment valide, aussi les plus grandes précautions doivent être prises en l'employant --- il faut prendre le double soin de, d'abord, ne jamais l'employer lorsqu'elle implique de sérieuses erreurs; ensuite, de ne pas se priver d'en tirer profit dans les cas où elle peut être employée.
Cette règle dépend du fait qu'en bien des cas la probabilité que _C_ soit vrai si _B_ l'est, est substantiellement la même que la probabilité que _C_ soit vrai si _A_ l'est.
Supposons, par exemple, que l'on ait le nombre moyen d'enfants mâles nés à New York; supposons que l'on ait aussi le nombre moyen d'enfants nés durant les mois d'[été] parmi les enfants nés à New York.
En ce cas, on peut supposer sans aucun doute, du moins en tant qu'estimation très proche (d'ailleurs aucun calcul élégant n'a cours en ce qui concerne les probabilités), que la proportion de mâles parmi tous les enfants nés à New York est la même proportion que la proportion d'enfants nés en été à New York, et partant, si les noms de tous les enfants d'une année étaient placés dans une urne, l'on pourrait multiplier la probabilité que quelque nom tiré serait le nom d'un enfant mâle par la probabilité que ce soit le nom d'un enfant né en été, afin d'obtenir la probabilité que ce soit le nom d'un enfant mâle né en été.
Les questions de probabilité, dans les traités qui portent sur ce sujet, se rapportent généralement à des questions de billes tirées d'une urne, de jeux de cartes, et ainsi de suite, pour lesquelles le traitement du problème de l'_indépendance_ des événements, comme elle est appelée --- c'est-à-dire, la question de ce que la probabilité de _C_, sous l'hypothèse _B_, est la même que sa probabilité sous l'hypothèse _A_, est généralement très simple; mais, dans l'application des probabilités aux questions ordinaires de la vie, il s'agit souvent d'une question très profonde de savoir si deux évènements peuvent être considérés indépendants avec suffisamment de précision ou non.
Dans tous les calculs portant sur des cartes il est généralement postulé que les cartes ont été abondamment battues, de sorte que toute main est généralement indépendante d'une autre.
En fait les cartes sont rarement, en pratique, mélangées suffisamment pour que cela soit vrai; aussi, dans un jeu de whist, où les cartes se retrouvent par suites de quatre de la même suite, et sont ainsi rassemblées, elles restent plus ou moins par ensembles de quatre cartes de la même suite, et ce même après qu'elles aient été mélangées.
Du moins certaines traces de cet assemblage perdureront, en conséquence de quoi le nombre de « courtes suites », comme on les appelle --- c'est-à-dire, le nombre de mains où les cartes sont très inégalement divisées du point de vue des suites --- est plus petit que celui donné par le calcul; de sorte que, lorsqu'il y a fausse donne, et que les cartes sont éparpillées sur la table et donc abondamment mélangées, il est courant de dire qu'à la prochaine main il y aura généralement des courtes suites.
Il y a quelques années un de mes amis, qui jouait souvent au whist, eut la bonté de compter le nombre de piques qu'on lui a distribué en 165 mains, où les cartes avaient, en l'occurence, été mélangées plus soigneusement qu'à l'accoutumée.
Selon le calcul, il aurait dû y avoir 85 de ces mains dans lesquelles mon ami obtenait trois ou quatre piques, mais en réalité il y en eu 94, ce qui montre l'influence d'un mélange imparfait.

Selon la vision adoptée ici, ce sont les seules règles fondamentales de calcul des chances.
Une règle additionnelle, dérivant d'une conception des probabilités différente, est parfois donnée dans certains traités, qui si elle était vraie pourrait former la base d'une théorie du raisonnement.
Étant, comme je la crois, absolument absurde, sa considération ne sert qu'à nous conduire à la véritable théorie; et c'est dans la perspective de cette discussion, qui doit être remise au prochain numéro, que j'ai porté la doctrine des chances à l'attention du lecteur à ce stade précoce de notre étude de la logique des sciences.

---
title: "Une théorie probabiliste de la causalité"
author: "Patrick Suppes"
translator: "Samuel Barreto"
date: "2019-06-14"
draft: true
---

# Introduction

Dans un essai fameux sur la causalité, Bertrand Russell (1913) énonce ce qui suit.

« Tous les philosophes, de quelque école que ce soit, s’imaginent que la causation est l’un des axiomes ou postulats fondamentaux de la science, or, étrangement, dans les sciences développées telles que l’astronomomie gravitationnelle, le terme de “cause” ne se recontre jamais. ...
La loi de la causalité, crois-je, comme bien des choses dont les philosophes se préoccuppent, est une relique d’un temps révolu, ne survivant, comme la monarchie, que parce que l’on croit à tort qu’elle n’est d’aucun mal. ...
Le principe “aux mêmes causes les mêmes effets”, dont les philosophes croient qu’il est vital à la science, est ainsi résolument futile.
Dès lors que les antécédents ont été donnés suffisamment clairement pour que l’on soit en mesure de calculer le conséquent avec quelque exactitude, les antécédents s’en trouvent si compliqués qu’il devient hautement improbables qu’ils se reproduisent jamais.
Ainsi, si c’était là le principe en jeu, la science n’en resterait que platement stérile. ...
Il ne fait pas de doute que la raison pour laquelle la vieille “loi de causalité” ait continué si longtemps à fréquenter les livres des philosophes est simplement que l’idée d’une fonction est étrangère à la plupart d’entre eux, et partant ils vise à un énoncé par trop simplifié.
Il n’est pas question de répétitions d’une “même” cause produisant les “mêmes” effets; ça n’est pas dans quelque similitude de causes et d’effets que consiste la constance des lois scientifiques, mais dans une similitude de relations.
D’ailleurs même “similitude de relations” est une phrase trop simple; “similitude d’équations différentielles” est la seule phrase correcte. »

Sans doute que la chose la plus drôle dans cet extrait de Russell est qu’il prétend que l’usage du terme « cause » en physique n’a plus cours.
Contrairement à l’époque à laquelle Russell écrivit ce passage, les mots « causalité » et « cause » sont couramment et largement employés par les physiciens dans leur travaux les plus sophistiqués.
Il n’a guère un numéro de _Physical Review_ qui ne contienne au moins un article mentionnant dans son titre les termes de « cause » ou de « causalité ».
Un titre typique de ce genre est celui d’un volume récemment édité par le physicien renommé, E.P. Wigner, « Les relations de dispersions et leur connexion à la causalité » (1964).
Un autre bon exemple est celui de l’article récent par E.C. Zeeman (1964), « La causalité implique le groupe de Lorentz ».
^[Pour plus de preuves, les cinq titres suivants sont cités:
B. Ferretti, « Sur la possibilité d’une théorie quantum-relativiste macroscopiquement causale » (1963);
M. Gell-Mann, M.L. Goldberger et W.E. Thirring, « L’usage des conditions de causalité dans la théorie quantique » (1954);
W. Schützer et J. Tiomno, « Sur la connection des matrices de diffusion et dérivatives avec la causalité » (1951);
Yu. M. Shirokov, « Microvariance et microcausalité dans la théorie quantique » (1963) et
N.G. Kampen, « S-matrices et condition de causalité » (1953).]
Aussi le premier point que je souhaite avancer, est que les discussions de causalité font désormais partie intégrante de la physique contemporaine.
Les raisons à cela sont, à mon sens, très proches des raisons pour lesquelles les notions de causalité continuent à constituer un ingrédient important du discours ordinaire, et le resteront à n’en pas douter.

À la fin de la citation de Russell, est soulignée la substitution à un discours sur les causes un discours sur les relations fonctionnelles, ou plus exactement, un discours sur des équations différentielles adaptées.
Cette remarque s’inscrit tout à fait dans l’esprit de la physique classique, lorsque les phénomènes physiques en question étaient perçus comme bien mieux compris au niveau fondamental qu’ils ne le sont de nos jours.
L’on a le sentiment qu’en physique contemporaine la situation est très proche de l’expérience ordinaire, c’est-à-dire qu’il n’est pas possible d’appliquer des lois fondamentales simples pour en dériver des relations exactes telles qu’exprimées par les équations différentielles.
Ce sur quoi l’on a prise est une diversité de relations hétérogènes et partielles.
Au sens brut de l’expérience ordinaire, ces relations partielles expriment souvent des relations causales, aussi est-il bien naturel de parler de causes de la même façon qu’on le fait dans la conversation de tous les jours.

Au regard de l’analyse philosophique de la causalité, l’épisode probablement le plus déroutant dans l’histoire de la pensée fut le reigne de la mécanique Newtonienne, du début du dix-huitième siècle jusqu’à la fin du dix-neuvième.
L’apparente universalité et certitude de cette mécanique a conduit Kant et d’autres philosophes à une notion de causalité erronée.
L’irrésistible succès empirique de la mécanique Newtonienne, particulièrement en ce qu’elle rend compte des mouvements du système solaire, a inévitablement placé les notions de causalité et de déterminisme sous sa coupe.
Dans les grandes heures de la mécanique classique au dix-neuvième siècle, il était impossible de parler de causes sans les penser comme intrinsèquement déterministes.
Probablement l’expression la plus claire de cette conception se trouve dans le traité de Laplace sur la probabilité.
Le fameux passage sur le caractère déterministe de l’univers est un énoncé classique de la position du dix-neuvième siècle, or celui-ci est tout à fait en désaccord avec les notions de causalité employées dans le discours ordinaire, et même avec celles qu’emploie Laplace à l’application de sa théorie des probabilités.

C’est un point important à souligner pour la discussion subséquente que le concept ordinaire de la causalité n’est pas résolument intrinsèquement déterministe.
Nous avons tous déjà dit, à un moment ou un autre, des choses telles que:
« Sa manière de conduire imprudente ne peut qu’entraîner un accident. »
Ce que l’on entend par là est que la probabilité que cette personne ait un accident est élevée, et que sa manière de conduire sera une cause, au moins partielle, de l’accident.
Les mots « ne peut que » signifient que la probabilité d’avoir un accident est élevée.
Le mot « entraîner » véhicule la relation causale entre la conduite imprudente et l’accident prédit.
Si la personne en question n’avait pas d’accident pendant une longue période de temps et qu’elle le faisait remarquer au cours d’une conversation, cela entraînerait naturellement des négations de la tête et la remarque, « C’est difficile à croire. Il faut voir comme il conduit. Je n’arrive pas à comprendre comment il se fait qu’il n’ait pas encore eu d’accident sérieux ».

Il existe un grand nombre d’expressions en français qui véhiculent des notions causales du même genre que l’expression « ne peut qu’entraîner ».
Un exemple courant est l’expression « du fait de » (_due to_).
Une enseignante se trouve devoir dire à un étudiant, « Du fait de ta propre fainéantise, tu échoueras selon toutes probabilités à ce cours ».
Il y a, bien sûr, l’expression naturelle et fréquemment employée « parce que ».
Une mère dit à son enfant, « Parce qu’il fait froid aujourd’hui, l’on ne pourra probablement pas aller au cirque demain ».
Ou, dit-elle encore, « L’enfant est effrayé à cause du tonnerre », ou plus tard, « L’enfant a peur du tonnerre ».
Elle ne veux pas dire qu’à chaque fois que l’enfant entend le tonnerre, un état d’effroi s’ensuive, mais plutôt qu’il y a une probabilité relativement élevée que cela arrive; et lorsque cela arrive, la cause de l’effroi est le tonnerre.
Si une commerçante éreintée disait, « Je suis debout toute la journée et mes pieds me fatiguent », elle ne voudrait pas dire qu’absolument toutes les fois où elle se tient debout au travail pendant longtemps, ses pieds seront douloureux à la fin de la journée.
C’est plutôt que cela arrivera avec un degré de probabilité élevé, et « la » cause de la douleur est l’événement antérieur de sa station debout une bonne partie de la journée.
Dans ce dernier exemple, nous sommes en présence d’une cas familier pour lequel la conjonction omniprésente « et » est employée pour relier deux énoncés, dont le premier exprime la cause de l’événement décrit par le second.
Quand l’analyse causale est partielle, il est sans doute particulièrement naturel d’employer « et » plutôt que « parce que ».

Il est facile de construire un grand nombre d’exemples additionnels du langage causal ordinaire, exprimant des relations causales éminemment probabilistes.
L’une des raisons principales à ce caractère probabiliste est la nature à structure ouverte (_the open-textured nature_) de l’analyse des événements tels qu’exprimés par le langage ordinaire.
Les conditions de complétude et de clotûre qui font tant partie de la physique classique sont totalement exclues du discours ordinaire.
Aussi en décrivant une relation causale en circonstances courantes, l’on n’établit pas explicitement les conditions limites et les contraintes pesant sur les interactions entre les événements en questions et d’autres événements que l’on ne mentionne pas.
Par contraste, en physique classique, c’est un standard que de poser les conditions limites du système et d’énumérer explicitement toutes les forces opérant dans le système à un temps donné.
C’est précisément ce manque de connaissance des causes qui a conduit naturellement à l’introduction de concepts de probabilités dans l’expression des relations causales.
Comme Laplace le faisait remarquer pour diverses raisons, la théorie des probabilités est a pour dessein de découvrir et d’analyser les causes partielles en situations complexes pour lesquelles une analyse causale complète n’est pas réalisable.
Laplace avait à l’esprit les applications de la théorie physique systématique à de telles situations complexes, mais c’est précisément pour les même raisons que les concepts de probabilités sont si naturels au discours ordinaire portant sur les causes.
Une analyse causale complète est bien trop complexe et subtile, et n’est pas le but que sert le discours ordinaire.

Il est intéressant de noter dans ce contexte que l’analyse des causes dans un cadre juridique donné par l’ouvrage récent de Hart et Honoré (1959) semble plus proche de la physique classique que le discours ordinaire quant aux causes, au regard du rôle des concepts de probabilité.
Les raisons semblent évidentes.
C’est une caractéristique de l’analyse juridique, autant que de la physique classique, de ne pas se satisfaire de résultats ouverts et probabilistes.
Un jury n’est pas autorisé à rendre le verdict que l’accusé a probablement commis le crime.
Dans une poursuite civile concernant une rupture de contrat, le juge ne peut dire, « Il semble probable qu’une rupture de contrat se soit produite, mais cela n’est pas certain, aussi appliquerons-nous la règle d’utilité suivante dans l’octroi de dommages et intérêts ».
En d’autres termes, la loi recourt en pratique à une fiction très similaire à celle qui est inscrite au cœur de la physique classique.
Il est significatif que dans la discussion relativement longue et détaillée de la causation dans le livre de Hart et Honoré ne soit guère fait mention de questions de probabilité.
^[En discutant de ce passage lors de deux séminaires sur la causalité que j’ai donné à la London School of Economics (Printemps 1966), l’on m’a fait remarqué que dans des affaires civiles, comme par exemple ceux traitant d’accidents de voiture, il y a parfois des jugements attribuant le blâme entre les deux parties.
Néanmois, pour autant que je le sache, cette attribution n’est pas censée refléter un jugement du partage proportionné des torts et n’est jamais formulé explicitement en termes probabilistes.
En outre, le concept de blâme escompté, le pendant de celui d’utilité escomptée, n’est jamais employé.]

L’omission de considérations sur les probabilités est sans doute la plus grande faiblesse de l’analyse fameuse de Hume sur la causalité.
Comme il est notoirement connu, Hume disait que les relations entre cause et effet ont trois caractéristiques essentielles, c’est-à-dire, la contiguïté, la succession dans le temps, et la conjonction constante.
En d’autres termes, les causes et leurs effets sont contigus dans l’espace et le temps, une cause précède son effet dans le temps, et les causes sont suivies de leurs effets de façon constante.
L’extrait important sur la conjonction constante dans le _Traité de la Nature Humaine_ (Édition Selby-Bigge, 1888, pp. 86-87) est le suivant:

> Il est aisé de remarquer que, quand nous suivons cette relation, l’inférence que nous tirons de la cause à l’effet ne dérive pas simplement de l’inspection de ces objets particuliers et d’une pénétration de leurs essences, telle qu’elle puisse découvrir la dépendance de l’un par rapport à l’autre.
> Il n’est aucun objet qui implique l’existence d’un autre objet, si nous considérons ces objets en eux-mêmes et si nous ne regardons pas au-delà des idées que nous nous en formons.
> Une telle inférence s’élèverait jusqu’au niveau de la connaissance et impliquerait l’absolue contradiction et l’impossibilité d’admettre quelque chose de différent.
> Mais comme toutes les idées distinctes sont séparables, il est évident qu’il ne peut y avoir une impossibilité de ce genre.
> Quand nous passons d’une impression présente à l’idée d’un objet, nous aurions pu séparer l’idée de l’impression et lui substituer toute autre idée.

> C’est donc seulement par EXPÉRIENCE que nous pouvons inférer l’existence d’un objet de celle d’un autre.
> La nature de l’expérience est celle-ci: nous nous souvenons d’avoir eu de fréquents exemples de l’existence d’objets d’une espèce, et nous nous souvenons aussi que les individus d’une autre espèce d’objets les ont toujours accompagnés, et ont existé dans un ordre régulier de contiguïté et de succession par rapport à eux.
> Ainsi, nous nous souvenons d’avoir vu cette espèce d’objet que nous nommons _flamme_ et d’avoir senti cette espèce de sensation que nous nommons _chaleur_.
> Nous rappelons également à l’esprit leur constante conjonction dans tous les cas passés.
> Sans plus de cérémonie, nous nommons l’une _cause_ et l’autre _effet_, et inférons l’existence de l’un de l’existence de l’autre.
> Dans tous les cas à partir desquels nous sommes instruits de la conjonction de causes particulières et d’effets particuliers, à la fois les causes et les effets ont été perçus par les sens et nous nous en souvenons; mais, dans tous les cas où nous raisonnons sur eux, c’est seulement l’un que nous percevons ou dont nous nous souvenons, et l’autre se donne en conformité avec notre expérience passée.

> Ainsi, chemin faisant, nous avons, sans nous apercevoir, découvert une nouvelle relation entre la cause et l’effet, quand nous nous y attendions le moins et que nous nous employions entièrement à un autre sujet.
> Cette relation est leur CONJONCTION CONSTANTE.
> Contiguïté et succession ne suffisent pas à nous faire affirmer que deux objets sont cause et effet, à moins que nous nous apercevions que ces deux relations se conservent dans plusieurs cas.
^[Traité de la Nature Humaine, traduction Philippe Folliot 2003. NdT]

Hume fait suivre ce passage d’une analyse des raisons pour lesquelles le concept de conjonction constante est celui qui convient pour remplacer l’idée fallacieuse d’une connection nécessaire existant entre une cause et ses effets.
L’argument avancé ici est qu’en se restreignant au concept de conjonction constante, Hume n’était pas tout à fait fidèle à l’emploi des notions causales dans le langage et l’expérience ordinaire.
Pour ainsi dire, la modification de l’analyse humienne que je propose consiste à dire qu’un événement est la cause d’un autre si l’occurence du premier est suivie avec un degré de probabilité élevé par l’occurence du second, et qu’il n’y a pas de troisième événement dont on pourrait user pour factoriser les relations de probabilité entre le premier et le second événement.

C’est l’objet de cette monographie que de déterminer les détails techniques de cette idée fondamentale et d’en appliquer les résultats à certains des problèmes philosophiques caractéristiques qui se font jour dans les discussions de causalité.
La partie 2 développe une analyse de relations causales entre événements dans un cadre probabiliste standard.
Le partie 3 examine dans quelle mesure cette analyse peut encore être retenue lorsque seulement des relations de probabilité qualitatives sont en jeu, et la partie 4 développe une algèbre causale qualitative.
La partie 5 analyse les relations causales entre variables quantitatives ou entre propriétés, et comme l’on pourrait s’y attendre, recourt comme concept central au concept probabiliste de variable aléatoire.
Dans la partie 6, la dernière partie, un grand nombre de problème de causalité sont discutés, certains plus exhaustivement que d’autres, allant des problèmes de direction du temps à ceux du libre arbitre.

# Relations causales entre événements

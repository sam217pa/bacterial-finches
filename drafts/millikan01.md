---
title: "Un Monde Coalescent"
author: "Ruth Millikan"
translator: "Samuel Barreto"
date: 2019-03-18
draft: true
---

# Aperçu

Toute chose dotée d’une structure tendant activement à se maintenir ou se reconstituer au cours du temps, par simple persistance, par cycle, par reproduction ou parce qu’elle est cause de sa réplication, maintient ou accroît sa propre espèce en diminuant les matériaux et les ressources constituants d’autres espèces.
Ainsi, l’essentiel de ce qui advient dans le monde ou bien perdure ou bien duplique l’existant.
L’essentiel du monde naturel est auto-organisé en individus discrets et en espèces naturelles étroitement entrelacées, avec des espaces raisonnablement larges entre eux.
Il en résulte une variété limitée dans le temps et l’espace capable de supporter l’induction.
Les similitudes entre étapes temporelles d’un individu sont semblables aux similitudes entre membres d’une espèce naturelle en ce sens qu’elles sont similaires pour une bonne raison, de sorte que la connaissance d’un individu au cours du temps est très proche de la connaissance inductive d’une espèce naturelle par rapport à ses membres.
Les propriétés de chacun de ces individus reconstitués ou espèces se chevauchent dans le temps ou entre instances de sorte qu’il y a beaucoup à apprendre d’elles, et il y a généralement plusieurs manières alternatives de les identifier de façon fiable.
À côté de ces espèces naturelles il y a aussi des « catégories réelles », dont les membres ont en commun non, ou pas seulement, des facteurs déterminants mais des facteurs déterminables, pour lesquels chaque membre de la catégorie propose sa propre valeur stable.
Ceci aide à rendre compte de la rapidité de la découverte de nombreuses espèces naturelles dans le développement cognitif.
Ces attributs structuraux du monde sont les fondements, d’abord pour la cognition, ensuite pour le développement du langage.
Mais les divisions entre espèces et entre catégories ne sont pas franches, aussi cela doit-il être permis par la cognition et le langage.

# Espèces naturelles

Langage et pensée font partie du monde.
Pour comprendre en quoi ils s’adaptent au reste du monde, il nous faut considérer à la fois leur structure propre et la structure plus globale du monde auquel ils s’adaptent.
Tous les mondes ne peuvent être pensés ou décrits.
Un monde pensable ou descriptible doit être organisé de façon adéquate.
Ce chapitre décrit certaines structures évidentes présentes dans notre monde, un certain type d’organisation que le monde arbore clairement, en quoi ce type de structure du monde est important à l’animal connaissant, et offre une explication partielle de la manière dont le monde s’organise de cette façon.
Le deuxième chapitre explique en quoi cette organisation offre une prise au langage, permettant d’employer les noms d’individus et d’espèces et de les dénommer.

Commençons par une observation grossière, ce que Dennett appellerait une « pompe à intuition », qui semble en principe saine et totalement généralisable.^[Kevin Keamey souligne que les sections 9 et 10 de l’ouvrage de C.D. Broad « The Relation Between Induction and Probability », 1920, sont dévolues précisément aux mêmes points que ceux de ce paragraphe et du suivant, et d’une manière très similaire.
Si donc je ne suis pas claire, essayez C.D. Broad, dont les écrits sont toujours limpides.]
Imaginez un gigantesque graphe représenté dans un espace multidimensionnel sur lequel chacun des objets physiques ordinaires du monde est représenté, à un temps donné, par un point.
Le graphe représente un large espace logique où chacune des dimensions correspond à une propriété différente.
Commençons par les propriétés physiques uniquement, mais l’idée se généralisera facilement.
Il y a une dimension pour la masse et une dimension pour le volume, un ensemble de dimensions qui décrit la forme effectivement,^[En fait, de quelle façon l’on comparerait des formes volumétriques entre elles de cette manière n’est pas clair pour moi.
Les similitudes entre bien d’autres espaces de propriétés sont également malaisées sinon impossibles à traiter de cette façon.
Cela n’a pas d’importance pour l’idée générale de ce que je veux avancer.]
des dimensions pour diverses propriétés de réflectance électromagnétique, une dimension pour la température, des dimensions pour la quantité de chacun des constituants matériels, des dimensions de flexibilité, maléabilité, de viscosité, de granularité de chacun des matériaux, et ainsi de suite.
Imaginez que ce graphe ait suffisamment de dimensions pour que chaque object physique du monde puisse être décrit entièrement à toute fin ordinaire simplement en le situant par un point sur le graphe.

Il nous faut noter la chose suivante.
D’abord, lorsque tous les objets existant réellement ont été représentés par un point situé de façon approprié sur le graphe, l’ensemble du graphe, à l’exception de quelques régions très petites, serait vide.
Qu’il en soit ainsi vient du fait que presque toute liste arbitraire de propriétés logiques compatibles qui décrirait un objet de façon exhaustive, même s’il était désigné de façon relativement vague, échouerait à décrire un objet réel.
Ensuite, là où il ne serait pas vide, le graphe contiendrait essentiellement des amas de points éminemment proches sous de nombreuses dimensions, étroitement regroupées en larges sous-espaces de l’espace des propriétés.
Il y aurait un amas pour tous les lapins, mettons, un autre pour toutes les cathédrales Gothiques, un autre pour tous les restaurants McDonald, un autre pour tous les disques compacts et ainsi de suite, et il y aurait, pour l’essentiel, de larges régions vides autour de chacun de ces _clusters_ sous diverses dimensions.
C’est-à-dire que, pour l’essentiel, ces _clusters_ ne se fondent pas imperceptiblement l’un dans l’autre, pas plus qu’il n’y a de volutes de points étroitement reliés allant d’un cluster à l’autre.
Il n’y aura aucune voie allant par quelque route que ce soit de Kermit la grenouille au voisinage immédiat de la Tour Eiffel, ou à mon ordinateur, ou même à Toady le crapaud.
Il n’y a aucun moyen imaginable de changer lentement Kermit en mon ordinateur, ni même en Toady, de sorte qu’il y ait des choses réelles proches de chacune des étapes du changement, proches dans la plupart des dimensions.
Le monde d’objet physique est pour une grande part empli de _clusters_ aux propriétés densément entrelacées, des _clusters_ pour l’essentiel distincts, quoique pas toujours parfaitement séparés les uns des autres.
Ce genre de structure est ce qui sous-tend le succès d’inductions ordinaires et quotidiennes, ce qui permet de savoir ce qu’il faut attendre d’un autre membre de ce que l’on tient pour le même _cluster_.
Cela permet de savoir ce à quoi l’on doit s’attendre d’un chat, d’un camion, d’un piano ou d’une cathédrale.

En dehors des _clusters_ que l’on trouverait en représentant toutes les propriétés des objets matériels, il y a les _clusters_ que l’on trouverait en en représentant seulement certaines.
Sur le graphe de toutes les propriétés, ces _clusters_ apparaîtraient comme des _clusters_ de _clusters_, regroupés sous certaines dimensions mais pas toutes.
Ce sont des genre plus élevés, tels que amphibiens et restaurants.
De plus, au sein d’un _cluster_, il peut y avoir divers _clusters_ plus petits, de plus petits amas séparés complètement l’un de l’autre, ou presque complètement.
Les _clusters_ ne forment pas nécessairement de hiérarchies.
Les animaux diabétiques, par exemple, forment un _cluster_ qui transgresse les frontières d’espèces, ayant de nombreuses particularités en commun.

Les _clusters_ à un niveau d’abstraction donné peuvent ne pas montrer de propriétés qui soient communes à tous leurs membres sans être également communes à divers non-membres.
C’est-à-dire, ils ne montrent pas de propriétés universellement distinctives ressortant de descriptions nécessaires.
Les êtres humains peuvent n’être pas bipèdes, même à la naissance, ne pas être rationnels, ne pas posséder de langage, même adultes, et il n’y a pas de gènes que tout être humain ait en commun avec tout autre --- pas de gènes sans allèles --- en dehors des gènes également partagés par les membres de bien d’autres espèces.

De l’autre côté, ces _clusters_ montrent généralement un grand nombre de propriétés, ou un petit ensemble de propriétés, qui sont à la fois communes à beaucoup de membres et bien distantes de propriétés trouvées dans d’autres _clusters_.
C’est-à-dire, il y a souvent de nombreuses façons alternatives d’identifier les membres d’un même _cluster_ de façon relativement fiable, bien qu’aucune ne soit parfaitement sûre.
Il y a bien des façons d’identifier de façon fiable un être humain, un chat, un chêne, une automobile, un ordinateur ou un piano comme tels.
Certaines façons impliquent des propriétés uniques, d’autres impliquent des ensembles de propriétés.
Étant données les relations constamment variables et changeantes aux divers objets du monde, le fait que différentes méthodes d’identification soient disponibles pour reconnaître l’espèce-chat, l’espèce-camion, l’espèce-livre etc. est indispensable à l’accumulation et l’emploi de connaissances portant sur ces choses.
Être capable de reconnaître la même chose à nouveau alors qu’elle se montre aux sens d’une multitude de manières, offrant ainsi de nombreuses opportunités d’accumuler et d’appliquer des informations quant à elle, est, je crois, le défi le plus central à la cognition.
C’est un thème sur lequel nous reviendrons souvent dans les chapitres suivants.
Que les _clusters_ d’objets permettent généralement à leurs membres d’être identifiés par toute propriété d’un vaste ensemble de propriété ou tout ensemble desdites propriétés est essentiel à l’acquisition de connaissances portant sur eux et à l’emploi de ce que l’on a appris.

Dans les chapitres suivants je développe des thèmes concernant la cognition et le langage.
Ici je voudrais me concentrer sur ce qu’est le monde avant toute cognition.
Je voudrais généraliser les exemples du regroupement d’objets physiques en espèces grossières, et suggérer quelques explications quant à pourquoi ces regroupements, et d’autres, adviennent.
Presque toujours, pareils regroupements ne se font pas sans raison; ils n’adviennent pas simplement par chance.
J’emploierai le nom d’« espèce naturelle » pour les _clusters_ formés au sein d’une catégorie quelconque de choses, au sein de tout sous-espace logique multidimensionnel, dès lors que ce regroupement n’advient pas accidentellement mais pour une raison univoque.
Nous verrons, par exemple, que les belettes de Terre Jumelle (Putnam 1975) ne seraient pas membres de la même espèce naturelle que les belettes de notre Terre, car elles ne seraient pas similaires aux belettes de Terre pour les mêmes raisons que celles qui font que les belettes de Terre sont similaires les unes aux autres.

Parce que les _clusters_ d’espèces naturelles peuvent n’avoir pas de limites franches, le fait qu’une chose soit membre ou non d’une certaine espèce naturelle peut n’être pas déterminé; qu’on la traite ou non comme telle dépend des fins en jeu.
La notion « espèce naturelle » n’est d’ailleurs pas précise pour cette même raison.
Les _clusters_ d’espèces naturelles peuvent contenir, en fait contiennent généralement, de plus petits _clusters_ en leur sein, comme les chats parmi les mammifères, les Siamois parmi les chats, les Siamois diabétiques parmi les Siamois.
Et parce que les _clusters_ d’espèces naturelles sont ainsi au sein d’un, ou relatifs à, certaines sous-régions de l’espace des propriétés, les _clusters_ peuvent se recouper mutuellement, comme le _cluster_ des animaux hyperthyroïdiens (symptômes similaires, causes similaires, organes impliqués similaires, traitements similaires) recoupe le _cluster_ des chats.
D’importance notable, nombre d’espèces naturelles sont allongées ou multibranchées dans certaines parties de leurs sous-régions de propriétés, ou ont développé des protrusions dans des sous-régions quelque peu différentes, comme si l’espèce devait se déplacer ou s’étendre lentement, un groupe entier de propriétés variant de façon synchrone.
En effet, ce genre d’élongation, de protrusion, de branchage est très souvent causé par un changement bien réel au cours du temps, comme cela se produit lorsque d’anciennes espèces d’instruments ont évolué en nouvelles espèces, ou lorsque des animaux juvéniles maturent, prennent de l’âge, se reproduisent et meurent.
Les _clusters_ allongés ou branchés montrent souvent diverses régions plus denses séparées les unes des autres par des régions moins denses.
Diverses étapes distinctes et stables du cycle de vie sont présentes chez de nombreuses espèces animales, les têtards différant par de nombreuses dimensions corrélées des grenouilles, les enfants des adolescents et des personnes âgées.
Pareillement, on trouve des journaux, des magazines, des livres, des gazettes, des pamphlets, etc., sans qu’aucun n’ait de frontières strictes mais où chacun forme un amas distinct, un nombre soudainement bien plus large d’exemplaires très similaires au sein d’une espèce plus générale bien délimitée.
La notion « espèce naturelle » peut ainsi être pratiquement étendue aux cas des amas, des branches et des protrusions distinctes aussi bien qu’aux cas d’isolement total ou quasi-total.
Les espèces naturelles surviennent ainsi à divers degrés de netteté.
Lorsque des amas d’espèces sont clairement distincts mais avec quelques recoupements et types intermédiaires je dirai qu’il y a des « distinctions floues » entre elles; lorsqu’une espèce s’allonge ou branche je dirai qu’elle a des « étapes » ou des « branches » qui peuvent aussi fonctionner pour la cognition comme des espèces naturelles.
Diverses étapes ou branches ont souvent entre elles des distinctions floues mais discernables qui peuvent jouer un rôle dans la cognition (enfant, écolière, adolescente, jeune adulte).

Lorsque j’ai présenté plus haut la notion d’espèce naturelle au moyen de l’exemple des objets physiques, j’ai décrit les membres d’espèces naturelles comme des objets-temporels, comme ce qui est parfois appelé des « étapes temporelles » d’objets --- une précision importante que j’ai ignorée jusqu’alors.
En fait, de quelle façon l’on devrait organiser un espace de propriété pour comparer les similitudes de deux objets à travers l’étendue de leur existence temporelle, qu’elle soit longue ou brève, je n’en ai aucune idée.
Mais remarquez que ce sont précisément des objets temporels que nous rencontrons; ils sont nos sources originelles d’information.
D’un autre côté, c’est rarement à propos d’objets temporels que nous voudrions rassembler des informations.
Ce qu’il nous faut reconnaître et distinguer afin d’accumuler ou appliquer les connaissances que nous désirons n’est pas l’étape temporelle rencontrée mais les objets individuels dont elle constitue une étape ou l’espèce d’objets dont elle est une étape.
Parfois, ce qui compte, c’est de connaître l’individu, parfois c’est de connaître son espèce, et nous nous moquons alors bien de rendre compte de l’individu.
Ce qui révèle un parallèle inattendu mais important, entre la connaissance [_cognition_] d’individus perdurant et celle d’espèces naturelles, parallèle auquel je ferai souvent allusion.
Les diverses étapes temporelles d’un objet individuel représenté sur le graphe du genre de celui décrit plus haut formeront un _cluster_ très étroit --- en fait, une forme molle ou continue --- au sein d’une subdivision multidimensionnelle de l’espace des propriétés, une région solide souvent nettement délimitée sous certaines dimensions des _clusters_ représentant d’autres individus.
Si l’objet individuel se développe dans le temps, comme le font les objets vivants, cette région se développera dans de nombreuses dimensions telles que celles précédemment décrites.
Les objets individuels perdurant peuvent souvent être reconnus par différentes particularités --- souvent un grand nombre d’entre elles --- qui perdurent pendant de longues durées et qui les distinguent sinon de tous, au moins de la plupart des autres individus.
Ce qui rend leur connaissance parallèle à la connaissance d’espèces naturelles.
Le regroupement régulier de propriétés des étapes temporelles d’un individu n’est pas, assurément, ce qui les rend individus.
Les connections causales et spatio-temporelles entre ses étapes expliquant ces similitudes doivent entrer en jeu, de la même façon que les espèces naturelles ne le sont que lorsque leurs membres sont similaires pour une bonne raison.
Récemment, l’histoire a introduit des objets individuels produits en masse qui correspondent mal à ce genre de schémas, du moins tant qu’ils ne sont pas altérés par usage et abus d’usage.

Quelle diversité d’espèces d’individus et d’espèces naturelles
existe-t-il alors réellement, et pourquoi?

# Reproduction et Production de Masse

Comment se fait-il que tant d’objets physiques soient si semblables à d’autres, que leurs propriétés soient si corrélées, mais distantes d’objets d’autres _clusters_?
Pourquoi l’espace logique des objets est-il aggloméré et sous-aggloméré de cette façon?
Assurément, certaines raisons à cela ont trait aux nécessités causales et aux incompatibilités.
J’en dirai quelques mots plus loin en lien avec ce que j’appellerai les « espèces perpétuelles ».
Mais je voudrais d’abord me concentrer sur un autre type d’explication qui rend compte d’une bonne part de l’agglomération.

L’essentiel des objets physiques qui nous intéressent adviennent au monde par un processus quelconque de reproduction, ou, dans le monde moderne, de production de masse.
Un processus de « reproduction » implique à un moment donné une sorte de réplique.
Dans le cas d’espèces vivantes, par exemple, les gènes sont copiés.
La production de masse résulte de la simple répétition d’un processus.
Les items quittant la ligne d’assemblage ne sont pas copiés l’un à partir de l’autre mais découlent d’un processus répété unique.
La reproduction et la production de masse peuvent être impliquées toutes deux dans la prolifération d’une espèce.
Le _design_ d’un batteur électrique sortant d’une ligne d’assemblage découlera sans doute pour l’essentiel de ce qui s’est trouvé copié du _design_ de batteurs électriques antérieurs.
Les plans ou esquisses d’un processus de reproduction ou de la machinerie employée peuvent avoir été copiés de plans ou de machineries antérieurs.
Toutes les plantes et les parties de plantes, tous les animaux et toutes les parties d’animaux et leurs artéfacts (nids, digues, terriers) ont été, à bien des égards, reproduits.
^[Plus précisément, pour s’en tenir rigoureusement à ma propre terminologie, ça ne sont pas à strictement parler des « reproductions », mais des membres de « familles établies reproductives du premier ordre ou d’ordres supérieurs ».
Voir §12.3, pour plus de détail (LTOBC, Ch. 2).]
Les actes, activités ou événements fréquemment produits ou causés par ces objets sont également reproduits, constituant souvent de complexes développements répétants de nombreuses étapes ou de nombreux aspects.
Ainsi nous recontrons, par exemple, des humains et des chiens, des bras et des jambes, des arbres, des bâtons et du feu, des bananes et des beefsteaks, des humains qui mangent, dorment et courent.
Pareillement, la majorité d’objets artéfactuels a été ou bien produite en masse ou bien copiée d’exemples antérieurs de la même espèce.
Généralement, ils l’ont été parce qu’ils fonctionnaient bien ou parce qu’ils nous étaient agréables, ou, dans le cas de la production de masse, parce que l’on pensait qu’ils fonctionneraient bien ou seraient agréables.
Ainsi avons-nous des chaises à bascule, des échelles, des clochers d’église et diverses espèces d’ustensiles de cuisine, chacun étant généralement assez aisément reconnaissable comme membres d’une ou de plusieurs espèces naturelles distinctes.

Cette duplication n’est jamais que partielle, bien sûr.
Rien n’est jamais dupliqué à l’identique et la plupart des dupliques diffèrent de l’original à bien des égards.
Une photocopieuse duplique, mais les couleurs, ou encore les tailles et les matériaux dont sont fait les dupliques peuvent grandement varier.
Dans le cas d’une chaîne de reproduction, c’est seulement lorsque les items résultant sont totalement ou essentiellement des dupliques à certains égards, généralement parce qu’ils ont été dupliqués par le même processus, qu’il y aura prolifération des membres d’un _cluster_ distinct, d’une espèce naturelle.
La production continue ou dupliquée d’une espèce uniforme peut dépendre de la sélection naturelle ou de la probabilité d’une sélection naturelle, où « sélection naturelle » doit toujours être entendue non comme créatrice, mais comme rendant compte de la stabilité des caractères des membres d’une espèce dans le temps et l’espace.
La sélection naturelle et la sélection pour la reproduction d’artéfacts rend compte de l’agglomération, d’abord en éliminant l’inutile qui s’accumulerait sans cela par mutations ou erreurs de réplication, et partant, en utilisant les matériaux nécessaires à faire plus de copies de l’original.
Dans le cas d’espèces vivantes, les compétitions remportées contre d’autres individus pour la nourriture, l’habitat, et les partenaires libèrent des matériaux qui auraient été utilisés par ceux-là pour composer davantage d’individus similaires aux gagnants.
Dans le cas de la production artéfactuelle, les matériaux bruts employés dans la reproduction sont déviés d’autres usages à cet usage.
Il en résulte davantage d’individus de la même espèce et moins d’individus qui en divergeraient.

On peut généraliser cette description familière en y incluant persistance et cycles à la reproduction.
Assurément l’espèce d’évolution la plus importante qui rende possible toute cognition est l’évolution de ce que Lord Keynes appelait la « variété limitée », dont J.S. Mill a reconnu le premier la capacité à rendre compte de l’utilité de l’induction.
La variété limitée semble résulter d’une évolution graduelle de la stabilité, l’évolution d’individus et d’espèces stables.
Considérons d’abord de simples objets matériels ordinaires.
Certaines choses ayant trouvé le moyen d’advenir au monde ont été accidentellement construites de façon telle qu’elles ont persisté, grâce à leur organisation interne, en accord avec les lois de la conservation.
Ces choses restent, quand d’autres conçues pour se dissiper se sont dissipées.
Ce qui se trouvait être stable avait une tendance à perdurer, ne montrant que la variété accidentelle mais limitée qu’il se trouvait montrer.
Ensuite vinrent les choses qui se trouvent avoir été construites ou disposées de façon telle qu’elles décrivent des cycles, et plus tard les choses systématiquement copiées ou reproduites.
Partout où pareil processus stabilisant se trouve établi dans le monde, il emploie des matériaux et des localisations à la construction de ses continuants ou duplicants propres, éliminant toute disponibilité de ces matériaux pour la constitution d’autres choses.
Dès qu’ils sont initiés aléatoirement, les processus stabilisant tendent à prendre le dessus.
Il y a une raison à ce que la nature soit caractérisée par le principe de variété limitée.

# Espèces historiques

Les membres d’espèces telles que les espèces vivantes (les chiens, les jonquilles), leurs parties (les cœurs humains, les ailes d’oiseaux), leurs étapes (chenille, papillon), nombre de leurs états (la faim humaine, les états émotionnels, les états cognitifs divers) et leurs produits (artéfacts reproduits, des schèmes d’activité individuelle ou sociale, les mots), résultant de processus soutenus de reduplication, ont leurs propriétés caractéristiques en commun en raison des connections causales entre eux, ainsi qu’à l’assistance d’un environnement commun persistant.
Tout membre de l’espèce possède ces propriétés en partie parce que d’autres les ont possédées, ou parce qu’ils ont été causés par des choses connectées l’une à l’autre de cette façon.
Ces espèces peuvent être décrites comme des « coalescences » [_clots_] plutôt que des « agrégats » [_clumps_], car ses membres ont été, en quelques sortes, collés les uns aux autres, et nous aurons à nous rappeler de ceci.
Les belettes de Terre Jumelle, par exemple, ne font pas partie de la même motte que les belettes de Terre.
Les propriétés corrélées de ces espèces co-occurent de façon répétée dans le monde contingent historique réel, mais, en grande partie du moins, non parce qu’il y a une nécessité naturelle à ce qu’ils co-occurent.
^[C’est l’une des façons dont elles diffèrent de ce que Boyd appelle des « espèces homéostatiques agglomérées » [_homeostatic cluster kinds_] (Boyd 1999)]
Pour cette raison, ce sont des exemples d’espèces que j’appelle « espèces historiques », en ce sens que, comme les individus, ils adviennent de manière contigente « dans l’histoire », c’est-à-dire, dans l’espace et le temps de notre monde.
Il faut également qu’elles aient une certaine forme d’histoire, de la sorte que j’ai décrite.
Étant données les lois naturelles, d’autres espèces auraient pu peupler le monde à leur place; d’autres combinaisons récurrentes et stables de propriétés auraient pu être à leur place.
Une espèce historique rassemble un ensemble de propriétés de façon répétée, moins en raison d’une dépendance causale entre ces propriétés qu’en raison de leur source ou origine commune.
C’est là une autre raison pour laquelle les belettes de Terre Jumelle forment une espèce historique différente des nôtres.
Grâce aux relations causales qui unissent une espèce historique, ses membres se trouvent généralement agglomérés, dans une proximité d’espace-temps relativement élevée.

Les espèces résultant d’une production de masse ou de processus impliquant et la réplication et la production de masse (les tournevis, les disques durs) sont des espèces historiques.
Des processus cycliques stables peuvent aussi produire des espèces historiques.
La reproduction continue d’êtres vivants représente une collection énorme de processus cycliques de ce genre.
Les étapes répétées de ces cycles portent souvent des noms tels que « embryon », « nouveau-né », « chatton », « tétard », « papillon », et comptent comme espèces historiques également.
Mais il y a d’autres espèces historiques que les objets physiques résultant de cycles.
Il y a les espèces formées en partie par la persistance dans le temps de quelque individu ou quelques individus historiques.
La Terre perdure et pivote, occasionnant des patrons cycliques quotidiens, de même qu’elle décrit des ellipses autour du soleil persistant, occasionnant des patrons cycliques annuels.
Ces cycles affectent l’atmosphère persistante de la Terre, occasionnant des tendances [_patterns_] cycliques météorologiques et des tendances cycliques dans la vie des animaux et des plantes d’une région donnée.
À tout endroit de la Terre, la nuit et l’hiver, par exemple, ne sont pas seulement plus sombres et plus froids que le jour et l’été, mais emplis de leurs espèces propres de processus et d’activités.
Partout, les nuits et les hivers sont des choses qui se répètent de bien des façons instructives.
Pensez à travers l’œil d’un petit enfant qui apprendrait et ferait l’expérience des nombreuses propriétés de toutes ces choses pour la première fois.

Certaines espèces historiques sont des hybrides entre les espèces cycliques et les espèces produites par reproduction.
Les individus d’une espèce vivante donnée peuvent eux-mêmes décrirent des cycles répétés à travers des phases, changeant de façons diverses de l’été à l’hiver ou encore du jour à la nuit.
À l’exception de l’hermine et de la belette, nous n’avons généralement pas de noms simples pour ces phases mais nous pouvons en savoir beaucoup de leurs propriétés et activités agglomérées.
Ces phases sont des espèces naturelles qui peuvent être ré-identifiées et re-connues, comme le sont les espèces animales.

L’hiver et la nuit à New York et Johannesburg ne sont assurément pas des objets physiques.
Ce qui soulève la question générale de savoir quelle diversité d’espèces historiques existe-t-il, au-delà des objets physiques.
Quelques exemples en sont la malaria, la colère, les dances (la valse, la bourrée), les mélodies et les symphonies, les contes populaires et les jeux, les mariages Protestants, les enterrements Juifs, les messes Catholiques, les classes universitaires, les cérémonies de remise des diplômes.
Les derniers éléments de cette liste sont des espèces sociales.
Il existe aussi des espèces sociales constituées d’individus sociaux (les troupes de Scout), ce qui nous conduit au sujet des individus en général.

# Individus

Les individus sont des entités historiques au sens d’« historique » que j’emploie.
Ils sont localisés dans le temps et l’espace.
Les objets individuels pour lesquels on dispose de noms persistent.
Ils se maintiennent et se reconstituent eux-mêmes dans les mêmes endroits, ou dans des endroits continuellement connectés pendant une période de temps.
Toutes les configurations physiques ne le font pas.
La configuration de la matière constituant une étape d’explosion ne le fait pas.
Mais si une structure présente la forme adaptée pour perdurer ou pour se reconstituer continuellement au cours d’une période temporelle, comme le présente la structure d’un rocher ou d’un organisme vivant, dès lors qu’elle perdure, elle prive ses matériaux et ses localisations d’appartenance à toute forme d’espèce opposée.
Une fois advenus, les individus persistants, comme les espèces historiques, tendent entre eux à user d’une grande part du monde.

Pensez l’individu comme une motte d’étapes temporelles de cet individu.
Les étapes temporelles composant l’entièreté d’un objet physique individuel dans le temps sont contiguës dans leur localisation spatio-temporelle.
Il n’y a pas de vide temporel ou spatial entre elles, et elles sont également contiguës du point de vue de leurs diverses dimensions dans l’espace des propriétés.
De cette façon, les ensembles d’étapes temporelles constituant un individu sont différentes de ceux constituant les coalescences d’espèces réelles considérées plus haut.
Cependant, ces deux sortes d’agglomérats sont susceptibles de changer de certaines façons au cours du temps.
Des configurations légérèment postérieures peuvent être de très proches répliques de configurations antérieures, mais les répliques de répliques de répliques peuvent n’être en rien similaires aux originaux, comme c’est le cas lorsque les enfants grandissent et quand les espèces évoluent.
Il existe aussi de troublants cas intermédiaires entre l’individu et l’espèce du point de vue de la continuité espace-temps.
Les plantes individuelles qui forment un bosquet de peuplier faux-tremble ou un carré de fraise, par exemple, se sont toutes lentement développées à partir d’une plante originelle et peuvent ou non lui être encore liées spatialement, selon que d’autres évènements externes ont causé leur séparation ou non.
Le bosquet ou le carré composent-ils un individu très compliqué, ou sont-ils composés de nombreux membres d’une même espèce?
Les Amibes et les Paramécies croissent et se divisent lentement en deux, puis en deux à nouveau, de sorte qu’il n’y a pas de discontinuité spatio-temporelle entre ancêtres et progéniture.
Les organismes d’une lignée particulière sont continus de proche en proche, mais simultanément discontinus dans l’espace vis à vis d’autres lignées.

De quelle façon l’on doit distinguer les individus historiques d’espèces historiques, et dans quelle mesure certaines choses sont à la fois des individus et des espèces constituent n’est clairement qu’un enjeu verbal.
Ce qui importe à la compréhension de la connaissance est leur frappante similarité.
Dans les deux cas, il existe un _cluster_ local récurrent ou continu de propriétés; ce qui possède ces propriétés peut généralement être identifié par divers moyens alternatifs, promouvant ainsi l’apprentissage dans le temps des propriétés qui font partie du _cluster_ et permettant l’application inductive de cette connaissance.
^[Le fait qu’il y ait des similitudes entre individus et espèces naturelles est ce qui m’a conduit à les appeler toutes deux « substances » dans (Millikan 1984, 2000).
J’étais d’avis qu’Aristote avait profondément raison de placer les substances primaires et secondaires dans la même catégorie.]
C’est la diversité des stabilités à la fois dans le temps et l’espace de l’environnement d’un animal qui crée les opportunités de connaissance inductive, que ces stabilités se trouvent dans les individus au cours du temps ou dans la variété limitée des espèces naturelles.

Les espèces naturelles et les individus sont structurés de façons similaires et sont souvent pensés de manières parallèles.
Les propriétés d’individus qui restent relativement constantes ou fréquemment récurrentes sont attribuées aux individus eux-mêmes, plus qu’elles ne le sont à leurs étapes temporelles pour être ensuite évaluées à travers elles.
Nous pensons simplement de Pierre qu’il connaît le français ou qu’il est irascible, non que des étapes temporelles de Pierre connaissent toutes le français, ni que de nombreuses étapes temporelles sont coléreuses.
De la même manière, la façon dont l’on pense ordinairement aux Suisses, au tigre, aux personnes âgées peut être distordue lorsque l’on tente de l’exprimer (comme le faisait la tradition centrale en linguistique et philosophie, jusqu’à récemment (Liebesman 2011, Leslie 2015)) à l’aide de quantificateurs portant sur les instances de ces espèces.

Dans ce sens, Carson (1989) et Liebesman (2011) soutiennent que les phrases telles que « Les chiens aboient », « les abeilles piquent » et « les oiseaux volent » ne sont pas plus des généralisations quant aux chiens, abeilles et oiseaux que, mettons, « Les dinosaures sont éteints » ou « les moustiques sont très répandus ».
Liebesman prétend que les sujets de ces phrases sont simplement des espèces _nominales_, « les chiens aboient » présentant la même structure logique que « Socrate boit » (409).
D’autres ont débattu de ce que les prédicats de telles phrases peuvent être qualifiés d’espèces
^[Où leur sens n’est pas « décalé » [_type-shifted_].]
ou d’individus
^[Où leur sens n’est pas « décalé ».]
(Leslie 2015).
À mon sens il paraît raisonnable que les structures de ce type, en l’absence de plus d’informations, puissent continuer à faire référence indifféremment aux espèces ou aux individus.
Elles généralisent, lorsqu’elles le font, seulement sur les _rencontres_ [_encounters_] --- la rencontre d’un _chien à nouveau_, d’une _abeille à nouveau_, _d’un oiseau à nouveau_, ou, comme l’aurait dit Quine (1960) la rencontre de _plus de chiens_, _plus d’abeilles_ et _plus d’oiseau_.
« Les chiens mordent », par exemple, semble englober ou bien que certains chiens mordent, ou bien que tous mordent parfois ou dans certaines circonstances, généralisant ainsi seulement sur les _rencontres avec les chiens_, qu’ils soient les mêmes ou d’autres.
Conformément, notez que « Les chiens aboient », « _Canus domesticus_ aboie », « Le chien aboie » et « Un chien aboie » peuvent tous être lus comme synonymes.
Les phrases de ce type semblent exprimer des pensées portant sur un agglomérat de propriétés non-individuées, pensées qui portent seulement sur une chose pouvant être rencontrée ici ou là et pouvant montrer ou non ces traits ou ceux que d’autres ont rencontrés.

Il sera utile de garder cette possibilité à l’esprit pour la
compréhension de la manière dont les noms d’espèces naturelles, comme
les noms d’individus, peuvent être directement référentiels (§2.4).

Au delà des objets individuels se trouvent aussi des choses telles que des « individus sociaux ».
Il existe des colonies d’insectes, des troupes de lions, des clubs, des sociétés, des institutions financières, des corporations, des universités, des gouvernements, des systèmes monétaires, des cultures, des systèmes de cour, et ainsi de suite.
Chacune de ces structures individuelles tend à se reproduire dans le temps et l’espace, remplaçant ses parties au besoin, tout comme un organisme vivant.
C’est pour cela qu’il fait sens de les appeler « individus ».
Chacune est une chose dont on peut garder une trace et dont on peut apprendre au cours du temps.
Nous étudions les aspects de cultures ou de systèmes économiques, examinant de quelle façon ils s’entremêlent pour former un individu stable qui perdure dans le temps, se reconstituant continuellement.
Certaines espèces historiques sont des espèces sociales composées d’individus sociaux.
Pensez aux espèces telles que les troupes de Scout, les banques, les supermarchés, les pub Irlandais, le lycée, le mariage Catholique traditionnel, les cérémonies de remise des diplômes de telle ou telle institution ou type d’institution, les réunions suivant les _Robert’s rules of order_.
Elles sont toutes ou reproduites en nombre ou produites en masse à partir du même plan historique.
Pensez aussi aux espèces sociales retracées par les « cadres » [_frames_] de Minsky (1981) ou par les « scripts cognitifs » de Schank et Abelson (1977), contenant des instructions sur la façon de se comporter, ce qui est attendu, et ce à quoi il faut s’attendre lorsque l’on va, par exemple, au restaurant ou au cinéma.

En somme, les choses ayant tendance à se reconstituer ou se reproduire elles-mêmes au cours du temps tendent à s’emparer du monde à l’exclusion d’autres choses par ailleurs également cohérentes avec les lois naturelles.
Ce sont des individus ou espèces « historiques ».
Celles-ci représentent une cause majeure de limitation de la variété du monde.
Elles rendent compte de l’efficacité de la plupart des processus cognitifs inductifs.
Plus loin, j’argue que l’existence d’individus et espèces historiques est la source la plus fondamentale d’information naturelle utilisable, se tenant au fondement de toute cognition et de tout emploi du langage.
Les entités historiques --- les entités du monde réelles, pas leurs possibilités --- se tiennent au cœur de la vie cognitive.
Elles sont les principales structures qui rendent la pensée et le langage possible.
Les « significations » [_meanings_] ne découpent pas de mondes possibles.
Elles sont structurées et maintenues presqu’entièrement par le monde réel; leur degré de détermination dépend de processus historiques réels.

# Espèces éternelles

L’essentiel des choses pour lesquelles on dispose de noms propres ou communs sont des espèces ou des individus historiques.
Mais il existe au moins une autre catégorie importante d’espèces naturelles dont les membres s’agglomèrent pour une raison différente de celle d’avoir été ou copiés, ou reproduits, ou cyclés les uns à partir des autres, ou parce qu’ils ont une origine commune.
Ce sont des échantillons de matière organique ou inorganique comme l’eau, le quartz ou le glucose.
Chaque échantillon d’un élément chimique simple ou composé montre le même ensemble de dispositions et propriétés qu’un autre, celles-ci étant assez nettement distinctes de propriétés d’autres espèces chimiques.
Les découvertes volumineuses et détaillées de la chimie classique, probablement l’exemple le plus clair qu’il ait été d’une « science normale » Kuhnienne, traitaient des multiples propriétés stables de ces diverses espèces ou substances réelles.
Les raisons de cette agglomération de propriétés n’ont rien à voir avec l’histoire, mais dépendent plutôt d’une variété strictement limitée d’éléments dont le monde physique est composé et des tendances qui sont des lois naturelles et qui déterminent les dispositions de chaque élément de manière assez stricte.
J’appelle « éternelles » ces espèces réelles non-historiques, non parce qu’elles ou leurs membres perdurent éternellement, mais parce que la possibilité que davantage de membres de l’espèce puissent advenir dans l’histoire à des lieues et époques distinctes reste une éventualité permanente.
^[Il n’y a, assurément, rien de tel qu’un individu éternel.
Le même individu n’advient pas à des endroits et époques disjoints de ses localisations antérieures, pas plus que ça n’est là simplement une question de la manière dont « notre langage » distingue des individus.
Dans la même veine, les espèces éternelles peuvent être considérées comme étant simplement des propriétés complexes de leurs échantillons (§4.6.1), complexes non en ce qu’elles sont des conjonctures de propriétés plus simples mais parce toutes leurs propriétés de surface simples découlent d’une unique structure sous-jacente.]
Par contraste, dès lors qu’une espèce historique a disparu, elle l’est pour toujours.
Si, de façon accidentelle, une chose telle que nos belettes réapparaissait sur Terre à un moment donné après que nos belettes se soient éteintes, elles seraient membres d’une espèce historique différente.
^[Les « espèces homéostatiques de propriétés agglomérées » de Boyd (1999), telles qu’il les définit, ne seraient pas des espèces historiques puisque les divers membres d’un unique agglomérat homéostatique n’ont pas besoin d’avoir une origine historique unique en commun.
Elles seraient des espèces éternelles.
Cependant, les exemples ordinairement employés pour illustrer les espèces homéostatiques de propriétés agglomérées sont des espèces historiques.]

Mon but ici n’était pas de cataloguer les catégories d’espèces naturelles du monde mais seulement d’illustrer des attributs communs aux espèces naturelles qui nous aideront à comprendre en quoi ils sont employés par les systèmes cognitifs.
Les membres d’une espèce naturelle forment un agglomérat dans l’espace des propriétés pour une raison univoque, fournissant par là le support d’une variété de types d’inductions d’un membre à l’autre.
Une espèce naturelle présente certaines propriétés, ou un petit ensemble de propriétés, par lesquelles ses membres se distinguent de manière fiable des membres d’autres agglomérats, permettant ainsi à ses membres d’être identifiés comme tels.
Que la cognition ait prise sur une espèce naturelle requiert généralement ou que cette espèce ait suffisamment de membres pour qu’elle soit reconnue dans le temps comme une espèce naturelle; ou qu’elle soit reconnue comme espèce naturelle parce que la raison de son agglomération, c’est-à-dire, sa définition réelle, est comprise (§2.6);
^[C’est-à-dire, la manière dont beaucoup d’espèces chimiques réelles sont reconnues de nos jours: même structure moléculaire, même espèce naturelle.]
ou qu’elle soit membre d’une catégorie connue d’espèce naturelle, sous la catégorie de laquelle elle est reconnue (§1.8).

# Formes et Divisions des Coalescences d’Espèces Historiques

L’une des choses que j’ai dite _ne pas_ être vraie des espèces historiques est importante à garder à l’esprit.
Assez fréquemment, il n’y a ni d’attribut ni de groupes d’attributs qui caractériseraient chacun des membres d’une espèce de ce genre sans qu’ils ne soient également caractéristiques de membres d’autres espèces.
Les espèces historiques ne forment pas de « classes, » ce ne sont seulement des éléments groupés selon des propriétés communes.
Pas plus qu’elles ne sont le genre de choses qui, _parce qu’elles sont ce qu’elles sont_, obéissent à des lois qui ne souffrent pas d’exceptions.
Les chats et les œufs de Pâques obéissent tout deux aux lois de la gravité parce qu’ils sont des objets physiques pourvus d’une masse, mais il n’y a pas de lois sans exceptions qui s’appliquent à eux parce qu’ils sont chats ou œufs de Pâques.
Néanmoins, des espèces historiques de ce genre peuvent aisément être étudiées en ce sens.
Bien que l’information glanée en étudiant certains membres d’une coalescence de ce genre ne s’applique pas de quelque façon sans exceptions que ce soit à tous les membres, les inférences à d’autres membres sont généralement inductivement fondées.
Et les connections causales qui parcourent l’espèce fournissent une explication tout à fait substantielle pour la réussite de ce genre de méthode inductive.
Il y a de bonnes raisons pour que ces inductions engendrent fréquemment des résultats corrects.

Les coalescences qui sont des espèces historiques ne correspondent évidemment pas à des ensembles de conditions nécessaires et suffisantes.
En ce sens, elles sont à nouveau relativement proches d’individus historiques, car aucuns d’eux ne correspond à des définitions.
Les frontières entre espèces historiques sont bien souvent indéterminées.
Une dispersion diffuse, voire un regroupement moins strict de choses, pourrait compléter les espaces entre espèces.
Les coalescences peuvent présenter en leur sein de multiples noyaux [_nuclei_] discernables, et peuvent elles-mêmes apparaître comme noyaux au sein de coalescences plus vastes qui les contiendraient.
Ainsi, certaines maladies ont des cas centraux paradigmatiques quand d’autres présentent plusieurs syndromes séparés, mais avec bon nombre de cas particuliers entre eux.
Il y a des ordinateurs portables, des carnets de notes et des tablettes, mais certains carnets de notes sophistiqués ressemblent beaucoup à des ordinateurs portables, et certaines tablettes peuvent être munies de clavier.
Les coalescences historiques peuvent être séparées vaguement.

Comme les individus, les espèces historiques évoluent typiquement pendant leur parcours dans le temps, et, contrairement à la plupart des individus, sont susceptibles de se ramifier.
Différents stades temporels de l’espèce ou différentes branches peuvent être dissemblables à bien des égards.
D’anciennes espèces ont évoluée en espèces plus récentes (bien souvent rapidement, et en passant à travers des goulots relativement fins).
Considérez les ordinateurs depuis l’ENIAC de 1946 (20000 tubes à vide) jusqu’à votre ordinateur ou votre tablette, l’évolution des styles architecturaux tels que celui des églises Gothiques ou les modes vestimentaires.
La chrétienté s’est séparée en plusieurs branches au cours du temps, qui sont liées les unes aux autres de diverses manières.
Dans une illustration élégante de ce thème, Godfrey-Smith décrit la façon dont les variétés de populations Darwiniennes, intensément étudiées par les biologistes, sont dispersées, formant moins une classe définie qu’un réseau d’espèces liées, variant sous certaines dimensions.
^[Bon nombre des notions qui seront introduites dans ce livre sont proches de la notion de population Darwinienne (cf. Godfrey-Smith 2009).
Bien qu’elles soient, je le crois, utiles, il ne le serait pas d’insister sur des définitions précises.]
Cependant, de telles espèces naturelles dispersées ont bien souvent des segments ou branches relativement distincts.
Ou encore peuvent-elles présenter des arêtes, des renflements ou des projections où le regroupement est particulièrement dense.
De l’enfance, à l’adolescence, à l’âge adulte et au grand âge, on trouve de vagues coalescences: des « pré-adolescents », des « jeunes adultes » et ainsi de suite, chacune d’elles étant parfois étudiée séparément.
Des coalescences de coalescences se présentent sous diverses formes, formant des espèces, des familles, des phyla ou autres analogues de ce genre.
Aucun de ces groupes de groupes ne forme une quelconque hiérarchie ordonnée.
Ils surviennent à travers une variété de différents ensembles de dimensions, entrecroisés ou entremêlés de diverses manières.
La forme enragée et la forme diabétique constituent des espèces naturelles qui transcendent les espèces [biologiques].
Les formes démocratiques constituent une espèce naturelle qui se retrouve à travers différents types d’organisations.
Les espèces matérielles telles que le bois ou le plastique se retrouvent dans différentes espèces d’objets.
Les espèces naturelles ne se présentent pas sous la forme d’un arbre unique qui soit l’objet d’une classification ordonnée telle qu’Aristote ou Linné l’auraient souhaitée.

C’est là le monde désordonné avec lequel le langage et la pensée doivent traiter.
Néanmoins, le fait empirique est que, ayant pointé successivement un petit nombre d’exemples appropriés de l’une de ces coalescences ou projection, l’essentiel de ce qui pourrait compter comme relevant de « la même » coalescence ou projection est bien souvent, en pratique, dans un contexte adéquat, une question raisonnablement déterminée.
Les noms d’individus ou d’espèces naturelles sont appropriés très rapidement et par les enfants et par les adultes, et immédiatement appliqués de manières considérées comme adéquates par d’autres.
Jusqu’où exactement porte un nom donné dans une communauté linguistique donnée, dans quelle mesure il peut inclure des coalescences ou ramifications voisines, cela peut n’être découvert qu’après un long moment; par exemple qu’un bambou est un genre d’herbe, ou que les coopératives de crédit ne sont pas des banques.
Retenir quelque chose des configurations et des formes qu’adoptent ces divers agrégats est au fondement de la cognition, et ce bien avant l’émergence du langage, en fait bien avant l’émergence de la cognition humaine.
Les chiens en apprennent sur les voitures et les laisses sans avoir à en rencontrer de nombreux exemples.

En outre, il y a bien souvent différentes manières fiables de reconnaître la  rencontre d’une coalescence, d’identifier quelque chose comme constituant de cette coalescence.
Il y a beaucoup de manières fiables de dire, par exemple, que quelque chose est un chat --- par la forme qu’il prend dans telle ou telle posture, vue depuis tel ou tel angle, par ses mouvements et comportements, par ses os (en paléontologie), par son sang (vétérinaires), par sa fourrure, ou si l’on est malchanceux par l’odeur de son urine ou les sons de ses feulements.
De la même manière, je peux distinguer chacune de mes filles de face, de dos ou de quelque angle que ce soit, de près ou de loin, par des gestes caractéristiques ou des façons de se mouvoir, par leur voix dans la pièce ou à travers le téléphone, par leur écriture, par nombre de leurs activités, par plusieurs centaines de descriptions, et ainsi de suite.
Ces attributs du monde sont nécessaires pour rendre connaissance et langage possibles.

# Catégories naturelles

Une dernière observation quant aux structures de notre monde qui permettent la cognition s’avérera utile pour comprendre le développement rapide des capacités cognitives des individus humains.
Les espèces naturelles se rangent habituellement dans ce que j’appellerai des « catégories naturelles ».
Les catégories naturelles sont un (certain genre de) genre d’espèce plutôt que des espèces d’individus, et jouent un rôle bien différent dans le fait de rendre la cognition possible.

Supposez que l’on définisse un « espace contraire » d’ensemble de propriétés qui soient incompatibles les unes avec les autres, certains types de choses étant restreints à ne jamais posséder plus d’une propriété de chacun de ces espaces contraires, ou seulement l’une à la fois, ou comme le disait Aristote, « à un seul égard ».
Les couleurs forment un espace contraire parce qu’une partie donnée d’une chose à un moment donné ne peut avoir qu’une couleur.
Les espaces contraires ont des dimensionnalités distinctes.
Les espaces contraires de la longueur et de la masse n’ont qu’une dimension, quand celui des couleurs en a trois.
(Le « cône des couleurs » est tri-dimensionnel.)
Un espace contraire correspond à un déterminable.
Couleur, forme et masse sont déterminables.
Les contraires individuels qui se rangent dans un espace contraire sont déterminés.
Les couleurs spécifiques, les formes et les masses sont des valeurs déterminées de ces déterminables.
Ces spécifications peuvent être plus ou moins grossières ou exactes.
Il y a la spécification _rouge_, et des spécifications plus précises comme _pourpre_ et _357 sur la charte des couleurs RAL_.
Les spécifications grossières peuvent être également considérées comme déterminables.
Rouge est un déterminable plus restreint que couleur.

Cette terminologie posée, l’on peut expliquer les « catégories naturelles » comme des analogues d’espèces naturelles.
Les espèces naturelles correspondent à des corrélations entre valeurs déterminées qui se trouvent conjointes, caractérisant plusieurs membres ou l’essentiel des membres de l’espèce.
Les _catégories_ naturelles correspondent à des corrélations entre déterminables qui se trouvent conjoints, _l’une des valeurs déterminées de chacune des déterminables_ caractérisant l’essentiel des membres de la catégorie.
C’est-à-dire, chaque membre de la catégorie présente l’une des valeurs déterminées parmi la gamme de chaque déterminable et s’y tient.
Ainsi, l’un quelconque des individus tombant dans la catégorie des chats n’a qu’une teinte, qu’un volume et qu’un poids, qu’une mère et qu’un père, qu’un lieu de naissance, qu’une couleur des yeux, qu’un tempérament, etc., mais non une seule posture, ni une seule humeur ni un seul apparenté.
Chaque membre de la catégorie des espèces mammifères individuelles n’a qu’une forme grossière, ou bien n’a qu’une queue ou bien n’en a pas, n’a qu’un taux métabolique grossier, qu’un poids grossier, qu’une espèce de dents, des organes sensoriels d’un certain type, d’une certaine acuité etc., mais les membres n’ont pas en commun un lieu de naissance unique, une taille précise, et dans certains cas, une seule couleur.
Ainsi, là où les espèces naturelles déterminent un ensemble de réponses possibles aux questions portant sur diverses propriétés de chacun de leurs membres, les catégories naturelles déterminent seulement un ensemble de questions qui peuvent être répondues de façon déterminée à propos de chacun de leurs membres.
Les membres d’une espèce naturelle peuvent avoir plusieurs valeurs déterminées en commun; les membres d’une catégorie naturelle peuvent avoir divers déterminables en commun, chacun prenant une valeur stable chez chaque membre de la catégorie.
Les catégories naturelles, comme cela sera expliqué dans les chapitres  ultérieurs, rendent possible l’emploi de méta-inductions, lesquelles rendent possible la prédiction des propriétés qui sont susceptibles d’être stables au sein d’une variété donnée d’espèces naturelles et celles qui sont susceptibles d’être variables.

Considérez la catégorie naturelle éternelle des macro-objets physiques.
C’est une catégorie « pure », une catégorie qui n’est pas une espèce, parce qu’il n’y a pas de propriétés déterminées qui soient toutes corrélées et qui permettraient de fonder cette catégorie; un agrégat de simples déterminables la composent.
Ce qui est vraie de chaque macro-objet physique est qu’il a une taille, une forme, une masse, chaque partie de l’objet a une couleur, est brillante ou non, est inflammable ou non, est flexible ou non, a une composition, une origine, certaines relations temporelles et spatiales à d’autres objets, etc..
Savoir qu’une chose est un objet physique, c’est savoir qu’il y a une réponse à une question portant sur sa taille, sa forme, sa masse et ainsi de suite.
Que ces questions particulières soient applicables à chacun de ses membres rendent la catégorie _objet physique_ tout à fait dissemblable, par exemple, aux catégories _composé chimique_ ou _club social_, dont les membres n’ont ni taille, ni forme, ni poids etc.

Considérez la catégorie élément chimique.
Pour chaque membre il existe une réponse à la question de ce qu’est son numéro atomique, sa température de fusion et d’ébullition, sa capacité à conduire la chaleur et l’électricité, quelle est sa gravité spécifique, est-ce qu’il forme un cristal à l’état solide, sa résistance à la traction, dans quelle mesure il est malléable, quelle est sa tension de surface à l’état liquide, quelle est sa valence, quels autres éléments interagissent avec lui et dans quelles circonstances, dans quelle mesure ces réactions sont endothermiques ou exothermiques, quelle couleur, quelle texture, quel goût ou odeur a-t-il, et ainsi de suite.
À propos des composés chimiques, vous pouvez poser bon nombre de ces mêmes questions et vous pouvez aussi vous enquérir de leur structure moléculaire.
L’avènement de la chimie moderne fut la découverte que de tels éléments et composés existaient séparément de simples mélanges, la découverte des manières de les reconnaître, et quelle incroyable quantité de questions pouvaient être posées et résolues de façon déterminée à propos de chacun d’eux.
La chimie moderne a commencé avec la découverte des catégories naturelles élément chimique et composé chimique.

Considérez la catégorie naturelle espèce animale.
Pour l’essentiel, chaque espèce a une forme définie, une couleur définie, un type d’enveloppe externe (fourrure, peau, écaille, coquille, membrane), une anatomie et physiologie extrêmement détaillées, des tendances comportementales définies, un mode de reproduction, un habitat naturel, une structure génétique, une méthode d’appariemment et de techniques de reproduction, certains besoins nutritionnels, certaines méthodes d’acquisition et d’ingestion de la nourriture, etc..
L’on peut poser des questions sur chacun de ces aspects, et il n’y a généralement qu’une seule réponse.
Aucune de ces questions n’aurait de sens si elle était posée à propos d’un composé chimique.
Que représenterait au juste une méthode d’appariemment pour l’or?
(À quoi reviendrait pour une idée verte de furieusement dormir?
^[_What would it be for green ideas to sleep furiously?_ Allusion à une construction de Noam Chomsky dans _Syntactic Structures_ pour représenter une phrase sémantiquement insensée. (NdT.)])

D’autres catégories naturelles sont les danses, les livres et les œuvres musicales, les maladies, les clubs, les cérémonies, les pays, les supermarchés, les systèmes monétaires etc..
Ce qui devient prégnant à mesure que l’on en vient à de tels exemples, c’est que la distinction entre espèces et catégories naturelles, comme la distinction entre valeurs déterminées et déterminables mentionnée plus haut, n’est que relative.
Découvrir qu’une chose est un chien peut véhiculer un grand nombre d’informations spécifiques et tout à fait fiables pour vous, particulièrement si vous êtes vétérinaires.
Cela peut aussi véhiculer un grand nombre de questions spécifiques qui peuvent être raisonnablement posées et résolues lorsqu’elles portent sur ce chien particulier, questions qui n’auraient pas de sens à propos d’un papillon individuel ou d’un livre.
Les chiens et les livres sont des espèces naturelles mais aussi des catégories « impures », selon la direction dans laquelle vous regardez.
Les catégories totalement pures comme celle des macro-objets physiques et des éléments chimiques sont rares.

Comme les espèces naturelles, les catégories naturelles devraient être pensées comme des coalescences ou des agrégats, non comme des classes.
Elles n’ont pas toujours des frontières franches ni de centres très nets.
Cela peut sembler évident d’après le fait que les catégories naturelles sont souvent simplement des espèces naturelles (elles leur sont coextensives), mais dans la direction opposée, pour ainsi dire, dans la direction de leurs caractéristiques déterminables plutôt que celle de leurs caractéristiques déterminées.

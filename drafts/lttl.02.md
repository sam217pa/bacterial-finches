# Truismes et Vérité^[_Truisms about Truth_]

## Le barrage à débat

Demandez à quelqu'un ce qu'est la vérité, et vous en serez quitte soit pour un silence troublé soit pour un rire nerveux.
Les deux réactions sont compréhensibles.
La vérité est l'une de ces idées --- le bonheur en est une autre --- que l'on emploi tout le temps mais que l'on est bien en peine de définir.
C'est pourquoi la question « Qu'est-ce que la Vérité? » est si souvent traitée comme purement rhétorique.

L'une des raisons pour laquelle la vérité semble si difficile à décrire est que l'on a sur elle des croyances contradictoires: l'on pense parfois qu'elle est découverte, parfois créée, parfois connaissable, parfois mystérieuse.
Lorsqu'on utilise cette idée dans la vie ordinaire --- comme lorsqu'on est d'accord ou non avec ce qu'une personne a dit ---, elle semble une question simple.
Pourtant, plus l'on se penche sur le problème, plus il devient compliqué.

Il serait souhaitable que l'on puisse exposer, une fois pour toute, tout ce que l'on a pensé de la vérité --- pour trouver l'entière vérité et rien que la vérité de la vérité, en quelque sorte.
Souhaitable, mais en pratique impossible.
La thèse de ce livre est beaucoup plus simple.
De toutes les choses que l'on peut croire de la vérité, il y en a au moins une que l'on _devrait_ croire: la vérité compte.
La vérité, j'essaierai de vous en convaincre, est de première importance à la fois dans votre vie personnelle et politique.

L'idée que la vérité compte se résume en fait à quatre arguments.
Collectivement, ces truismes, comme je les appelle, expliquent ce que j'entend par « vérité » et ce que j'entend par « compter ».
Aussi commencerai-je par introduire ces truismes quant à la vérité, dans le but de vous convaincre qu'ils sont précisément ce que je dis qu'ils sont, des truismes évidents.
Cela ne signifie pas que tout le monde les approuve.
Comme je l'ai déjà noté, certains d'entre nous sont confus au sujet de la vérité --- nous avons sur elle des croyances contradictoires.
Nous pouvons donc croire ces truismes tout en croyant d'autres éléments qui grèvent notre croyance en l'un d'eux ou en chacun d'eux.
De plus, rien n'est si évident qu'il n'ait jamais été prétendu faux, peu judicieux, naïf, incohérent, impossible ou corrompant la jeunesse.
Et bon nombre de personnes, comme nous le verrons, persistent à en dire autant sur ces quatre idées.
Wittgenstein remarque quelque part que le travail du philosophe consiste à « assembler des rappels » --- de nous rappeler ce qui se trouvait là devant nous depuis toujours.
Si ça n'est pas la seule tâche du philosophe, cette remarque est néanmoins lourde de sens.
L'accoutumance à une chose peut nous faire oublier, voire nier son importance.
Lorsque cela advient, l'on doit se souvenir de son rôle dans nos vies quotidienne.
C'est précisément ce dont on a besoin dans le cas de la vérité.

## La vérité est objective

Si je sais une chose, c'est que je ne sais pas tout et personne d'autre non plus.
Il y a des choses que l'on ne saura simplement jamais, et d'autres que l'on pense savoir mais que l'on ne sait pas.
Acceptez ce peu de bon sens, et vous voilà engagés envers le premier truisme au sujet de la vérité: la vérité est objective.

Au début de la pièce la plus célèbre de Shakespeare, Hamlet et son livresque ami Horatio rencontrent le fantôme du défunt père d'Hamlet.
Sans surprise, Horatio trouve difficile à avaler le fait qu'un monarque Danois décédé hante les remparts du château.
La réponse d'Hamlet à l'inquiétude d'Horatio est brutale: il y a plus sur Terre et au Paradis qu'il n'est conçu par la seule philosophie d'Horatio.
Hamlet cherche à rappeler à Horatio qu'il ne sait pas tout, parce que l'univers est plus grand que nous.

Non seulement, comme Hamlet, sommes-nous parfois ignorants; nous faisons également des erreurs.
Les gens pensaient autrefois que la Terre était plate.
La plupart d'entre nous trouvent désormais cette idée bien ridicule.
Mais imaginez pour un temps vivre à une époque antérieure aux mathématiques avancées, aux voyages à longue-distance, aux avions, aux photographies.
Croiriez-vous que la Terre est plate?
Bien sûr que vous le croiriez.
Il suffit de regarder autour de soi, diriez-vous, montrant au loin l'horizon (plat).

Même des théories scientifiques bien établies peuvent être fausses.
Les chimistes du dix-septième siècle, par exemple, avaient noté à raison qu'une chose similaire se produit quand le métal rouille et quand le bois brûle.
Les deux processus en cours résultent en une perte de masse.
Selon la toute meilleure science de l'époque, la cause commune en était le relâchement d'un gaz invisible, le « phlogistón », dans l'atmosphère.^[Voir Thomas Kuhn, _La Structure des Révolutions Scientifiques_, troisième édition, (Chicago: University of Chicago Press, 1996), pp. 53-54; et James Conant, _The Overthrow of the Phlogiston Theory: The Chemical Revolution ef 1775-1789_ (Cambridge, Mass.: Harvard Case Histories in Experimental Science, 1950).]
Puisque ce gaz occupait un volume, avait une masse, et ainsi de suite, sa perte expliquait pourquoi et le métal et le bois se réduisent après qu'ils aient rouillé ou brûlé.
Il est facile de sourire de la théorie phlogistique de nos jours, car s'il y a bien un gaz impliqué dans les deux processus, c'est en fait l'_oxygène_, qui est acquis, non perdu, par le système en jeu.
Le phlogistón n'existe pas.
Pourtant la théorie phlogistique était une hypothèse tout à fait raisonnable à l'époque.
Elle était bien établie par les standards de l'époque.
Les scientifiques les plus éminents y croyaient.
Elle était pourtant fausse.

Le risque permanent d'ignorance et d'erreur souligne le fait que, quoi qu'elle puisse être d'autre, la vérité est objective.
Simplement parce que l'on croit à une chose ne la rend pas vraie, pas plus que le fait qu'une chose soit vraie n'implique pas qu'on la croie.
Comme on dit, croire ne veut pas dire que c'est le cas.
La vérité de ce que le Mt. Everest est la montagne la plus haute, par exemple, n'a rien à voir avec le fait que je le croie ou non.
Ce qui compte, c'est si le Mt. Everest est réellement la plus haute montagne, et si elle l'est, alors assurément elle le serait quand bien même personne n'aurait jamais été en mesure de le voir.
Évidemment, s'il n'y avait personne doué de langage, alors le Mt. Everest ne serait pas _appelé_ « Mt. Everest », puisqu'il n'aurait pas même de nom.
Mais il serait toujours là, tout comme si nous l'avions appelé différemment, comme « Mt. Zippy ».

Voltaire plaisante quelque part en disant « définissons la vérité, en attendant mieux, ce qui est énoncé tel qu'il est ».^[Voltaire, _Dictionnaire Philosophique_, article Vérité, (NdT)]
Voltaire l'entendait comme une plaisanterie, mais en tant que définition opératoire, elle est plutôt bonne.
Voltaire lui-même pensait sans doute à cette fameuse remarque d'Aristote lon lequel « dire de l'Être qu'il est, et du Non-Être qu'il n'est pas, c'est le vrai ».^[Aristote, _Métaphysique_, IV, 5, 1011b. Voir aussi W.P. Alston, _A Realist Conception of Truth_ (Ithaca: Cornell University Press, 1996).]
Voilà qui est encore mieux.
Lorsque l'on dit d'une chose qu'elle est vraie, le monde est tel que l'on le dit être.
Et lorsqu'on croit à raison, le monde est tel que nous le croyons être.
C'est la façon dont est le monde qui compte pour la vérité, pas ce que l'on croit de lui.

En ce sens, l'objectivité de la vérité n'est pas, ou en tout les cas ne devrait pas être, controversée.
Comme je l'ai indiqué, c'est une conséquence de ce que l'on accepte ce que tout le monde admet d'ors et déjà (ou devrait l'admettre): l'on ne sait pas tout, et l'on peut se tromper.

L'idée que la vérité est objective est parfois exposé en disant que les croyances vraies correspondent à la réalité.
Et c'est très bien ainsi, du moment que l'on comprends que cette phrase laisse la place à des désaccords quant à la nature et la portée de ce que veulent dire « correspondance » et « réalité ».
Certains soutiennent que les croyances ne peuvent être vraies à moins qu'elles ne correspondent à des objets physiques, indépendants de l'esprit, comme les montagnes, les électrons, les vaisseaux de guerre et les barbiers.
D'après ces théories, la vérité est toujours _radicalement_ objective, puisque selon elles ce qui rend nos croyances vraies, c'est toujours leur relation à des objets physiques réels.
C'est évidemment un problème de théorie philosophique complexe, ceci dit, et non un truisme.
Il n'est pas nécessaire d'y adhérer pour croire que la vérité est objective au sens minimal que j'ai décrit.

Nous n'avons pas à tout savoir d'une chose pour être capable d'en parler.
Prenez, par exemple, le disque dur de mon ordinateur, dont (et j'ai honte de l'admettre) je ne sais presque rien.
Je ne sais pas de quoi il est composé (de petits bouts de métal et de plastique probablement), je ne sais pas où il se situe précisément, et je ne sais pas vraiment comment il effectue sa tâche.
Mais je sais cependant ce qu'est cette tâche: il sert de lieu principal de stockage de l'information de mon ordinateur, c'est là que sont stockées les divers programmes et fichiers.
Dans la majorité des cas, cette description opératoire d'un disque dur convient.
Elle capture ce que l'on veut généralement dire lorsqu'on parle de telles choses.
En fait, bon nombre de nos concepts ordinaires sont ainsi, et c'est aussi une bonne chose. 
C'est pour cela que l'on peut sensément parler d'une chose comme la gravité avant de connaître sa véritable nature sous-jacente, voire même si l'on ne connaît jamais sa véritable nature.
L'on sait ce que fait la gravité avant de savoir ce qu'elle est.

Notre croyance élémentaire en l'objectivité de la vérité est telle que mon idée élémentaire du disque dur de mon ordinateur.
Nous connaissons la tâche des croyances vraies, même si l'on ne sait pas exactement comment elles l'accomplissent.
Les croyances vraies sont celles qui dépeignent le monde tel qu'il est et non tel que l'on espère, que l'on craint ou que l'on souhaite qu'il soit.

## La vérité est bonne

Personne n'aime avoir tort.
S'il y a bien une chose qui est un truisme, en voilà une.
Et ceci révèle un autre aspect de ce que l'on croit de la vérité: qu'elle est bonne.
Plus précisément, il est bon de croire le vrai.

Pourquoi trouvons-nous si évident qu'il est bon de croire le vrai?
Une raison à cela a trait avec le but du concept de vérité même.
Les êtres humains tendent à être en désaccords les uns avec les autres: nous nous querellons, nous disputons, formons des opinions différentes, et construisons différentes théories.
Pourtant la possibilité même qu'il y ait désaccord entre opinions requiert qu'il y ait une _différence_ entre avoir tort et avoir raison.
Lorsque j'asserte une opinion sur un sujet, j'asserte ce que je crois être _correct_.
Vous procédez de même.
Et lorsque l'on est en désaccord, à l'évidence, on l'est sur la question de savoir quelle opinion est correcte.
Aussi, s'il n'y a rien de tel que de parvenir à une (ou aucune, ou plus d'une) réponse correcte à une question donnée, alors on ne peut pas réellement être en désaccord.

Mon propos est que l'on distingue la vérité de la fausseté parce que l'on a besoin d'un moyen de séparer parmi nos croyances, énoncés ou autres celles qui sont correctes de celles qui ne le sont pas.
En particulier, il nous faut un moyen de distinguer entre les croyances pour lesquels on dispose de données probantes, ou sont soutenues par le Pentagone, ou dénoncées par le président, ou qui nous rendent riches, nous attirent des amis, ou qui sonnent simplement bien, et celles qui se trouvent éventuellement être le cas.
Non pas que l'on ne peut évaluer les croyances de toutes ces façons --- bien sûr qu'on le peut.
L'on peut, et doit, critiquer une croyance pour ce qu'elle n'est pas fondée sur des preuves fiables, par exemple.
Mais la force de ce genre d'évaluation dépend d'un genre d'évaluation plus élémentaire.
L'on pense qu'il est bon d'avoir quelques preuves pour nos croyances _parce que_ l'on pense qu'il est plus probable que les croyances basées sur des données probantes soient vraies.
L'on critique les gens qui se bercent d'illusions (_wishful thinking_) _parce que_ l'on pense que cela conduit à croire des faussetés.

Aussi l'un des points essentiels de posséder un concept de vérité est qu'il nous faut un moyen très simple d'apprécier et d'évaluer nos croyances sur le monde.
En fait, c'est conçu directement par notre langage: le mot même de « vrai » a une dimension évaluative.
Une partie de ce que vous faites lorsque vous dites qu'une chose est vraie est de la recommander comme une chose en laquelle il est bon de croire.
Tout comme « bonne » et « mauvaise » sont les façons les plus simples d'évaluer une action comme correcte ou incorrecte, « vraie » et « fausse » sont nos deux manières les plus simples d'évaluer une croyance comme correcte ou incorrecte.

En effet, la connexion entre croyance et vérité est si intime qu'à moins de penser qu'une chose est vraie, on ne peut même pas dire qu'on la croie.
Croire c'est simplement tenir pour vrai.
Si vous ne vous souciez pas de ce qu'une chose est vraie ou non, vous ne la croyez pas vraiment.
William James expose cela en disant que la vérité « est le bon sur la voie de la croyance ».^[William James, _Pragmatism and the Meaning of Truth_ (Cambridge, Mass.: Harvard University Press, 1975), p.42]
D'autres disent parfois que la vérité est ce que vise la croyance.
Ça n'est assurément pas à prendre au sens littéral.
Les croyances ne visent littéralement à rien.
Mais ces deux énoncés capturent l'idée que la croyance est une propriété qu'il est bon d'avoir pour une croyance.
Puisque les propositions sont le contenu des croyances, et que c'est le contenu d'une croyance et non l'acte de croire qui est vrai, l'on peut aussi dire que la vérité est la propriété faisant d'une proposition qu'il est bon de la croire.
En croyant, nous sommes guidés par la valeur de la vérité: _toutes choses égales par ailleurs, il est bon de croire une proposition si et seulement si elle est vraie_.
Puisque le bon vient par degrés, l'on peut aussi énoncer cette « norme » ou règle en disant que, toutes choses égales par ailleurs, il est préférable de croire une chose si et seulement si elle est vraie.
Ou plus simplement: il vaut mieux croire le vrai que le faux.
Je ne veux pas dire que ça soit nécessairement _moralement_ meilleur.
Les choses peuvent être meilleures ou pires, bonnes ou mauvaises de bien des façons.
Une écriture limpide est un bien esthétique; une nourriture délicieuse est un bien culinaire; croire une proposition vraie, pourrait-on dire, est un bien cognitif ou intellectuel.


## La vérité est un digne but d'enquête

Les valeurs guident l'action.
La valeur selon laquelle, toutes choses égales par ailleurs, il est bon de tenir mes promesses impliques que je doive, toutes choses égales par ailleurs, essayer de les tenir.
Qu'il soit bon de tenir ses promesses me fournit une raison d'agir d'une façon plutôt que d'une autre.
Ainsi en va-t-il pour la vérité: il est bon, toutes choses égales par ailleurs, de croire ce qui est vrai, et intuitivement, cela me donne une raison de faire certaines choses; à l'évidence, je dois, toutes choses égales par ailleurs, _poursuivre la vérité_.
Le fait qu'il soit bon de croire le vrai signifie qu'avoir des croyances vraies, comme des dettes remboursées et des promesses tenues, est un but qu'il faut viser.

Que les croyances vraies soient un but que l'on doit viser ne veut pas dire que nous poursuivons ce but directement.
La poursuite de la vérité est en fait toujours indirecte.
C'est parce que la croyance n'est pas quelque chose sur laquelle on a un contrôle direct.
L'on ne peut croire sur demande.
Si vous en doutez, exigez de vous-mêmes de croire sur le champ qu'une fleur bleue vous pousse sur la tête.
Bien sûr, vous pourriez vous redresser, prendre une voix grave et prononcez les mots « Une fleur bleue me pousse sur la tête », mais cela ne suffirait pas à ce que vous le croyiez, car le fait est (du moins j'espère qu'il l'est) qu'aucune fleur bleue ne vous pousse sur la tête.

Néanmoins, nous avons assurément un contrôle _indirect_ sur ce que l'on croit, ce qui suffit à qualifier de contrôle.
Je peux affecter ce que je crois en me plaçant dans certaines situations et en évitant d'autres.
C'est-à-dire, je peux contrôler comment je _poursuis la vérité_, en accordant un soin particulier à l'examen des preuves, en donnant et demandant des raisons, en faisant des recherches adéquates, en restant ouvert d'esprit, et ainsi de suite.
En bref, en disant que la vérité est un digne but, nous entendons que vous devriez (toutes choses égales par ailleurs) adopter des conduites (_policies_), des méthodes et des habitudes d'_enquête_ qui soient fiables, ou susceptibles d'aboutir à des croyances vraies.
L'on pense ordinairement qu'il est bon de donner et demander des raisons, bon d'être ouvert d'esprit, bon d'avoir des preuves empiriques pour une conclusion scientifique, parce que ce sont des méthodes d'enquête qui nous conduisent à la vérité.
Si l'on n'accordait pas de valeur aux croyances vraies, l'on n'en accorderait pas à ce genre d'activités; et si l'on en accorde à ce genre d'activités, c'est parce que l'on pense qu'elle nous conduisent, plus souvent que l'inverse, à des croyances vraies plutôt que fausses.

Aussi poursuivons-nous des croyances vraies en s'engageant dans l'enquête.
J'emploie ici le terme « enquête » en un sens très général.
J'entends par lui non les seules méthodes d'acquisitions de croyances vraies que je viens de mentionner, mais aussi tous les divers processus, diverses pratiques et activités dans lesquelles on s'engage, et lorsque l'on pose et lorsque l'on répond à des questions d'intérêt pour nous.
Cela inclut évidemment les tâches expérimentales et théoriques des diverses sciences, mais aussi d'autres activités plus ordinaires, telles que le type de tests de diagnostic que l'on peut effectuer pour découvrir d'où vient l'étrange bruit sourd que produit sa voiture, ou lorsque l'on cherche sous le lit sa chaussette perdue.
Lorsque l'on fait de telles choses, on ne se contente pas de vieilles réponses.
Quoi qu'on puisse vouloir d'autre, l'on veut toujours la vérité du problème qui nous intéresse.
Aussi l'une des choses que l'on vise au cours de l'enquête est d'acquérir des croyances vraies, ou vérité, en clair.

Intuitivement, nos deuxième et troisième truismes sont étroitement liés: puisqu'il est bon de croire ce qui est vrai, la vérité est un but qu'il faut viser, c'est un digne but d'enquête.

## La vérité mérite que l'on s'en soucie pour elle-même

Supposez que j'aie à disposition une machine qui permette de vous faire vivre ce que vous voulez.
Une fois à l'intérieur, flottant dans une cuve, vous vivriez dans la réalité virtuelle de votre propre conception --- emplie d'expériences d'amis chaleureux, d'aventures merveilleuses, de nourriture spectaculaire, de sexe et de conversations profondes.
Rien de tout cela ne serait réel ceci dit, mais il semblerait l'être.
L'on pourrait même faire en sorte qu'une fois à l'intérieur de la machine, vous oubliiez complètement que vous _êtes_ à l'intérieur d'une machine.

Il n'y a à cela qu'un prix. Une fois à l'intérieur, vous ne _pouvez plus en sortir_.

Le feriez-vous?

La plupart d'entre nous diraient probablement non.
Si l'on serait assurément enclin à y passer quelques heures voire quelques semaines, l'on ne voudrait pas passer le reste de sa vie dans une monde virtuel.
Certains d'entre nous, dont les vies réelles sont perclues de tragédies et de misère, pourraient être plus prompts à y rester à long terme.
Mais là encore, la plupart préféreraient voir leurs problèmes réellement disparaître que de vivre une vie où ils _semblent_ seulement disparaître.
La machine produit des illusions magnifiques, mais l'on veut plus que des illusions.
L'on veut la vérité, bon gré mal gré.

De tels scénarios se trouvent dans toute l'histoire et la philosophie, sans compter la culture contemporaine.
Dans le film _The Matrix_, par exemple, le personnage central doit choisir entre deux pillules.
S'il choisit la première, il reste dans une vie d'illusion parfaite (fournie là encore par l'ordinateur ubiquiste tout puissant).
Il ne se rappellera pas même avoir fait ce choix. 
Tout continuera à se passer comme avant.
S'il choisit la seconde, il découvre la vérité de sa vie et de sa réalité, une vérité dont il est prévenu qu'elle n'est guère agréable.
Il choisit, sans surprise pour le public, la vérité.

Des scénarios de science-fiction comme ceux-ci sont populaires en partie parce qu'en pensant à ce que l'on ferait dans de telles situations, on se rend compte de ce à quoi l'on tient.
Comme je le montrerai dans quelques lignes, réfléchir à ce genre de situations, du moins pour la plupart d'entre nous, suggère que l'on se soucie de la vérité plus que pour les seuls bénéfices qu'elle procure.
Assurément, nul besoin de science fiction pour nous dire ceci.
Il y a des fois où l'on veut simplement savoir, pour nulle autre raison que le savoir lui-même.
La curiosité n'est pas toujours motivée par des intérêts pratiques.
Pensez à des conjectures mathématiques hautement abstraites.
Connaître la vérité d'au moins certaines d'entre elles ne nous approcherait pas d'un pouce de quoi que ce soit que l'on cherche.
Néanmoins, si l'on était contraint de choisir entre croire le vrai ou le faux d'une question, nous préfèrerions, au moins dans une certaine mesure, le vrai.
Si l'on doit deviner, on préfère deviner correctement.
Et bien sûr, nous souhaitons parfois la vérité en dépit de ses conséquences extrêmement fâcheuses.
Les gens cherchent souvent à connaître la vérité de l'infidélité d'une épouse lors même qu'il y a d'excellentes chances qu'il n'en sorte rien de productif.
De la même façon, bon nombre de personnes souhaitent savoir s'ils sont en train de mourrir d'une maladie incurable, lors même qu'ils n'y peuvent rien faire, et que la connaissance de leur état n'est d'aucun intérêt pratique pour leur état de santé actuel.

Le fait que nous apprécions la vérité pour elle-même ne signifie pas que nous ne l'apprécions pas comme moyen de parvenir à d'autres fins.
En fait, la raison la plus évidente de poursuivre des croyances vraies est que les croyances vraies peuvent nous fournir toutes sortes de choses que l'on cherche.
Croire le vrai est pratiquement avantageux.
Imaginez que vous traversiez la rue; regardant de part et d'autre, vous essayez d'estimer la vitesse des véhicules approchants.
Lorsque vous prenez une telle décision, et bien d'autres, il vous faut avoir juste.
Sans quoi des choses terribles adviendront, comme se faire renverser par un bus.
Croire la vérité est appréciable parce qu'elle est un moyen pour d'autres fins --- des ponts résistants, des traitements aux maladies, de la sécurité.
On peut le résumer en disant que la vérité est instrumentalement bonne.

Bien des choses que l'on cherche dans la vie, de l'argent à l'écriture manuscrite lisible, sont _seulement_ instrumentalement bonnes.
On se soucie d'elles non pour elles-mêmes mais pour ce qu'elles font pour nous.
La plupart d'entre nous voient les billets de dollar de cette façon.
Avoir des dollars est appréciable, sans aucun doute, mais seulement en tant que moyen pour d'autres fins --- une maison, de la nourriture, des vêtements, etc …
Il n'y a rien de valeur dans le bout de papier lui-même.

Une chose est _normative_ s'il est bon de chercher à l'atteindre, ou de s'en soucier.
Mais une chose est _profondément normative_, une valeur à proprement parler, lorsqu'il est bon de s'en soucier pour elle-même.
L'amour est peut-être ainsi.
Être amoureux ou amoureuse de quelqu'un n'est pas bon seulement en tant que moyen pour d'autres fins.
Assurément, cela _peut_ conduire à d'autres biens (sexe, épanouissement, une vie plus riche, etc …), mais ça n'est clairement pas toute l'histoire.
Les gens accordent de la valeur à l'amour même lorsqu'il a des conséquences fâcheuses, voire même lorsqu'il empêche de poursuivre d'autres fins.
Il est bon de se soucier de l'amour pour lui-même.
En fait, n'aimer quelqu'un que parce que cela vous procure ce que vous voulez signifie probablement que l'on n'aime pas réellement l'autre. 

Aussi l'une des façons de penser notre quatrième truisme au sujet de la vérité est que la vérité est plus proche de l'amour que de l'argent.
Nous --- du moins l'essentiel d'entre nous --- nous soucions de la vérité, parfois au moins, pour d'avantage que des raisons instrumentales.
La vérité est profondément normative; il est bon de s'en soucier pour elle-même.

De nos quatre truismes au sujet de la vérité, c'est peut-être le plus litigieux.
Pourtant comme je l'ai noté plus haut, songer à certaines situations hypothétiques peut contribuer à comprendre si l'on y croit.
Penchons-nous alors plus attentivement sur ce genre de situation imaginaire que j'ai déjà mentionné au début de cette partie.
En réfléchissant à nos réactions face à de telles situations et à ce que ces réactions révèlent, il s'avèrera utile de procéder par étapes.
La première étape est de vérifier si l'on a ce que j'appelerai une préférence basale pour la vérité.
Par « préférence basale » j'entends la préférence pour une chose que l'on ne peut pas expliquer par nos préférences pour d'autres choses.
Chercher à éviter la douleur est sans doute une préférence basale; la préférence pour l'argent non.

Si la vérité _n' était pas_ une préférence basale, alors de deux croyances B1 et B2 aux valeurs instrumentales identiques, je ne devrais pas préférer croire l'une plutôt que l'autre.
Les considérations ci-dessus indiquent d'ors et déjà qu'il n'en va pas ainsi, cependant.
Spécifiquement, si l'on n'avait pas de préférence basale pour la vérité, il serait difficile d'expliquer pourquoi l'on trouve l'idée d'avoir indétectablement tort si dérangeante.
Imaginez une version modifiée du scénario de la machine à expériences avec laquelle nous avons débuté.
Quelque super-neuroscientifique vous offre le choix entre continuer à vivre normalement, ou avoir votre cerveau branché à un super-ordinateur qui fait en sorte qu'il _semble_ que vous continuiez à vivre normalement (bien que vous soyez réellement en train de flotter dans une cuve quelque part).
Lorsque vous serez dans la cuve, vous continuerez à vivre exactement les mêmes expériences que vous vivriez dans le monde réel.
Grâce à cela, vous croiriez que vous êtes en train de lire un livre, que vous avez faim, et ainsi de suite.
En bref, vos croyances et vos expériences seraient les mêmes, mais l'essentiel de vos croyances seraient fausses.

Si l'on ne préférait pas vraiment les croyances vraies aux fausses, l'on serait simplement ambivalent par rapport à ce choix.
Cuve ou pas cuve, qui s'en soucie?
Mais l'on ne répond pas ceci.
L'on ne veut pas vivre dans la cuve, quand bien même y vivre ne changerait rien à ce dont on fait l'expérience ou à ce que l'on croit.
Ce qui suggère que l'on a bien une préférence basale pour la vérité.

D'aucuns pourraient rétorquer que l'on attend de la vie plus que de simples expériences, et que c'est _ce_ fait _là_ --- et non pas une quelconque préférence pour la vérité --- qui nous conduit à opter plutôt pour le monde réel que pour la cuve.
Considérons donc un autre scénario, qui fut conçu par Bertrand Russell.
Supposez que, à notre insu, le monde ait commencé hier --- il _semble_ plus ancien, mais ne l'est pas.
Si je vivais réellement dans un monde Russell, comme je l'appelerai, presque toutes mes croyances quant au passé seraient fausses.
Pourtant mes désirs seraient également satisfaits dans les deux mondes.
^[Le philosophe des sciences Tim Lewens m'a fait remarquer que dans le monde Russell je ne serait pas capable de satisfaire mes désirs _guidés par le passé_, comme celui d'aller à la boutique où l'on s'était rendu la semaine dernière.
À cela je réponds comme suit: Considérez mes croyances lorsque je n'ai pas de désirs guidés par le passé.
En ces moments les deux mondes sont rigoureusement identiques du point de vue des valeurs instrumentales, et je préfère assurément ne pas vivre dans le monde Russell.
De plus, même en considérant des moments où j'ai bien des désirs guidés par le passé, il est certainement possible de _mettre temporairement entre parenthèses_ ou d'isoler les quelques désirs guidés par le passé que j'ai à un moment donné et de simplement considérer les deux scénarios eu égards à tous mes autres désirs.
Alors, je choisis encore clairement le monde réel plutôt que le monde Russell.]
Car le future des deux mondes se déroule de la même exacte façon.
Si je crois à raison dans le monde actuel que si j'ouvre le réfrigirateur, j'aurai une bière, alors j'aurai une bière si j'ouvre le réfrigirateur.
Puisque les événements du monde Russell sont rigoureusement les mêmes que ceux du monde actuel dès lors qu'il débute, j'aurai aussi une bière dans le monde Russell si j'ouvre le réfrigirateur, quand bien même (dans le monde Russell) je crois à tort que je l'ai mise là hier.
En d'autres termes, quel que soit l'action que j'accomplis à cet instant, je l'accomplirais aussi si le monde avait commencé hier, en dépit du fait que, dans ce cas, mes actions seraient basées sur de fausses croyances quant au passé.
Pourtant, si l'on me donne le choix entre vivre dans le monde actuel et vivre dans un monde Russell, je préfère fortement le monde actuel.
Bien sûr, une fois « dans » ce monde, je ne verrais aucune différence entre lui et le monde réel; dan les deux mondes après tout, les évènements s'enchaînent de la même manière.
Mais ça n'est pas le point.
Car le fait reste qu'en pensant les deux mondes uniquement dans la mesure où ils ont la même valeur instrumentale, il y a entre les deux mondes _une différence là, maintenant_ qui compte pour moi.
Quand bien même elle n'aurait aucun effet sur mes autres préférences, je --- et vraisemblablement vous aussi --- préfère les croyances vraies aux croyances fausses.

En préférant ne pas vivre ni dans la cuve ni dans le monde Russell, je ne préfère pas seulement que le monde soit d'une certaine façon.
Ma préférence implique mes croyances et leur bon fonctionnement, pour ainsi dire.
Car non seulement je ne ne veux pas vivre dans un monde où je serais un cerveau dans une cuve, _je ne veux pas non plus vivre dans un monde où je ne suis pas duppé, mais crois que je le suis_.
C'est-à-dire, si ceci ou cela est le cas, je veux croire qu'il l'est, et si je crois qu'il est le cas, je veux qu'il le soit.
L'on peut le formuler en disant que je souhaite que mes _croyances et la réalité_ soient dans un certain rapport --- je veux que mes croyances suivent la réalité à la trace, qu'elles « s'accordent avec ce qu'est réellement le monde » ---, ce qui revient à dire que je veux que mes croyances soient vraies.

De plus, notre préférence pour la vérité n'est pas une _simple_ préférence --- comme une préférence pour la glace au chocolat.
Cela va plus loin que ça.
C'est apparant lorsque je songe à ma posture par rapport à cette préférence.
Comme bien d'autres gens, non seulement je préfère la vérité pour elle-même, je ne veux pas non plus être le genre de personne qui ne chérit pas la vérité pour elle-même --- qui préfèrerait une vie d'illusions.
Je veux être le genre de personne qui, par exemple, est intellectuellement intègre, qui, toutes choses égales par ailleurs, est enclin à poursuivre le vrai même lorsque cela s'avère dangereux, délicat ou couteux.
Non seulement je souhaite la vérité, mais je souhaite souhaiter la vérité.
Je ne voudrais pas plus avaler une pillule qui me rendrait ambivalent sur la question de vivre en tant que cerveau dans une cuve que je ne voudrais vivre en tant que cerveau dans une cuve.
Ceci suggère que mon désir de vérité n'est pas une simple fantaisie passagère; elle est au cœur de ce qui me tient à cœur.
Ça n'est pas juste que je préfère la vérité, en d'autres termes; je me _soucie_ d'elle.
Généralement, le fait que l'on se soucie d'une chose est un bon indice du fait que l'on trouve qu'il est bon de s'en soucier.
^[Un bon indice mais néanmoins défaisable bien sûr.
Il est possible de se soucier d'une chose sans croire qu'il est bon de s'en soucier.]
Par conséquent, si vous vous souciez de la vérité pour elle-même, alors vous croyez vraisemblablement à notre dernier truisme, à savoir qu'il est bon de se soucier de vérité précisément de cette manière.

Ceci donc, est ce que j'entends lorsque je dis que l'on peut en apprendre sur ce que l'on croit par ces histoires de science-fiction: pour bon nombre d'entre nous, nos réactions instinctives à ces cas suggèrent que nous avons pour la vérité une préférence basale; que cette préférence compte pour nous; et donc que l'on croit qu'il est bon de se soucier de vérité pour elle-même.
Si vous préférez ne pas vivre dans une cuve, ou dans un monde Russell, alors vous acceptez implicitement que, où la croyance que _p_ et la croyance que non-_p_ ont la même valeur instrumentale, il est préférable, simplement sur la base de la seule vérité, d'avoir la croyance vraie que la croyance fausse.

Bien sûr, rien de tout cela ne prouve que tout le monde l'accepte, ni pour ceux d'entre nous qui l'acceptent, que nous sommes _justifiés_ de le faire.
C'est ce dont le reste de ce livre traite.
Mais cela montre bien d'où l'on part.

## Bonnes et mauvaises idées

Voici donc nos truismes au sujet de la vérité:  
La vérité est objective.  
La vérité est bonne.  
La vérité est un digne but d'enquête.  
Il est bon de se soucier de la vérité pour elle-même.

Voilà ce que j'entends par la vérité compte.
Je veux dire que les croyances qui dépeignent le monde tel qu'il est sont bonnes et qu'il faut s'en soucier, non seulement pour leurs conséquences mais pour elles-mêmes.
Ces truismes nous rappellent chacun une chose essentielle sur la vérité et le rôle qu'elle joue dans nos vies.
Ils nous rappellent que la vérité, comme le courage ou une promesse tenue, est un concept que les philosophes appellent une valeur épaisse: elle a des aspects normatifs _et_ non-normatifs.
Lorsque l'on décrit à raison un acte comme courageux, on le décrit et on l'évalue tout à la fois.
On le recommande comme une chose qu'il faut chercher à imiter, disant qu'il est bon etc., _et_ on le décrit comme une action effectuée en dépit du danger.
Ces deux aspects sont des faits importants et essentiels au sujet du courage.
De la même façon, lorsque l'on dit d'une croyance qu'elle est vraie, dans le même temps on l'évalue --- disant qu'elle est correcte, bonne, digne d'être poursuivie, etc., _et_ on la décrit comme dépeignant le monde tel qu'il est.
^[La distinction entre valeurs minces et épaisses provient de Bernard Williams; voir, e.g. son _Ethics and the Limits of Philosophy_ (Cambridge, Mass.: Harvard University Press, 1985), p. 128.
Williams cependant, applique la distinction aux concepts plutôt qu'aux propriétés, comme je le fait dans ce texte.
Adam Kovach soutient que la vérité est un _concept_ de valeur épais dans « Truth as a Value Concept », _in_ _Circularity, Definition and Truth_, éd. A. Chapuis et A. Gupta (New Delhi: Indian Council of Philosophical Research, 2000) pp. 199-215.
J'ai décrit cette idée moi-même dans « Truisms about Truth », dans _Perspectives on the Philosophy of William P. Alston_, éd. H. Battaly et M. Lynch (Lanham, MD: Rowman et Littlefield, 2004).]
Ce sont des faits essentiels et interconnectés au sujet de la vérité.

En défendant l'idée que la vérité compte, je défendrai ces truismes.
D'expérience, je sais que les réactions qu'ils suscitent sont presque aussi éloignées les unes des autres que les rives du Mississippi.
Certain d'entre vous diront, « bah, évidemment! » et d'autres s'en étrangleront de rire.
Mais à la vérité, ces idées ne sont pas vraiment si simples; pas plus qu'elles ne sont désespérément naïves.
Elles méritent qu'on y réfléchisse sérieusement, et je pense qu'il faut se battre pour elles.
Fondamentalement, cependant, je _ne_ crois _pas_ que cela implique que l'on doive adhérer aux idées qui suivent, avec lesquelles nos truismes sont souvent associés:

Il n'y a qu'Une Vérité.  
Seule la raison « pure » peut atteindre la Vérité.  
La vérité est mystérieuse.  
Seules certaines personnes peuvent connaître la vérité.  
L'on doit poursuivre la vérité à tout prix.  

Ce sont de très mauvaises idées.
Il s'avère qu'en fait, trois des raisons les plus courantes de rejetter nos truismes --- par cynisme quant à la valeur de la vérité, en d'autres termes --- reposent sur des mythes qui confondent l'un ou l'autre de ces mauvaises idées avec nos truismes.
Puisque ces mythes et confusions sont aussi répandus que nos truismes eux-mêmes, il est important de s'y attaquer d'emblée.
Dans la partie II, je discute des raisons très différentes de rejetter certains de nos truismes.
Ces raisons sont moins répandues, mais plus sérieuses.
Elles sont fondées non sur des confusions, mais sur des théories erronées de ce qu'est la vérité.
Après quoi l'on sera prêt à traiter des questions réellement difficiles: _de quelles façons_ et _pourquoi_ la vérité compte.

SHELL := /bin/bash

EXT := md

find_posts = $(shell find posts -name $(1))

SOURCES := \
    $(call find_posts, "*.$(EXT)") \
	$(call find_posts, "*.html")

TARGETS := \
    $(SOURCES:posts/%$(EXT)=public/%html)

_pdfs := putnam1 putnam2 putnam3 putnam4 putnam5 putnam6				\
	canguilhem-connaissance information-biologique mindless-selection	\
	peirce-antideterminism woese-goldenfeld-2011 formation-ethique		\
	lynch1 lynch2 peirce1 houser01 plutynski01 sakoparnig-review chargaff.01

PDFS := $(_pdfs:%=pdfs/%.pdf)

TEMPLATE := \
	template/html/tufte.html5

CSS := \
	$(shell find static -name "*.css")

BIB := \
	pdfs/references.bib

CSL := \
	pdfs/chicago-note-bibliography-with-ibid.csl



.PHONY: publish
publish: $(TARGETS) $(PUBLIC_STATIC) $(PDFS:%=public/%)

.PHONY: pdfs
pdfs: $(PDFS)

.PHONY: rebuild
rebuild: extra-clean public $(TARGETS) $(PDFS:%=public/%)

public/pdfs/%.pdf: pdfs/%.pdf
	cp $< $@

STATIC := $(shell find static -type f)
PUBLIC_STATIC := $(STATIC:static/=public/)

public/%: static/%
	cp $< $@

# * POST

## Generalized rule: how to build a .html file from each .$(EXT)
public/%.html: posts/%.$(EXT) $(TEMPLATE) $(STYLES)
	pandoc \
		--katex \
		--section-divs \
		--from markdown \
		--lua-filter scripts/fix-link.lua \
		--filter pandoc-citeproc \
		--filter $(HOME)/.cabal/bin/pandoc-sidenote \
		--metadata link-citations=true \
		--bibliography $(BIB) \
		--csl $(CSL) \
		--to html5 \
		--template=$(TEMPLATE) \
		$(foreach style,$(CSS:static/%css=%css),--css $(style)) \
		--output $@ \
		$<


pdfs/%.pdf: posts/%.$(EXT)
	pandoc \
		--template handout \
        --filter pandoc-citeproc \
		--bibliography $(BIB) \
		--csl $(CSL) \
		--output $@ \
		$<

# * DRAFT

pandoc_undraft = \
	$$(pandoc --template=scripts/undrafter.tpl $(1) | jq '.draft == false')

_undraft = \
	if [ $(call pandoc_undraft,$(1)) = true ] ; then \
		mv $(1) $(2); \
	else \
		echo "File $(1) has flag 'draft: true'. Not undrafted."; \
	fi

DRAFTS := $(shell find drafts -name "*.$(EXT)")

.PHONY: undraft
undraft: $(DRAFTS:drafts%=posts%)

posts/%.md: drafts/%.md
	$(call _undraft,$<,$@)


.PHONY: draft
draft: $(DRAFTS:.md=.pdf)

drafts/%.pdf: drafts/%.$(EXT)
	pandoc \
		--template blog \
        --filter pandoc-citeproc \
		--bibliography $(BIB) \
		--csl $(CSL) \
		--output $@ \
		$<

.PHONY: clean
clean:
	rm -f $(TARGETS)

.PHONY: clean-pdf
clean-pdf:
	$(foreach tex,$(PDFS:pdf=*), rm -f $(tex))


.PHONY: extra-clean
extra-clean:
	rm -rf public/*

# The default tufte.css file expects all the assets to be in the same folder.
# In real life, instead of duplicating the files you'd want to put them in a
# shared "css/" folder or something, and adjust the `--css` flags to the pandoc
# command to give the correct paths to each CSS file.
.PHONY: public
public:
	@mkdir -p public/{css,pdfs}
	cp $(CSS) public/css/


.PHONY: help
help:
	$(info make public      -- create the directory structure)
	$(info make extra-clean -- clean everything public)
	$(info make clean-pdf   -- remove only pdfs)
	$(info make clean       -- remove htmls)
	$(info make draft       -- compile the drafts)
	$(info make undraft     -- make a draft public)
	$(info make rebuild     -- rebuild everything)
	$(info make pdfs        -- compile the public pdfs)
	$(info make publish     -- publish the damn blog)
